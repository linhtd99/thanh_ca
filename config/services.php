<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'facebook' => [
        'client_id' => '674553619717239',
        'client_secret' => 'e67dc3b2f8248d4aaee3255e0423c7fa',
        'redirect' => 'https://thanhca.pveser.com/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '970081576780-csqgr672iaj4rqf3u3lsmufak8v0a97t.apps.googleusercontent.com',
        'client_secret' => 'oQckQgyslzh2IOX6xMhBnEy3',
        'redirect' => 'https://thanhca.pveser.com/login/google/callback',
    ],
];
