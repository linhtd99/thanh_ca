<?php

/**
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These


| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');

    return 'Cache is cleared';
});
/*
 * Route Admin
 */
Route::group(['prefix' => 'admin', 'middleware' => ['auth'], 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');
    /*
     * Dashboard
     */
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'comments', 'as' => 'comments.'], function () {
        Route::get('/', 'CommentController@index')->name('index');
        Route::post('delete', 'CommentController@destroy')->name('delete');
        Route::post('deleteMulti', 'CommentController@deleteMulti')->name('deleteMulti');
        Route::post('updateStatus', 'CommentController@updateStatus')->name('updateStatus');
        Route::post('acceptAll', 'CommentController@acceptAll')->name('acceptAll');
        Route::post('accept', 'CommentController@accept')->name('accept');
    });

    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
        Route::get('/', 'ReportController@index')->name('index');
        Route::post('updateStatus', 'ReportController@updateStatus')->name('updateStatus');
        Route::post('delete', 'ReportController@destroy')->name('delete');
        Route::post('deleteMulti', 'ReportController@deleteMulti')->name('deleteMulti');
    });
    /*
     * Route Songs
     */
    Route::group(['prefix' => 'songs', 'as' => 'songs.'], function () {
        Route::get('/', 'SongController@index')->name('index');

        Route::get('add', 'SongController@create')->name('add');

        Route::get('edit/{id}', 'SongController@edit')->name('edit');
        Route::post('delete', 'SongController@destroy')->name('delete');
        //save create ablum
        Route::any('postSong', 'SongController@store')->name('postSong');
        //save edit song
        Route::any('updateSong', 'SongController@update')->name('updateSong');
        Route::any('multiDel', 'SongController@multiDel')->name('multiDel');
        Route::any('setActiveStatus', 'SongController@setActiveStatus')->name('setActiveStatus');
        Route::any('setActiveStatusAll', 'SongController@setActiveStatusAll')->name('setActiveStatusAll');
    });
    Route::group(['prefix' => 'videos', 'as' => 'videos.'], function () {
        Route::get('/', 'VideoController@index')->name('index');
        Route::get('add', 'VideoController@create')->name('add');
        Route::post('saveAdd', 'VideoController@store')->name('saveAdd');

        Route::post('delete', 'VideoController@destroy')->name('delete');
        Route::post('deleteMulti', 'VideoController@deleteMulti')->name('deleteMulti');
        Route::any('multiDel', 'VideoController@multiDel')->name('multiDel');

        Route::get('edit/{id}', 'VideoController@edit')->name('edit');
        Route::post('saveEdit/{id}', 'VideoController@update')->name('saveEdit');
        ############-
        Route::get('custom', 'VideoController@custom')->name('custom');
        Route::any('setActiveStatusAll', 'VideoController@setActiveStatusAll')->name('setActiveStatusAll');
        Route::any('setActiveStatus', 'VideoController@setActiveStatus')->name('setActiveStatus');
    });
    /*
     * Route Tags
     */
    Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
        Route::get('/', 'TagController@index')->name('index');

        Route::get('add', 'TagController@create')->name('add');
        //save create ablum
        Route::any('postTag', 'TagController@store')->name('postSong');
        Route::get('edit/{id?}', 'TagController@edit')->name('edit');
        Route::any('saveEdit/{id}', 'TagController@update')->name('saveEdit');

        // data select2
        Route::any('dataSelect2', 'TagController@dataSelect2')->name('data_select2');
        Route::any('dataSelect2_ch', 'TagController@dataSelect2_ch')->name('dataSelect2_ch');
        Route::any('dataTable', 'TagController@dataTable')->name('dataTable');
        Route::any('remove', 'TagController@destroy')->name('remove');

        Route::any('dataSelect21', 'TagController@dataSelect2')->name('data_select21');

        Route::any('saveAdd', 'TagController@store')->name('saveAdd');

    });

    /*
     * Route Group Category
     */
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
        // index
        Route::any('/', 'CategoryController@index')->name('index');
        // data categories
        Route::get('data', 'CategoryController@dataCategories')->name('data');
        Route::get('edit/{id?}', 'CategoryController@edit')->name('edit');
        Route::any('data_select2', 'CategoryController@dataCategories_select2')->name('data_select2');
        Route::any('data_select2_pa', 'CategoryController@dataCategories_select2_pa')->name('data_select2_pa');

        Route::any('get_child_cat_by_id_current', 'CategoryController@get_child_cat_by_id_current')->name('get_child_cat_by_id_current');
        //post create category
        Route::any('postCategory', 'CategoryController@store')->name('postCategory');
        //update category
        Route::any('updateCategory', 'CategoryController@update')->name('updateCategory');
        // remove category
        Route::any('remove', 'CategoryController@destroy')->name('remove');
    });

    /*
     * Route Group Artists
     */
    Route::group(['prefix' => 'artists', 'as' => 'artists.'], function () {
        // index
        Route::any('/', 'ArtistController@index')->name('index');
        // data artists
        Route::get('data', 'ArtistController@data')->name('data');

        Route::get('add', 'ArtistController@add')->name('add');

        Route::post('saveAdd', 'ArtistController@saveAdd')->name('saveAdd');

        Route::get('edit/{id?}', 'ArtistController@edit')->name('edit');

        Route::post('saveEdit', 'ArtistController@saveEdit')->name('saveEdit');

        Route::get('detail/{id}', 'ArtistController@detail');

        Route::get('delete/{id}', 'ArtistController@delete');

        Route::any('data_select2', 'ArtistController@dataSelect2')->name('data_select2');

        Route::post('deleteMulti', 'ArtistController@deleteMulti')->name('deleteMulti');
    });

    /*
     * Route Group User
     */
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'UserController@index')->name('index');

        Route::get('data', 'UserController@data')->name('data');

        Route::get('delete/{id}', 'UserController@delete');

        Route::get('add', 'UserController@add')->name('add');

        Route::post('saveAdd', 'UserController@saveAdd')->name('saveAdd');

        Route::get('edit/{id}', 'UserController@edit');

        Route::post('saveEdit', 'UserController@saveEdit')->name('saveEdit');

        Route::get('profile', 'UserController@profile')->name('profile');

        Route::post('saveProfile', 'UserController@saveProfile')->name('saveProfile');
    });

    /*
     * Route Group Customer
     */

    Route::group(['prefix' => 'customers', 'as' => 'customers.'], function () {
        Route::get('/', 'CustomerController@index')->name('index');
        Route::get('data', 'CustomerController@data')->name('data');

        Route::get('add', 'CustomerController@add')->name('add');
        Route::post('saveAdd', 'CustomerController@saveAdd')->name('saveAdd');

        Route::get('edit/{id}', 'CustomerController@edit');
        Route::post('saveEdit', 'CustomerController@saveEdit')->name('saveEdit');

        Route::get('detail/{id}', 'CustomerController@detail');

        Route::get('delete/{id?}', 'CustomerController@delete')->name('delete');

        Route::post('deleteMulti', 'CustomerController@deleteMulti')->name('deleteMulti');
    });

    /*
     * Route Group Album
     */

    Route::group(['prefix' => 'albums', 'as' => 'albums.'], function () {
        Route::get('/', 'AlbumController@index')->name('index');
        Route::get('data', 'AlbumController@data')->name('data');
        Route::any('data_select2', 'AlbumController@dataSelect2')->name('data_select2');

        //save create ablum
        Route::any('saveAdd', 'AlbumController@saveAdd')->name('saveAdd');

        // save edit album
        Route::get('edit/{id?}', 'AlbumController@edit')->name('edit');

        Route::any('saveEdit', 'AlbumController@saveEdit')->name('saveEdit');

        Route::get('delete/{id}', 'AlbumController@delete');
    });
    /*
     * Route Group Page
     */

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
        Route::get('/', 'PageController@index')->name('index');

        Route::get('data', 'PageController@data')->name('data');

        Route::get('add', 'PageController@add')->name('add');

        Route::post('saveAdd', 'PageController@saveAdd')->name('saveAdd');

        Route::get('edit/{id}', 'PageController@edit');

        Route::post('saveEdit', 'PageController@saveEdit')->name('saveEdit');

        Route::get('delete/{id}', 'PageController@delete');

        Route::post('deleteMulti', 'PageController@deleteMulti')->name('deleteMulti');
    });

    /*
     * Route Group Post
     */

    Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
        Route::get('/', 'PostController@index')->name('index');

        Route::get('data', 'PostController@data')->name('data');

        Route::get('add', 'PostController@add')->name('add');

        Route::post('saveAdd', 'PostController@saveAdd')->name('saveAdd');

        Route::get('edit/{id}', 'PostController@edit');

        Route::post('saveEdit', 'PostController@saveEdit')->name('saveEdit');

        Route::get('delete/{id}', 'PostController@delete');

        Route::post('deleteMulti', 'PostController@deleteMulti')->name('deleteMulti');
    });

    /*
     * Route Group Playlist
     **/
    Route::group(['prefix' => 'playlists', 'as' => 'playlists.'], function () {
        Route::get('/', 'PlaylistController@index')->name('index');

        Route::get('add', 'PlaylistController@add')->name('add');

        Route::post('saveAdd', 'PlaylistController@saveAdd')->name('saveAdd');

        Route::get('edit/{id}', 'PlaylistController@edit')->name('edit');

        Route::post('saveEdit', 'PlaylistController@saveEdit')->name('saveEdit');

        Route::post('delete', 'PlaylistController@delete')->name('delete');

        Route::post('deleteMulti', 'PlaylistController@deleteMulti')->name('deleteMulti');
        Route::any('multiDel', 'PlaylistController@multiDel')->name('multiDel');
        Route::any('setActiveStatus', 'PlaylistController@setActiveStatus')->name('setActiveStatus');
        Route::any('setActiveStatusAll', 'PlaylistController@setActiveStatusAll')->name('setActiveStatusAll');
    });

    /*
     * Route Group Lyric
     **/
    Route::group(['prefix' => 'lyrics', 'as' => 'lyrics.'], function () {
        Route::post('dataSelect2', 'LyricController@dataSelect2')->name('data_select2');
    });

    /*
     * Route Group Video
     **/
    Route::group(['prefix' => 'videos', 'as' => 'videos.'], function () {
        Route::post('dataSelect2', 'VideoController@dataSelect2')->name('data_select2');
    });

    /*
     * Route Group Options
     **/
    Route::group(['prefix' => 'options', 'as' => 'options.'], function () {
        Route::get('generals', 'OptionController@general')->name('generals');
        Route::post('save_general', 'OptionController@save_general')->name('save_general');
        /*
         * today listen
         */
        Route::any('todaylisten', 'OptionController@todaylisten')->name('todaylisten');

        Route::get('menu_footer', 'OptionController@menu_footer')->name('menu_footer');
        Route::post('save_menu_footer', 'OptionController@save_menu_footer')->name('save_menu_footer');

        Route::get('sort_category', 'OptionController@sort_category')->name('sort_category');
        Route::post('save_sort_category', 'OptionController@save_sort_category')->name('save_sort_category');

        Route::get('choose_category_bxh', 'OptionController@choose_category_bxh')->name('choose_category_bxh');
        Route::post('save_choose_category2', 'OptionController@save_choose_category2')->name('save_choose_category2');
        Route::post('save_choose_category', 'OptionController@save_choose_category')->name('save_choose_category');
    });

    /*
     * Route Group Topics
     **/
    Route::group(['prefix' => 'topics', 'as' => 'topics.'], function () {
        Route::get('/', 'TopicController@index')->name('index');
        Route::get('add', 'TopicController@create')->name('add');
        Route::get('delete/{id}', 'TopicController@destroy')->name('delete');
        Route::post('saveAdd', 'TopicController@store')->name('saveAdd');
        Route::any('dataSelect2', 'TopicController@dataSelect2')->name('dataSelect2');
        Route::post('deleteMulti', 'TopicController@deleteMulti')->name('deleteMulti');
    });
});

// Route::get('/', function () {
//     return view('welcome');
// });php
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    // Route::get('/','Auth\LoginController@login');
    // Route::get('home', 'AdminController@index')->name('home');

    Route::get('login', 'Auth\LoginController@login')->name('login');
    Route::post('postLogin', 'Auth\LoginController@postLogin')->name('postLogin');
    Route::get('forgetPassword', 'Auth\LoginController@forgetPassword')->name('forgetPassword');

    Route::post('postForget', 'Auth\LoginController@postForget')->name('postForget');
    Route::get('resetPassword/{token}', 'Auth\LoginController@resetPassword');

    Route::get('changeForgetPw/{email}', 'Auth\LoginController@changeForgetPw');
    Route::post('postChangePw', 'Auth\LoginController@postChangePw')->name('postChangePw');

    Route::get('logOut', 'Auth\LoginController@logOut')->name('logOut');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route không cần đăng nhập
Route::group(['namespace' => 'Client'], function () {
    Route::get('/', 'ClientController@index')->name('/');
    // Đăng nhập
    Route::post('loginCustomer', 'LoginController@login')->name('loginCustomer');

    Route::get('kich-hoat-tai-khoan/{token}', 'LoginController@activeAccount')->name('activeAccount');
    // Đăng kí
    Route::any('registerCustomer', 'LoginController@registerCustomer')->name('registerCustomer');
    // Đăng xuất
    Route::get('logoutCustomer', 'LoginController@logoutCustomer')->name('logoutCustomer');
    // Quên mật khẩu
    //Route::post('forgetPwCustomer', 'LoginController@forgetPwCustomer')->name('forgetPwCustomer');
    // Thay đổi mật khẩu
    //Route::post('change_forgetPassword', 'LoginController@change_forgetPassword')->name('change_forgetPassword');
    //Tìm kiếm bài hát
    Route::any('user/searchSongAddPlaylist', 'UserInfoController@searchSongAddPlaylist')->name('searchSongAddPlaylist');
    // Tìm bài hát theo id
    Route::any('user/searchSongById', 'UserInfoController@searchSongById')->name('searchSongById');
    // Lưu playlist
    Route::post('user/savePlaylist', 'UserInfoController@savePlaylist')->name('savePlaylist');
    // Lưu chỉnh sửa playlist
    Route::post('user/saveEditPlaylist', 'UserInfoController@saveEditPlaylist')->name('saveEditPlaylist');
    // Thay đổi thông tin người dùng
    Route::post('postEditUser', 'UserInfoController@postEditUser')->name('postEditUser');
    // thay đỔi mật khẩu người dùng
    Route::post('postEditPwUser', 'UserInfoController@postEditPwUser')->name('postEditPwUser');

    Route::get('reset_password/{token?}', 'LoginController@reset_password')->name('reset_password');
    Route::post('reset_password/{token?}', 'LoginController@post_reset_password');

    Route::get('password', 'LoginController@password')->name('password');
    Route::post('password', 'LoginController@post_password');
    /*
     * listen song
     */
    Route::any('songListen', 'CountListensController@postListens')->name('songListen');
    Route::any('videoListen', 'CountListensController@postListensVideo')->name('videoListen');
    Route::any('postListensPlaylist', 'CountListensController@postListensPlaylist')->name('postListensPlaylist');
    Route::any('postListensAlbum', 'CountListensController@postListensAlbum')->name('postListensAlbum');
});

// Đã đăng nhập và là người dùng hiện tại thì có thể truy cập vào những router này
Route::group(['namespace' => 'Client', 'middleware' => ['customerLoginCheck']], function () {
    // Tài khoản người dùng
    Route::get('user/{username}/quan-ly', 'UserInfoController@index');
    //Cập nhật playlist
    Route::get('user/{username}/cap-nhat-playlist/{playlist?}', 'UserInfoController@playList');
    // Danh sách playlist của người dùng
    Route::get('user/{username}/playlist', 'UserInfoController@listPlayList');
    // lịch sử
    Route::get('user/{username}/lich-su', 'UserInfoController@history')->name('user_history');
    // Xóa playlist
    Route::get('user/{username}/deletePlaylist/{id}', 'UserInfoController@deletePlaylist');
    // Upload
    Route::get('user/{username}/upload', 'UserInfoController@upload');
    // Lưu upload video
    Route::post('user/{username}/saveUploadSong', 'UserInfoController@saveUploadSong')->name('saveUploadSong');
    //  video
    Route::post('user/{username}/saveUploadvideo', 'UserInfoController@saveUploadvideo')->name('saveUploadvideo');
    // xóa nhiều playlist
    Route::post('user/{username}/deleteMultiPlaylist', 'UserInfoController@deleteMultiPlaylist')->name('deleteMultiPlaylist');
    // them cookie playlist moi nghe
    Route::post('user/{username}/setCookiePlaylist', 'UserInfoController@setCookiePlaylist')->name('setCookiePlaylist');
    // Chinh sua wishlist
    Route::get('wishlist-{username}/chinh-sua', 'ClientController@editwishList')->name('editwishList');
    // Luu wishlist
    Route::post('wishlist-{username}/saveEditWishList', 'ClientController@saveEditWishList')->name('saveEditWishList');
    // Xóa lich su
    Route::any('user/{username}/lich-su/delete/{id}', 'UserInfoController@deleteHistory')->name('deleteHistory');
    // Xoa nhieu
    Route::any('user/{username}/lich-su/deleteAll', 'UserInfoController@deleteAllHistory')->name('deleteAllHistory');

});

// Route không cần đăng nhập
Route::group(['namespace' => 'Client'], function () {
    // theem bai hat vao danh sach yeu thich
    Route::post('addSongWishlist', 'ClientController@addSongWishlist')->name('addSongWishlist');
    // xem danh sach yeu thich
    Route::get('wishlist-{username}', 'ClientController@wishList')->name('wishList');
    //luu comment
    Route::post('saveComment', 'ClientController@saveComment')->name('saveComment');
    // like comment
    Route::post('likeComment', 'ClientController@likeComment')->name('likeComment');
    // xóa comment
    Route::get('delete_comment/{id?}', 'ClientController@delete_comment')->name('delete_comment');
    // load comment
    Route::any('more_comment', 'ClientController@more_comment')->name('more_comment');
    // them lich su xem cua nguoi dung
    Route::post('addToHistory', 'ClientController@addToHistory')->name('addToHistory');

    Route::get('user/{username}/trang-ca-nhan', 'UserInfoController@proFile')->name('trangCaNhan');

    Route::get('user/{username}/danh-sach-playlist', 'UserInfoController@playListProfile');
    // playlist chi tiet
    Route::get('playlist/{name?}.{code}.html', 'ClientController@playlistPlayer')->name('playlist')->where(['code' => '[0-9a-zA-Z]+']);
    // Album chi tiet sử dụng trình phát nhạc
    Route::get('album/{name?}.{code}.html', 'ClientController@albumPlayer')->name('album')->where(['code' => '[0-9a-zA-Z]+']);;
    // Album chi tiet sử dụng trình phát nhạc
    Route::get('bai-hat/{name?}.{code}.html', 'ClientController@songPlayer')->name('song')->where(['code' => '[0-9a-zA-Z]+']);
    // Trinh nghe nhac lich su user
    Route::get('bai-hat-ban-da-nghe/{id}', 'ClientController@history_song_player')->name('history_song_player');
    // list video
    Route::get('video.html', 'VideoController@listVideo')->name('indexVideo');
    // video chitiet
    Route::get('video/{name?}.{code}.html', 'VideoController@detailVideo')->name('detailVideo')->where(['code' => '[0-9a-zA-Z]+']);;
    /*
     * route BXH bai hat
     *
     */
    Route::get('bai-hat/bxh/{slug_category}.{week?}.{year?}', 'ChartController@index')->name('bxh_song');
    /*
     * route tag
     *
     */
    Route::get('tags/{p1?}/{p2?}/{p3?}/{p4?}', 'TagController@index')->name('tags');

    /*
     * route danh sách bảng xếp hạng video
     * chi tiết bảng xếp hạng video
     */
    Route::get('video/bxh/{slug_category}.{week?}.{year?}', 'ChartController@indexVideo')->name('bxh_video');

    Route::get('playlist/bxh/video.{slug_category}.{week?}.{year?}', 'ChartController@chartVideoDetail')->name('playlist_bxh_video');

    /*
     * Route play list bảng xếp hạng
     */
    Route::get('playlist/bxh/song.{slug_category}.{week}.{year}', 'ChartController@chartSongDetail')->name('playlist_bxh_song');

    /*
     *  Route top 100 bài hát
     */
    Route::get('bai-hat/top100/{slug_category}', 'ChartController@top100')->name('top100_song');

    /*
     * Route chi tiết 100 bài hát
     * top100Detail
     */
    Route::get('playlist/top100/{slug_category}', 'ChartController@top100Detail')->name('playlist_top100_song');

    // list song

    Route::get('bai-hat.html', 'SongController@listSong')->name('indexSong');
    // list song moi nhat
    Route::get('bai-hat/moi-nhat.html', 'SongController@listSongNew')->name('indexSongNew');
    // list song danh muc
    Route::get('bai-hat-{slug}.html', 'SongController@listSongByCategory')->name('listSongByCategory');
    Route::get('bai-hat-{slug}/moi-nhat.html', 'SongController@listSongByCategoryNew')->name('listSongByCategoryNew');

    // danh muc video
    Route::get('video-{slug}.html', 'VideoController@categoryVideo')->name('categoryVideo');

    Route::get('video-{slug}/moi-nhat.html', 'VideoController@videoOrderNew')->name('videoOrderNew');
    // danh muc nghe si
    Route::get('nghe-si/{keyword?}', 'ArtistController@index')->name('indexArtist');
    // // nghe si by name
    Route::get('nghe-si-{slug}.html', 'ArtistController@artistDetail')->name('artistDetail');
    // list bai hat nghe si
    Route::get('nghe-si-{slug}/bai-hat.html', 'ArtistController@artistSong')->name('artistSong');
    // list album nghe si
    Route::get('nghe-si-{slug}/album.html', 'ArtistController@artistAlbum')->name('artistVideo');
    // list album hot nhat
    Route::get('nghe-si-{slug}/album-hot-nhat.html', 'ArtistController@artistAlbumHot')->name('artistVideoHot');
    // list video nghe si
    Route::get('nghe-si-{slug}/video.html', 'ArtistController@artistMv')->name('artistMv');

    //list play list
    Route::get('playlist.html', 'PlaylistController@index')->name('indexPlaylist');
    //list palylist moi nhat
    Route::get('playlist/moi-nhat.html', 'PlaylistController@indexPlaylistNew')->name('indexPlaylistNew');
    // list playlist danh muc
    Route::get('playlist-{slug}.html', 'PlaylistController@playlistByCategory')->name('playlistByCategory');
    Route::get('playlist-{slug}/moi-nhat.html', 'PlaylistController@playlistByCategoryNew')->name('playlistByCategoryNew');
    //route chủ đề
    Route::get('chu-de.html', 'TopicController@index')->name('topic');

    Route::get('login/{social}', [
        'as' => 'login.{social}',
        'uses' => 'SocialAuthController@redirect',
    ]);
    Route::get('login/{social}/callback', [
        'as' => 'login.{social}.callback',
        'uses' => 'SocialAuthController@callback',
    ]);

    Route::get('tim-kiem', 'SearchController@index')->name('search');
    Route::get('tim-kiem/bai-hat', 'SearchController@songs')->name('searchSong');
    Route::get('tim-kiem/playlist', 'SearchController@playlists')->name('searchPlaylist');
    Route::get('tim-kiem/video', 'SearchController@videos')->name('searchVideo');
    Route::get('tim-kiem/karaoke', 'SearchController@videoKaraoke')->name('searchVideoKaraoke');

    Route::post('delete_history_search', 'SearchController@delete_history_search')->name('delete_history_search');

    Route::get('pages/{slug}.html', 'ClientController@page')->name('page');

    Route::get('download', 'ClientController@download')->name('download');
    Route::post('route_random_song', 'ClientController@randomSongs')->name('route_random_song');
    Route::any('add_to_wishlist', 'ClientController@addTowishList')->name('add_to_wishlist');
    Route::any('player_search', 'ClientController@playerSearch')->name('player_search');

    Route::get('tin-tuc.html', 'PostController@index')->name('posts');
    Route::get('tin-tuc/{slug}.html', 'PostController@postDetail')->name('postDetail');

    Route::post('saveReport', 'ReportController@saveReport')->name('saveReport');
});
