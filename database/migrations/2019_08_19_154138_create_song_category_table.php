<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('song_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('song_id')->nullable()->unsigned();
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->foreign('song_id')
                        ->references('id')->on('songs')
                            ->onDelete('cascade');
            $table->foreign('category_id')
                        ->references('id')->on('categories')
                            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('song_category');
    }
}
