<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistSongTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Schema::dropIfExists('artist_song');
        Schema::create('artist_song', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('artist_id')->nullable()->unsigned();
            $table->unsignedBigInteger('song_id')->nullable()->unsigned();
            $table->timestamps();
            $table->foreign('artist_id')
                        ->references('id')->on('artists')->onDelete('cascade');
            $table->foreign('song_id')
                        ->references('id')->on('songs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('artist_song');
    }
}
