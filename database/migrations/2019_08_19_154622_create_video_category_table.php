<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('video_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('video_id')->nullable()->unsigned();
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->foreign('video_id')
                        ->references('id')->on('videos')
                            ->onDelete('cascade');
            $table->foreign('category_id')
                        ->references('id')->on('categories')
                            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('video_category');
    }
}
