<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('code', 20)->unique();
            $table->string('image')->nullable()->default('https://stc-id.nixcdn.com/v11/images/img-plist-full.jpg');
            $table->string('slug')->nullable();
            $table->bigInteger('topic_id')->nullable()->unsigned();
            $table->string('description')->nullable();
            $table->tinyInteger('status')->default(-1)->comment('1: kích hoạt, -1: vô hiệu hoá');
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->bigInteger('tag_id')->nullable()->unsigned();
            $table->integer('status')->default(1);
            // $table->bigInteger('customer_id')->nullable()->unsigned();
            $table->bigInteger('customer_id')
                ->references('id')->on('customers')
                ->onDelete('set null');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('set null');
            $table->foreign('tag_id')
                ->references('id')->on('tags')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('playlists');
    }
}
