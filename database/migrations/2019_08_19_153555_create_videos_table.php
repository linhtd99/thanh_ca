<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 20)->unique();
            $table->string('image')->nullable();
            $table->string('name');
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->tinyInteger('beat_type')->nullable()->default(0)->comment('0: bài hát thường; 1: Beat');
            $table->tinyInteger('karaoke_type')->nullable()->default(0)->comment('0: bài hát thường; 1: karaoke');
            $table->bigInteger('album_id')->nullable()->unsigned();
            $table->bigInteger('lyric_id')->nullable()->unsigned();
            $table->bigInteger('author_id')->nullable()->unsigned()
                ->comment('0: admin; Còn lại id người dùng');
            $table->text('description')->nullable();
            $table->string('link')->nullable();
            $table->string('video')->nullable();
            $table->enum('status', [0, 1, 2])->default(1)->comment('0: vô hiệu hoá; 1: publish; 2:Chờ duyệt');
            $table->timestamps();
            $table->foreign('album_id')
                ->references('id')->on('albums')
                ->onDelete('set null');
            $table->foreign('lyric_id')
                ->references('id')->on('lyrics')
                ->onDelete('set null');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
