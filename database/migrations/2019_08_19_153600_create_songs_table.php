<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Schema::dropIfExists('songs');
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 20)->unique();
            $table->string('name');
            $table->bigInteger('category_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('video_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('album_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('lyric_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('author_id')->nullable()->unsigned()->default(0)
                ->comment('0: admin; Còn lại id người dùng');
            $table->string('filename')->nullable();
            // $table->tinyInteger('beat_type')->nullable()->default(0)->comment('0: bài hát thường; 1: Beat');
            // $table->tinyInteger('karaoke_type')->nullable()->default(0)->comment('0: bài hát thường; 1: karaoke');
            $table->string('image')->nullable();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('lyric_karaoke')->nullable()->default(null);
            $table->enum('status', [0, 1, 2])->default(2)
                ->comment('1:kích hoạt; 0: vô hiệu hoá; 2: chờ duyệt');
            $table->bigInteger('count_downloads')->default(0);

            $table->timestamps();

            // $table->foreign('category_id')
            //     ->references('id')->on('categories')
            //     ->onDelete('set null');
            // $table->foreign('album_id')
            //     ->references('id')->on('albums')
            //     ->onDelete('set null');
            // $table->foreign('lyric_id')
            //     ->references('id')->on('lyrics')
            //     ->onDelete('set null');
            // $table->foreign('author_id')
            //     ->references('id')->on('customers')
            //     ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
