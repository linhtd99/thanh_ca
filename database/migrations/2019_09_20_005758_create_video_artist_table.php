<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoArtistTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('video_artist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('video_id')->nullable();
            $table->unsignedBigInteger('artist_id')->nullable();
            $table->foreign('video_id')
                ->references('id')->on('videos')->onDelete('cascade');
            $table->foreign('artist_id')
                ->references('id')->on('artists')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('video_artist');
    }
}
