<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTopicPlaylist extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('topic_playlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('topic_id')->nullable();
            $table->bigInteger('playlist_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('table_topic_playlist');
    }
}
