<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSongPlaylistTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('song_playlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('playlist_id')->nullable()->unsigned();
            $table->BigInteger('song_id')->nullable()->unsigned();
            // $table->foreign('song_id')
            //     ->references('id')->on('songs')
            //     ->onDelete('cascade');
            // $table->foreign('playlist_id')
            //     ->references('id')->on('playlists')
            //     ->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('song_playlist');
    }
}
