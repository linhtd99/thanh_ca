<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLyricsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Schema::dropIfExists('lyrics');
        Schema::create('lyrics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            // $table->bigInteger('song_id')->nullable()->unsigned();
            // $table->bigInteger('video_id')->nullable()->unsigned();
            $table->text('content')->nullable();
            // $table->foreign('song_id')->references('id')->on('songs')->onDelete('set null');
            // $table->foreign('video_id')->references('id')->on('videos')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('lyrics');
    }
}
