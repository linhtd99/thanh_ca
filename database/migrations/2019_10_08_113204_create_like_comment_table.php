<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikeCommentTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('like_comment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable()->unsigned();
            $table->bigInteger('comment_id')->nullable()->unsigned();
            $table->enum('type', [0, 1])->default(1)
                        ->comment('1:like; 0: dislike');
            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');
            $table->foreign('comment_id')
                ->references('id')->on('comments')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('like_comment');
    }
}
