<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHistoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('user_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('provider', 191);
            $table->text('code', 191);
            $table->bigInteger('customer_id')->nullable()->unsigned();
            $table->timestamps();
            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('user_history');
    }
}
