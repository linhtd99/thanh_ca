<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Schema::dropIfExists('artists');
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('fullname')->nullable();
            $table->date('birthday')->nullable();
            $table->string('country')->nullable();
            $table->enum('gender', [0, 1, -1])->default(-1)->comment('0: nam; 1: nữ; -1: chưa xác định');
            $table->string('slug')->unique();
            $table->string('banner')->nullable()->default('https://i.imgur.com/zuZYtNd.jpg');
            $table->text('description')->nullable();
            $table->enum('status', [0, 1])->default(1)->comment('1:kích hoạt; 0: vô hiệu hoá');
            $table->string('image')->nullable()->default('https://i.imgur.com/l9p4wip.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
