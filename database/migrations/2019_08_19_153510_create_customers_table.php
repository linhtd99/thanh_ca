<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 100)->unique();
            $table->string('name', 100)->nullable();
            $table->date('birthday')->nullable();
            $table->integer('sex')->nullable()->default(-1)->comment('-1:Khác ; 0: nam ; 1: nữ');
            $table->string('address')->nullable();
            $table->string('introduction')->nullable();
            $table->string('email', 100);
            $table->string('phone', 11)->nullable();
            $table->text('avatar')->nullable();
            $table->string('password');
            $table->integer('status')->default(1);
            $table->bigInteger('views')->nullable();
            $table->string('token', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
