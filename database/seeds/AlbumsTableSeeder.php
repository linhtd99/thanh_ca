<?php

use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (DB::table('albums')->count() == 0) {
            $album = [];
            $faker = Faker\Factory::create();
            for ($i = 0; $i < 50; ++$i) {
                $album[] = [
                    'code' => getToken(12),
                    'name' => $faker->name.rand(1, 9),
                    'slug' => $faker->name.rand(1, 9),
                    'image' => $faker->imageUrl(400, 300),
                    'description' => $faker->realText($maxNbChars = 300, $indexSize = 2),
                ];
            }
            DB::table('albums')->insert($album);
        }
    }
}
