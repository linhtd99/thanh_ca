<?php

use App\Models\Video;
use Illuminate\Database\Seeder;

class VideoviewTablesSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (true) {
            $data = [];
            $faker = Faker\Factory::create();
            for ($i = 0; $i < 20; $i++) {
                for ($i = 0; $i < 100; ++$i) {
                    $video = Video::all()->random(rand(1, 3))->first();
                    $data[] = [
                        'code' => $video->code,
                        'ip' => $faker->ipv4,
                        'created_at' => date('Y-m-d H:i:s', rand(1459902400, 1572580800)),
                    ];
                }
                DB::table('videos_listens')->insert($data);
            }
        }
    }
}
