<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        /* `thanhca`.`users` */
        $users = array(
            array('id' => '1', 'username' => 'admin', 'name' => 'Luigi Mayer', 'email' => 'admin@gmail.com', 'email_verified_at' => null, 'avatar' => 'https://thanhca.pveser.com//storage/uploads/images/B%C3%ADch%20Hi%E1%BB%81n.jpg', 'password' => '$2y$10$8cQ9b5N0RjUShMigowhGg.9tG/Mh2sn5L6hsmSgBsGjsAN7lG60.y', 'role' => '101', 'remember_token' => null, 'created_at' => '2012-03-05 04:30:55', 'updated_at' => '2019-11-06 02:40:29', 'count_downloads' => '0'),
            array('id' => '2', 'username' => null, 'name' => 'Jeffrey Powlowski Sr.', 'email' => 'admin1@gmail.com', 'email_verified_at' => null, 'avatar' => null, 'password' => '$2y$10$H4RhYTCa0JAW3qCr55ycAeDYHOY4udrtRC1xyG4dez25qaq7UgAtO', 'role' => '100', 'remember_token' => null, 'created_at' => '1978-02-09 00:54:48', 'updated_at' => null, 'count_downloads' => '0'),
            array('id' => '3', 'username' => null, 'name' => 'Mrs. Reva Marvin III', 'email' => 'admin2@gmail.com', 'email_verified_at' => null, 'avatar' => null, 'password' => '$2y$10$EcMuFe/53nYWPrqSP9DvFOa/jFkEQSEO1ml4pKU4wfNmCqreDIqpO', 'role' => '100', 'remember_token' => null, 'created_at' => '2009-03-01 09:52:52', 'updated_at' => null, 'count_downloads' => '0'),
            array('id' => '4', 'username' => null, 'name' => 'trong.phaolo', 'email' => 'trong.phaolo@gmail.com', 'email_verified_at' => null, 'avatar' => null, 'password' => '$2y$10$YnHtjXt6obY0R7afvTxAPeNiqkluvMUG5SVPmElXiKEi0jZ2TXOki', 'role' => '100', 'remember_token' => null, 'created_at' => '2019-11-12 09:28:20', 'updated_at' => null, 'count_downloads' => '0'),
        );

        if (DB::table('users')->count() <= 0) {
            DB::table('users')->insert($users);
        }
    }
}
