<?php

use Illuminate\Database\Seeder;
use App\Models\Song;

class CommentTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (DB::table('comments')->count() == 0) {
            $faker = Faker\Factory::create();
            $comments = [];
            for ($i = 1; $i < 201; ++$i) {
                $song = Song::where('id', '=', rand(1, 10))->first();
                $comments[] = [
                    'id' => $i,
                    'content' => $faker->text,
                    'customer_id' => rand(1, 199),
                    'code' => $song->code,
                    'parent_id' => $i < 101 ? 0 : rand(1, 100),
                    'created_at' => date('Y-m-d H:i:s', time()),
                ];
            }
            DB::table('comments')->insert($comments);
        }
    }
}
