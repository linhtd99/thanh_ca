<?php

use App\Models\Song;
use Illuminate\Database\Seeder;

class SongsListensSedder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (1 != 0) {
            $data = [];
            $faker = Faker\Factory::create();
            for ($i = 0; $i < 100; $i++) {
                for ($i = 0; $i < 100; ++$i) {
                    $song = Song::all()->random(rand(1, 1000))->first();
                    $data[] = [
                        'code' => $song->code,
                        'ip' => $faker->ipv4,
                        'created_at' => date('Y-m-d H:i:s', rand(1459902400, 1572580800)),
                    ];
                }
                DB::table('songs_listens')->insert($data);
            }
        }
    }
}
