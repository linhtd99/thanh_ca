<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (DB::table('tags')->count() == 0) {
            /* `thanhca`.`tags` */
            $tags = array(
                array('id' => '2', 'title' => 'Nhạc trẻ', 'parent_id' => '5', 'slug' => 'nhac-tre', 'created_at' => '2019-11-13 08:53:52', 'updated_at' => '2019-11-14 02:09:59'),
                array('id' => '3', 'title' => 'Nhạc thánh ca', 'parent_id' => '5', 'slug' => 'nhac-thanh-ca', 'created_at' => '2019-11-13 08:54:03', 'updated_at' => '2019-11-14 02:10:09'),
                array('id' => '5', 'title' => 'Thể loại', 'parent_id' => '0', 'slug' => 'the-loai', 'created_at' => '2019-11-13 09:17:43', 'updated_at' => '2019-11-13 09:17:43'),
                array('id' => '6', 'title' => 'Nhạc vàng', 'parent_id' => '5', 'slug' => 'nhac-vang', 'created_at' => '2019-11-13 09:24:46', 'updated_at' => '2019-11-13 09:24:46'),
                array('id' => '7', 'title' => 'Mùa', 'parent_id' => '0', 'slug' => 'mua', 'created_at' => '2019-11-14 02:15:13', 'updated_at' => '2019-11-14 02:15:13'),
                array('id' => '8', 'title' => 'Mùa xuân', 'parent_id' => '7', 'slug' => 'mua-xuan', 'created_at' => '2019-11-14 02:16:37', 'updated_at' => '2019-11-14 02:16:37'),
                array('id' => '9', 'title' => 'Mùa Đông', 'parent_id' => '7', 'slug' => 'mua-dong', 'created_at' => '2019-11-14 02:16:48', 'updated_at' => '2019-11-14 02:16:48'),
                array('id' => '10', 'title' => 'Mùa Thu', 'parent_id' => '7', 'slug' => 'mua-thu', 'created_at' => '2019-11-14 02:16:56', 'updated_at' => '2019-11-14 02:16:56'),
                array('id' => '11', 'title' => 'Mùa Hạ', 'parent_id' => '7', 'slug' => 'mua-ha', 'created_at' => '2019-11-14 02:17:14', 'updated_at' => '2019-11-14 02:17:14'),
            );

            DB::table('tags')->insert($tags);
        }
    }
}
