<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (DB::table('customers')->count() == 0) {
            $customers = array(
                array('id' => '1', 'username' => 'luongnd2286', 'name' => 'Lượng Nguyễn', 'birthday' => null, 'sex' => '-1', 'address' => null, 'introduction' => null, 'email' => 'luongnd2286@gmail.com', 'phone' => null, 'avatar' => 'https://graph.facebook.com/v3.0/784049498680693/picture?type=normal', 'password' => '', 'status' => '1', 'views' => '5', 'created_at' => '2019-11-06 04:47:34', 'updated_at' => '2019-11-13 09:19:31'),
                array('id' => '2', 'username' => 'trongphaolo', 'name' => 'trongphaolo', 'birthday' => null, 'sex' => '-1', 'address' => null, 'introduction' => null, 'email' => 'trong.phaolo@gmail.com', 'phone' => '0377567685', 'avatar' => null, 'password' => '$2y$10$6Px5RujIOma48fHOlsuCrukr5Nozb4rJNcRZyed61lu6BFKUk0whK', 'status' => '1', 'views' => '12', 'created_at' => '2019-11-07 03:02:23', 'updated_at' => '2019-11-12 10:06:56'),
                array('id' => '3', 'username' => 'trongtrong', 'name' => 'trong', 'birthday' => '1912-01-01', 'sex' => '-1', 'address' => null, 'introduction' => null, 'email' => 'trlkjdsasklas@fnsfnf.com', 'phone' => null, 'avatar' => null, 'password' => '$2y$10$FYXhmu2gxZsiSRgs/GGh4eTlDHSQV9a4xswF0CfEjNv2UK7nqsJ6.', 'status' => '0', 'views' => '2', 'created_at' => '2019-11-12 08:24:27', 'updated_at' => '2019-11-12 08:44:09'),
                array('id' => '4', 'username' => 'ninh', 'name' => 'trong', 'birthday' => '1912-01-01', 'sex' => '-1', 'address' => null, 'introduction' => null, 'email' => 'ninhstrongly@gmail.com', 'phone' => null, 'avatar' => null, 'password' =>bcrypt('123456'), 'status' => '0', 'views' => '2', 'created_at' => '2019-11-12 08:24:27', 'updated_at' => '2019-11-12 08:44:09'),
            );

            DB::table('customers')->insert($customers);
        }
    }
}
