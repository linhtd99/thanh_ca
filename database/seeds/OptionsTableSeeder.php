<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (DB::table('options')->count() == 0) {
            $options = [];
            /* `thanhca`.`options` */
            $options = array(
                array('id' => '1', 'name' => 'Menu Footer', 'key' => 'menu_footer', 'value' => '[[{"title":"Th\\u00f4ng tin","content":"<ul>\\r\\n\\t<li><a href=\\"#\\">Gi\\u1edbi thi\\u1ec7u<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">\\u0110i\\u1ec1u kho\\u1ea3n s\\u1eed d\\u1ee5ng<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">H\\u01b0\\u1edbng d\\u1eabn s\\u1eed d\\u1ee5ng<\\/a><\\/li>\\r\\n<\\/ul>"},{"title":"B\\u00e0i h\\u00e1t","content":"<ul>\\r\\n\\t<li><a href=\\"http:\\/\\/luongnd2286.tk\\/#\\">Album<\\/a><\\/li>\\r\\n\\t<li><a href=\\"http:\\/\\/luongnd2286.tk\\/#\\">Hotlist<\\/a><\\/li>\\r\\n<\\/ul>"},{"title":"BXH","content":"<ul>\\r\\n\\t<li><a href=\\"http:\\/\\/luongnd2286.tk\\/#\\">MV<\\/a><\\/li>\\r\\n\\t<li><a href=\\"http:\\/\\/luongnd2286.tk\\/#\\">Ngh\\u1ec7 s\\u0129<\\/a><\\/li>\\r\\n<\\/ul>"}],{"title":"K\\u1ebft n\\u1ed1i v\\u1edbi ch\\u00fang t\\u00f4i","content":"#"},{"title":"T\\u1ea3i \\u1ee9ng d\\u1ee5ng","link_down1":"#","link_down2":"#"},"<div>\\r\\n<p>C\\u01a1 quan ch\\u1ee7 qu\\u1ea3n C&ocirc;ng ty C\\u1ed5 ph\\u1ea7n B\\u1ea1ch Minh - \\u0110\\u1ecba ch\\u1ec9: P804, T&ograve;a nh&agrave; VET, 98 Ho&agrave;ng Qu\\u1ed1c Vi\\u1ec7t, H&agrave; N\\u1ed9i<\\/p>\\r\\n\\r\\n<p>Email: Nhac@vega.com.vn Tel: 024 37554190 - Ng\\u01b0\\u1eddi ch\\u1ecbu tr&aacute;ch nhi\\u1ec7m n\\u1ed9i dung: &Ocirc;ng L&ecirc; H\\u1eefu To&agrave;n<\\/p>\\r\\n\\r\\n<p>Gi\\u1ea5y ph&eacute;p MXH s\\u1ed1 311\\/GP-BTTTT do B\\u1ed9 Th&ocirc;ng Tin v&agrave; Truy\\u1ec1n th&ocirc;ng c\\u1ea5p ng&agrave;y 04\\/07\\/2017<\\/p>\\r\\n\\r\\n<p>&copy; 2015 Vega Corporation<\\/p>\\r\\n<\\/div>"]', 'created_at' => null, 'updated_at' => null),
                array('id' => '2', 'name' => 'Thông tin chung', 'key' => 'generals', 'value' => '{"name":"Th\\u00e1nh ca","logo":"https:\\/\\/nhac.vn\\/web-v2\\/new-image\\/logo.png","slideshow":[{"link":"#","slide":"https:\\/\\/thanhca.pveser.com\\/\\/storage\\/uploads\\/images\\/Slideshow\\/1570873509392_org.jpg"},{"link":"#","slide":"https:\\/\\/thanhca.pveser.com\\/\\/storage\\/uploads\\/images\\/Slideshow\\/1571642689516_org.jpg"},{"link":"#","slide":"https:\\/\\/thanhca.pveser.com\\/\\/storage\\/uploads\\/images\\/Slideshow\\/1571805565726_org.jpg"},{"link":"#","slide":"https:\\/\\/thanhca.pveser.com\\/\\/storage\\/uploads\\/images\\/Slideshow\\/1570873509392_org.jpg"},{"link":"#","slide":"https:\\/\\/thanhca.pveser.com\\/\\/storage\\/uploads\\/images\\/Slideshow\\/1571642689516_org.jpg"}],"topic":"1"}', 'created_at' => null, 'updated_at' => '2019-11-11 03:44:05'),
                array('id' => '3', 'name' => 'Sắp xếp danh mục', 'key' => 'sort_category', 'value' => '[{"category":"11","categories":["9","10"]},{"category":"12","categories":["13","14"]},{"category":"17","categories":["16","18"]}]', 'created_at' => null, 'updated_at' => '2019-11-11 07:20:38'),
                array('id' => '4', 'name' => 'Chọn danh mục bảng xếp hạng', 'key' => 'choose_category_bxh', 'value' => '[{"id":9},{"id":14}]', 'created_at' => null, 'updated_at' => '2019-11-12 09:19:23'),
                array('id' => '5', 'name' => 'Danh mục TOP100', 'key' => 'top100', 'value' => '[{"id":36},{"id":41},{"id":40},{"id":42}]', 'created_at' => null, 'updated_at' => null),
            );

            DB::table('options')->insert($options);
        }
    }
}
