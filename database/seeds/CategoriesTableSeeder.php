<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        /* `thanhca`.`categories` */
        $categories = array(
            array('id' => '9', 'name' => 'Thánh Ca Mùa Vọng', 'slug' => 'thanh-ca-mua-vong', 'parent_id' => '11', 'description' => null, 'created_at' => '2019-11-02 02:02:41', 'updated_at' => '2019-11-11 07:09:20'),
            array('id' => '10', 'name' => 'Thánh Ca Mùa Giáng Sinh', 'slug' => 'thanh-ca-mua-giang-sinh', 'parent_id' => '11', 'description' => null, 'created_at' => '2019-11-02 02:03:11', 'updated_at' => '2019-11-11 07:18:50'),
            array('id' => '11', 'name' => 'Thánh Ca Mùa Chay', 'slug' => 'thanh-ca-mua-chay', 'parent_id' => '0', 'description' => null, 'created_at' => '2019-11-02 02:03:31', 'updated_at' => '2019-11-02 02:03:31'),
            array('id' => '12', 'name' => 'Thánh Ca Mùa Phục Sinh', 'slug' => 'thanh-ca-mua-phuc-sinh', 'parent_id' => '0', 'description' => null, 'created_at' => '2019-11-02 02:03:48', 'updated_at' => '2019-11-02 02:03:48'),
            array('id' => '13', 'name' => 'Thánh Ca Mùa Quanh Năm', 'slug' => 'thanh-ca-mua-quanh-nam', 'parent_id' => '12', 'description' => null, 'created_at' => '2019-11-02 02:04:03', 'updated_at' => '2019-11-11 07:09:43'),
            array('id' => '14', 'name' => 'Thánh Ca Mùa Xuân', 'slug' => 'thanh-ca-mua-xuan', 'parent_id' => '12', 'description' => null, 'created_at' => '2019-11-02 02:08:51', 'updated_at' => '2019-11-11 07:09:47'),
            array('id' => '15', 'name' => 'Thánh Ca Thiếu Nhi', 'slug' => 'thanh-ca-thieu-nhi', 'parent_id' => '0', 'description' => null, 'created_at' => '2019-11-11 03:11:54', 'updated_at' => '2019-11-11 03:11:54'),
            array('id' => '16', 'name' => 'Thánh Ca Lễ Trọng', 'slug' => 'thanh-ca-le-trong', 'parent_id' => '17', 'description' => null, 'created_at' => '2019-11-11 03:12:11', 'updated_at' => '2019-11-11 07:10:09'),
            array('id' => '17', 'name' => 'Thánh Ca Dâng Mẹ', 'slug' => 'thanh-ca-dang-me', 'parent_id' => '0', 'description' => null, 'created_at' => '2019-11-11 03:12:27', 'updated_at' => '2019-11-11 03:12:27'),
            array('id' => '18', 'name' => 'Thánh Ca Lòng Chúa Thương Xót', 'slug' => 'thanh-ca-long-chua-thuong-xot', 'parent_id' => '17', 'description' => null, 'created_at' => '2019-11-11 03:12:44', 'updated_at' => '2019-11-11 07:18:40'),

        );

        if (DB::table('categories')->count() < 1) {
            DB::table('categories')->insert($categories);
        }
    }
}
