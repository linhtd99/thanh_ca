<?php

use Illuminate\Database\Seeder;

class SongTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (true) {
            $songs = [];
            $faker = Faker\Factory::create();
            for ($i = 1; $i < 500; ++$i) {
                $songs[] = [
                    'name' => $faker->name . rand(1, 9),
                    'code' => getToken(),
                    'status' => '1',
                    'category_id' => rand(9, 18),
                    'slug' => $faker->name . rand(1, 9),
                    'author_id' => 0,
                    'filename' => 'NgayKhongConMe2-5de8c2a3762d4.mp3',
                ];
            }
            DB::table('songs')->insert($songs);
        }
    }
}
