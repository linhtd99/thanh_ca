<?php

use Illuminate\Database\Seeder;

class VideosTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (true) {
            $videos = [];
            /* `thanhca`.`videos` */
            $videos = array(
                array('id' => '1', 'code' => 'km101B73657A', 'image' => null, 'name' => 'Nhạc Thánh Ca 2019 Mới Nhất - Hãy Đưa Con Về, Cho Con Thấy Chúa - LK Thánh Ca Hay Nhất Tháng 8 2019', 'category_id' => '13', 'beat_type' => '0', 'karaoke_type' => '0', 'album_id' => '3', 'lyric_id' => null, 'author_id' => null, 'description' => null, 'link' => 'https://www.youtube.com/watch?v=6-fd4hhUcTE', 'status' => '1', 'created_at' => '2019-10-09 00:00:00', 'updated_at' => '2019-11-11 02:52:55'),
                array('id' => '2', 'code' => 'Ove5qgU0FXps', 'image' => null, 'name' => 'Nhạc Thánh Ca Tận Hiến 2019 Hay Nhất - Cho Con Thấy Chúa - Nghe Để Vững Bước Trên Con Đường Ơn Gọi', 'category_id' => '13', 'beat_type' => '0', 'karaoke_type' => '1', 'album_id' => '4', 'lyric_id' => null, 'author_id' => null, 'description' => '<p>Nhạc Th&aacute;nh Ca Tận Hiến 2019 Hay Nhất - Cho Con Thấy Ch&uacute;a - Nghe Để Vững Bước Tr&ecirc;n Con Đường Ơn Gọi<br />
Nhấn chu&ocirc;ng 🔔 th&ocirc;ng b&aacute;o để nhận những video ra mới nhất nh&eacute;!<br />
#nhacthanhca2019 #nhacthanhcatanhien2019 #thanhca2019<br />
───────────────────────────────────────<br />
► Hợp t&aacute;c, quảng c&aacute;o, l&agrave;m video, đăng nhạc l&ecirc;n Nhạc Th&aacute;nh Ca li&ecirc;n hệ ch&uacute;ng con qua mail: nhacthanhcaorg@gmail.com</p>', 'link' => 'https://www.youtube.com/watch?v=08g_wiMHe5g', 'status' => '1', 'created_at' => '2019-11-11 02:53:47', 'updated_at' => '2019-11-11 02:53:47'),
                array('id' => '3', 'code' => 'yYz9VDjs3GVp', 'image' => 'https://thanhca.pveser.com//storage/uploads/images/hinh-nen-thien-chua-giao.jpg', 'name' => 'Nhạc Thánh Ca Hay Nhất 2019 - Đường Con Theo Chúa, Cho Con Thấy Chúa | Thánh Ca Tuyển Chọn', 'category_id' => '12', 'beat_type' => '1', 'karaoke_type' => '0', 'album_id' => '5', 'lyric_id' => null, 'author_id' => null, 'description' => '<h1>Nhạc Th&aacute;nh Ca Hay Nhất 2019 - Đường Con Theo Ch&uacute;a, Cho Con Thấy Ch&uacute;a | Th&aacute;nh Ca Tuyển Chọn</h1>', 'link' => 'https://www.youtube.com/watch?v=FPtSPj03MBI', 'status' => '1', 'created_at' => '2019-11-11 02:54:15', 'updated_at' => '2019-11-11 02:54:15'),
                array('id' => '4', 'code' => 'D54odwj15H3p', 'image' => 'https://thanhca.pveser.com//storage/uploads/images/V-2017-CN10TN-HolyTrinity-Ga3_16-18-1.jpg', 'name' => 'Nhạc Giáng Sinh 2020 Hay Nhất - LK Nhạc Noel Sôi Động Ngập Tràn Không Khí Giáng Sinh 2020', 'category_id' => '13', 'beat_type' => '1', 'karaoke_type' => '1', 'album_id' => '7', 'lyric_id' => null, 'author_id' => null, 'description' => '<h1>Nhạc Gi&aacute;ng Sinh 2020 Hay Nhất - LK Nhạc Noel S&ocirc;i Động Ngập Tr&agrave;n Kh&ocirc;ng Kh&iacute; Gi&aacute;ng Sinh&nbsp;</h1>', 'link' => 'https://www.youtube.com/watch?v=xvYtmxEW4BQ', 'status' => '1', 'created_at' => '2019-11-11 02:54:43', 'updated_at' => '2019-11-11 02:54:43'),
                array('id' => '5', 'code' => 'MqhB0sU5k3h5', 'image' => null, 'name' => 'Thánh Ca Mùa Hoa Dâng Mẹ - Tiếng Hát Hoàng Oanh - Cung Chúc Trinh Vương | Thánh Ca Tuyển Chọn', 'category_id' => '11', 'beat_type' => '1', 'karaoke_type' => '1', 'album_id' => '5', 'lyric_id' => null, 'author_id' => null, 'description' => '<h1><br />
Giới thiệu b&agrave;i h&aacute;t</h1>

<h1>Th&aacute;nh Ca M&ugrave;a Hoa D&acirc;ng Mẹ - Tiếng H&aacute;t Ho&agrave;ng Oanh - Cung Ch&uacute;c Trinh Vương | Th&aacute;nh Ca Tuyển Chọn&nbsp;</h1>', 'link' => 'https://www.youtube.com/watch?v=butJNnMnNkY&t=3382s', 'status' => '1', 'created_at' => '2019-11-11 03:30:07', 'updated_at' => '2019-11-11 03:30:07'),
                array('id' => '6', 'code' => 'JhQaOjeH5XFF', 'image' => null, 'name' => 'Full Thánh Ca Hà Thanh Xuân - Bỏ Ngài Con Biết Theo Ai-Đoản Khúc Nguyện Cầu nhạc lossless', 'category_id' => '9', 'beat_type' => '1', 'karaoke_type' => '1', 'album_id' => '6', 'lyric_id' => null, 'author_id' => null, 'description' => '<h1><br />
Giới thiệu b&agrave;i h&aacute;t</h1>

<h1>Full Th&aacute;nh Ca H&agrave; Thanh Xu&acirc;n - Bỏ Ng&agrave;i Con Biết Theo Ai-Đoản Kh&uacute;c Nguyện Cầu nhạc lossless</h1>', 'link' => 'https://www.youtube.com/watch?v=jwp9k69zDyM&t=1076s', 'status' => '1', 'created_at' => '2019-11-11 03:30:48', 'updated_at' => '2019-11-11 03:30:48'),
                array('id' => '7', 'code' => '16y3b1EL34fq', 'image' => 'https://thanhca.pveser.com//storage/uploads/images/151212stargiaan02_64270.jpg', 'name' => 'Thánh Ca Chọn Lọc 1 - Cầu Cho Cha Mẹ', 'category_id' => '12', 'beat_type' => '1', 'karaoke_type' => '1', 'album_id' => '7', 'lyric_id' => null, 'author_id' => null, 'description' => '<h1>Th&aacute;nh Ca Chọn Lọc 1 - Cầu Cho Cha Mẹ</h1>', 'link' => 'https://www.youtube.com/watch?v=pNoUnKOa3BE&t=1811s', 'status' => '1', 'created_at' => '2019-11-11 03:31:13', 'updated_at' => '2019-11-11 03:31:13'),
            );

            DB::table('videos')->insert($videos);
        }
    }
}
