<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /*
     * Seed the application's database.
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            AlbumsTableSeeder::class,
            ArtistsTableSeeder::class,
            // TagsTableSeeder::class,
            SongsListensSedder::class,
            OptionsTableSeeder::class,
            // VideosTableSeed::class,
            CustomerTableSeeder::class,
            // SongTableSeeder::class,
            // CommentTableSeed::class,
            // VideoviewTablesSeed::class,
        ]);
    }
}
