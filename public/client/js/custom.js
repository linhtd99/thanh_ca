//Menu
jQuery(document).ready(function () {
    var window_height = $(window).height();
    var bar_player = $('body').find('#player').height();
    console.log(bar_player);
    console.log(window_height);
    // jQuery('body').find('.box-list .left-list').height(( - 130));
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery('.menu-box .main-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-down"></i>');
    jQuery('.menu-box .main-menu .sub-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-right"></i>');

    jQuery('.menu-site .btn-show-menu').click(function () {
        jQuery(this).parents('.menu-site').find('.menu-box').css('width','100%');
    });
    jQuery('.menu-box .btn-hide-menu, .menu-box .bg-menu').click(function () {
        jQuery(this).parents('.menu-box ').css('width','0');
    });

    if($(window).width()<992){
        jQuery('.main-menu li.menu-item-has-children>a>i').click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().children('.sub-menu').slideToggle('fast');
        });
    }
});

$(document).ready(function() {

  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1.owlCarousel({
    items : 1,
    slideSpeed : 2000,
    nav: false,
    autoplay: true,
    dots: false,
    loop: true,
    responsiveRefreshRate : 200,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
  }).on('changed.owl.carousel', syncPosition);

  sync2
    .on('initialized.owl.carousel', function () {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
    items : 5,
    dots: false,
    nav: false,
    smartSpeed: 200,
    slideSpeed : 500,
    slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
    responsiveRefreshRate : 100
  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);

    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }

    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();

    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }

  sync2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });
});


$(document).ready(function() {

  var sync3 = $("#sync3");
  var sync4 = $("#sync4");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  sync3.owlCarousel({
    items : 1,
    slideSpeed : 2000,
    nav: false,
    autoplay: false,
    dots: false,
    loop: true,
    responsiveRefreshRate : 200,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
  }).on('changed.owl.carousel', syncPosition);

  sync4
    .on('initialized.owl.carousel', function () {
      sync4.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
    items : 5,
    dots: false,
    nav: false,
    smartSpeed: 200,
    slideSpeed : 500,
    slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
    responsiveRefreshRate : 100
  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);

    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }

    //end block

    sync4
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync4.find('.owl-item.active').length - 1;
    var start = sync4.find('.owl-item.active').first().index();
    var end = sync4.find('.owl-item.active').last().index();

    if (current > end) {
      sync4.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync4.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync3.data('owl.carousel').to(number, 100, true);
    }
  }

  sync4.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    sync3.data('owl.carousel').to(number, 300, true);
  });
});

$('.show_slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    dots:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})

$('.history_listen').owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})

// $('.history_song').owlCarousel({
//     loop: true,
//     margin: 10,
//     nav: false,
//     navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
//     dots: false,
//     responsive: {
//         0: {
//             items: 2
//         },
//         600: {
//             items: 3
//         },
//         1000: {
//             items: 5
//         }
//     }
// })

jQuery(document).ready(function() {
  jQuery('.more_add #seeMoreLyric').click(function(e) {
    e.preventDefault();
    jQuery('#divLyric').css('max-height', '100%');
    jQuery(this).hide();
    jQuery('#hideMoreLyric').show();
  });
  jQuery('.more_add #hideMoreLyric').click(function(e) {
    e.preventDefault();
    jQuery('#divLyric').css('max-height', '255px');
    jQuery(this).hide();
    jQuery('#seeMoreLyric').show();
  });
  jQuery('.comment-header .sorting-block-wrapper .active-sorting').click(function() {
    jQuery('.comment-header .dropdown-menu-list').slideToggle();
  });

});
// page-user
jQuery(document).ready(function () {
  jQuery('#up-btnEdit').click(function (e) {
    e.preventDefault();
    jQuery('.form-edit1').hide();
    jQuery('.form-edit2').show();
  });
  jQuery('#btnCloseEdit').click(function (e) {
    e.preventDefault();
    jQuery('.form-edit1').show();
    jQuery('.form-edit2').hide();
  });
  jQuery('.wn-reset-pas').click(function (e) {
    e.preventDefault();
    jQuery('.form-edit1').hide();
    jQuery('.form-edit3').show();
  });
  jQuery('#wn-back-user').click(function (e) {
    e.preventDefault();
    jQuery('.form-edit1').show();
    jQuery('.form-edit3').hide();
  });
  jQuery('.btn_edit_item').click(function (e) {
    // e.preventDefault();
    jQuery('.form-playlist2').show();
    jQuery('.form-playlist1').hide();
  });
  jQuery('.btn_accept_item').click(function (e) {
    e.preventDefault();
    jQuery('.form-playlist2').hide();
    jQuery('.form-playlist1').show();
  });


  jQuery('.2a').click(function (e) {
    // e.preventDefault();
    jQuery('.form-playlist1').show();
    jQuery('.form-playlist2').hide();
  })
});

// js trinh nghe nhac
jQuery(document).ready(function() {
	jQuery('body').on('click', '.plyr__controls .plyr__controls__item.plyr__control:eq(0)', function() {
		if(jQuery(this).hasClass('plyr__control--pressed'))
		{
			jQuery('#mini__player').removeClass('active');
		}
		else
		{
			jQuery('#mini__player').addClass('active');
		}
	});
	jQuery('.btn-down').click(function() {
    	jQuery('#plwrap').toggleClass('active-list');
    	jQuery('.z-expand-btn').show();
    });
    jQuery('.z-playlist-wraper .z-btn-playlist-expand ').click(function() {
        jQuery('#plwrap').toggleClass('active-list');
        jQuery('.z-expand-btn,.z-mini-media-btn').toggle();
        jQuery('.lyrics-box').hide();
    });

    jQuery('#audio0 .z-expand-btn ').click(function() {
    	jQuery('.lyrics-box').slideToggle();
        jQuery('.lyrics-box').removeClass('full-width');
    });
	jQuery('.shuffle-hh').click(function() {
    	jQuery('.shuffle-hh').toggleClass('active-color');

    });
    jQuery('.z-mini-media-btn').click(function() {
        jQuery('.lyrics-box').toggleClass('full-width');

        if (jQuery('.lyrics-box').hasClass('full-width')) {
            jQuery('.lyrics-box').show();
        }else{
            jQuery('.lyrics-box').hide();
        }
    });
    jQuery('.tab-btn>ul>li:first-child>a').click(function(e) {
        e.preventDefault();
        if (jQuery(this).hasClass('btn-play-pause')) {
            jQuery('#audio1').trigger('play');
            jQuery(this).children('i').removeClass('fa-play');
            jQuery(this).children('i').addClass('fa-pause');
            jQuery(this). addClass('btn-pause-tab');
            jQuery(this). removeClass('btn-play-pause');
            var y= jQuery(this).html();
            y=y.replace('Nghe bài hát','Tạm dừng');

        }
        else{
            jQuery('#audio1').trigger('pause');
            jQuery(this).children('i').addClass('fa-play');
            jQuery(this).children('i').removeClass('fa-pause');
            jQuery(this). removeClass('btn-pause-tab');
            jQuery(this). addClass('btn-play-pause');
            var y= jQuery(this).html();
            y=y.replace('Tạm dừng','Nghe bài hát');
        }
        jQuery(this).html(y);
    });

    jQuery('.lyrics-box').click(function() {
        if (jQuery(this).children('.icon').hasClass('ic-svg-play-outline')) {
            jQuery('#audio1').trigger('play');
            jQuery(this).children('.icon').removeClass('ic-svg-play-outline');
            jQuery(this).children('.icon').addClass('ic-svg-pause-outline');
            jQuery(this).children('.icon').removeClass('show-icon');
        }
        else{
            jQuery(this).children('.icon').removeClass('ic-svg-pause-outline');
            jQuery(this).children('.icon').addClass('ic-svg-play-outline');
            jQuery('#audio1').trigger('pause');
            jQuery(this).children('.icon').addClass('show-icon');

        }

    });

    jQuery('#mini__player').click(function() {
        var src_vd= jQuery('#vid-big').attr('src');
        src_vd= src_vd.length;
        if (src_vd>0) {
            if (jQuery(this).children('.icon').hasClass('ic-svg-play-outline')) {
                jQuery('#audio1').trigger('play');
                jQuery(this).children('.icon').removeClass('ic-svg-play-outline');
                jQuery(this).children('.icon').addClass('ic-svg-pause-outline');
                jQuery(this).children('.icon').removeClass('show-icon');
            }
            else{
                jQuery(this).children('.icon').removeClass('ic-svg-pause-outline');
                jQuery(this).children('.icon').addClass('ic-svg-play-outline');
                jQuery('#audio1').trigger('pause');
                jQuery(this).children('.icon').addClass('show-icon');

            }
        }
    });

    jQuery('.z-btn-features .hover-view li a.z-btn-search').click(function(e) {
        e.preventDefault();
        jQuery(this).parents('.z-head').children('.z-miniplayer-search-groups').show();
        jQuery(this).parents('.z-head').children('.zz-show').hide();
    });
    jQuery('.z-head .qh-close-input').click(function() {
        jQuery(this).parents('.z-miniplayer-search-groups').hide();
        jQuery('.zz-show').show();
    });


});


jQuery(document).ready(function () {
    jQuery('.menu-box .main-menu>li.menu-item-has-children>a').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
    jQuery('.menu-box .main-menu .sub-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-right"></i>');

    jQuery('.menu-site .btn-show-menu').click(function () {
        jQuery(this).parents('.menu-site').find('.menu-box').css('width','100%');
    });
    jQuery('.menu-box .btn-hide-menu, .menu-box .bg-menu').click(function () {
        jQuery(this).parents('.menu-box ').css('width','0');
    });

    if($(window).width()<992){
        jQuery('.main-menu li.menu-item-has-children>a>i').click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().children('.sub-menu').slideToggle('fast');
        });
    }
});


jQuery(function(){

    jQuery('body').on('click', '.hover-view-z>a.action', function(e){
        e.preventDefault();
        // $('body').find('.z-dropdown').hide();
        jQuery(this).parents('.z-body-area .nav-item-li').children('.z-dropdown').toggle();
    } )
})

// get share url

jQuery(function(){

    jQuery('body').on('click', '.z-b-share', function(e){
        e.preventDefault();
        var url = jQuery(this).data('url');
        jQuery('#input-share').val(url);
    } )
})

jQuery(document).ready(function(){

    setTimeout(() => {
            jQuery('body').on("mouseover",'.plyr__control--pressed',function(){

            jQuery(this).parent().find('input[type=range]').show();
            //  alert('adad');
        })
    }, 1000);

    // jQuery('body').not('.plyr__control--pressed input[type=range] ').on('mouseover',function(e){
    //     e.target;

    //     jQuery('body').find('.plyr__control--pressed input[type=range]').hide();


    // })
})


