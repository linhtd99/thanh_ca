function readableDuration(seconds) {
    sec = Math.floor(seconds);
    min = Math.floor(sec / 60);
    min = min >= 10 ? min : '0' + min;
    sec = Math.floor(sec % 60);
    sec = sec >= 10 ? sec : '0' + sec;
    return min + ':' + sec;
}

function tc_player(data_s, suggest_s, route_listen_song, route_random_song, add_to_wishlist, player_search) {
    jQuery(function ($) {
        'use strict';
        var supportsAudio = !!document.createElement('audio').canPlayType;
        var data_songs = data_s;
        var suggest_songs = suggest_s;
        if (supportsAudio) {
            // initialize plyr
            var player = new Plyr('#audio1', {
                controls: [
                    'play-large', // The large play button in the center
                    //'rewind', // Rewind by the seek time (default 10 seconds)
                    'play', // Play/pause playback
                    //'fast-forward', // Fast forward by the seek time (default 10 seconds)
                    'restart', // Restart playback
                    'progress', // The progress bar and scrubber for playback and buffering
                    'current-time', // The current time of playback
                    'duration', // The full duration of the media
                    'mute', // Toggle mute
                    'volume',
                    // Volume control
                    // 'captions', // Toggle captions
                    'settings', // Settings menu
                    'pip', // Picture-in-picture (currently Safari only)
                    'airplay', // Airplay (currently Safari only)
                    'fullscreen', // Toggle fullscreen
                ],
                'autoplay': true,
                'settings': [
                    'loop',
                ],

            });
            // initialize playlist and controls
            var index = 0,
                playing = false,
                playrand = false,
                checkvideo = false,
                tracks = data_songs,
                mediaPath = 'audio/',
                extension = '',
                suggest_tracks = suggest_songs,
                searchSongs,
                buildPlaylist = jQuery(tracks).each(function (key, value) {
                    setTimeout(() => {
                        var trackNumber = value.track,
                            trackName = value.name,
                            trackCode = value.code,
                            trackImage = value.image,
                            trackUrl = value.url,
                            trackDownload = value.download,
                            twishlist = value.wishlist;
                        // console.log(twishlist);
                        let trackDuration = '';
                        var audio = new Audio();
                        audio.src = value.filename;

<<<<<<< HEAD
                        // setTimeout(() => {
=======
                        setTimeout(() => {
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71
                        $(audio).on("loadedmetadata", function () {
                            var val = audio.duration;
                            console.log(val);
                        });
<<<<<<< HEAD
                        // }, 1000);
=======
                        }, 10);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71



                        jQuery('#plList').append(
                            `
                        <li data-code="${trackCode}" class="nav-item-li ${trackNumber == 1 ? 'plSel' : ''}">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${trackCode}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${trackCode}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImage}" alt="${trackName}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackName}</span>
                                <span class="plLength">${trackDuration}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${trackCode}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">

                                <div data-url="${trackUrl}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${trackDownload}" class="dropdown-item"><a href="${trackDownload}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrl}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>
                            </div>
                        </li>
                        `
                        );
                    }, 500 * key);


                }),
                buildSuggestPlaylist = jQuery(suggest_tracks).each(function (key, value) {
                    var trackNumberSuggest = value.track,
                        trackNameSuggest = value.name,
                        trackDurationSuggest = '',
                        trackCodeSuggest = value.code,
                        trackImageSuggest = value.image,
                        trackUrlSuggest = value.url,
                        trackDownloadSuggest = value.download,
                        twishlist = value.wishlist;
                    if (trackNumberSuggest.toString().length === 1) {
                        trackNumberSuggest = '0' + trackNumberSuggest;
                    }
                    jQuery('#plListS').append(
                        `
                        <li data-code="${trackCodeSuggest}" class="nav-item-li">
                            <div data-code="${trackCodeSuggest}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImageSuggest}" alt="${trackNameSuggest}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackNameSuggest}</span>
                                <span class="plLength">${trackDurationSuggest}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a  data-code="${trackCodeSuggest}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">

                                <div class="dropdown-item  z-share z-b-share" data-url="${trackUrlSuggest}"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div class="dropdown-item"><a role="button" href="${trackDownloadSuggest}" class="action menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrlSuggest}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div  style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>

                            </div>
                        </li>

                        `
                    );
                }),
                npAction = jQuery('#npAction'),
                npTitle = jQuery('#npTitle'),
                npImage = jQuery('#mini__player img'),
                audio = jQuery('#audio1').on('play', function () {
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').removeClass('fa-play');
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').addClass('fa-pause');
                    jQuery('.tab-btn>ul>li:first-child>a').addClass('btn-pause-tab');
                    jQuery('.tab-btn>ul>li:first-child>a').removeClass('btn-play-pause');
                    var y = jQuery('.tab-btn>ul>li:first-child>a').html();
                    y = y.replace('Nghe bài hát', 'Tạm dừng');
                    jQuery('.tab-btn>ul>li:first-child>a').html(y);
                    jQuery('#vid, #vid-big').trigger('play');
                    jQuery('.lyrics-box').children('.icon').removeClass('show-icon');
                    playing = true;
                    npAction.text('Now Playing...');
                }).on('pause', function () {
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').addClass('fa-play');
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').removeClass('fa-pause');
                    jQuery('.tab-btn>ul>li:first-child>a').removeClass('btn-pause-tab');
                    jQuery('.tab-btn>ul>li:first-child>a').addClass('btn-play-pause');
                    var y = jQuery('.tab-btn>ul>li:first-child>a').html();
                    y = y.replace('Tạm dừng', 'Nghe bài hát');
                    jQuery('.tab-btn>ul>li:first-child>a').html(y);
                    jQuery('#vid,#vid-big').trigger('pause');
                    jQuery('.lyrics-box').children('.icon').addClass('show-icon');
                    playing = false;
                    npAction.text('Paused...');
                }).on('ended', function () {
                    npAction.text('Paused...');
                    if (playrand) {
                        var min = 0;
                        var max = 4;
                        var random = Math.floor(Math.random() * 10 % (max - min + 1)) + min;
                        index = random;
                        loadTrack(index);
                        audio.play();
                    } else {
                        if ((index + 1) < tracks.length) {
                            index++;
                            loadTrack(index);
                            audio.play();
                        } else {
                            if (suggest_tracks.length != 0) {
                                if (document.getElementById("switch-auto_play").checked) {
                                    index++;
                                    var add_song = suggest_tracks[0];
                                    add_song.track = tracks.length;
                                    tracks.push(add_song);
                                    suggest_tracks.shift();
                                    loadTrack(index);
                                    audio.play();
                                    var trackNumber = add_song.track,
                                        trackName = add_song.name,
                                        trackDuration = '',
                                        trackCode = add_song.code,
                                        trackImage = add_song.image,
                                        trackUrl = add_song.url,
                                        trackDownload = add_song.download,
                                        twishlist = add_song.wishlist;
                                    jQuery('#plList').append(
                                        `
                                                                    <li data-code="${trackCode}" class="nav-item-li plSel">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${trackCode}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${trackCode}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImage}" alt="${trackName}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackName}</span>
                                <span class="plLength">${trackDuration}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a  data-code="${trackCode}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">
                                <div data-url="${trackUrl}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${trackDownload}" class="dropdown-item"><a href="${trackDownload}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrl}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>
                            </div>
                        </li>
                                            `
                                    );
                                    rebuildPlaylistSuggest(suggest_tracks);
                                } else {
                                    audio.pause();
                                    index = 0;
                                    loadTrack(index);
                                }
                            } else {
                                audio.pause();
                                index = 0;
                                loadTrack(index);
                            }
                        }
                    }
                }).get(0),
                rand = jQuery('.shuffle-hh').on('click', function () {
                    playrand = !playrand;
                }),
                btnPrev = jQuery('#btnPrev').on('click', function () {
                    if ((index - 1) > -1) {
                        index--;
                        let title = tracks[index].name;
                        jQuery('.z-song-name a').text(title)

                        let singer = tracks[index].singer;
                        jQuery('.z-artists a').text(singer)

                        let album = tracks[index].album;
                        jQuery('.z-album a').text(album)

                        loadTrack(index);
                        if (playing) {
                            audio.play();
                        }
                    } else {
                        audio.pause();
                        index = 0;
                        loadTrack(index);
                    }
                }),
                btnRandom = jQuery('.z-btn-random').on('click', function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: route_random_song,
                            data: {
                                listsongs: tracks,
                            },
                            success: function (data) {
                                // console.log(data);
                                suggest_tracks = data;
                                rebuildPlaylistSuggest(suggest_tracks);
                            }
                        });
                    }, 1);
                }),
                btnNext = jQuery('#btnNext').on('click', function () {
                    if ((index + 1) < tracks.length) {
                        index++;
                        loadTrack(index);
                        if (playing) {
                            audio.play();
                        }
                    } else {
                        if (suggest_tracks.length != 0) {
                            if (document.getElementById("switch-auto_play").checked) {
                                index++;
                                var add_song = suggest_tracks[0];
                                add_song.track = tracks.length + 1;
                                tracks.push(add_song);
                                suggest_tracks.shift();
                                loadTrack(index);
                                audio.play();
                                var trackNumber = add_song.track,
                                    trackName = add_song.name,
                                    trackDuration = '',
                                    trackCode = add_song.code,
                                    trackImage = add_song.image,
                                    trackUrl = add_song.url,
                                    trackDownload = add_song.download,
                                    twishlist = add_song.wishlist;
                                jQuery('#plList').append(
                                    `
                            <li data-code="${trackCode}" class="nav-item-li plSel">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${trackCode}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${trackCode}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImage}" alt="${trackName}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackName}</span>
                                <span class="plLength">${trackDuration}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a  data-code="${trackCode}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">
                                <div data-url="${trackUrl}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${trackDownload}" class="dropdown-item"><a href="${trackDownload}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrl}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>

                            </div>
                        </li>
                                `
                                );
                                rebuildPlaylistSuggest(suggest_tracks);
                            } else {
                                audio.pause();
                                index = 0;
                                loadTrack(index);
                            }
                        } else {
                            audio.pause();
                            index = 0;
                            loadTrack(index);
                        }
                    }
                    let title = tracks[index].name;
                    jQuery('.z-song-name a').text(title)

                    let singer = tracks[index].singer;
                    jQuery('.z-artists a').text(singer)

                    let album = tracks[index].album;
                    jQuery('.z-album a').text(album)
                }),
                btnDelete = jQuery('body').on('click', '.z-delete', function () {
                    var $this = jQuery(this);
                    var checkbox = jQuery('body').find('#plList').find("input[name='z-checkbox']:checked");
                    var trackPlaying = jQuery('body').find('.plSel').data('code');
                    var arr = [];
                    $.each(checkbox, function () {
                        arr.push(jQuery(this).val());
                    });
                    var newTrack = [];
                    var count_new_track = 0;
                    tracks.forEach(e => {
                        if (!arr.includes(e.code) || e.code == trackPlaying) {
                            count_new_track++;
                            let n = e;
                            n.track = count_new_track;
                            newTrack.push(n);
                        }
                    });
                    tracks = newTrack;
                    console.log(tracks);
                    console.log(index);
                    index = 0;
                    // console.log(trackPlaying);
                    rebuildPlaylist(tracks, trackPlaying);
                    jQuery(this).parents('#plList').find('input[type=checkbox]').prop('checked', false);
                    jQuery('body').find('.check-all ').prop('checked', false);
                    let html = "DANH SÁCH PHÁT (" + tracks.length + ")";
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox-title').html(html);
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox').removeClass('appear-all');
                    jQuery(this).addClass('hide');

                }),
                search = jQuery('body').on('keyup', '#search_player', function () {
                    var keyword = jQuery(this).val();
                    // console.log(keyword);
                    if (keyword.length > 0) {
                        jQuery('body').find('#plList li').each(function () {
                            var $this = jQuery(this);
                            let title = $this.find('.plTitle').html();
                            // console.log(title.includes(keyword));
                            if (title.toLowerCase().includes(keyword) == false) {
                                $this.addClass('hideElm');
                            }
                        });
                        jQuery('body').find('#plListSearch').show();
                        jQuery('body').find('#plListS').hide();
                        jQuery('body').find('.z-checkbox-2').hide();
                        jQuery('body').find('.z-checkbox-3').show();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        setTimeout(function () {
                            $.ajax({
                                type: 'POST',
                                url: player_search,
                                data: {
                                    keyword: keyword,
                                    listsongs: tracks,
                                },
                                success: function (data) {
                                    if (typeof data == 'object') {
                                        buildListSearch(data);
                                        searchSongs = data;
                                    }
                                }
                            });
<<<<<<< HEAD
                        }, 500);
=======
                        }, 100);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71
                    } else {
                        jQuery('body').find('#plListSearch').hide();
                        jQuery('body').find('#plListS').show();
                        jQuery('body').find('.z-checkbox-3').hide();
                        jQuery('body').find('.z-checkbox-2').show();
                        jQuery('body').find('#plList li').show();
                        jQuery('body').find('#plList li').removeClass('hideElm');
                    }
                }),
                closeSearch = jQuery('body').on('click', '.qh-close-input', function (e) {
                    jQuery('body').find('#search_player').val('');
                    jQuery('body').find('#plListS').show();
                    jQuery('body').find('#plListSearch').show();
                    jQuery('body').find('#plListSearch').html('');
                    jQuery('body').find('.z-checkbox-3').hide();
                    jQuery('body').find('.z-checkbox-2').show();
                    jQuery('body').find('#plList li').show();
                    jQuery('body').find('#plList li').removeClass('hideElm');
                }),
                chooseSongSearch = jQuery('body').on('click', '#plListSearch li .nav-item', function (e) {
                    let $this = jQuery(this);
                    let code = $this.data('code');
                    var trackPlaying = jQuery('body').find('.plSel').data('code');
                    let choose = searchSongs.find(element => element.code == code);
                    // console.log(choose);
                    let newLists = new Array();
                    console.log(tracks);
                    tracks.forEach(e => {
                        if (e.code == trackPlaying) {
                            newLists.push(e);
                            newLists.push(choose);
                        } else {
                            newLists.push(e);
                        }
                    });
                    tracks = newLists;
                    let trackActive = jQuery('body').find('.plSel');
                    trackActive.after(`
                    <li data-code="${choose.code}" class="nav-item-li plSel">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${choose.code}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${choose.code}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${choose.image}" alt="${choose.name}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${choose.name}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${choose.code}" class="z-like-btn liked" title="Thích"><i class="fa ${choose.wishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">
                                <div data-url="${choose.url}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${choose.download}" class="dropdown-item"><a href="${choose.download}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${choose.url}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>
                            </div>
                        </li>
                    `);
                    trackActive.removeClass('plSel');
                    index++;
                    loadTrack(index);
                    $this.parent().remove();
                    searchSongs = searchSongs.filter(song => song.code != choose);

                }),
                chooseSongSuggest = jQuery('body').on('click', '#plListS li .nav-item', function (e) {
                    let $this = jQuery(this);
                    let code = $this.data('code');
                    var trackPlaying = jQuery('body').find('.plSel').data('code');
                    let choose = suggest_tracks.find(element => element.code == code);
                    // console.log(choose);
                    let newLists = new Array();
                    console.log(tracks);
                    tracks.forEach(e => {
                        if (e.code == trackPlaying) {
                            newLists.push(e);
                            newLists.push(choose);
                        } else {
                            newLists.push(e);
                        }
                    });
                    tracks = newLists;
                    let trackActive = jQuery('body').find('.plSel');
                    trackActive.after(`
                    <li data-code="${choose.code}" class="nav-item-li plSel">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${choose.code}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${choose.code}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${choose.image}" alt="${choose.name}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${choose.name}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${choose.code}" class="z-like-btn liked" title="Thích"><i class="fa ${choose.wishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">
                                <div data-url="${choose.url}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${choose.download}" class="dropdown-item"><a href="${choose.download}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${choose.url}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>
                            </div>
                        </li>
                    `);
                    trackActive.removeClass('plSel');
                    index++;
                    loadTrack(index);
                    $this.parent().remove();
                    searchSongs = searchSongs.filter(song => song.code != choose);

                }),
                btnWishListTrack = jQuery('body').on('click', '#plList .z-like-btn', function () {
                    var $this = jQuery(this);
                    var $par = $this.parents('.nav-item-li');
                    console.log($par);

                    let active = $par.hasClass('plSel');
                    console.log(active);
                    var code = $this.data('code');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: add_to_wishlist,
                            data: {
                                code: code,
                            },
                            success: function (data) {
                                // console.log(data.message);
                                var index = tracks.findIndex(p => p.code == code);
                                if (data.message == 'unliked') {
                                    $this.find('i').removeClass('fa-heart');
                                    $this.find('i').addClass('fa-heart-o');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart-o');
                                    }
                                    tracks[index].wishlist = false;
                                }
                                if (data.message == 'liked') {
                                    $this.find('i').removeClass('fa-heart-o');
                                    $this.find('i').addClass('fa-heart');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart-o');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart');
                                    }
                                    tracks[index].wishlist = true;
                                }
                                if (data.message == 'login') {
                                    alert('Vui lòng đăng nhập ')
                                }
                            }
                        });
<<<<<<< HEAD
                    }, 1000);
=======
                    }, 100);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71


                }),
                btnWishListCTrack = jQuery('body').on('click', '.this-z-like-btn', function () {

                    var $par = jQuery('body').find('.plSel');
                    var $this = $par.find('.z-like-btn');
                    var code = $par.data('code');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: add_to_wishlist,
                            data: {
                                code: code,
                            },
                            success: function (data) {
                                // console.log(data.message);
                                var index = tracks.findIndex(p => p.code == code);
                                if (data.message == 'unliked') {
                                    $this.find('i').removeClass('fa-heart');
                                    $this.find('i').addClass('fa-heart-o');

                                    jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart');
                                    jQuery('body').find('.this-z-like-btn i').addClass('fa-heart-o');

                                    tracks[index].wishlist = false;
                                }
                                if (data.message == 'liked') {
                                    $this.find('i').removeClass('fa-heart-o');
                                    $this.find('i').addClass('fa-heart');

                                    jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart-o');
                                    jQuery('body').find('.this-z-like-btn i').addClass('fa-heart');

                                    tracks[index].wishlist = true;
                                }
                                if (data.message == 'login') {
                                    alert('Vui lòng đăng nhập ')
                                }
                            }
                        });
<<<<<<< HEAD
                    }, 1000);
=======
                    }, 100);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71


                }),
                btnWishListSuggest = jQuery('body').on('click', '#plListS .z-like-btn', function () {
                    var $this = jQuery(this);
                    var $par = $this.parents('.nav-item-li');
                    console.log($par);

                    let active = $par.hasClass('plSel');
                    console.log(active);
                    var code = $this.data('code');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: add_to_wishlist,
                            data: {
                                code: code,
                            },
                            success: function (data) {
                                // console.log(data.message);
                                var index = suggest_tracks.findIndex(p => p.code == code);
                                if (data.message == 'unliked') {
                                    $this.find('i').removeClass('fa-heart');
                                    $this.find('i').addClass('fa-heart-o');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart-o');
                                    }
                                    suggest_tracks[index].wishlist = false;
                                }
                                if (data.message == 'liked') {
                                    $this.find('i').removeClass('fa-heart-o');
                                    $this.find('i').addClass('fa-heart');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart-o');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart');
                                    }
                                    suggest_tracks[index].wishlist = true;
                                }

                                if (data.message == 'login') {
                                    alert('Vui lòng đăng nhập ')
                                }
                            }
                        });
<<<<<<< HEAD
                    }, 1000);
=======
                    }, 500);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71

                }),
                btnWishListSearch = jQuery('body').on('click', '#plListSearch .z-like-btn', function () {
                    var $this = jQuery(this);
                    var $par = $this.parents('.nav-item-li');
                    console.log($par);

                    let active = $par.hasClass('plSel');
                    console.log(active);
                    var code = $this.data('code');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: add_to_wishlist,
                            data: {
                                code: code,
                            },
                            success: function (data) {
                                // console.log(data.message);
                                var index = searchSongs.findIndex(p => p.code == code);
                                if (data.message == 'unliked') {
                                    $this.find('i').removeClass('fa-heart');
                                    $this.find('i').addClass('fa-heart-o');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart-o');
                                    }
                                    searchSongs[index].wishlist = false;
                                }
                                if (data.message == 'liked') {
                                    $this.find('i').removeClass('fa-heart-o');
                                    $this.find('i').addClass('fa-heart');
                                    if (active == true) {
                                        jQuery('body').find('.this-z-like-btn i').removeClass('fa-heart-o');
                                        jQuery('body').find('.this-z-like-btn i').addClass('fa-heart');
                                    }
                                    searchSongs[index].wishlist = true;
                                }

                                if (data.message == 'login') {
                                    alert('Vui lòng đăng nhập ')
                                }
                            }
                        });
<<<<<<< HEAD
                    }, 1000);
=======
                    }, 500);
>>>>>>> a1a67597f5daad62e08d252930271b500ccdcd71

                }),
                li = jQuery('body').on('click', '#plList li .nav-item', function () {
                    var id = parseInt(jQuery(this).parent().index());
                    if (id !== index) {
                        let title = tracks[id].name;
                        jQuery('.z-song-name a').text(title);

                        let singer = tracks[id].singer;
                        jQuery('.z-artists a').text(singer);

                        let album = tracks[id].album;
                        jQuery('.z-album a').text(album);

                        playTrack(id);
                        jQuery('#mini__player').addClass('active');

                        audio.play();
                    }
                }),
                loadTrack = function (id) {
                    var tl = tracks.length;
                    jQuery('body').find('.z-item-total').html(`(${tl})`);
                    jQuery('body').find('.z-checkbox-title').html(`DANH SÁCH PHÁT (${tl})`);
                    if (typeof tracks[id] == 'undefined') {
                        var id = id - 1;
                    }
                    if (typeof tracks[id] == 'undefined') {
                        id = id - 1;
                    }
                    jQuery('#audio1').attr('src', tracks[id].filename);
                    jQuery('.plSel').removeClass('plSel');
                    jQuery('#plList li:eq(' + id + ')').addClass('plSel');
                    npTitle.text(tracks[id].name);
                    npImage.attr("src", tracks[id].image);
                    let title = tracks[id].name;
                    jQuery('.z-song-name a').text(title);
                    jQuery('')
                    let singer = tracks[id].singer;
                    jQuery('.z-artists a').text(singer);

                    let album = tracks[id].album;
                    jQuery('.z-album a').text(album);
                    jQuery('#mini__player').addClass('active');
                    var heart;
                    if (tracks[id].wishlist == true) {
                        heart = 'fa-heart';
                    } else {
                        heart = 'fa-heart-o';
                    }
                    var heartcurrent = jQuery('body').find('#audio0 .fa-heart-current');
                    heartcurrent.removeClass('fa-heart');
                    heartcurrent.removeClass('fa-heart-o');
                    heartcurrent.addClass(heart);
                    index = id;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {
                        $.ajax({
                            type: 'POST',
                            url: route_listen_song,
                            data: {
                                code: tracks[id].code
                            },
                            success: function (data) {
                                console.log('view + 1');
                            }
                        });
                    }, 100000);

                    /**
                     * Lyrics
                     */
                    let text = '',
                        file_lyrics = tracks[id].lyrics;
                    if (file_lyrics != '') {
                        file_lyrics.forEach((val, key) => {
                            text += `[${val.time}] ${val.text} \n`;
                        });
                        jQuery('#lyrics').text(text);
                        jQuery('#lyrics1').text(text);

                        new RabbitLyrics({
                            audioElement: jQuery('#audio1'),
                            lyricsElement: jQuery('#lyrics1')
                        });
                        new RabbitLyrics({
                            audioElement: jQuery('#audio1'),
                            lyricsElement: jQuery('#lyrics')
                        });
                    } else {
                        jQuery('#lyrics').text('Không có lời bài hát');
                    }

                    if (tracks[id].video) {
                        jQuery('#vid').attr('src', 'audio/' + tracks[id].video + '.mp4');
                        jQuery('#vid').trigger('play');
                        jQuery('#vid-big').attr('src', 'audio/' + tracks[id].video + '.mp4');
                        jQuery('#vid-big').trigger('play');
                        checkvideo = true;
                        jQuery('#audio1').trigger('play');
                        jQuery('.lyrics-box').children('#lyrics').hide();
                        jQuery('#mini__player img').hide();
                        jQuery('#vid-big').show();
                    } else {
                        jQuery('#vid').removeAttr('src');
                        jQuery('#vid-big').removeAttr('src');
                        checkvideo = false;
                        jQuery('.lyrics-box').children('#lyrics').show();
                        jQuery('#mini__player img').show();
                        jQuery('#vid').trigger('pause');
                        jQuery('#vid-big').trigger('pause');
                        jQuery('#vid-big').hide();
                    }

                    audio.src = tracks[id].filename;

                },
                getDuration = function (src) {
                    var audio = new Audio();
                    jQuery(audio).on("loadedmetadata", function () {
                        return audio.duration;
                    });
                    audio.src = src;
                },
                rebuildPlaylist = function (thisTrack, code) {
                    console.log(code);
                    var rebuildHtml = '';
                    jQuery(thisTrack).each(function (key, value) {
                        var trackNumber = value.track,
                            trackName = value.name,
                            trackDuration = '',
                            trackCode = value.code,
                            trackImage = value.image,
                            trackUrl = value.url,
                            trackDownload = value.download,
                            twishlist = value.wishlist;
                        let active = '';
                        if (code == trackCode) {
                            active = 'plSel';
                        } else {
                            active = '';
                        }
                        rebuildHtml += `
                            <li data-code="${trackCode}" class="nav-item-li ${active}">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${trackCode}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${trackCode}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImage}" alt="${trackName}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackName}</span>
                                <span class="plLength">${trackDuration}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${trackCode}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">

                                <div data-url="${trackUrl}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${trackDownload}" class="dropdown-item"><a href="${trackDownload}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrl}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>

                            </div>
                        </li>
                            `;
                    });
                    jQuery('body').find('#plList').html(rebuildHtml);
                },
                buildListSearch = function (tracks) {
                    var rebuildHtml = '';
                    // console.log(tracks);
                    jQuery(tracks).each(function (key, value) {
                        var trackNumber = value.track,
                            trackName = value.name,
                            trackDuration = '',
                            trackCode = value.code,
                            trackImage = value.image,
                            trackUrl = value.url,
                            trackDownload = value.download,
                            twishlist = value.wishlist;
                        rebuildHtml += `
                            <li data-code="${trackCode}" class="nav-item-li">
                            <label class="z-checkbox appear">
                                <input type="checkbox" value="${trackCode}" name="z-checkbox" class="check-item hide">
                                <span class="z-checkmark"></span>
                            </label>
                            <div data-code="${trackCode}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImage}" alt="${trackName}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackName}</span>
                                <span class="plLength">${trackDuration}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${trackCode}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">
                                <div data-url="${trackUrl}" class="dropdown-item z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div data=url="${trackDownload}" class="dropdown-item"><a href="${trackDownload}" role="button" class="action z-download menu-link" title="Tải xuống"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrl}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>

                            </div>
                        </li>
                            `;
                    });
                    jQuery('body').find('#plListSearch').html(rebuildHtml);
                },
                rebuildPlaylistSuggest = function (thisTrack) {
                    var rebuildHtml = '';
                    jQuery(thisTrack).each(function (key, value) {
                        var trackNumber = value.track,
                            trackNameSuggest = value.name,
                            trackDurationSuggest = '',
                            trackCodeSuggest = value.code,
                            trackUrlSuggest = value.url,
                            trackDownloadSuggest = value.download,
                            trackImageSuggest = value.image,
                            twishlist = value.wishlist;
                        rebuildHtml += `
                        <li data-code="${trackCodeSuggest}" class="nav-item-li">
                            <div data-code="${trackCodeSuggest}" class="nav-item plItem">
                                <span class="ava-song">
                                    <img src="${trackImageSuggest}" alt="${trackNameSuggest}">
                                    <i class="icon"></i>
                                    <span class="opa-z"></span>
                                </span>
                                <span class="plTitle">${trackNameSuggest}</span>
                                <span class="plLength">${trackDurationSuggest}</span>
                            </div>
                            <div class="hover-view-z">
                                <a title="Phát MV" href="#"><i class="fa fa-film fa-hide" aria-hidden="true"></i></a>
                                <a data-code="${trackCodeSuggest}" class="z-like-btn liked" title="Thích"><i class="fa ${twishlist == true ? 'fa-heart': 'fa-heart-o'}" aria-hidden="true"></i></a>
                                <a class="action" title="Khác"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            </div>
                            <div class="z-dropdown">

                                <div data-url="${trackUrlSuggest}" class="dropdown-item  z-share z-b-share"  data-toggle="modal" data-target="#shareModal"><a role="button" class="menu-link"><i class="fa fa-share-square-o" aria-hidden="true"></i>Chia sẻ</a></div>
                                <div class="dropdown-item"><a role="button" class="action menu-link" title="Tải xuống" href="${trackDownloadSuggest}"><i class="fa fa-download" aria-hidden="true"></i>Tải xuống</a></div>
                                <div   style="display:none" class="dropdown-item"><a role="button" class="z-btn-reply z-btn-item menu-link"><i class="fa fa-ban" aria-hidden="true"></i>Chặn</a></div>
                                <div class="dropdown-item"><a role="button" class="z-btn-item menu-link" href="${trackUrlSuggest}"><i class="fa fa-info-circle" aria-hidden="true"></i>Thông tin</a></div>
                                <div  style="display:none" class="dropdown-item"><a role="button" class="z-btn-item menu-link"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a></div>

                            </div>
                        </li>
                            `;
                    });
                    jQuery('body').find('#plListS').html(rebuildHtml);
                },
                setDur = function (d) {
                    duration = d;
                },

                playTrack = function (id) {
                    loadTrack(id);
                    if (checkvideo == false) audio.play();
                };
            extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ?
                '.ogg' : '';
            loadTrack(index);

        } else {
            // boo hoo
            jQuery('.column').addClass('hidden');
            var noSupport = jQuery('#audio1').text();
            jQuery('.container').append('<p class="no-support">' + noSupport + '</p>');
        }
        jQuery(document).ready(function () {
            let title = tracks[index].name;
            jQuery('.z-song-name a').text(title);

            let singer = tracks[index].singer;
            jQuery('.z-artists a').text(singer);

            let album = tracks[index].album;
            jQuery('.z-album a').text(album);
            let image = tracks[index].image;
            if (image != 'null') {

                jQuery('#mini__player > img').attr('src', image);
            } else {

                jQuery('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`);
            }

            var number_song = jQuery('.z-item-total').html();
            var numbersg = jQuery('.z-checkbox-title').html();

            numbersg = numbersg.replace('1', tracks.length);
            number_song = number_song.replace('1', tracks.length);

            jQuery('.z-checkbox-title').html(numbersg);
            jQuery('.z-item-total').html(number_song);
            /* ===== extend js ===== */
            var countChecked = 0;

            jQuery('body').on('click', '.check-all', function () {
                if (jQuery(this).prop('checked')) {
                    let html = "ĐÃ CHỌN (" + tracks.length + "/" + tracks.length + ")";
                    jQuery(this).parent().find('.z-checkbox-title').html(html);
                    jQuery(this).parents('.z-checkbox').addClass('appear-all');
                    jQuery(this).parents('.left-list').find('.z-body-area .nav-item-li .z-checkbox .check-item').prop('checked', true);
                    jQuery(this).parents('.left-list').find('.z-body-area .nav-item-li .z-checkbox').addClass('has-checked');
                    if (jQuery(this).parents('.left-list').find('.hover-view .z-delete').hasClass('hide')) {
                        jQuery(this).parents('.left-list').find('.hover-view .z-delete').removeClass('hide');
                    }
                } else {
                    let html = "DANH SÁCH PHÁT (" + tracks.length + ")";
                    jQuery(this).parent().find(' .z-checkbox-title').html(html);
                    jQuery(this).parents('.z-checkbox').removeClass('appear-all');
                    jQuery(this).parents('.left-list').find('.z-body-area .nav-item-li .z-checkbox .check-item').prop('checked', false);
                    jQuery(this).parents('.left-list').find('.z-body-area .nav-item-li .z-checkbox').removeClass('has-checked');
                    if (!jQuery(this).parents('.left-list').find('.hover-view .z-delete').hasClass('hide')) {
                        jQuery(this).parents('.left-list').find('.hover-view .z-delete').addClass('hide');
                    }
                }
            })
            jQuery('body').on('click', '.check-item', function () {
                if (jQuery(this).prop('checked')) {
                    countChecked = countChecked + 1;
                    jQuery(this).parents('.z-checkbox').addClass('has-checked');
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox ').addClass('appear-all');
                    if (jQuery(this).parents('.left-list').find('.hover-view .z-delete').hasClass('hide')) {
                        jQuery(this).parents('.left-list').find('.hover-view .z-delete').removeClass('hide');
                    }
                } else {
                    countChecked = countChecked - 1;
                    jQuery(this).parents('.z-checkbox').removeClass('has-checked');
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox .check-all').prop('checked', false);
                }
                if (countChecked > 0 && countChecked < tracks.length) {
                    let html = "ĐÃ CHỌN (" + countChecked + "/" + tracks.length + ")";
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox-title').html(html);
                    // jQuery(this).parents('.left-list').find('.z-head .z-checkbox').removeClass('appear-all');
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox .check-all').prop('checked', false);
                }
                if (countChecked == 0) {
                    let html = "DANH SÁCH PHÁT (" + tracks.length + ")";
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox-title').html(html);
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox').removeClass('appear-all');
                    if (!jQuery(this).parents('.left-list').find('.hover-view .z-delete').hasClass('hide')) {
                        jQuery(this).parents('.left-list').find('.hover-view .z-delete').addClass('hide');
                    }
                }
                if (countChecked == tracks.length) {
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox').addClass('has-checkall');
                    jQuery(this).parents('.left-list').find('.z-head .z-checkbox .check-all').prop('checked', true);
                }
            })

            // jQuery('.z-delete').click(function() {
            //     jQuery(this).parents('.left-list').find('input[type=checkbox]').prop('checked', false);
            //     let html = "DANH SÁCH PHÁT (" + tracks.length + ")";
            //     jQuery(this).parents('.left-list').find('.z-head .z-checkbox-title').html(html);
            //     jQuery(this).parents('.left-list').find('.z-head .z-checkbox').removeClass('appear-all');
            //     jQuery(this).addClass('hide');
            // })

        });
    });
}

