<?php
namespace App\Services;
use App\Models\SocialAccount;
use App\Models\Customer;

use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{
    public static function createOrGetUser(ProviderUser $providerUser, $social)
    {

        $account = SocialAccount::where(['provider'=> $social,'provider_customer_id' => $providerUser->getId()])
        ->first();
        if ($account) {
            $customer = $account->load('customer');

            if ($customer->customer->status == 0) {
                $a = false;
                return $a;
            }else{
                return $customer->customer;
            }
        } else {
            $email = $providerUser->getEmail() ?? $providerUser->getNickname();
            $account = new SocialAccount([
                'provider_customer_id' => $providerUser->getId(),
                'provider' => $social,
            ]);
            $customer = Customer::whereEmail($email)->first();

            if (!$customer) {
                $email_arr = explode('@',$email);
                $customer = Customer::create([
                    'username' => $email_arr[0],
                    'email' => $email,
                    'name' => $providerUser->getName(),
                    'password' => bcrypt($email),
                    'avatar' => $providerUser->getAvatar(),
                    'status' => 1,
                ]);
            }

            $account->customer()->associate($customer);
            $account->save();

            return $customer;
        }
    }
}
