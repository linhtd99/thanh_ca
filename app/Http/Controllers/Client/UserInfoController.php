<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Artist_song;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Playlist;
use App\Models\Song;
use App\Models\Song_playlist;
use App\Models\User_history;
use App\Models\Video;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;
use Validator;

class UserInfoController extends Controller
{

    public function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        // $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        // $str = preg_replace('/([\s]+)/', '', $str);

        return $str;
    }

    public function index($username)
    {
        $user = Customer::where('username', $username)->first();

        $playlists = Playlist::where('customer_id', $user->id)->limit(3)->orderBy('created_at', 'desc')->get();

        $countPlaylist = Playlist::where('customer_id', $user->id)->count();

        return view('client.user-manager', ['user' => $user, 'playlists' => $playlists, 'countPlaylist' => $countPlaylist]);
    }

    public function listPlayList($username)
    {
        $user = Customer::where('username', $username)->first();

        $playlists = Playlist::where('customer_id', $user->id)->with('songs')->orderBy('created_at', 'desc')->paginate(10);

        return view('client.playlist-user', ['playlists' => $playlists]);
    }
    public function postEditUser(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'myfile' => 'image|mimes:jpeg,png,jpg,svg',
            ],
            [
                'myfile.image' => 'Avatar chỉ chấp nhận ảnh và thuộc các định dạng jpeg,png,jpg,svg',
                'myfile.mimes' => 'Avatar chỉ chấp nhận ảnh và thuộc các định dạng jpeg,png,jpg,svg',

            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token', 'id', 'day', 'month', 'year', 'myfile');
            $birthday = date_create($request->year . '-' . $request->month . '-' . $request->day);

            if (!empty($request->myfile)) {
                $imageName = time() . '.' . $request->myfile->getClientOriginalExtension();
                $request->myfile->move(public_path('client/imgCustomer'), $imageName);

                $data['avatar'] = asset('client/imgCustomer') . '/' . $imageName;
            }

            $data['birthday'] = $birthday;
            $data['updated_at'] = date('Y-m-d H:i:s', time());

            $result = Customer::where('id', $request->id)->update($data);
            if ($result == 1) {
                return response(
                    [
                        'errors' => false,
                        'data' => 'success',
                    ]
                );
            } else {
                return response(
                    [
                        'errors' => true,
                        'data' => 'Có lỗi xảy ra',
                    ]
                );
            }

        }
    }
    public function postEditPwUser(Request $request)
    {
        $customer = Customer::where('id', $request->id)->get();
        // dd($request->all());
        $validate = Validator::make(
            $request->all(),
            [
                'old_password' => [
                    'required',
                    function ($attribute, $value, $fail) use ($customer) {
                        if (!Hash::check($value, $customer[0]->password)) {
                            return $fail('Mật khẩu cũ không chính xác');
                        }
                    },
                ],
                'new_password' => 'required|min:6',
                'confirm_password' => 'required|same:new_password',
            ],
            [
                'required' => 'Không được để trống',
                'new_password.min' => 'Mật khẩu chứa ít nhất 6 kí tự',
                'confirm_password.same' => 'Mật khẩu xác nhận chưa trùng với mật khẩu',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $result = Customer::where('id', $request->id)->update(['password' => bcrypt($request->new_password)]);
            if ($result == 1) {
                return response(
                    [
                        'errors' => false,
                        'data' => 'success',
                    ]
                );
            } else {
                return response(
                    [
                        'errors' => true,
                        'data' => 'fail',
                    ]
                );
            }

        };
    }

    // Profile
    public function proFile($username)
    {
        $user = Customer::where('username', $username)->first();
        Customer::where('username', $username)->update(['views' => $user->views + 1]);
        $playlists = Playlist::where('customer_id', $user->id)->with('songs')->orderBy('created_at', 'desc')->paginate(10);
        $playlistRandom = Playlist::where('customer_id', $user->id)->orderByRaw("RAND()")->limit(5)->get();
        return view('client.profile.user', ['user' => $user, 'playlists' => $playlists, 'playlistRandom' => $playlistRandom]);
    }
    public function playListProfile($username)
    {
        $user = Customer::where('username', $username)->first();
        Customer::where('username', $username)->update(['views' => $user->views + 1]);
        $playlists = Playlist::where('customer_id', $user->id)->with('songs')->orderBy('created_at', 'desc')->paginate(10);

        $playlistRandom = Playlist::where('customer_id', $user->id)->orderByRaw("RAND()")->limit(5)->get();

        $songRandom = Song::orderByRaw("RAND()")->limit(3)->with('artists')->get();
        return view('client.profile.playlist', ['user' => $user, 'playlists' => $playlists, 'playlistRandom' => $playlistRandom, 'songRandom' => $songRandom]);
    }

    public function upload($username)
    {
        $user = Customer::where('username', $username)->first();
        $categories = Category::all();
        $c = $this->categoriesRecursive($categories);
        $songs = Song::where('author_id', $user->id)->with('artists')->orderBy('created_at', 'desc')->paginate(10);
        $video = Video::with('artists')->orderBy('created_at', 'desc')->paginate(10);

        return view('client.profile.upload', ['user' => $user, 'categories' => $c, 'songs' => $songs], compact('video'));
    }

    // lich su xem
    public function history($username)
    {
        $username = Customer::where('username', $username)->first();
        // Xóa tất cả bản ghi trừ 10 bản ghi đầu từ các lịch sử song,video,playlist
        DB::select(DB::raw("delete t
            FROM user_history t JOIN
            (SELECT tt.created_at,tt.code
            FROM user_history tt
            WHERE tt.provider = 'song'
            AND tt.customer_id = $username->id
            ORDER BY tt.created_at DESC
            LIMIT 50
            OFFSET 10
         ) tt
         ON t.code = tt.code"));

        DB::select(DB::raw("delete t
            FROM user_history t JOIN
            (SELECT tt.created_at,tt.code
            FROM user_history tt
            WHERE tt.provider = 'video'
            AND tt.customer_id = $username->id
            ORDER BY tt.created_at DESC
            LIMIT 50
            OFFSET 10
         ) tt
         ON t.code = tt.code"));

        DB::select(DB::raw("delete t
            FROM user_history t JOIN
            (SELECT tt.created_at,tt.code
            FROM user_history tt
            WHERE tt.provider = 'playlist'
            AND tt.customer_id = $username->id
            ORDER BY tt.created_at DESC
            LIMIT 50
            OFFSET 10
         ) tt
         ON t.code = tt.code"));

        // end

        $songs = Song::select('songs.*', 'user_history.id as history_id')
            ->join('user_history', 'songs.code', '=', 'user_history.code')
            ->where('user_history.customer_id', $username->id)
            ->with('artists')
            ->orderBy('user_history.created_at', 'desc')
            ->limit(10)
            ->get();
        $playlists = User_history::select('playlists.*', 'user_history.id as history_id')
            ->join('playlists', 'playlists.code', '=', 'user_history.code')
            ->where('user_history.customer_id', $username->id)
            ->orderBy('user_history.created_at', 'desc')
            ->limit(10)
            ->get();
        $videos = Video::select('videos.*', 'user_history.id as history_id')
            ->join('user_history', 'videos.code', '=', 'user_history.code')
            ->where('user_history.customer_id', $username->id)
            ->orderBy('user_history.created_at', 'desc')
            ->limit(10)
            ->get();
        // dd($songs);
        return view('client.user-history', compact('songs', 'playlists', 'videos'));
    }
    public function deleteHistory($username, $id)
    {
        $username = Customer::where('username', $username)->first();
        if (!empty($username)) {
            $check = User_history::where('id', $id)->delete();
            return redirect()->back();
        } else {
            abort(404);
        }
    }

    public function saveUploadSong(Request $request)
    {
        if ($request->hasFile('fileUpload')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'name_song' => 'required',
                ],
                [
                    'name_song.required' => 'Tên bài hát không được để trống',
                ]
            );
            if ($validate->fails()) {
                return response(
                    [
                        'errors' => true,
                        'data' => $validate->errors(),
                    ]
                );
            } else {
                $file = $request->fileUpload;
                // dd($file);
                $data = $request->except('_token', 'fileUpload', 'artist_id', 'name_song');
                if ($request->category_id == 0) {
                    $data['category_id'] = null;
                };
                $data['code'] = getToken(12);
                $data['slug'] = str_replace(' ', '-', $this->to_slug($request->name_song) . '-' . uniqid());
                $data['name'] = $request->name_song;
                $data['status'] = '2';

                $name = str_replace(' ', '', $this->to_slug($request->name_song) . '-' . uniqid());
                $fileName = $name . '.' . $request->fileUpload->getClientOriginalExtension();

                Storage::disk('public')->putFileAs('/uploads/files/', $file, $fileName);

                $data['filename'] = $fileName;
                $data['created_at'] = date('Y-m-d H:i:s', time());
                $result = Song::create($data);

                if (!empty($request->artist_id)) {
                    foreach ($request->artist_id as $key => $value) {
                        Artist_song::insert(['artist_id' => $value, 'song_id' => $result->id]);
                    };
                }
                return response(
                    [
                        'errors' => false,
                        'data' => 'success',
                    ]
                );
            }
        } else {
            return response(
                [
                    'errors' => true,
                    'data' => 'fail',
                ]
            );
        };
    }

    public function saveUploadvideo(Request $request)
    {
        if ($request->hasFile('fileUploadvideo')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'name_video' => 'required',
                ],
                [
                    'name_video.required' => 'Tên bài hát không được để trống',
                ]
            );
            if ($validate->fails()) {
                return response(
                    [
                        'errors' => true,
                        'data' => $validate->errors(),
                    ]
                );
            } else {
                $file = $request->fileUploadvideo;
                // dd($file);
                $data = $request->except('_token', 'fileUploadvideo', 'artist_id', 'name_video');
                if ($request->category_id_video == 0) {
                    $data['category_id'] = null;
                } else {
                    $data['category_id'] = $request->category_id_video;
                };
                $data['code'] = getToken(12);
                $data['name'] = $request->name_video;
                $data['status'] = '2';
                $data['karaoke_type'] = $request->input('beat');
                $data['beat_type'] = $request->input('karaoke');
                $name = str_replace(' ', '', $this->to_slug($request->name_video) . '-' . uniqid());
                $fileName = $name . '.' . $request->fileUploadvideo->getClientOriginalExtension();

                Storage::disk('public')->putFileAs('uploads/videos/', $file, $fileName);
                $data['link'] = $fileName;

                $data['created_at'] = date('Y-m-d H:i:s', time());
                $result = Video::create($data);

                if (!empty($request->artist_id_video)) {
                    foreach ($request->artist_id_video as $key => $value) {
                        Artist_song::insert(['artist_id' => $value, 'video_id' => $result->id]);
                    };
                }
                return response(
                    [
                        'errors' => false,
                        'data' => 'success',
                    ]
                );
            }
        } else {
            return response(
                [
                    'errors' => true,
                    'data' => 'fail',
                ]
            );
        };
    }

    public function playList($username, $playlist = null)
    {
        $user = Customer::where('username', $username)->get();
        $categories = Category::all();
        $c = $this->categoriesRecursive($categories);
        if ($playlist == null) {
            return view('client.cap-nhap-playlist', ['user' => $user[0], 'categories' => $c]);
        } else {
            $playlist = Playlist::where('slug', $playlist)->with('songs')->first();

            // thêm relation artists vào từng bài nhạc

            foreach ($playlist->songs as $key => $value) {
                $playlist->songs[$key]['artists'] = Song::find($value->id)->load('artists')->artists;
                // dd(Song::find($value->id)->load('artists'));
            }
            // dd($playlist->songs);
            return view('client.cap-nhap-playlist', ['user' => $user[0], 'categories' => $c, 'playlist' => $playlist]);
        }

    }

    // đệ quy category
    public function categoriesRecursive($categories, $parent_id = 0, $step = 0)
    {
        foreach ($categories as $key => $item) {
            if ((int) $item['parent_id'] == $parent_id) {
                $c = $item;
                $category = Category::find($item['parent_id']);
                $c['step'] = $step;
                $c['parent_name'] = $category['name'];
                $this->_data['categories'][] = $c;
                unset($categories[$key]);
                $this->categoriesRecursive($categories, (int) $item['id'], $step + 1);
            }
        }
        return $this->_data['categories'];
    }

    public function searchSongAddPlaylist(Request $request)
    {
        if ($request->keysearch == null) {
            $data = Song::where('status', '=', '1')->limit(20)->get()->load('artists');
        } else {
            $data = Song::where([['name', 'LIKE', '%' . $request->keysearch . '%'], ['status', '=', '1']])->limit(20)->get()->load('artists');
        }
        return response()->json($data, 200);
    }

    public function searchSongById(Request $request)
    {
        // dd($request->all());
        $data = Song::find($request->id)->load('artists');
        return response()->json($data, 200);
    }

    public function savePlaylist(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'arr_listSong' => 'required',
            ],
            [
                'name.required' => 'Tên playlist không được để trống',
                'arr_listSong.required' => 'Playlist phải có ít nhất một bài hát.',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token', 'arr_listSong', 'playlist_img');
            $data['code'] = getToken(12);
            $data['slug'] = 'playlist-' . uniqid();
            if (!empty($request->playlist_img)) {
                $imageName = time() . '.' . $request->playlist_img->getClientOriginalExtension();
                $request->playlist_img->move(public_path('client/imgCustomer'), $imageName);

                $data['image'] = asset('client/imgCustomer') . '/' . $imageName;
            }

            $arr_listSong = explode(',', $request->arr_listSong);
            $result = Playlist::create($data);
            // dd($result);
            foreach ($arr_listSong as $value) {
                Song_playlist::insert([
                    'playlist_id' => $result->id,
                    'song_id' => $value,
                ]);
            };
            return response(
                [
                    'errors' => false,
                    'data' => $validate->errors(),
                ]
            );
        }

    }

    public function saveEditPlaylist(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'arr_listSong' => 'required',
            ],
            [
                'name.required' => 'Tên playlist không được để trống',
                'arr_listSong.required' => 'Playlist phải có ít nhất một bài hát.',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {

            $data = $request->except('_token', 'arr_listSong', 'id', 'playlist_img');

            if (!empty($request->playlist_img)) {
                $imageName = time() . '.' . $request->playlist_img->getClientOriginalExtension();
                $request->playlist_img->move(public_path('client/imgCustomer'), $imageName);

                $data['image'] = asset('client/imgCustomer') . '/' . $imageName;
            }

            $arr_listSong = explode(',', $request->arr_listSong);
            $result = Playlist::where('id', $request->id)->update($data);
            // dd($result);
            Song_playlist::where('playlist_id', $request->id)->delete();
            foreach ($arr_listSong as $value) {
                Song_playlist::insert([
                    'playlist_id' => $request->id,
                    'song_id' => $value,
                ]);
            };
            return response(
                [
                    'errors' => false,
                    'data' => $validate->errors(),
                ]
            );

        }
    }
    public function deletePlaylist($username, $id)
    {
        $playlist = Playlist::find($id);
        Song_playlist::where('playlist_id', $id)->delete();
        $check = $playlist->delete();
        if ($check == 1) {
            return redirect('user/' . $username . '/playlist')->with('success', 'Xoá playlist thành công');
        } else {
            return redirect('user/' . $username . '/playlist')->with('error', 'Xoá playlist không thành công');
        }
    }

    public function deleteMultiPlaylist($username, Request $request)
    {
        foreach (json_decode($request->arr_id) as $key => $value) {
            Song_playlist::where('playlist_id', $value);
            $playlist = Playlist::find($value);
            $check = $playlist->delete();
        };
        return response(
            [
                'errors' => true,

            ]
        );
    }

    public function deleteAllHistory($username, Request $request)
    {
        foreach (json_decode($request->arr_id) as $key => $value) {
            User_history::where('id', $value)->delete();
        };
        return response(
            [
                'errors' => true,

            ]
        );
    }
    public function setCookiePlaylist(Request $request)
    {
        // dd($_COOKIE);
        $reponse = setcookie($request->username, $request->playlist, time() + (86400 * 10));
        return Response::json($reponse);
    }

}
