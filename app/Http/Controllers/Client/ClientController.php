<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Comment;
use App\Models\Customer;
use App\Models\Like_comment;
use App\Models\Page;
use App\Models\Playlist;
use App\Models\Song;
use App\Models\User_history;
use App\Models\Video;
use App\Models\Wishlist;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use View;

class ClientController extends Controller
{
    public function __construct()
    {

        $songRandom5 = Song::where('status', '1')->inRandomOrder()->with('artists', 'categories')->limit(5)->get();
        $listSongRandom5 = $this->getTrack($songRandom5);
        $playList4 = Playlist::where('customer_id', '=', '0')->inRandomOrder()->limit(4)->get();
        View::share(compact('songRandom5', 'playList4', 'listSongRandom5'));
    }

    public function index()
    {
        $playlistsNew = Playlist::orderBy('created_at', 'desc')->limit(10)->get();
        $videosHot = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
            ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
            ->groupBy('videos.code')
            ->with('artists')
            ->orderBy(DB::raw('listen'), 'desc')
            ->limit(10)->get();
        $songs = Song::select(DB::raw('songs.*,COUNT(songs_listens.code) as listen'))
            ->leftJoin('songs_listens', 'songs_listens.code', '=', 'songs.code')
            ->where('songs.status', '=', '1')
            ->groupBy('songs.code')
            ->with('artists')
            ->orderBy('songs.created_at', 'desc')
            ->limit(12)->get();
        $karaoke = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
            ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
            ->where('karaoke_type', 1)
            ->groupBy('videos.code')
            ->with('artists')
            ->orderBy('videos.created_at', 'desc')
            ->limit(8)->get();

        $playlist_topic = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
            ->leftJoin('playlists_listens', 'playlists_listens.code', 'playlists.code')
            ->where('customer_id', '0')
            ->groupBy('playlists.code')
            ->limit(4)
            ->get();

        return view('client.index', compact('playlistsNew', 'videosHot', 'songs', 'karaoke', 'playlist_topic'));
    }

    public function history_song_player($id)
    {
        $songs = Song::select(DB::raw('songs.*'), DB::raw('COUNT(songs_listens.code) AS listen'))
            ->join('user_history', 'user_history.code', '=', 'songs.code')
            ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where(['user_history.provider' => 'song', 'user_history.customer_id' => $id])
            ->groupBy('songs.code')
            ->orderBy('user_history.created_at', 'desc')
            ->with('artists', 'lyric')
            ->get();
        $json_listSong = $this->getTrack($songs);
        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();

        return view('client.trinh-nghe-nhac-lich-su', ['json_listSong' => $json_listSong, 'mv' => $mv]);

    }
    public function playlistPlayer($name = null, $code)
    {

        $playlist = Playlist::where('code', $code)->with('songs')->first();

        foreach ($playlist->songs as $key => $value) {
            $playlist->songs[$key]['artists'] = Song::find($value->id)->load('artists')->artists;
            $playlist->songs[$key]['lyric'] = Song::find($value->id)->load('lyric')->lyric;
            $playlist->songs[$key]['video'] = Video::find($value->video_id);
        }
        $playlist->customer_id = Song::getNameAuthor($playlist->customer_id);

        $json_listSong = $this->getTrack($playlist->songs);

        $newPlaylist = $playlist;
        foreach ($newPlaylist->songs as $key => $value) {
            unset($newPlaylist->songs[$key]['lyric']);
        };

        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();

        // coment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $newPlaylist->code, 'status' => 1])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $newPlaylist->code, 'status' => 1])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where(['code' => $newPlaylist->code, 'status' => 1])->groupBy('comments.code')->first();

        foreach ($comments as $key => $value) {
            Carbon::setLocale('vi');
            $datetime = new Carbon($value->created_at);
            $timeNow = Carbon::now();
            $comments[$key]['date'] = $datetime->diffForHumans($timeNow);
        }
        // end comment
        return view('client.trinh-nghe-nhac', ['json_listSong' => $json_listSong, 'playlist' => $newPlaylist, 'mv' => $mv, 'comments' => $comments, 'totalComment' => !empty($totalComment) ? $totalComment->total_comment : 0]);
    }

    public function albumPlayer($name = null, $code)
    {
        $album = Album::where('code', $code)->first();

        $songs = Song::where(['album_id' => $album->id, 'status' => '1'])->with('artists', 'lyric')->get();
        foreach ($songs as $key => $value) {
            $songs[$key]['video'] = Video::find($value->video_id);
        }
        // dd($album);
        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();
        $json_listSong = $this->getTrack($songs);

        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();

        // coment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $album->code, 'status' => 1])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $album->code, 'status' => 1])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where(['code' => $album->code, 'status' => 1])->groupBy('comments.code')->first();

        foreach ($comments as $key => $value) {
            Carbon::setLocale('vi');
            $datetime = new Carbon($value->created_at);
            $timeNow = Carbon::now();
            $comments[$key]['date'] = $datetime->diffForHumans($timeNow);
        }
        // end comment

        return view('client.trinh-nghe-nhac', ['json_listSong' => $json_listSong, 'playlist' => $album, 'mv' => $mv, 'comments' => $comments, 'totalComment' => !empty($totalComment) ? $totalComment->total_comment : 0]);
    }

    public function songPlayer($name = null, $code)
    {
        // dd($code);
        $song = Song::where('code', $code)->where('status', '=', '1')->with('artists', 'lyric')->first();
        $json_listSong = $this->getTrackSong($song);
        $mvArtist = [];
        $mv = [];

        if (!empty($song->artists) && count($song->artists) > 0) {
            foreach ($song->artists as $key => $artist) {
                $mvArtist = $artist->with('videos')->get();
            };
            foreach ($mvArtist as $value) {
                foreach ($value->videos as $key => $value1) {
                    // dd($value1);
                    $mv[] = $value1->load('artists')->toArray();
                }
            }
        }
        $mv = array_unique($mv, SORT_REGULAR);

        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $song->code, 'status' => 1])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $song->code, 'status' => 1])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where(['code' => $song->code, 'status' => 1])->groupBy('comments.code')->first();

        foreach ($comments as $key => $value) {
            Carbon::setLocale('vi');
            $datetime = new Carbon($value->created_at);
            $timeNow = Carbon::now();
            $comments[$key]['date'] = $datetime->diffForHumans($timeNow);
        }

        return view('client.trinh-nghe-nhac-bai-hat', ['json_listSong' => $json_listSong, 'song' => $song, 'mv' => $mv, 'comments' => $comments, 'totalComment' => !empty($totalComment) ? $totalComment->total_comment : 0]);
    }

    public function more_comment(Request $request)
    {
        if ($request->s == 'normal') {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where([
                    ['parent_id', '=', '0'],
                    ['code', '=', $request->code],
                    ['status', '=', 1],
                ])->skip($request->commentCurrent)->take(10)->with('customer')->orderBy('created_at', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where([
                    ['parent_id', '=', '0'],
                    ['code', '=', $request->code],
                    ['status', '=', 1],
                ])->skip($request->commentCurrent)->take(10)->with('customer')->orderBy('total_like', 'desc')->get();
        }

        foreach ($comments as $key => $value) {
            Carbon::setLocale('vi');
            $datetime = new Carbon($value->created_at);
            $timeNow = Carbon::now();
            $comments[$key]['date'] = $datetime->diffForHumans($timeNow);
            $comments[$key]['child_comment'] = $value->getChildComment();
        }

        return response($comments);
    }

    // dung cho bai hat
    public function getTrackSong($song)
    {
        $new_arr = [];
        $artist_id = '';
        if (Auth::guard('customers')->check()) {
            $customer_id = Auth::guard('customers')->user()->id;
        }

        if (!empty($song)) {
            foreach ($song->artists as $artist) {
                $artist_id .= $artist->id;
            }
            $new_arr[0] = [
                "track" => 1,
                "name" => $song['name'],
                'image' => $song['image'],
                "singer" => $song['song'],
                'code' => $song['code'],
                'download' => route('download', ['file' => $song['filename'], 'code' => $song['code']]),
                'url' => route('song', ['name' => name_to_slug($song['name']), 'code' => $song['code']]),
                "filename" => Storage::disk('public')->url('/uploads/files/' . $song->filename),
                // "video" => ($song->video != null ? $song->video->link : null),
                "lyrics" => ($song->lyric_karaoke != null ? json_decode($song->lyric_karaoke, true) : ''),
            ];
            if (!empty($customer_id)) {
                $new_arr[0]['wishlist'] = Wishlist::where('customer_id', $customer_id)->where('song_code', $song['code'])->exists();
            } else {
                $new_arr[0]['wishlist'] = 'login';
            }

        }

        // die;
        return $new_arr;
    }
    // dung cho playlist,album
    public function getTrack($song)
    {
        if (Auth::guard('customers')->check()) {
            $customer_id = Auth::guard('customers')->user()->id;
        }
        $new_arr = [];
        $artist_id = '';
        foreach ($song as $key => $value) {

            foreach ($value->artists as $artist) {
                $artist_id .= $artist->id;
            };
            $new_arr[$key] = [
                "track" => $key + 1,
                "name" => $value['name'],
                "code" => $value['code'],
                'image' => $value['image'],
                "singer" => $value['song'],
                'download' => route('download', ['file' => $value['filename'], 'code' => $value['code']]),
                "filename" => Storage::disk('public')->url('/uploads/files/' . $value->filename),
                'url' => route('song', ['name' => name_to_slug($value['name']), 'code' => $value['code']]),
                "video" => ($value->video != null ? $value->video->link : null),
                "lyrics" => ($value->lyric_karaoke != null ? json_decode($value->lyric_karaoke, true) : ''),
            ];
            if (!empty($customer_id)) {
                $new_arr[$key]['wishlist'] = Wishlist::where('customer_id', $customer_id)->where('song_code', $value['code'])->exists();
            } else {
                $new_arr[$key]['wishlist'] = 'login';
            }
        }
        // die;
        return $new_arr;
    }

    public function addSongWishlist(Request $request)
    {
        $checkSongAlready = Wishlist::where([
            ['song_code', '=', $request->code],
            ['customer_id', '=', $request->customer_id],
        ])->first();

        if (!$checkSongAlready) {
            $result = Wishlist::insert([
                'song_code' => $request->code,
                'customer_id' => $request->customer_id,
            ]);

            if ($result == 1) {
                return response([
                    'err' => false,
                    'mes' => 'Thêm bài hát thành công',
                ]);
            } else {
                return response([
                    'err' => true,
                    'mes' => 'Thêm bài hát không thành công',
                ]);
            }
        } else {
            return response([
                'err' => true,
                'mes' => 'Bài hát đã tồn tại',
            ]);
        }
    }

    public function wishList($username)
    {
        $username = Customer::where('username', $username)->first();
        $songs = Song::select('songs.*')
            ->join('wishlists', 'songs.code', '=', 'wishlists.song_code')
            ->where('wishlists.customer_id', $username->id)
            ->with('artists', 'lyric')->get();
        // foreach ($songs as $key => $value) {
        //     $songs[$key]['video'] = Video::find($value->video_id);
        // }

        $json_listSong = $this->getTrack($songs);

        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();

        return view('client.trinh-nghe-nhac-wishlist', ['json_listSong' => $json_listSong, 'username' => $username, 'mv' => $mv]);
    }

    public function editwishList($username)
    {
        $username = Customer::where('username', $username)->first();
        $songs = Song::select('songs.*')
            ->join('wishlists', 'songs.code', '=', 'wishlists.song_code')
            ->where('wishlists.customer_id', $username->id)
            ->with('artists')->get();
        return view('client.cap-nhap-wishlist', ['songs' => $songs, 'username' => $username]);
    }

    public function saveEditWishList(Request $request)
    {
        Wishlist::where('customer_id', $request->customer_id)->delete();
        if ($request->arr_listSong != null) {
            $arr_listSong = explode(',', $request->arr_listSong);
            foreach ($arr_listSong as $key => $value) {
                Wishlist::insert([
                    'customer_id' => $request->customer_id,
                    'song_code' => $this->getCodeSong($value),
                ]);
            };
        }

        return response([
            'msg' => 'Chỉnh sửa danh sách yêu thích thành công',
        ]);
    }

    public function getCodeSong($id)
    {
        $song = Song::where('id', $id)->first();
        return $song->code;
    }

    public function saveComment(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'content' => 'required',
            ],
            [
                'content.required' => 'Nội dung bình luận không được để trống',
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->with('alert_comment', 'Nội dung bình luận không được để trống !!!');
        } else {
            $comment = new Comment();
            $comment->code = $request->code;
            $comment->parent_id = $request->parent_id;
            $comment->customer_id = (int) $request->customer_id;
            $comment->content = $request->content;
            $comment->status == $request->status;

            $comment->created_at = date('Y-m-d H:i:s', time());
            $comment->save();
            return redirect()->back()->with('alert_comment', 'Bình luận thành công. Chờ quản trị viên phê duyệt !!!');
        }
    }

    public function likeComment(Request $request)
    {
        $like = Like_comment::where(['customer_id' => $request->customer_id, 'comment_id' => $request->comment_id])->first();
        if (!$like) {
            Like_comment::insert([
                'customer_id' => (int) $request->customer_id,
                'comment_id' => (int) $request->comment_id,
                'type' => 1,
            ]);
        } else {
            Like_comment::where('id', $like->id)->delete();
        }
    }

    public function delete_comment($id)
    {
        Comment::where('id', $id)->orWhere('parent_id', $id)->delete();
        Like_comment::where('comment_id', $id)->delete();
        return redirect()->back();
    }

    // them vao lich su
    public function addToHistory(Request $request)
    {
        $check = User_history::where(['code' => $request->code, 'customer_id' => $request->customer_id, 'provider' => $request->provider])->first();
        if ($check) {
            User_history::where(['code' => $request->code, 'customer_id' => $request->customer_id, 'provider' => $request->provider])->delete();
        };
        User_history::insert([
            'code' => $request->code,
            'provider' => $request->provider,
            'customer_id' => $request->customer_id,
            'created_at' => date('Y-m-d H:i:s', time()),
        ]);
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if (!$page) {
            abort(404);
        }
        return view('client.page', ['page' => $page]);
    }

    public function download(Request $request)
    {
        Song::where('code', $request->code)->update(['count_downloads' => DB::raw('count_downloads+1')]);
        $fileName = $request->file;
        $file = Storage::disk('public2')->url('uploads/files/' . $request->file);
        $name = explode('-', $fileName);
        $ext = explode('.', $fileName);
        return Storage::disk('public')->download('/uploads/files/' . $request->file, $name[0] . '.' . $ext[count($ext) - 1]);
    }

    public function randomSongs(Request $request)
    {
        $listsongs = $request->listsongs;
        $arr = array_column($listsongs, 'code');
        $playList = Song::whereNotIn('code', $arr)
            ->where('status', '1')
            ->inRandomOrder()
            ->with('artists', 'categories')
            ->limit(10)
            ->get();
        $playList = $this->getTrack($playList);
        return response()->json($playList);
    }

    public function addTowishList(Request $request)
    {
        if (Auth::guard('customers')->check()) {
            $customer_id = Auth::guard('customers')->user()->id;
        } else {
            $data = [
                'message' => 'login',
            ];
            return response()->json($data);
        }
        $code = $request->code;

        $check = Wishlist::where('customer_id', $customer_id)->where('song_code', $code)->exists();
        if ($check) {
            $remove = Wishlist::where('customer_id', $customer_id)->where('song_code', $code)->delete();
            $data = [
                'message' => 'unliked',
            ];
            return response()->json($data);

        } else {
            $add = Wishlist::insert(
                [
                    'song_code' => $code,
                    'customer_id' => $customer_id,
                ]
            );
            $data = [
                'message' => 'liked',
            ];
            return response()->json($data);

        }
    }

    public function playerSearch(Request $request)
    {
        $listsongs = $request->listsongs;
        $arr = array_column($listsongs, 'code');
        $keyword = $request->keyword;
        $songs = Song::where('status', '1')
            ->whereNotIn('code', $arr)
            ->where('name', 'LIKE', "%" . trim($keyword) . "%")
            ->orderBy('created_at', 'desc')->limit(20)
            ->get();
        $songs = $this->getTrack($songs);
        return response()->json($songs);

    }

}
