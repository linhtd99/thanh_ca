<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Video;
use App\Models\Artist;
use App\Models\Option;
use App\Models\Playlist;
use App\Models\Category;
use View;

class PlaylistController extends Controller
{
    public function __construct()
    {
        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));


        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        }
        // dd(count($artistTrending));
        $artistRandom5 = Artist::limit(5)->get();
        $categories = Option::select('value')->where('key', '=', 'sort_category')->first();
        !empty($categories->value) ? $cate_value = json_decode($categories->value, true) : $cate_value = '';
        View::share(['artistTrending' => $artistTrending, 'artistRandom5' => $artistRandom5, 'categories' => $cate_value]);
    }

    public function index()
    {
        $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
            ->leftJoin('playlists_listens', 'playlists_listens.code', 'playlists.code')
            ->where('customer_id', '0')
            ->groupBy('playlists.code')
            ->orderBy(DB::raw('listen'), 'desc')
            ->paginate(30);
        // dd($playlists);
        return view('client.list-playlist', compact('playlists'));
    }

    public function indexPlaylistNew()
    {
        $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
            ->leftJoin('playlists_listens', 'playlists_listens.code', 'playlists.code')
            ->where('customer_id', '0')
            ->groupBy('playlists.code')
            ->orderBy('playlists.created_at', 'desc')
            ->paginate(30);
        // dd($playlists);
        return view('client.list-playlist', compact('playlists'));
    }

    public function playlistByCategory($slug)
    {
        $categoryCurrent = Category::where('slug', $slug)->first();
        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) AS listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where(['category_id' => $categoryCurrent['id'], 'customer_id' => '0'])
                    ->groupBy('playlists.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(30);
            } else {
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) AS listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->join('categories', 'categories.id', '=', 'playlists.category_id')
                    ->where('customer_id', '0')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('playlists.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(30);
            }
        } else {
            about(404);
        }
        //  dd($playlists);
        return view('client.list-playlist-by-category', compact('playlists', 'categoryCurrent'));
    }

    public function playlistByCategoryNew($slug)
    {
        $categoryCurrent = Category::where('slug', $slug)->first();
        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) AS listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where(['category_id' => $categoryCurrent['id'], 'customer_id' => '0'])
                    ->groupBy('playlists.code')
                    ->orderBy('playlists.created_at', 'desc')
                    ->paginate(30);
            } else {
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) AS listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->join('categories', 'categories.id', '=', 'playlists.category_id')
                    ->where('customer_id', '0')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('playlists.code')
                    ->orderBy('playlists.created_at', 'desc')
                    ->paginate(30);
            }
        } else {
            about(404);
        }
        return view('client.list-playlist-by-category', compact('playlists', 'categoryCurrent'));
    }
}
