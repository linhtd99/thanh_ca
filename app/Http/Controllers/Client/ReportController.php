<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;
class ReportController extends Controller
{
    public function saveReport(Request $request){
        $data = $request->except('full','_token', 'radio_group');

        if ($request->radio_group != '0') {
            $data['content'] = $request->radio_group;
        }else{
            $data['content'] = $request->full;
        };

        $data['ip'] = $request->ip();
        $data['created_at'] = date('Y-m-d H:i:s', time());

        $check = Report::insert($data);
        if ($check) {
            return response([
                'status' => true,
            ]);
        }else{
            return response([
                'status' => false,
            ]);
        }
    }
}
