<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Video;
use App\Models\Playlist;
use App\Models\Song;
use App\Models\Top_keyword_search;
use Auth;
use DB;
use View;
use App\Models\Artist;
use App\Models\Category;
use App\Models\User_history;
// use JustBetter\PaginationWithHavings\PaginationWithHavings;

class SearchController extends Controller
{
    // use PaginationWithHavings;
    public function __construct()
    {

        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));

        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        }

        // dd($artistTrending);
        $artistRandom5 = Artist::limit(5)->get();

        View::share(['artistTrending' => $artistTrending, 'artistRandom5' => $artistRandom5]);
    }

    public function index(Request $request){
        // dd(!empty($request->l));
        if (!empty($request->q)) {
            $keywordConvert = DB::select(DB::raw("SELECT name FROM songs WHERE name LIKE '%$request->q%' UNION SELECT name FROM videos WHERE name LIKE '$request->q%' LIMIT 1"));
            if (count($keywordConvert) > 0 && !empty($keywordConvert)) {
                Top_keyword_search::insert([
                'keyword' => $keywordConvert[0]->name,
                'created_at' => date('Y-m-d H:i:s', time()),
                ]);
            }


            if (Auth::guard('customers')->check()) {
                User_history::insert([
                    'provider' => 'search_history',
                    'code' => $request->q,
                    'customer_id' => Auth::guard('customers')->user()->id,
                    'created_at' => date('Y-m-d H:i:s', time())
                ]);
            }

            $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('artist_song', 'songs.id','=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id','=', 'artists.id')
                        ->where('songs.status','1')
                        ->groupBy('songs.id')
                        ->havingRaw('songs.name LIKE "%'.$request->q.'%"')
                        ->orHavingRaw('artists_name LIKE "%'.$request->q.'%"')
                        ->limit(5)
                        ->get();

            $playlists = Playlist::where('name','LIKE','%'.$request->q.'%')->orderBy('created_at','desc')->limit(20)->get();
            $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('video_artist', 'videos.id','=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id','=', 'artists.id')
                        ->where('videos.status','1')
                        ->groupBy('videos.id')
                        ->havingRaw('videos.name LIKE "%'.$request->q.'%"')
                        ->orHavingRaw('artists_name LIKE "%'.$request->q.'%"')
                        ->limit(10)
                        ->get();

            // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = 'GROUP_CONCAT(artists.name)';
                $fieldFillVideo = 'GROUP_CONCAT(artists.name)';
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = 'songs.name';
                $fieldFillVideo = 'videos.name';
            }

            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if(!empty($request->s) && $request->s == 'hot'){
                $field_s = 'listen';
            }elseif(!empty($request->s) && $request->s == 'new'){
                $field_s = 'created_at';
            }

            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s) ) {


                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status','1')
                    ->groupBy('songs.id')
                    ->havingRaw($fieldFillSong.' LIKE "%' . $request->q . '%"')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::where('name', 'LIKE', '%' . $request->q . '%')->orderBy('created_at', 'desc')->limit(20)->get();
                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status','1')
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->limit(10)
                    ->get();

                    // k + l
            }elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)){


                $category = Category::where('slug',$request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status','1')
                    ->whereIn('category_id',$categoryChild)
                    ->groupBy('songs.id')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::where([
                    ['name', 'LIKE', '%' . $request->q . '%'],
                ])->whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status','1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->limit(10)
                    ->get();

             // k + c
            }elseif(!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)){

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status','=','1' ],
                        [$field_c ,'=',1]
                        ])
                    ->groupBy('songs.id')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::where('name', 'LIKE', '%' . $request->q . '%')->orderBy('created_at', 'desc')->limit(20)->get();
                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->limit(10)
                    ->get();

            // k + s
            }elseif(!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)){
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens','songs.code','=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s,'desc')
                    ->limit(5)
                    ->get();
                    $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                                ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                                ->where('name', 'LIKE', '%' . $request->q . '%')
                                ->orderBy($field_s, 'desc')->limit(20)->get();
                    $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

            // k + l + c
            }elseif(!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)){
                    $category = Category::where('slug', $request->l)->first();
                    $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                    foreach ($categoryChild as $key => $value) {
                        $categoryChild[$key] = $value['id'];
                    }

                    $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')

                        ->limit(5)
                        ->get();

                    $playlists = Playlist::where([
                        ['name', 'LIKE', '%' . $request->q . '%'],
                    ])->whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->limit(20)->get();

                    $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')

                        ->limit(10)
                        ->get();

            // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                    $category = Category::where('slug', $request->l)->first();
                    $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                    foreach ($categoryChild as $key => $value) {
                        $categoryChild[$key] = $value['id'];
                    }

                    $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1']
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(5)
                        ->get();

                    $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->whereIn('category_id', $categoryChild)
                        ->orderBy($field_s, 'desc')->limit(20)->get();

                    $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1']
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(10)
                        ->get();
            // k + c + s
            }elseif(!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)){
                    $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c, '=', 1]
                        ])

                        ->groupBy('songs.code')
                        ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(5)
                        ->get();

                    $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->orderBy($field_s, 'desc')->limit(20)->toSql();

                    $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])

                        ->groupBy('videos.code')
                        ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(10)
                        ->get();
            // k + l + c +s
            }elseif(!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)){
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(5)
                        ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=','playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->whereIn('category_id', $categoryChild)
                        ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(10)
                        ->get();

                    // l
            }elseif(empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)){
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(5)
                        ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=','playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->whereIn('category_id', $categoryChild)
                        ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(10)
                        ->get();

                // l + c
            }elseif(empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)){
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c,'=',1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(5)
                        ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=','playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->whereIn('category_id', $categoryChild)
                        ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(10)
                        ->get();

            // l + c + s
            }elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c,'=',1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('songs.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(5)
                        ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=','playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')
                        ->whereIn('category_id', $categoryChild)
                        ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])
                        ->whereIn('category_id', $categoryChild)
                        ->groupBy('videos.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy($field_s, 'desc')
                        ->limit(10)
                        ->get();

            // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                        ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                        ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                        ->where([
                            ['songs.status','=','1'],
                            [$field_c,'=',1]
                        ])

                        ->groupBy('songs.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(5)
                        ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                        ->leftJoin('playlists_listens', 'playlists.code', '=','playlists_listens.code')
                        ->where('name', 'LIKE', '%' . $request->q . '%')

                        ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                        ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                        ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                        ->where([
                            ['videos.status', '=', '1'],
                            [$field_c, '=', 1]
                        ])

                        ->groupBy('videos.code')
                        ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                        ->orderBy('created_at', 'desc')
                        ->limit(10)
                        ->get();

             // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')

                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

            // s
            }elseif(empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)){
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])

                    ->groupBy('songs.code')
                    ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')

                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw('name' . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

            }
        }else{
            $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('artist_song', 'songs.id','=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id','=', 'artists.id')
                        ->where(['songs.status' => '1'])
                        ->groupBy('songs.id')
                        ->limit(5)
                        ->get();
            $playlists = Playlist::limit(20)->orderBy('created_at', 'desc')->get();
            $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('video_artist', 'videos.id','=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id','=', 'artists.id')
                        ->where(['videos.status' => '1'])
                        ->groupBy('videos.id')
                        ->limit(10)
                        ->get();
            // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = 'GROUP_CONCAT(artists.name)';
                $fieldFillVideo = 'GROUP_CONCAT(artists.name)';
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = 'songs.name';
                $fieldFillVideo = 'videos.name';
            }

            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {


                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.id')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::orderBy('created_at', 'desc')->limit(20)->get();
                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.id')

                    ->limit(10)
                    ->get();

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.id')

                    ->limit(5)
                    ->get();

                $playlists = Playlist::whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.id')
                    ->limit(10)
                    ->get();

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('songs.id')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::orderBy('created_at', 'desc')->limit(20)->get();
                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('videos.id')

                    ->limit(10)
                    ->get();

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();
                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')

                    ->limit(5)
                    ->get();

                $playlists = Playlist::whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->limit(10)
                    ->get();

                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();
                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->limit(20)->toSql();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();
                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(10)
                    ->get();

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(10)
                    ->get();

                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy('created_at', 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->limit(10)
                    ->get();

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();

                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])

                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(5)
                    ->get();

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->limit(20)->get();

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->get();
            }
        }
        return view('client.search.index',compact('songs','playlists','videos'));
    }

    public function songs(Request $request){

        if (!empty($request->q)) {
            $keyword = $request->q;
            $songs = Song::select(DB::raw('songs.*,songs.name as song_name,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('artist_song', 'songs.id','=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id','=', 'artists.id')
                        ->where('songs.status','1')
                        ->whereRaw('songs.name LIKE "%'.$request->q.'%"')
                        ->groupBy('songs.code')
                        ->paginate(20);

            //  dd($songs);
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            }elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = "'videos.name'";
            }else{
                $fieldFillSong =  "'videos.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {
                $songs = Song::select(
                                    DB::raw('songs.*'),
                                    DB::raw('GROUP_CONCAT(artists.name) AS artists_name'),
                                    DB::raw('GROUP_CONCAT(artists.slug) AS artists_slug')
                                    )
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.id')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->paginate(20);
                // dd($songs);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->whereRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->paginate(20);


                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->paginate(20);


                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->paginate(20);



                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);



                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);


                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);


                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);



                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])
                    ->groupBy('songs.code')
                    ->havingRaw($fieldFillSong . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);

            }

        }else{
             $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('artist_song', 'songs.id','=', 'artist_song.song_id')
                        ->leftJoin('artists', 'artist_song.artist_id','=', 'artists.id')
                        ->where(['songs.status' => '1'])
                        ->groupBy('songs.id')
                        ->paginate(20);
            // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = 'videos.name';
            } else {
                $fieldFillSong =  "'songs.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }

            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {
                $songs = Song::select(
                    DB::raw('songs.*'),
                    DB::raw('GROUP_CONCAT(artists.name) AS artists_name'),
                    DB::raw('GROUP_CONCAT(artists.slug) AS artists_slug')
                )
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.id')
                    ->paginate(20);
                // dd($songs);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->paginate(20);


                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('songs.code')
                    ->paginate(20);


                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where('songs.status', '1')
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->paginate(20);



                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);


                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);



                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);


                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('songs.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(20);


                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);



                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $songs = Song::select(DB::raw('songs.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(songs_listens.code) as listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->leftJoin('artist_song', 'songs.id', '=', 'artist_song.song_id')
                    ->leftJoin('artists', 'artist_song.artist_id', '=', 'artists.id')
                    ->where([
                        ['songs.status', '=', '1'],
                    ])
                    ->groupBy('songs.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(20);
            }

        }

        return view('client.search.bai-hat',compact('songs'));
    }

    public function videos(Request $request){
        if (!empty($request->q)) {
            $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                ->where(['videos.status' => '1', 'videos.author_id' => 0])
                ->groupBy('videos.id')
                ->orWhere(DB::raw('videos.name LIKE "%' . $request->q . '%"'))
                ->orHavingRaw(DB::raw('GROUP_CONCAT(artists.name) LIKE "%' . $request->q . '%"'))
                ->paginate(30);

            // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = "'videos.name'";
            } else {
                $fieldFillVideo =  "'videos.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
            }
        } else {
           $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('video_artist', 'videos.id','=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id','=', 'artists.id')
                        ->where(['videos.status' => '1', 'videos.author_id' => 0])
                        ->groupBy('videos.id')
                        ->paginate(30);

            // gan
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = "'videos.name'";
            } else {
                $fieldFillVideo =  "'videos.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where('videos.status', '1')
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1']
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);
                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);
                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->paginate(32);
            }
        }

        return view('client.search.video', compact('videos'));
    }

    public function videoKaraoke(Request $request){
        if (!empty($request->q)) {
            $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                ->where(['videos.status' => '1', 'videos.author_id' => 0])
                ->groupBy('videos.id')
                ->orWhere(DB::raw('videos.name LIKE "%' . $request->q . '%"'))
                ->orHavingRaw(DB::raw('GROUP_CONCAT(artists.name) LIKE "%' . $request->q . '%"'))
                ->paginate(30);

            // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = 'videos.name';
            } else {
                $fieldFillSong =  "'songs.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }

            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                     ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.id')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                     ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->paginate(30);

                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);

                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                     ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->havingRaw($fieldFillVideo . ' LIKE "%' . $request->q . '%"')
                    ->orderBy($field_s, 'desc')
                    ->paginate(30);
            }
        } else {
           $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                        ->leftJoin('video_artist', 'videos.id','=', 'video_artist.video_id')
                        ->leftJoin('artists', 'video_artist.artist_id','=', 'artists.id')
                        ->where(['videos.status' => '1', 'videos.author_id' => 0])
                        ->groupBy('videos.id')
                        ->paginate(30);
             // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "GROUP_CONCAT(artists.name)";
                $fieldFillVideo = "GROUP_CONCAT(artists.name)";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = 'videos.name';
            } else {
                $fieldFillSong =  "'songs.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                     ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                     ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug'))
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->paginate(32);

                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c,'=',1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);
                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        [$field_c, '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);
                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                       ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])
                    ->whereIn('category_id', $categoryChild)
                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy('created_at', 'desc')
                    ->paginate(32);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->paginate(32);

                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $videos = Video::select(DB::raw('videos.*,GROUP_CONCAT(artists.name) AS artists_name,GROUP_CONCAT(artists.slug) AS artists_slug,COUNT(videos_listens.code) as listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->leftJoin('video_artist', 'videos.id', '=', 'video_artist.video_id')
                    ->leftJoin('artists', 'video_artist.artist_id', '=', 'artists.id')
                    ->where([
                        ['videos.status', '=', '1'],
                        ['karaoke_type', '=', 1]
                    ])

                    ->groupBy('videos.code')
                    ->orderBy($field_s, 'desc')
                    ->limit(10)
                    ->paginate(32);
            }
        }

        return view('client.search.karaoke', compact('videos'));
    }

    public function playlists(Request $request){
        if (!empty($request->q)) {
            $playlists = Playlist::where('name', 'LIKE', '%' . $request->q . '%')->orderBy('created_at', 'desc')->paginate(20);
             // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "'artists_name'";
                $fieldFillVideo = "'artists_name'";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = 'videos.name';
            } else {
                $fieldFillSong =  "'songs.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }

            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $playlists = Playlist::where('name', 'LIKE', '%' . $request->q . '%')->orderBy('created_at', 'desc')->paginate(20);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::where([
                    ['name', 'LIKE', '%' . $request->q . '%'],
                ])->whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->paginate(20);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {


                $playlists = Playlist::where('name', 'LIKE', '%' . $request->q . '%')->orderBy('created_at', 'desc')->paginate(20);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->orderBy($field_s, 'desc')->paginate(20);


                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::where([
                    ['name', 'LIKE', '%' . $request->q . '%'],
                ])->whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->paginate(20);


                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);

                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->orderBy($field_s, 'desc')->limit(20)->toSql();

                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->paginate(20);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->paginate(20);
                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);


                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')

                    ->orderBy('created_at', 'desc')->paginate(20);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')

                    ->orderBy($field_s, 'desc')->paginate(20);
                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->where('name', 'LIKE', '%' . $request->q . '%')
                    ->orderBy($field_s, 'desc')->limit(20)->get();
            }
        } else {
            $playlists = Playlist::orderBy('created_at', 'desc')->paginate(20);
             // gan cac field
            if (!empty($request->k) && $request->k == 'singer') {
                $fieldFillSong = "'artists_name'";
                $fieldFillVideo = "'artists_name'";
            } elseif (!empty($request->k) && $request->k  == 'title') {
                $fieldFillSong = "'songs.name'";
                $fieldFillVideo = 'videos.name';
            } else {
                $fieldFillSong =  "'songs.name'";
            }
            // dd($fieldFillSong);
            if (!empty($request->c) && $request->c == 'lyric') {
                $field_c = 'beat_type';
            } elseif (!empty($request->c) && $request->c == 'karaoke') {
                $field_c = 'karaoke_type';
            }

            if (!empty($request->s) && $request->s == 'hot') {
                $field_s = 'listen';
            } elseif (!empty($request->s) && $request->s == 'new') {
                $field_s = 'created_at';
            }
            // k
            if (!empty($request->k) && empty($request->l) && empty($request->c) && empty($request->s)) {

                $playlists = Playlist::orderBy('created_at', 'desc')->paginate(20);

                // k + l
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {


                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->paginate(20);

                // k + c
            } elseif (!empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {


                $playlists = Playlist::orderBy('created_at', 'desc')->paginate(20);

                // k + s
            } elseif (!empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->paginate(20);


                // k + l + c
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::whereIn('category_id', $categoryChild)->orderBy('created_at', 'desc')->paginate(20);


                // k + l + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);

                // k + c + s
            } elseif (!empty($request->k) && !empty($request->l) && empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('COUNT(playlists_listens.code) as listen,playlists.*'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->limit(20)->toSql();

                // k + l + c +s
            } elseif (!empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);

                // l
            } elseif (empty($request->k) && !empty($request->l) && empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }


                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->paginate(20);

                // l + c
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy('created_at', 'desc')->paginate(20);
                // l + c + s
            } elseif (empty($request->k) && !empty($request->l) && !empty($request->c) && !empty($request->s)) {
                $category = Category::where('slug', $request->l)->first();
                $categoryChild = Category::select('id')->where('id', $category->id)->orWhere('parent_id', '=', $category->id)->get()->toArray();

                foreach ($categoryChild as $key => $value) {
                    $categoryChild[$key] = $value['id'];
                }

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->whereIn('category_id', $categoryChild)
                    ->orderBy($field_s, 'desc')->paginate(20);


                // c
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && empty($request->s)) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy('created_at', 'desc')->paginate(20);

                // c + s
            } elseif (empty($request->k) && empty($request->l) && !empty($request->c) && !empty($request->s)) {

                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->paginate(20);
                // s
            } elseif (empty($request->k) && empty($request->l) && empty($request->c) && !empty($request->s)) {
                $playlists = Playlist::select(DB::raw('playlists.*,COUNT(playlists_listens.code) as listen'))
                    ->leftJoin('playlists_listens', 'playlists.code', '=', 'playlists_listens.code')
                    ->orderBy($field_s, 'desc')->paginate(20);
            }
        }
        return view('client.search.playlist', compact('playlists'));
    }

    public function delete_history_search(Request $request){
        if (!empty($request->customer_id)) {
            User_history::where(['customer_id' =>  $request->customer_id,'provider' => 'search_history'])->delete();
        }else{
            User_history::where('id', $request->id)->delete();
        };
        return response([
            'err' => false
        ]);
    }
}
