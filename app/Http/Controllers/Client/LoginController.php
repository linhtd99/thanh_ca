<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Password_reset;
use Validator;
use Auth;
use Mail;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'username' => 'required',
                'password' => 'required',
            ],
            [
                'required' => 'Không được để trống',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
                $check = Auth::guard('customers')->attempt(['email' => $request->username, 'password' => $request->password,'status' => 1]);

                return response(
                    [
                        'errors' => ($check == true ? false : true),
                        'data' => ($check == true ? 'Đăng nhập thành công' : 'Sai tài khoản hoặc mật khẩu'),
                    ]
                );
            } else {
                $check =  Auth::guard('customers')->attempt(['username' => $request->username, 'password' => $request->password,'status' => 1]);
                return response(
                    [
                        'errors' => ($check == true ? false : true),
                        'data' => ($check == true ? 'Đăng nhập thành công' : 'Sai tài khoản hoặc mật khẩu'),
                    ]
                );
            }

        }
    }
    public function registerCustomer(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'username' => [
                    'required',
                    function($att,$val,$fail){
                        $customer = Customer::where('username',$val)->first();
                        // dd($customer);
                        if (!empty($customer) && $customer->status != -1) {
                            return $fail('Tên đăng nhập này đã tồn tại. Vui lòng điền tên đăng nhập khác');
                        };
                    },
                    'min:3',
                    'regex:/^[A-Za-z0-9.-_]+$/u',
                ],
                'email' => [
                    'required',
                    'email',
                    function($att,$val,$fail){
                        $customer = Customer::where('email',$val)->first();
                        // dd($customer);
                        if (!empty($customer) && $customer->status != -1) {
                            return $fail('Email này đã tồn tại. Vui lòng chọn tên đăng nhập khác');
                        };
                    },
                ],
                'password' => 'required|min:6',
                'cf_password' => 'required|same:password',
                'sex' => 'required',
                'birthday' => 'required',
            ],
            [
                'required' => 'Không được để trống',
                'username.unique' => 'Tên đăng nhập này đã tồn tại. Vui lòng điền tên đăng nhập khác',
                'username.min' => 'Chỉ được sử dụng chữ cái,chữ số,gạch dưới và dấu chấm và có ít nhất 3 kí tự',
                'username.regex' => 'Chỉ được sử dụng chữ cái,chữ số,gạch dưới và dấu chấm và có ít nhất 8 kí tự',
                'email.unique' => 'Email này đã tồn tại. Vui lòng chọn tên đăng nhập khác',
                'email.email' => 'Nhập đúng dịnh dạng email',
                'password.min' => 'Mật khẩu có ít nhất 6 kí tự',
                'cf_password.same' => 'Chưa trùng khớp với mật khẩu',
            ]

        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            // dd($request->all());
            $data = $request->except('_token', 'cf_password','email');
            $data['password'] = bcrypt($request->password);
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $data['status'] = -1;
            $customer = Customer::where('email',$request->email)->first();

            $data['token'] = getToken(20);

            $email = $request->email;
            $data_sendmail['title'] = "Kích hoạt tài khoản";
            $data_sendmail['name'] = $request->username;
            $data_sendmail['email'] = $request->email;
            $data_sendmail['token'] = $data['token'];
            Mail::send('send_mail_cofirm_register', $data_sendmail, function ($message) use ($email) {
                $message->to($email)->subject('Pveser');
            });

            if ($customer) {
                Customer::where('email',$request->email)->update($data);
                return response(
                    [
                        'errors' => false,
                        'data' => $request->all(),
                    ]
                );
            }else{
                $data['email'] = $request->email;
                Customer::insert($data);
                return response(
                    [
                        'errors' => false,
                        'data' => $request->all(),
                    ]
                );
            }

        }
    }

    public function activeAccount($token){
        $activeCustomer = Customer::where('token',$token)->first();
        if ($activeCustomer) {
            Customer::where('email',$activeCustomer->email)->update(['token' => null,'status' => 1]);
            Auth::guard('customers')->loginUsingId($activeCustomer->getAuthIdentifier());
            return redirect()->route('/')->with(['active_success' => 'Kích hoạt tài khoản thành công']);
        }else{
            abort(404);
        }
    }
    public function logoutCustomer()
    {
        Auth::guard('customers')->logout();
        return redirect()->route('/');
    }

    
    public function password(){
        return view('client/layout/password');
    }
    public function post_password(Request $request){
       
        $db = Customer::where('email',$request->email)->first();
        $token = gettoken(40);
      
        if($db){
            $email = $request->email;
            $reset = new Password_reset;
            $reset->token = $token;
            $reset->email = $request->email;
            $reset->created_at = date('Y-m-d H:i:s', time());
            $reset->save();
            Mail::send('mail', array('db'=>$reset), function($message) use ($email) {
                $message->to($email, 'Visitor')->subject('Visitor Feedback!');
            });
            return back()->with('success', 'Kiểm tra email của bạn để lấy đường dẫn xác nhận đăng ký.');
        }
        else{
            return back()->with('errors', 'Email không tồn tại');
        }
    }
    public function reset_password(){
        return view('client/layout/reset_password');
    }
    public function post_reset_password(Request $request,$token){
        $db = Password_reset::where('token',$token)->first();
        if($db){
           $cus =  Customer::where('email',$db->email)->first();
           if ($request->cf_password == $request->password ) {
                $cus->password = bcrypt($request->password);
                $cus->save();
                return redirect()->route('/')->with(['active_success' => 'Đổi mật khẩu tài khoản thành công']);
           }
           else{
                return back()->with('success', 'Mật khẩu không trùng khớp');
           }
        }
    }
}
