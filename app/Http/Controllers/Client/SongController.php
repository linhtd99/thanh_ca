<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Playlist;
use App\Models\Video;
use App\Models\Artist;
use App\Models\Category;
use App\Models\Option;
use Illuminate\Support\Facades\DB;
use App\Models\Song;
use View;

class SongController extends Controller
{
    public function __construct()
    {
        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));

        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        }

        // dd($artistTrending);
        $artistRandom5 = Artist::limit(5)->get();
        $categories = Option::select('value')->where('key', '=', 'sort_category')->first();
        !empty($categories->value) ? $cate_value = json_decode($categories->value, true) : $cate_value = '';

        View::share(['artistTrending' => $artistTrending, 'artistRandom5' => $artistRandom5, 'categories' => $cate_value]);
    }

    public function listSong()
    {
        $songs = Song::select(DB::raw('songs.*,COUNT(songs_listens.code) as listen'))
            ->leftJoin('songs_listens', 'songs_listens.code', '=', 'songs.code')
            ->where('songs.status', '=', '1')
            ->groupBy('songs.code')
            ->with('artists')
            ->orderBy('listen', 'desc')
            ->paginate(20);
        // dd($songs);
        return view('client.songs', compact('songs'));
    }
    public function listSongNew()
    {
        $songs = Song::select(DB::raw('songs.*,COUNT(songs_listens.code) as listen'))
            ->leftJoin('songs_listens', 'songs_listens.code', '=', 'songs.code')
            ->where('songs.status', '=', '1')
            ->groupBy('songs.code')
            ->with('artists')
            ->orderBy('songs.created_at', 'desc')
            ->paginate(20);
        // dd($songs);
        return view('client.songs', compact('songs'));
    }

    public function listSongByCategory($slug)
    {

        $categoryCurrent = Category::where('slug', $slug)->first();
        // dd($categoryCurrent);
        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $songs = Song::select(DB::raw('songs.*,COUNT(songs.code) AS listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->where('category_id', $categoryCurrent['id'])
                    ->groupBy('songs.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(20);
            } else {
                //  dd('aa');
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $songs = Song::select(DB::raw('songs.*,COUNT(songs.code) AS listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->join('categories', 'categories.id', '=', 'songs.category_id')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('songs.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(20);
            };
        } else {
            abort(404);
        }
        // dd('aa');
        return view('client.song-category', compact('songs', 'categoryCurrent'));
    }
    public function listSongByCategoryNew($slug)
    {
        $categoryCurrent = Category::where('slug', $slug)->first();
        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $songs = Song::select(DB::raw('songs.*,COUNT(songs.code) AS listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->where('category_id', $categoryCurrent['id'])
                    ->groupBy('songs.code')
                    ->orderBy('songs.created_at', 'desc')
                    ->paginate(20);
            } else {
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $songs = Song::select(DB::raw('songs.*,COUNT(songs.code) AS listen'))
                    ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
                    ->join('categories', 'categories.id', '=', 'songs.category_id')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('songs.id')
                    ->orderBy('songs.created_at', 'desc')
                    ->paginate(20);
            };
        } else {
            abort(404);
        }

        return view('client.song-category', compact('songs', 'categoryCurrent'));
    }
}
