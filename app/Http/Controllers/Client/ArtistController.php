<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Video;
use Illuminate\Support\Facades\DB;
use App\Models\Artist;
use View;
use App\Models\Song;
use App\Models\Albums_listens;

class ArtistController extends Controller
{
    public function __construct()
    {
        $artistsRandom = Artist::inRandomOrder()->limit(9)->get();
        View::share(['artistsRandom' => $artistsRandom]);
    }
    public function index($keyword = null){

        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));

        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '". $firstDayOfPreviousWeek. "' AND '" . $lastDayOfPreviousWeek . "' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 10"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '". $firstDayOfTheWeek."' AND '". $lastDayOfTheWeek."' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 10"));
        }
        // dd($artistTrending);
        if($keyword != null){
            $artists = Artist::where('name','LIKE',$keyword.'%')->paginate(40);
        }else{
            $artists = Artist::inRandomOrder()->limit(40)->get();
        }
        // dd($artistTrending);
        $artistsHighlight = Artist::with(['songs' => function($q){
            $q->latest()->limit(4);
        }])->limit(5)->get();

        return view('client.singer', ['artistTrending' => $artistTrending,'artists' => $artists,'artistsHighlight' => $artistsHighlight, 'keyword' => $keyword]);
    }

    public function artistDetail($slug){
        $artist = Artist::where('slug',$slug)->with('albums')->first();

        if (!$artist) {
            abort(404);
        }
        $albums = Artist::select(DB::raw('artists.slug AS artist_slug,albums.*'))
                        ->join('artist_song', 'artists.id','=', 'artist_song.artist_id')
                        ->join('songs', 'artist_song.song_id', '=', 'songs.id')
                        ->join('albums', 'songs.album_id','=', 'albums.id')
                        ->where('artists.slug','=',$slug)
                        ->groupBy('albums.code')
                        ->orderBy('albums.id','desc')
                        ->limit(10)
                        ->get();

        $mvs = Artist::where('slug',$slug)->with(['videos' => function($q){
            $q->latest()->limit(8);
        }])->first();

        $songs = Artist::where('slug',$slug)->with(['songs' => function($q){
            $q->where('status','=','1')->with('artists')->latest()->limit(10);
        }])->first();

        // dd($songs);
        return view('client.singer-detail',compact('artist','albums','mvs','songs'));
    }

    public function artistSong($slug){
        $artist = Artist::where('slug', $slug)->first();
        // dd($artist);
        if (!$artist) {
            abort(404);
        }

        $songs = Song::select('songs.*', DB::raw('group_concat(artists.name) as artist_name'), DB::raw('group_concat(artists.slug) as artist_slug'))
        ->join('artist_song','songs.id','=', 'artist_song.song_id')
        ->join('artists','artist_song.artist_id','=', 'artists.id')
        ->where('songs.status','=','1')
        ->groupBy('songs.name')
        ->having(DB::raw('group_concat(artists.slug)'),'LIKE', '%'.(string)$slug.'%')
        ->orderBy('songs.created_at','desc')
        ->paginate(15);
        // ->toSql();
        // dd($songs);
        return view('client.singer-beat', compact('songs','artist'));
    }

    public function artistAlbum($slug){
        $artist = Artist::where('slug', $slug)->first();
        if (!$artist) {
            abort(404);
        }

        $albums = Artist::select(DB::raw('artists.slug AS artist_slug,albums.*'))
            ->join('artist_song', 'artists.id', '=', 'artist_song.artist_id')
            ->join('songs', 'artist_song.song_id', '=', 'songs.id')
            ->join('albums', 'songs.album_id', '=', 'albums.id')
            ->where('artists.slug', '=', $slug)
            ->groupBy('albums.code')
            ->orderBy('albums.id', 'desc')
            ->paginate(15);
            // ->toSql();
        return view('client.singer-album',compact('albums','artist'));
    }

    public function artistAlbumHot($slug){
        // dd($slug);
        $artist = Artist::where('slug', $slug)->first();
        if (!$artist) {
            abort(404);
        }

        $albums = Albums_listens::select(DB::raw('albums.*,albums_listens.code,COUNT(albums_listens.code) as listen'))
                                ->join('albums','albums_listens.code','=','albums.code')
                                ->whereRaw('albums.id IN (select album_id from album_artist join artists ON album_artist.artist_id = artists.id where artists.slug = '.'"'.str_replace(' ','',$slug).'")')
                                ->groupBy('albums_listens.code')
                                ->orderBy('listen','desc')
                                ->paginate(15);
        // dd($albums);
        return view('client.singer-album', compact('albums', 'artist'));
    }

    public function artistMv($slug){
        $artist = Artist::where('slug', $slug)->first();
        if (!$artist) {
            abort(404);
        }

        $mvs = Video::select('videos.*',DB::raw('GROUP_CONCAT(artists.name) as artist_name'),DB::raw('GROUP_CONCAT(artists.slug) as artist_slug'))
                        ->join('video_artist', 'video_artist.video_id','=','videos.id')
                        ->join('artists', 'video_artist.artist_id','=', 'artists.id')
                        ->where('videos.status','1')
                        ->groupBy('videos.id')
                        ->having(DB::raw('GROUP_CONCAT(artists.slug)'), 'LIKE','%'.$slug.'%')
                        ->paginate(20);
        // dd($mvs);
        return view('client.singer-video', compact('artist','mvs'));
    }
}
