<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Playlist;
use App\Models\Tag;

class TagController extends Controller
{
    private $params = [
        '0' => 'p1',
        '1' => 'p2',
        '2' => 'p3',
        '3' => 'p4',
    ];
    public function index($p1 = null, $p2 = null, $p3 = null, $p4 = null)
    {
        $pa = Tag::where('parent_id', '=', 0)->with('getTagChildren')->orderBy('created_at', 'ASC')->get();
        $params = $this->params;
        $pargs = [];
        foreach ($pa as $key => $value) {

            $params[$key] = $$value;
            if ($$value != null) {
                $pargs[$key] = $$value;
            }
        }
        $arg = function ($value) {
            if ($value != null) {
                return ['tags.slug', '=', $value];
            }
        };
        // dd($pargs);
        $args = array_values(array_map($arg, $pargs));
        dd($pargs);
        $playlists = Playlist::join('tag_playlist', 'playlists.id', '=', 'tag_playlist.playlist_id')
            ->join('tags', 'tag_playlist.tag_id', '=', 'tags.id')
            ->where($args)->groupBy('playlists.id')->orderby('playlists.name')->get();
        // dd($playlists);

        // dd($pa);

        return view('client.tag', compact('pa', 'params', 'playlists', 'pargs'));
    }

}
