<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Topic;

class TopicController extends Controller
{
    public function index()
    {
        $topics = Topic::with([
            'playlists' => function ($query) {
                $query->orderBy('created_at', 'desc')->limit(3);
            },
        ]
        )->orderBy('created_at', 'desc')->limit(9)->get();

        return view('client.topic', compact('topics'));
    }
}
