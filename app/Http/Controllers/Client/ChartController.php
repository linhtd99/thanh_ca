<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Playlist;
use App\Models\Song;
use App\Models\Video;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use View;

class ChartController extends Controller
{
    public function __construct()
    {
        $songRandom5 = Song::where('status', '1')->inRandomOrder()->with('artists', 'categories')->limit(5)->get();
        $listSongRandom5 = $this->getTrack($songRandom5);
        $playList4 = Playlist::where('customer_id', '=', '0')->inRandomOrder()->limit(4)->get();
        View::share(compact('songRandom5', 'playList4', 'listSongRandom5'));
    }
    /**
     * Bảng xếp hạng bài hát.
     *
     * @param string $slug_category
     * @param string $month
     * @param string $year
     */
    public function index($slug_category, $month = '', $year = '')
    {
        if (empty($month) && empty($year)) {
            $month = date('m') - 1;
            $year = date('Y');
        }
        if ($month > 12) {
            $month = date('m') - 1;
        }
        $currentYear = date('Y');
        $currenMonth = date('m');
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $currentYear - 1;
        } else {
            $lastMonth = $currenMonth - 1;
            $lastYear = $currentYear;
        }
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);
        // dd($lastMonth);
        if (strtotime($arr_date['start']) >= strtotime($current_date['start']) || (int) $month > 12) {
            return redirect()->route('bxh_song', ['slug_category' => $slug_category, 'month' => $lastMonth, $lastYear]);
        }
        $category = Category::where('slug', '=', $slug_category)->first();
        $category_id = $category->id;
        $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
            ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')

            ->where([
                ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.songs.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
            ->groupBy('songs.code')->orderBy('listens', 'desc')
            ->limit(20)->with('artists', 'lyric')
            ->get();
        if ($chart->count() < 1) {
            $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')

                ->where([
                    ['.songs.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                ->groupBy('songs.code')->orderBy('listens', 'desc')
                ->limit(20)->with('artists', 'lyric')
                ->get();

        }
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $year - 1;
        } else {
            $lastMonth = $month - 1;
            $lastYear = $year;
        }

        return view('client.bxh', compact('chart', 'month', 'year', 'lastYear', 'lastMonth', 'arr_date', 'category'));
    }

    /**
     * Bảng xếp hạng video.
     *
     * @param string $slug_category
     * @param string $week
     * @param string $year
     */
    public function indexVideo($slug_category, $month = '', $year = '')
    {
        if (empty($month) && empty($year)) {
            $month = date('m') - 1;
            $year = date('Y');
        }
        if ($month > 12) {
            $month = date('m') - 1;
        }
        $currentYear = date('Y');
        $currenMonth = date('m');
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $currentYear - 1;
        } else {
            $lastMonth = $currenMonth - 1;
            $lastYear = $currentYear;
        }
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);
        if (strtotime($arr_date['start']) >= strtotime($current_date['start']) || (int) $month > 12) {
            return redirect()->route('bxh_video', ['slug_category' => $slug_category, 'week' => $currenMonth - 1, $currentYear]);
        }

        $category = Category::where('slug', '=', $slug_category)->first();
        $category_id = $category->id;
        $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
            ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')

            ->where([
                ['videos_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.videos_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.videos.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
            ->groupBy('videos.code')->orderBy('listens', 'desc')
            ->limit(20)->with('artists', 'lyric')
            ->get();

        if ($chart->count() < 1) {
            $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
                ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')

                ->where([
                    ['.videos.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
                ->groupBy('videos.code')->orderBy('listens', 'desc')
                ->limit(20)->with('artists')
                ->get();

        }
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $year - 1;
        } else {
            $lastMonth = $month - 1;
            $lastYear = $year;
        }

        return view('client.bxh-video', compact('chart', 'month', 'year', 'lastYear', 'lastMonth', 'arr_date', 'category'));
    }

    /**
     * chi tiết bảng xếp hạng.
     *
     * @param string $slug_category
     * @param string $week
     * @param string $year
     */
    public function chartSongDetail($slug_category, $month = '', $year = '')
    {
        if (empty($month) && empty($year)) {
            $month = date('m') - 1;
            $year = date('Y');
        }
        if ($month > 12) {
            $month = date('m') - 1;
        }

        $currentYear = date('Y');
        $currenMonth = date('m');
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $currentYear - 1;
        } else {
            $lastMonth = $currenMonth - 1;
            $lastYear = $currentYear;
        }
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);

        if (strtotime($arr_date['start']) >= strtotime($current_date['start'])) {
            return redirect()->route('bxh_song', ['slug_category' => $slug_category, 'week' => $currenMonth - 1, $currentYear]);
        }

        // $arr_date = getStartAndEndDate($month, $year);
        $category = Category::where('slug', '=', $slug_category)->first();
        $category_id = $category->id;
        $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
            ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where([
                ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.songs.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
            ->groupBy('songs.code')->orderBy('listens', 'desc')->with('artists')
            ->limit(20)
            ->get();
        if ($chart->count() < 1) {
            $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
                ->where([
                    ['.songs.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                ->groupBy('songs.code')->orderBy('listens', 'desc')->with('artists')
                ->limit(20)
                ->get();

        }

        foreach ($chart as $key => $value) {
            $chart[$key]['artists'] = Song::find($value->id)->load('artists')->artists;
            $chart[$key]['lyric'] = Song::find($value->id)->load('lyric')->lyric;
            $chart[$key]['video'] = Video::find($value->video_id);
        }
        $json_listSong = $this->getTrack($chart);
        $infoPlaylist = $chart;
        foreach ($infoPlaylist as $key => $value) {
            unset($infoPlaylist[$key]['lyric']);
        }
        $playlist = $infoPlaylist;
        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();
        // end comment
        if ($month == 1) {
            $lastWeek = 12;
            $lastYear = $year - 1;
        } else {
            $lastWeek = $month - 1;
            $lastYear = $year;
        }
        $scope = 'song.' . $slug_category . '.' . $month . '.' . $year;

        // comment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }
        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where('code', $scope)->groupBy('comments.code')->first();
        return view('client.trinh-nghe-nhac-bxh', compact('chart', 'month', 'year', 'lastYear', 'lastWeek', 'arr_date', 'category', 'json_listSong', 'mv', 'playlist', 'comments', 'totalComment'));
    }

    /**
     * chi tiết bảng xếp hạng.
     *
     * @param string $slug_category
     * @param string $week
     * @param string $year
     */
    public function chartVideoDetail($slug_category, $month = '', $year = '', $code = '')
    {
        if (empty($month) && empty($year)) {
            $month = date('m') - 1;
            $year = date('Y');
        }
        if ($month > 12) {
            $month = date('m') - 1;
        }
        $currentYear = date('Y');
        $currenMonth = date('m');
        if ($month == 1) {
            $lastMonth = 12;
            $lastYear = $currentYear - 1;
        } else {
            $lastMonth = $currenMonth - 1;
            $lastYear = $currentYear;
        }
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);

        if (strtotime($arr_date['start']) >= strtotime($current_date['start'])) {
            return redirect()->route('bxh_song', ['slug_category' => $slug_category, 'week' => $currenMonth - 1, $currentYear]);
        }

        // $arr_date = getStartAndEndDateOfMonth($month, $year);
        $category = Category::where('slug', '=', $slug_category)->first();
        $category_id = $category->id;
        $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
            ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')
            ->where([
                ['videos_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.videos_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.videos.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
            ->groupBy('videos.code')->orderBy('listens', 'desc')->with('artists')
            ->limit(20)->with('artists', 'lyric')
            ->get();
        if ($chart->count() < 1) {
            $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
                ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')
                ->where([
                    ['.videos.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
                ->groupBy('videos.code')->orderBy('listens', 'desc')->with('artists')
                ->limit(20)
                ->get();

        }

        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();
        // end comment
        if ($month == 1) {
            $lastWeek = 12;
            $lastYear = $year - 1;
        } else {
            $lastWeek = $month - 1;
            $lastYear = $year;
        }

        $scope = 'video.' . $slug_category . '.' . $month . '.' . $year;

        // comment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where('code', $scope)->groupBy('comments.code')->first();

        // END COMMENT
        $videoRandom4 = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(5)->get();
        $playList4 = Playlist::inRandomOrder()->limit(4)->get();

        // dd($comments);
        return view('client.bxhvideo-detail', compact('chart', 'month', 'year', 'lastYear', 'lastWeek', 'arr_date', 'category', 'mv', 'comments', 'totalComment', 'videoRandom4', 'playList4'));
    }

    /**
     * Top 100 bài hát theo tuần.
     *
     * @param string $slug_category
     */
    public function top100($slug_category)
    {

        $week = date('m');
        $year = date('Y');
        if ($week == 1) {
            $week = 12;
            $year = $year - 1;
        } else {
            $week = $week - 1;
            $year = $year;
        }
        $category = Category::where('slug', '=', $slug_category)->first();
        $parentCategory = Category::find($category->parent_id);
        // dd($parentCategory->name);
        $childCategories = Category::where('parent_id', '=', $category->parent_id)->limit(6)->get();
        $otherParentCategories = Category::where([
            ['parent_id', '=', '0'],
            ['id', '<>', $category->parent_id],
        ])->inRandomOrder()->limit(5)
            ->get();
        $category_id = $category->id;
        $arr_date = getStartAndEndDateOfMonth($week, $year);

        $charts = Song::join('categories', 'songs.category_id', '=', 'categories.id')
            ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where([
                ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.categories.parent_id', '<>', '0'],
                ['.songs.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
            ->groupBy('songs.code')->orderBy('listens', 'desc')
            ->limit(100)->with('artists', 'lyric')
            ->get();
        if (count($charts) < 1) {
            $charts = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
                ->where([
                    ['.categories.parent_id', '<>', '0'],
                    ['.songs.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                ->groupBy('songs.code')->orderBy('listens', 'desc')
                ->limit(100)->with('artists', 'lyric')
                ->get();

        }

        return view('client.top100', compact('category', 'charts', 'year', 'arr_date', 'parentCategory', 'childCategories', 'otherParentCategories'));
    }

    /**
     * Chi tiet top 100 bai hat.
     *
     * @param [type] $slug_category
     */
    public function top100Detail($slug_category)
    {
        $week = date('W');
        $year = date('Y');
        if ($week == 1) {
            $week = 12;
            $year = $year - 1;
        } else {
            $week = $week - 1;
            $year = $year;
        }
        $category = Category::where('slug', '=', $slug_category)->first();
        $parentCategory = Category::find($category->parent_id);
        $childCategories = Category::where('parent_id', '=', $category->parent_id)->limit(6)->get();
        // dd(empty($childCategories));
        // if (empty($childCategories)) {
        //     abort(404, 'Not found');
        // }
        $category_id = $category->id;
        $arr_date = getStartAndEndDateOfMonth($week, $year);

        $charts = Song::join('categories', 'songs.category_id', '=', 'categories.id')
            ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where([
                ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.categories.parent_id', '<>', '0'],
                ['.songs.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
            ->groupBy('songs.code')->orderBy('listens', 'desc')
            ->limit(100)->with('artists', 'lyric')
            ->get();
        if (count($charts) < 1) {
            $charts = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
                ->where([
                    ['.categories.parent_id', '<>', '0'],
                    ['.songs.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                ->groupBy('songs.code')->orderBy('listens', 'desc')
                ->limit(100)->with('artists', 'lyric')
                ->get();

        }
        foreach ($charts as $key => $value) {
            $charts[$key]['artists'] = Song::find($value->id)->load('artists')->artists;
            $charts[$key]['lyric'] = Song::find($value->id)->load('lyric')->lyric;
            $charts[$key]['video'] = Video::find($value->video_id);
        }
        $json_listSong = $this->getTrack($charts);
        $infoPlaylist = $charts;
        foreach ($infoPlaylist as $key => $value) {
            unset($infoPlaylist[$key]['lyric']);
        }
        $playlist = $infoPlaylist;
        $mv = Video::where('status', '1')->inRandomOrder()->with('artists')->limit(4)->get()->toArray();

        $scope = 'top100.' . $slug_category;

        // comment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $scope])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where('code', $scope)->groupBy('comments.code')->first();

        return view('client.trinh-nghe-nhac-top100', compact('category', 'charts', 'year', 'arr_date', 'parentCategory', 'childCategories', 'json_listSong', 'mv', 'playlist', 'comments', 'totalComment'));
    }

    /**
     * dung cho playlist,album.
     *
     * @param [type] $song
     */
    public function getTrack($song)
    {
        $new_arr = [];
        $artist_id = '';
        foreach ($song as $key => $value) {
            foreach ($value->artists as $artist) {
                $artist_id .= $artist->id;
            }
            $new_arr[$key] = [
                'track' => $key + 1,
                'name' => $value['name'],
                'code' => $value['code'],
                'image' => $value['image'],
                'singer' => $value['song'],
                'filename' => Storage::disk('public')->url('/uploads/files/' . $value->filename),
                'video' => ($value->video != null ? $value->video->link : null),
                'lyrics' => ($value->lyric != null ? json_decode($value->lyric->content, true) : ''),
            ];
        }

        return $new_arr;
    }
}
