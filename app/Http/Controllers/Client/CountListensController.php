<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Albums_listens;
use App\Models\Playlists_listens;
use App\Models\Songs_listens;
use App\Models\Videos_listens;
use Illuminate\Http\Request;

class CountListensController extends Controller
{
    public function postListens(Request $request)
    {
        $clientIP = \Request::getClientIp(true);
        // dd($clientIP);
        if (Songs_listens::where('code', '=', $request->code)->first()) {
            $latest_listen = Songs_listens::where([['code', '=', $request->code], ['ip', '=', $clientIP]])->latest('created_at')->first();
            // $created_at = (date('Y-m-d H:i:s', strtotime($latest_listen->created_at)));
            if (strtotime('-1 minutes', time()) > strtotime($latest_listen->created_at)) {
                $listen = new Songs_listens();
                $listen->code = $request->code;
                $listen->ip = $clientIP;
                $listen->save();
                return response()->json($listen);
            }
        } else {
            $listen = new Songs_listens();
            $listen->code = $request->code;
            $listen->ip = $clientIP;
            $listen->save();
            return response()->json($listen);
        }
    }

    public function postListensVideo(Request $request)
    {
        // dd(Video::where('code', '=', $request->code)->first());
        $clientIP = \Request::getClientIp(true);
        if (Videos_listens::where('code', '=', $request->code)->first()) {
            $latest_listen = Videos_listens::where([['code', '=', $request->code], ['ip', '=', $clientIP]])->latest('created_at')->first();
            // $created_at = (date('Y-m-d H:i:s', strtotime($latest_listen->created_at)));
            if (strtotime('-2 minutes', time()) > strtotime($latest_listen->created_at)) {
                $listen = new Videos_listens();
                $listen->code = $request->code;
                $listen->ip = $clientIP;
                $listen->save();
                echo 'done';
            }
        } else {
            // dd('áda');
            $listen = new Videos_listens();
            $listen->code = $request->code;
            $listen->ip = $clientIP;
            $listen->save();
        }
    }

    public function postListensPlaylist(Request $request)
    {
        // dd(Video::where('code', '=', $request->code)->first());
        $clientIP = \Request::getClientIp(true);
        if (Playlists_listens::where('code', '=', $request->code)->first()) {
            $latest_listen = Playlists_listens::where([['code', '=', $request->code], ['ip', '=', $clientIP]])->latest('created_at')->first();
            // $created_at = (date('Y-m-d H:i:s', strtotime($latest_listen->created_at)));
            if (strtotime('-2 minutes', time()) > strtotime($latest_listen->created_at)) {
                $listen = new Playlists_listens();
                $listen->code = $request->code;
                $listen->ip = $clientIP;
                $listen->save();
                echo 'done';
            }
        } else {
            // dd('áda');
            $listen = new Playlists_listens();
            $listen->code = $request->code;
            $listen->ip = $clientIP;
            $listen->save();
        }
    }

    public function postListensAlbum(Request $request)
    {
        // dd(Video::where('code', '=', $request->code)->first());
        $clientIP = \Request::getClientIp(true);
        if (Albums_listens::where('code', '=', $request->code)->first()) {
            $latest_listen = Albums_listens::where([['code', '=', $request->code], ['ip', '=', $clientIP]])->latest('created_at')->first();
            // $created_at = (date('Y-m-d H:i:s', strtotime($latest_listen->created_at)));
            if (strtotime('-2 minutes', time()) > strtotime($latest_listen->created_at)) {
                $listen = new Albums_listens();
                $listen->code = $request->code;
                $listen->ip = $clientIP;
                $listen->save();
                echo 'done';
            }
        } else {
            // dd('áda');
            $listen = new Albums_listens();
            $listen->code = $request->code;
            $listen->ip = $clientIP;
            $listen->save();
        }
    }

    public function countListens(Request $request)
    {
    }

    // Videos_listens
}
