<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Option;
use App\Models\Playlist;
use App\Models\Video;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use View;

class VideoController extends Controller
{
    public function __construct()
    {
        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));

        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 5"));
        }

        // dd($artistTrending);
        $artistRandom5 = Artist::limit(5)->get();
        $categories = Option::select('value')->where('key', '=', 'sort_category')->first();

        !empty($categories->value) ? $cate_value = json_decode($categories->value, true) : $cate_value = '';
        View::share(['artistTrending' => $artistTrending, 'artistRandom5' => $artistRandom5, 'categories' => $cate_value]);
    }
    public function listVideo()
    {

        return view('client.list-video');
    }

    public function detailVideo($name = null, $code)
    {
        $video = Video::where('code', $code)->where('status', '=', '1')->with('artists', 'categories', 'album')->first();
        if (empty($video)) {
            abort(404);

        }
        $video['listens'] = $video->countListen();
        $videoRandom5 = Video::where('status', '1')->whereNotIn('id', [$video->id])->inRandomOrder()->with('artists')->limit(5)->get();
        $playList4 = Playlist::inRandomOrder()->limit(4)->get();
        // dd($playList4);
        $mvArtist = [];
        $mv = [];
        // dd($video->artists);
        if (!empty($video->artists) && count($video->artists) > 0) {
            foreach ($video->artists as $key => $artist) {
                $mvArtist = $artist->with('videos')->get();
            };
            foreach ($mvArtist as $value) {
                foreach ($value->videos as $key => $value1) {
                    // dd($value1);
                    $mv[] = $value1->load('artists')->toArray();
                }
            }
        } else {
            $mv = Video::where('status', '1')->whereNotIn('id', [$video->id])->inRandomOrder()->with('artists')->limit(4)->get()->toArray();
        }

        $mv = array_unique($mv, SORT_REGULAR);
        // comment
        if (Input::has('s')) {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $video->code])->limit(10)->with('customer')->orderBy('total_like', 'desc')->get();
        } else {
            $comments = Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
                ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
                ->groupBy('comments.id')
                ->where(['parent_id' => '0', 'code' => $video->code])->limit(10)->with('customer')->orderBy('created_at', 'desc')->get();
        }

        $totalComment = Comment::select(DB::raw('COUNT(*) as total_comment'))->where('code', $video->code)->groupBy('comments.code')->first();
        // END COMMENT

        return view('client.video', compact('video', 'videoRandom5', 'mv', 'playList4', 'comments', 'totalComment'));
    }

    public function categoryVideo($slug)
    {

        $categoryCurrent = Category::where('slug', $slug)->with('videos')->first();

        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $videos = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->where('category_id', $categoryCurrent['id'])
                    ->groupBy('videos.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(48);
            } else {
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $videos = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->join('categories', 'categories.id', '=', 'videos.category_id')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('videos.code')
                    ->orderBy(DB::raw('listen'), 'desc')
                    ->paginate(48);
            };
        } else {
            abort(404);
        }

        return view('client.list-video-by-category', ['videos' => $videos, 'categoryCurrent' => $categoryCurrent]);
    }

    public function videoOrderNew($slug)
    {

        $categoryCurrent = Category::where('slug', $slug)->with('videos')->first();

        if ($categoryCurrent != null) {
            if ($categoryCurrent['parent_id'] != 0) {
                $videos = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->where('category_id', $categoryCurrent['id'])
                    ->groupBy('videos.code')
                    ->orderBy(DB::raw('created_at'), 'desc')
                    ->paginate(48);
            } else {
                $list_categories = Category::select('id as category_id')->where('id', $categoryCurrent['id'])->orWhere('parent_id', '=', $categoryCurrent['id'])->get()->toArray();
                $list_categories = array_column($list_categories, 'category_id');
                $videos = Video::select(DB::raw('videos.*,COUNT(videos.code) AS listen'))
                    ->leftJoin('videos_listens', 'videos.code', '=', 'videos_listens.code')
                    ->join('categories', 'categories.id', '=', 'videos.category_id')
                    ->whereIn('category_id', $list_categories)
                    ->groupBy('videos.code')
                    ->orderBy(DB::raw('created_at'), 'desc')
                    ->paginate(48);
            };
        } else {
            abort(404);
        }

        // dd($videos);
        return view('client.list-video-by-category', ['videos' => $videos, 'categoryCurrent' => $categoryCurrent]);
    }
}
