<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\SocialAccountService;
use Auth;
use Socialite;

class SocialAuthController extends Controller
{

    public function redirect($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function callback($social)
    {
        $user = SocialAccountService::createOrGetUser(Socialite::driver($social)->user(), $social);
        if ($user == false) {
            return redirect()->route('/')->with(['login_fail' => 'Tài khoản của bạn đã bị khóa. Liên hệ admin để biết thêm chi tiết']);
        }else{
            Auth::guard('customers')->loginUsingId($user->getAuthIdentifier());
            return redirect()->route('/');
        }

    }
}
