<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album_artist;
use App\Models\Artist;
use App\Models\Artist_song;
use App\Models\Category;
use App\Models\Lyric;
use App\Models\Song;
use App\Models\Songs_listens;
use App\Models\Tag_song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // private $_data = [];

    public function __construct()
    {

    }

    public function index()
    {
        $search = trim(Input::get('search'));
        $status = trim(Input::get('status'));
        $category_id = trim(Input::get('category'));
        $artist_id = trim(Input::get('artist'));
        $category_search = new Category();
        $artist_search = new Artist();
        if (strlen($search) > 0 || strlen($status) > 0 || strlen($category_id) > 0 || strlen($artist_id) > 0) {
            $arg = [];
            if (strlen($search) > 0) {
                $arg[] = ['name', 'LIKE', '%' . $search . '%'];
            }
            if (((int) $status === 1 || (int) $status === 0 || (int) $status === 2) && is_numeric($status)) {
                $arg[] = ['status', '=', $status];
            }
            if (is_numeric($category_id)) {
                $arg[] = ['category_id', '=', $category_id];
                $category_search = Category::where('id', '=', $category_id)->first();
                // dd($category_search);
            }

            if (is_numeric($artist_id)) {
                $artist_search = Artist::where('id', '=', $artist_id)->first();
            }

            $listSongs = Song::where($arg)->where(
                function ($query) use ($artist_id) {
                    if (is_numeric($artist_id)) {
                        $list_artist_song_id = Artist_song::select('song_id')->where('artist_id', '=', $artist_id)->get()->toArray();
                        $query->whereIn('id', $list_artist_song_id);
                    }
                    return $query;

                }
            )
                ->orderBy('id', 'desc')->paginate(100);
        } else {
            $listSongs = Song::orderBy('id', 'desc')->paginate(100);
        }
        $listSongs->load(['categories', 'albums', 'video', 'artists', 'author']);

        $listSongs->appends(['search' => $search]);

        return view('admin.songs.index', compact('listSongs', 'category_search', 'artist_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.songs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:191',
                'slug' => 'required',
                'video' => [
                    'nullable',
                    'regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/',
                ],
                'image' => [
                    'nullable',
                    'regex:/\.gif$|\.png$|\.jpg$/i',
                ],
                'lyric' => [
                    'nullable',
                    // 'regex:/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im',
                ],
                'lyric_karaoke' => [
                    'nullable',
                    'regex:/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im',
                ],
                'filename' => [
                    'nullable',
                ],
                'category_id' => [
                    'required',
                    'numeric',
                ],
                'link' => [
                    'required',
                    'regex:/\.mp3$/i',
                ],
            ],
            [
                'required' => 'Không được để trống',
                'name.max' => 'Tên bài hát tối đa 191 kí tự',
                'name.unique' => 'Tên bài hát đã tồn tại. Vui lòng chọn tên khác',
                'video.regex' => 'Không đúng định dạng link video Youtube',
                'lyric.regex' => 'Không đúng định dạng lời bài hát!',
                'lyric_karaoke.regex' => 'Không đúng định dạng lời karaoke!',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng nhập slug khác',
                'link.required' => 'Mời chọn bài hát!',
                'link.regex' => 'Sai định dạng âm thanh!',
                'image.regex' => 'Sai định dạng hình ảnh!',
                'category_id.required' => 'Mời chọn danh mục bài hát',
                'category_id.numeric' => 'Mời chọn danh mục bài hát',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            // $data = $request->except('_token');
            $song = new Song();
            // lyric
            if ((int) $request->check_change_lyric == 1 && !empty($request->lyric)) {
                $lyric = new Lyric();
                $lyric->name = $request->name;
                // $lyric->song_id = $song_id;
                $lyric->content = $request->lyric;
                $check = $lyric->save();
                $lyric_id = $lyric->id;
            } else {
                $lyric_id = $request->lyric_id;
                if (is_numeric($lyric_id) && $lyric_id > 0) {
                    $lyric = Lyric::find($lyric_id);
                    if (!empty($lyric)) {
                        $lyric_id = $lyric->id;
                    }
                }
            }

            if (!empty($request->link)) {
                $link = $request->link;
                $link_array = explode('/', $link);
                $filename = $link_array[count($link_array) - 1];
                $file_name = explode('.', $filename);
                $extension = $file_name[count($file_name) - 1];
                $name = $request->name;
                $nfn = str_replace(' ', '', ucwords(implode(' ', explode('-', to_slug($name))))) . '-' . uniqid() . '.' . $extension;
                if (Song::where('filename', '=', $request->filename)->exists()) {
                    Storage::disk('public2')->copy('uploads/files/' . $request->filename, 'uploads/files/' . $nfn);
                } else {
                    Storage::disk('public2')->move('uploads/files/' . $request->filename, 'uploads/files/' . $nfn);
                }
            }

            $song->name = $request->name;
            $song->slug = $request->slug;
            $song->description = $request->description;
            if (!empty($request->image)) {
                $song->image = trim($request->image);

            } else {
                $song->image = asset('images/song-default.png');
            }

            if (preg_match('/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im', $request->lyric_karaoke)) {
                $lyric_array = lyricToJson($request->lyric_karaoke);
                $song->lyric_karaoke = $lyric_array;
            }
            $song->filename = $nfn;
            $song->code = getToken(10);
            $song->status = $request->status;
            $song->author_id = $request->author_id;
            $song->category_id = $request->category_id;
            if (!empty($request->album_id)) {
                $song->album_id = $request->album_id;
            }
            $song->lyric_id = $lyric_id;
            $check = $song->save();
            $song_id = $song->id;

            if (!empty($request->tag) && is_array($request->tag) && count($request->tag) > 0) {
                $data_tag_song = [];
                $array_tags_id = $request->tag;
                for ($i = 0; $i < count($array_tags_id); ++$i) {
                    $data_tag_song[] = [
                        'song_id' => $song_id,
                        'tag_id' => $array_tags_id[$i],
                        'created_at' => date('Y-m-d H:i:s', time()),
                    ];
                }
                Tag_song::insert($data_tag_song);
            }
            // artist_song
            if (!empty($request->artist_id)) {
                $array_artist_id = $request->artist_id;
                if (is_array($array_artist_id) && count($array_artist_id) > 0) {
                    $data_artist_song = [];
                    for ($i = 0; $i < count($array_artist_id); ++$i) {
                        $data_artist_song[] = [
                            'song_id' => $song_id,
                            'artist_id' => $array_artist_id[$i],
                            'created_at' => date('Y-m-d H:i:s', time()),
                        ];
                        $data_artist_album = [];
                        if (!empty($request->album_id)) {
                            $data_artist_album[] = [
                                'album_id' => $request->album_id,
                                'artist_id' => $array_artist_id[$i],
                            ];

                        }

                    }
                    Artist_song::insert($data_artist_song);
                    if (!empty($request->album_id)) {
                        Album_artist::insert($data_artist_album);
                    }
                }
            }

            return response(
                [
                    'errors' => false,
                    'data' => $song,
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $song = Song::findOrFail($id);

        $song->load(['categories', 'albums', 'video', 'tags', 'artists']);
        $lyric = $song->lyric;
        $lyric_content = '';
        if (!empty($song->lyric_karaoke)) {
            $lyric_arr = json_decode($song->lyric_karaoke, true);
            $lyric_content = implode('&#13;&#10;', array_map(function ($entry) {
                return '*' . $entry['time'] . '|' . $entry['text'];
            }, $lyric_arr));
        }
        $link = Storage::disk('public2')->url($song->filename);
        $song->link = $link;
        return view('admin.songs.edit', compact('song', 'lyric_content', 'lyric'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:191',
                'slug' => 'required',
                // 'video' => [
                //     'nullable',
                //     'regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/',
                // ],
                'image' => [
                    'nullable',
                    'regex:/\.gif$|\.png$|\.jpg$/i',
                ],
                'lyric' => [
                    'nullable',
                    // 'regex:/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im',
                ],
                'lyric_karaoke' => [
                    'nullable',
                    'regex:/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im',
                ],
                'filename' => [
                    'required',
                    // 'regex:/\.mp3$/i',
                    // function ($attribute, $value, $fail) {
                    //     if (!Storage::disk('public2')->exists('uploads/files/' . $value)) {
                    //         $fail('Mời chọn lại file');
                    //     }
                    // },
                ],
                'category_id' => [
                    'required',
                    'numeric',
                ],
                'link' => [
                    'required',
                    'regex:/\.mp3$/i',
                ],
            ],
            [
                'required' => 'Không được để trống',
                'name.max' => 'Tên bài hát tối đa 191 kí tự',
                'name.unique' => 'Tên bài hát đã tồn tại. Vui lòng chọn tên khác',
                'video.regex' => 'Không đúng định dạng link video Youtube',
                'lyric.regex' => 'Không đúng định dạng lời bài hát!',
                'lyric_karaoke.regex' => 'Không đúng định dạng lời karaoke!',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng nhập slug khác',
                'link.required' => 'Mời chọn bài hát!',
                'link.regex' => 'Sai định dạng âm thanh!',
                'image.regex' => 'Sai định dạng hình ảnh!',
                'category_id.required' => 'Mời chọn danh mục bài hát',
                'category_id.numeric' => 'Mời chọn danh mục bài hát',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            // $data = $request->except('_token');
            $song = Song::find($request->id);

            // lyric
            $lyric_id = 0;
            if ((int) $request->check_change_lyric == 1 && !empty($request->lyric) && !empty($request->lyric_id)) {
                $lyric = Lyric::findOrFail($request->lyric_id);
                $lyric->name = $request->name;
                // $lyric->song_id = $song_id;
                $lyric->content = $request->lyric;
                $check = $lyric->save();
                $lyric_id = $lyric->id;
            } else {
                if (is_numeric($request->lyric_id) && $request->lyric_id > 0) {
                    $lyric_id = $request->lyric_id;
                }
            }

            //video
            $video_id = null;
            // if ((int) $request->check_change_video == 1 && preg_match('/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/', $request->video)) {
            //     $link_video = $request->video;
            //     $video = new Video();
            //     $video->name = $request->name;
            //     $video->link = $link_video;
            //     $video->code = getToken(10);
            //     $video->slug = $request->slug;
            //     $video->description = $request->description;
            //     $video->status = $request->status;
            //     $video->lyric_id = $lyric_id;
            //     // song_video
            //     $check1 = $video->save();
            //     $video_id = $video->id;
            //     if ($check1) {
            //         $song->video_id = $video_id;
            //     }
            // } else {
            //     $video_id = $request->video_id;
            //     if ($video_id != $song->video_id) {
            //         $song->video_id = $video_id;
            //     }
            // }

            if (!empty($request->link)) {
                $link = $request->link;
                $link_array = explode('/', $link);
                $filename = $link_array[count($link_array) - 1];
                $file_name = explode('.', $filename);
                $extension = $file_name[count($file_name) - 1];
                $name = $request->name;
                $nfn = str_replace(' ', '', ucwords(implode(' ', explode('-', to_slug($name))))) . '-' . uniqid() . '.' . $extension;
                if ($request->filename != $nfn) {
                    if (Song::where([
                        ['filename', '=', $request->filename],
                        ['id', '<>', $request->id],
                    ])->exists()) {
                        Storage::disk('public2')->copy('uploads/files/' . $request->filename, 'uploads/files/' . $nfn);
                    } else {
                        if (Song::where([
                            ['filename', '!=', $request->filename],
                            ['id', '=', $request->id],
                        ])->exists()) {
                            Storage::disk('public2')->move('uploads/files/' . $request->filename, 'uploads/files/' . $nfn);
                            $song->filename = $nfn;

                        }

                    }
                }
            }

            $song->name = $request->name;
            $song->slug = $request->slug;
            $song->description = $request->description;
            $song->image = trim($request->image);
            $song->status = $request->status;
            $song->category_id = $request->category_id;
            $song->album_id = $request->album_id;
            if (preg_match('/^\*[0-9]:[0-9]+(\.[0-9]+)?\|.*/im', $request->lyric_karaoke) && !empty($request->lyric_karaoke)) {
                $lyric_array = lyricToJson($request->lyric_karaoke);
                $song->lyric_karaoke = $lyric_array;
            }
            $song->lyric_id = $lyric_id;
            $check = $song->save();
            $song_id = $song->id;

            // if (!empty($request->tag) && is_array($request->tag) && count($request->tag) > 0) {
            //     $data_tag_song = [];
            //     $array_tags_id = $request->tag;
            //     for ($i = 0; $i < count($array_tags_id); ++$i) {
            //         $data_tag_song[] = [
            //             'song_id' => $song_id,
            //             'tag_id' => $array_tags_id[$i],
            //             'created_at' => date('Y-m-d H:i:s', time()),
            //         ];
            //     }
            //     $song->tags()->sync($array_tags_id);
            //     Tag_song::insert($data_tag_song);
            // }
            // artist_song
            $array_artist_id = $request->artist_id;

            if (is_array($array_artist_id) && count($array_artist_id) > 0) {
                $data_artist_song = [];
                for ($i = 0; $i < count($array_artist_id); ++$i) {
                    $data_artist_song[] = [
                        'song_id' => $song_id,
                        'artist_id' => $array_artist_id[$i],
                        'created_at' => date('Y-m-d H:i:s', time()),
                    ];
                    if (!empty($request->album_id)) {
                        $data_artist_album[] = [
                            'album_id' => $request->album_id,
                            'artist_id' => $array_artist_id[$i],
                        ];
                    }
                }
                $song->artists()->sync($array_artist_id);
                if (!empty($request->album_id)) {
                    if (!empty($request->album_id)) {
                        Album_artist::insert($data_artist_album);
                    }
                }
            }

            return response(
                [
                    'errors' => false,
                    'data' => $song,
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $song = Song::find((int) $request->id);
        $code = $song->code;
        $song->delete();
        $song->artists()->detach();
        $song->playlists()->detach();
        $song->tags()->detach();
        $song_listen = Songs_listens::where('code', '=', $code)->delete();
        Storage::disk('public3')->delete($song->filename);
        return response(['errors' => false]);
    }
    // public function dellAll()
    // {
    //     $songs = Song::all();
    // }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function multiDel(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Song::destroy($arr);

        if ($check) {
            $arr_filename = Song::select('songs.filename')->whereIn('id', $arr)->get();
            $arr_filename = array_column($arr_filename->toArray(), 'filename');
            // dd($arr_filename);
            Storage::disk('public3')->delete($arr_filename);
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr_filename,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function setActiveStatus(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Song::whereIn('id', $arr)->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function setActiveStatusAll(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Song::where('status', '=', '-1')->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
}
