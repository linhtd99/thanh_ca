<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Posts;
use Yajra\Datatables\Datatables;
use Validator;

class PostController extends Controller
{
    public function index()
    {
        return view('admin.posts.index');
    }
    public function data()
    {
        $posts = Posts::all()->sortByDesc('created_at');
        // dd($posts);
        return Datatables::of($posts)->make(true);
    }

    public function add()
    {
        return view('admin.posts.add');
    }

    public function saveAdd(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:posts,name',
                'slug' => 'required|unique:posts,slug'
            ],
            [
                'required' => 'Không được để trống',
                'name.unique' => 'Tên bài viết đã tồn tại. Vui lòng điền tên bài viết khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác',
            ]
        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $data = $request->except('_token');

            $data['created_at'] = date('Y-m-d H:i:s', time());
            // dd($data);
            $result = Posts::insert($data);
            if ($result == 1) {
                return redirect()->route('admin.posts.index')->with('success', 'Thêm bài viết thành công');
            } else {
                return redirect()->route('admin.posts.index')->with('error', 'Thêm bài viết không thành công');
            }
        }
    }

    public function edit($id)
    {
        $post = Posts::find($id);
        return view('admin.posts.edit', ['post' => $post]);
    }

    public function saveEdit(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:posts,name,' . $request->id,
                'slug' => 'required|unique:posts,slug,' . $request->id,
            ],
            [
                'required' => 'Không được để trống',
                'name.unique' => 'Tên bài viết đã tồn tại. Vui lòng điền tên bài viết khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác',
            ]

        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $data = $request->except('_token', 'id');
            $data['updated_at'] = date('Y-m-d H:i:s', time());
            $result = Posts::where('id', $request->id)->update($data);
            if ($result == 1) {
                return redirect()->route('admin.posts.index')->with('success', 'Sửa bài viết thành công');
            } else {
                return redirect()->route('admin.posts.index')->with('error', 'Sửa bài viết không thành công');
            }
        }
    }

    public function delete($id)
    {
        $result = Posts::where('id', $id)->delete();
        if ($result == 1) {
            return redirect()->route('admin.posts.index')->with('success', 'Xóa bài viết thành công');
        } else {
            return redirect()->route('admin.posts.index')->with('error', 'Có lỗi xảy ra');
        }
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Posts::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }
}
