<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

// use App\Globals;

class CategoryController extends Controller
{
    private $_data;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    public function dataCategories()
    {
        $categories = Category::all();

        $c = $this->categoriesRecursive($categories);
        // dd($c);
        return Datatables::of($c)->make(true);
    }

    public function dataCategories_select2(Request $request)
    {
        $data = [];
        $term = trim($request->q);
        // dd($term);

        if (strlen($term) <= 0) {
            $data = Category::all();
            // dd($data);
            return response()->json($data);
        } else {
            $data = Category::where('name', 'LIKE', "%$term%")
                ->orderBy('name', 'desc')
                ->get();

            return response()->json($data, 2000);
        }

        // $c = $this->categoriesRecursive($categories);
    }

    public function dataCategories_select2_pa(Request $request)
    {
        $data = [];
        $term = trim($request->q);
        // dd($term);

        if (strlen($term) <= 0) {
            $data = Category::where('parent_id', '=', 0)->get();
            // dd($categories);
            return response()->json($data);
        } else {
            $data = Category::where('name', 'LIKE', "%$term%")->where('parent_id', '=', 0)
                ->orderBy('name', 'desc')
                ->get();
            return response()->json($data);
        }

        // $c = $this->categoriesRecursive($categories);
    }

    public function dataCategories_select2_ch(Request $request)
    {
        $data = [];
        $term = trim($request->q);
        dd($term);

        if (strlen($term) <= 0) {
            $data = Category::where('parent_id', '<>', 0)->get();
            // dd($categories);
            return response()->json($data);
        } else {
            $data = Category::where('name', 'LIKE', "%$term%")->where('parent_id', '<>', 0)
                ->orderBy('name', 'desc')
                ->get();
            return response()->json($data);
        }

        // $c = $this->categoriesRecursive($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:100|unique:categories,name',
                'slug' => 'required|unique:categories,slug',
            ],
            [
                'name.required' => 'Tên danh mục không được để trống',
                'name.unique' => 'Tên danh mục đã tồn tại',
                'slug.unique' => 'Slug đã tồn tại',
                'slug.required' => 'Slug không được để trống',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token');
            $category = new Category();
            $category->name = $request->name;
            $category->slug = $request->slug;
            $category->parent_id = (int) $request->parent_id;
            $category->description = $request->description;
            $category->save();
            // Category::insert($data);

            return response(
                [
                    'errors' => false,
                    'data' => $category,
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if ($category->parent_id != 0) {
            $pa = Category::select('name')->where('id', $category->parent_id)->first();
            $category->pn = $pa->name;
        } else {
            $category->pn = '-- không --';
        }

        return response($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $validate = Validator::make(
            $request->all(),
            [
                'name' => [
                    'required',
                    'unique:categories,name,' . $request->id,
                ],
                'slug' => [
                    'required',
                    'unique:categories,slug,' . $request->id,
                ],
            ],
            [
                'name.required' => 'Tên danh mục không được để trống',
                'name.unique' => 'Tên danh mục đã tồn tại',
                'slug.unique' => 'Slug đã tồn tại',
                'slug.required' => 'Slug không được để trống',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token', 'id');
            $check = Category::where('id', $request->id)->update($data);

            // dd($check);
            return response(
                [
                    'errors' => false,
                    'data' => $check,
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $category = Category::find($id);
        $check = $category->delete();
        if ($check) {
            return response(
                [
                    'errors' => false,
                    'data' => $id,
                ]
            );
        } else {
            return response(
                [
                    'errors' => true,
                    'data' => $id,
                ]
            );
        }
    }

    // đệ quy category
    public function categoriesRecursive($categories, $parent_id = 0, $step = 0)
    {
        foreach ($categories as $key => $item) {
            if ((int) $item['parent_id'] == $parent_id) {
                // dd($item);
                $c = $item;
                $category = Category::find($item['parent_id']);
                $c['step'] = $step;
                $c['parent_name'] = $category['name'];
                // $c['position'] = $category['position'];
                $this->_data['categories'][] = $c;
                unset($categories[$key]);
                $this->categoriesRecursive($categories, (int) $item['id'], $step + 1);
            }
        }

        return $this->_data['categories'];
    }
}
