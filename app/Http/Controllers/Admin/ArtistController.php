<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use DateTime;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.artists.index');
    }

    public function data()
    {
        $artist = Artist::all();

        return Datatables::of($artist)->make(true);
    }

    public function add()
    {
        return view('admin.artists.add');
    }

    public function saveAdd(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:artists,name',
                'slug' => 'required|unique:artists,slug',
            ],
            [
                'required' => 'Không được để trống',
                'name.unique' => 'Nghệ danh đã tồn tại. Vui lòng điền nghệ danh khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác',
            ]
        );
        if ($validate->fails()) {
            if ($request->type == 'ajax') {
                return response(
                    [
                        'errors' => true,
                        'data' => $validate->errors(),
                    ]
                );
            } else {
                return redirect()->back()->withInput()->withErrors($validate->errors());
            }
        } else {
            $type = $request->type;
            $data = $request->except('_token', 'type');
            if ($request->birthday != null) {
                $myDateTime = DateTime::createFromFormat('d/m/Y', $request->birthday);
                $data['birthday'] = $myDateTime->format('Y-m-d');
            }
            if ($request->birthday == null) {
                $data['gender'] = '-1';
            }
            $data['slug'] = name_to_slug($data['slug']);
            // dd($data);
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $result = Artist::insert($data);
            if ($type == 'ajax') {
                return response(
                    [
                        'errors' => false,
                        'data' => true,
                    ]
                );
            } else {
                if ($result == 1) {
                    return redirect()->route('admin.artists.index')->with('success', 'Thêm nghệ sĩ thành công');
                } else {
                    return redirect()->route('admin.artists.index')->with('error', 'Thêm nghệ sĩ không thành công');
                }
            }

        }
    }

    public function edit($id)
    {
        $artist = Artist::find($id);

        return view('admin.artists.edit', ['artist' => $artist]);
    }

    public function saveEdit(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:artists,name,' . $request->id,
                'slug' => 'required|unique:artists,slug,' . $request->id,
            ],
            [
                'required' => 'Không được để trống.',
                'name.unique' => 'Nghệ danh đã tồn tại. Vui lòng điền nghệ danh khác.',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác.',
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $data = $request->except('_token', 'id');

            if ($request->birthday != null) {
                $myDateTime = DateTime::createFromFormat('d/m/Y', $request->birthday);
                $data['birthday'] = $myDateTime->format('Y-m-d');
            }
            if ($request->birthday == null) {
                $data['gender'] = -1;
            }
            $data['slug'] = name_to_slug($data['slug']);
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $result = Artist::where('id', $request->id)->update($data);

            if ($result == 1) {
                return redirect()->route('admin.artists.index')->with('success', 'Thêm nghệ sĩ thành công');
            } else {
                return redirect()->route('admin.artists.index')->with('error', 'Thêm nghệ sĩ không thành công');
            }
        }
    }

    public function delete($id)
    {
        $result = Artist::where('id', $id)->delete();
        if ($result == 1) {
            return redirect()->route('admin.artists.index')->with('success', 'Xoá nghệ sĩ thành công');
        } else {
            return redirect()->route('admin.artists.index')->with('error', 'Xoá nghệ sĩ không thành công');
        }
    }

    /**
     * data.
     */
    public function dataSelect2(Request $request)
    {
        $term = trim($request->q);
        $data = [];

        if (strlen($term) <= 2) {
            $artists = Artist::orderBy('created_at', 'desc')->limit(20)->get();
            // dd($artists);
            $data = $artists->toArray();

            return response()->json($data);
        } else {
            $artists = Artist::where('name', 'LIKE', "%$term%")
                ->orderBy('name', 'desc')
                ->get();
            foreach ($artists as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        }
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Artist::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }
}
