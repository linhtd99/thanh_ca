<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Category;
use App\Models\Topic;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    private $_data;

    public function _construct()
    {
    }

    public function general()
    {
        $options = Option::where('key', '=', 'generals')->first();
        $topic = Topic::orderBy('created_at', 'desc')->get();
        // dd($options);

        return view('admin.options.generals', ['options' => json_decode($options['value']), 'topic' => $topic]);
    }

    public function save_general(Request $request)
    {
        $arr['name'] = $request->name;
        $arr['logo'] = $request->image;
        $arr['slideshow'] = $request->slide;
        $arr['topic'] = $request->topic;
        // dd($request->all());
        if (!empty($arr)) {
            $options = Option::where('key', '=', 'generals')->update(['value' => json_encode($arr)]);
        } else {
            $options = Option::where('key', '=', 'generals')->update(['value' => '']);
        }

        if ($options == 1) {
            return redirect()->route('admin.options.generals')->with('success', 'Chỉnh sửa thành công');
        } else {
            return redirect()->route('admin.options.generals')->with('fail', 'Chỉnh sửa không thành công');
        }
    }

    public function menu_footer()
    {
        $options = Option::where('key', '=', 'menu_footer')->first();

        return view('admin.options.menu-footer', ['options' => json_decode($options['value'])]);
    }

    public function todaylisten()
    {
        $topics = Topic::orderBy('created_at', 'desc')->get();
        // dd($topics);

        return view('admin.options.todaylisten', compact('topics'));
    }

    public function save_menu_footer(Request $request)
    {
        foreach ($request->footer_title as $key => $rq) :
            $arr[0][] = array(
                'title' => $rq,
                'content' => $request->footer_content[$key],
            );
        endforeach;

        $arr[1] = array(
                'title' => $request->footer_title4,
                'content' => $request->footer_content4,
            );

        $arr[2] = array(
                'title' => $request->footer_title5,
                'link_down1' => $request->link_down1,
                'link_down2' => $request->link_down2,
            );

        $arr[3] = $request->info;

        if (!empty($arr)) {
            $options = Option::where('key', '=', 'menu_footer')->update(['value' => json_encode($arr)]);
        } else {
            $options = Option::where('key', '=', 'menu_footer')->update(['value' => '']);
        }
        if ($options == 1) {
            return redirect()->route('admin.options.menu_footer')->with('success', 'Chỉnh sửa thành công');
        } else {
            return redirect()->route('admin.options.menu_footer')->with('fail', 'Chỉnh sửa không thành công');
        }
    }

    public function sort_category()
    {
        $options = Option::where('key', '=', 'sort_category')->first();
        $c = Category::all();
        $categories = $this->categoriesRecursive($c);
        // dd($categories);
        return view('admin.options.sort_category', compact('options', 'categories'));
    }

    public function save_sort_category(Request $request)
    {
        $data = $request->except('_token');

        if (!empty($data['outer_list'])) {
            $options = Option::where('key', '=', 'sort_category')->update(['value' => json_encode($data['outer_list'])]);
        } else {
            $options = Option::where('key', '=', 'sort_category')->update(['value' => '']);
        }
        if ($options == 1) {
            return redirect()->route('admin.options.sort_category')->with('success', 'Sắp xếp danh mục thành công');
        } else {
            return redirect()->route('admin.options.sort_category')->with('fail', 'Sắp xếp danh mục không thành công');
        }
    }

    public function categoriesRecursive($categories, $parent_id = 0, $step = 0)
    {
        foreach ($categories as $key => $item) {
            if ((int) $item['parent_id'] == $parent_id) {
                // dd($item);
                $c = $item;
                $category = Category::find($item['parent_id']);
                $c['step'] = $step;
                $c['parent_name'] = $category['name'];
                // $c['position'] = $category['position'];
                $this->_data['categories'][] = $c;
                unset($categories[$key]);
                $this->categoriesRecursive($categories, (int) $item['id'], $step + 1);
            }
        }

        return $this->_data['categories'];
    }

    public function choose_category_bxh()
    {
        $options = Option::where('key', '=', 'choose_category_bxh')->first();
        $categories = Category::all();

        return view('admin.options.choose_category_bxh', ['categories' => $categories, 'options' => $options]);
    }

    public function save_choose_category(Request $request)
    {
        $data = $request->except('_token');
        if (!empty($data['categories'])) {
            $options = Option::where('key', '=', 'choose_category_bxh')->update(['value' => json_encode($data['categories'])]);
        } else {
            $options = Option::where('key', '=', 'choose_category_bxh')->update(['value' => '']);
        }
        if ($options == 1) {
            return redirect()->route('admin.options.choose_category_bxh')->with('success', 'Chọn danh mục thành công');
        } else {
            return redirect()->route('admin.options.choose_category_bxh')->with('fail', 'Chọn danh mục không thành công');
        }
    }

    public function save_choose_category2(Request $request)
    {
        $data = $request->except('_token');
        if (!empty($data['categories'])) {
            $options = Option::where('key', '=', 'choose_category_bxh')->update(['value' => strip_tags($data['categories'])]);
        }
        if ($options == 1) {
            return response()->json($data = [
                'error' => false,
            ]);
        }
    }
}
