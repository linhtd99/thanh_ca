<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lyric;
use Illuminate\Http\Request;

class LyricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.lyrics.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function dataSelect2(Request $request)
    {
        $term = trim($request->q);
        $data = [];

        if (strlen($term) <= 2) {
            $lyrics = Lyric::limit(20)->orderBy('created_at', 'desc')->get();
            foreach ($lyrics as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        } else {
            $lyrics = Lyric::where('name', 'LIKE', "%$term%")
                ->orderBy('name', 'desc')
                ->get();
            foreach ($lyrics as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        }
    }
}
