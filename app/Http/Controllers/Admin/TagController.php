<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

class TagController extends Controller
{
    private $_data;
    public function __construct()
    {
    }

    public function index()
    {
        return view('admin.tags.index');
    }

    public function create()
    {
    }

    public function edit($id)
    {
        $tag = Tag::findOrFail($id);

        return view('admin.tags.edit', compact('tag'));

    }
    public function update(Request $request, $id)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'title' => [
                    'required',
                    'max:190',
                    // 'unique:tags,title,' . $id,
                ],
                'slug' => [
                    'required',
                    'max:190',
                    // 'unique:tags,slug,' . $id,
                ],
                'category_id' => [
                    'nullable',
                    'numeric',
                ],

            ],
            [
                'required' => 'Không được để trống.....',
                'name.max' => 'Tên tag tối đa 191 kí tự',
                'name.unique' => 'Tên tag đã tồn tại. Vui lòng chọn tên khác',
                'video.regex' => 'Không đúng định dạng link video Youtube',
                'lyric.regex' => 'Không đúng định dạng lời tag!',
                'lyric_karaoke.regex' => 'Không đúng định dạng lời karaoke!',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng nhập slug khác',
                'title.unique' => 'Tên tag đã tồn tại. Vui lòng nhập tag khác',
                'image.regex' => 'Sai định dạng hình ảnh!',
                'parent_id.required' => 'Mời chọn danh mục tag',
                'parent_id.numeric' => 'Mời chọn danh mục tag',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        }

        $tag = Tag::findOrFail($id);
        $tag->title = $request->title;
        $tag->slug = $request->slug;
        $tag->parent_id = $request->parent_id;
        $tag->save();
        return response([
            'errors' => false,
        ]);

    }
    public function store(Request $request)
    {

        $validate = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:191|unique:tags,title',
                'slug' => 'required|unique:tags,slug',
                'category_id' => [
                    'nullable',
                    'numeric',
                ],

            ],
            [
                'required' => 'Không được để trống',
                'name.max' => 'Tên tag tối đa 191 kí tự',
                'name.unique' => 'Tên tag đã tồn tại. Vui lòng chọn tên khác',
                'video.regex' => 'Không đúng định dạng link video Youtube',
                'lyric.regex' => 'Không đúng định dạng lời tag!',
                'lyric_karaoke.regex' => 'Không đúng định dạng lời karaoke!',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng nhập slug khác',
                'image.regex' => 'Sai định dạng hình ảnh!',
                'parent_id.required' => 'Mời chọn danh mục tag',
                'parent_id.numeric' => 'Mời chọn danh mục tag',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        }

        $tag = new Tag();
        $tag->title = $request->title;
        $tag->slug = $request->slug;
        $tag->parent_id = $request->parent_id;
        $tag->save();

    }

    public function saveNewTags(Request $request)
    {
        $tags = $request->tags;
        $tags = explode(',', $tags);
        $data = [];
        for ($i = 0; $i < count($tags); ++$i) {
            if (!Tag::where('title', '=', $tags[$i])->exists()) {
                $time = time();
                $time = (float) $time++;
                $data[$i]['title'] = trim($tags[$i]);
                $data[$i]['slug'] = $this->to_slug(trim($tags[$i]));
                $data[$i]['created_at'] = date('Y-m-d H:i:s', $time);
            }
        }
        $check = false;
        if (count($data) > 0) {
            $check = Tag::insert($data);
        }

        return response()->json($check);
    }

    public function dataSelect2(Request $request)
    {
        $tags = [];
        $term = trim($request->q);
        if (strlen($term) == 0) {
            $tags = Tag::where('parent_id', '=', 0)->limit(20)->orderBy('id', 'desc')->get();
            // dd($tags);
            // foreach ($tags as $key) {
            //     $data[] = ['id' => $key->id, 'title' => $key->title];
            // }
        } elseif (strlen($term) > 1) {
            $tags = Tag::where('parent_id', '=', 0)->where('title', 'LIKE', "%$term%")
                ->orderBy('id', 'desc')
                ->get();
            // foreach ($tags as $key) {
            //     $data[] = ['id' => $key->id, 'title' => $key->title];
            // }
        }
        return response()->json($tags);
    }

    public function dataSelect2_ch(Request $request)
    {
        $tags = [];
        $term = trim($request->keysearch);
        // dd($term);
        if (strlen($term) == 0) {
            $tags = Tag::where('parent_id', '<>', 0)->limit(20)->orderBy('id', 'desc')->get();
            // dd($tags);
            // foreach ($tags as $key) {
            //     $data[] = ['id' => $key->id, 'title' => $key->title];
            // }
        } elseif (strlen($term) > 0) {
            $tags = Tag::where('parent_id', '<>', 0)->where('title', 'LIKE', "%$term%")
                ->orderBy('id', 'desc')
                ->limit(100)
                ->get();
            // foreach ($tags as $key) {
            //     $data[] = ['id' => $key->id, 'title' => $key->title];
            // }
        }
        return response()->json($tags);
    }

    public function dataTable(Request $request)
    {
        $categories = Tag::all();

        $c = $this->categoriesRecursive($categories);
        // dd($c);
        return Datatables::of($c)->make(true);

    }

    public function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;
    }

    // đệ quy category
    public function categoriesRecursive($categories, $parent_id = 0, $step = 0)
    {
        foreach ($categories as $key => $item) {
            if ((int) $item['parent_id'] == $parent_id) {
                // dd($item);
                $c = $item;
                $category = Tag::find($item['parent_id']);
                $c['step'] = $step;
                $c['parent_title'] = $category['title'];
                // $c['position'] = $category['position'];
                $this->_data['categories'][] = $c;
                unset($categories[$key]);
                $this->categoriesRecursive($categories, (int) $item['id'], $step + 1);
            }
        }

        return $this->_data['categories'];
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $tag = Tag::find($id);
        $parent_id = $tag->parent_id;
        $check = $tag->delete();
        if ($check) {
            if ($parent_id == 0) {
                Tag::where('parent_id', '=', $id)->update(['parent_id' => 0]);
            }
            return response(
                [
                    'errors' => false,
                    'data' => $id,
                ]
            );
        } else {
            return response(
                [
                    'errors' => true,
                    'data' => $id,
                ]
            );
        }
    }
}
