<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tracker;
use App\Models\Song;
use App\Models\Customer;
use DB;
class HomeController extends Controller
{
    private $_data = [];

    public function __construct()
    {
    }

    public function index()
    {
        $total_download = Song::select(DB::raw('SUM(count_downloads) as total_download'))->first();
        $total_traffic = Tracker::select(DB::raw('SUM(hits) as total_traffic'))->first();
        $total_customer = Customer::select(DB::raw('count(*) as total'))->first();
        $total_song = Song::select(DB::raw('count(*) as total'))->first();
        return view('admin.home', [
            'total_download' => $total_download->total_download ?? '0',
            'total_traffic' => $total_traffic->total_traffic  ?? '0',
            'total_customer' => $total_customer->total ?? '0',
            'total_song' => $total_song->total ?? '0',
        ]);
    }
}
