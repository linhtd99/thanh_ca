<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Input::get('type') == 'song') {
            $comments = Comment::select(DB::raw('COUNT(like_comment.comment_id) AS total,comments.*'))
                ->leftJoin('like_comment', 'comments.id', '=', 'like_comment.comment_id')
                ->join('songs', 'songs.code', '=', 'comments.code')
                ->groupBy('comments.id')
                ->orderBy('comments.created_at', 'desc')
                ->paginate(15);
        } elseif (Input::get('type') == 'video') {
            $comments = Comment::select(DB::raw('COUNT(like_comment.comment_id) AS total,comments.*'))
                ->leftJoin('like_comment', 'comments.id', '=', 'like_comment.comment_id')
                ->join('videos', 'videos.code', '=', 'comments.code')
                ->groupBy('comments.id')
                ->orderBy('comments.created_at', 'desc')
                ->paginate(15);
        } elseif (Input::get('type') == 'playlist') {
            $comments = Comment::select(DB::raw('COUNT(like_comment.comment_id) AS total,comments.*'))
                ->leftJoin('like_comment', 'comments.id', '=', 'like_comment.comment_id')
                ->join('playlists', 'playlists.code', '=', 'comments.code')
                ->groupBy('comments.id')
                ->orderBy('comments.created_at', 'desc')
                ->paginate(15);
        } elseif (Input::get('type') == 'bxh-video') {
            $comments = Comment::select(DB::raw('COUNT(like_comment.comment_id) AS total,comments.*'))
                ->leftJoin('like_comment', 'comments.id', '=', 'like_comment.comment_id')
                ->where('comments.code', 'LIKE', '%video.%')
                ->groupBy('comments.id')
                ->orderBy('comments.created_at', 'desc')
                ->paginate(15);
        } else {
            abort(404);
        }

        return view('admin.comments_reports.comments', compact('comments'));
    }

    public function updateStatus(Request $request)
    {
        // dd($request->val);
        Comment::where('id', $request->id)->update(['status' => $request->val]);
    }

    public function accept(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = Comment::whereIn('id', $arr)->update(['status' => 1]);
        if ($check) {
            return response()
                ->json([
                    'error' => false,
                    'message' => 'success',
                    'data' => $arr,
                ]);
        } else {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Error',
                    'data' => $arr,
                ]);
        }
        ;

    }

    public function acceptAll(Request $request)
    {
        $check = Comment::where('status', '<>', 1)->update(['status' => 1]);
        if ($check) {
            return response()
                ->json([
                    'error' => false,
                    'message' => 'success',
                    'data' => $arr,
                ]);
        } else {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Error',
                    'data' => $arr,
                ]);
        }
        ;

    }

    public function destroy(Request $request)
    {
        Comment::where('id', $request->id)->delete();
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = Comment::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Error',
                'data' => $arr,
            ]);
        };
    }
}
