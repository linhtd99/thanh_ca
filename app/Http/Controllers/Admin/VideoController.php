<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Video;
use App\Models\Videos_listens;
use App\Models\Video_artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = trim(Input::get('search'));
        $status = trim(Input::get('status'));
        if (strlen($search) > 0 || strlen($status) > 0) {
            $arg = [];
            if (strlen($search) > 0) {

                $arg[] = ['name', 'LIKE', '%' . $search . '%'];
            };
            if (((int) $status == 1 || (int) $status == 0) && is_numeric($status)) {
                // dd('aa');
                $arg[] = ['status', '=', $status];
            }
            // dd($arg);
            $videos = Video::where($arg)
                ->orderBy('id', 'desc')
                ->paginate(20);
        } else {

            $videos = Video::with('artists', 'categories', 'album')->orderBy('id', 'desc')->paginate(20);
        }
        return view('admin.videos.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videos.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'link' => [
                    'nullable',
                    'regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/',

                ],
                'image' => [
                    'nullable',
                    'regex:/\.gif$|\.png$|\.jpg$/i',
                ],
                'video' => [
                    'nullable',
                    'regex:/\.mp4$/i',
                ],
            ],
            [
                'name.required' => 'Không được để trống',
                'link.required' => 'Không được để trống',
                'link.regex' => 'Chưa đúng định dạng link video Youtube',
                'image.regex' => 'Chưa đúng định dạng ảnh',
                'video.regex' => 'Chưa đúng định dạng video',
            ]
        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            // dd($request->all());
            $video = new Video();

            if ($request->karaoke == 'on') {
                $video->karaoke_type = 1;

            } else {
                $video->karaoke_type = 0;
            }
            if ($request->beat == 'on') {
                $video->beat_type = 1;
            } else {
                $video->beat_type = 0;
            }
            $video->code = getToken(12);
            $video->album_id = $request->album_id;
            $video->category_id = $request->category_id;
            $video->status = $request->status;
            $video->description = $request->description;
            $video->image = $request->image;
            $video->video = $request->video;
            $video->author_id = $request->author_id;
            $video->link = $request->link;
            $video->name = $request->name;
            // dd($video);
            $result = $video->save();
            if (is_array($request->artist_id) && count($request->artist_id) > 0) {
                foreach ($request->artist_id as $value) {
                    Video_artist::insert([
                        'video_id' => $video->id,
                        'artist_id' => $value,
                    ]);
                }
            }

            if ($result) {
                return redirect()->route('admin.videos.index')->with('success', 'Thêm video thành công');
            } else {
                return redirect()->route('admin.videos.index')->with('error', 'Thêm video không thành công');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        $video = $video->load('categories', 'artists', 'album');
        return view('admin.videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'link' => [
                    'required',
                    'regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/',
                ],
                'image' => [
                    'nullable',
                    'regex:/\.gif$|\.png$|\.jpg$/i',
                ],
                'video' => [
                    'nullable',
                    'regex:/\.mp4$/i',
                ],

            ],
            [
                'name.required' => 'Không được để trống',
                'link.required' => 'Không được để trống',
                'link.regex' => 'Chưa đúng định dạng link video Youtube',
                'image.regex' => 'Chưa đúng định dạng ảnh',
                'video.regex' => 'Chưa đúng định dạng video',
            ]
        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $video = Video::find($id);
            if ($request->karaoke == 'on') {
                $video->karaoke_type = 1;

            } else {
                $video->karaoke_type = 0;
            }
            if ($request->beat == 'on') {
                $video->beat_type = 1;
            } else {
                $video->beat_type = 0;
            }
            $video->code = getToken(12);
            $video->album_id = $request->album_id;
            $video->category_id = $request->category_id;
            $video->status = $request->status;
            $video->description = $request->description;
            $video->image = $request->image;
            $video->video = $request->video;
            $video->author_id = $request->author_id;
            $video->link = $request->link;
            $video->name = $request->name;
            $video->updated_at = date('Y-m-d H:i:s', time());
            $result = $video->save();

            if ($request->artist_id == null) {
                Video_artist::where('video_id', $id)->delete();
            };

            if (is_array($request->artist_id) && count($request->artist_id) > 0) {
                Video_artist::where('video_id', $id)->delete();
                foreach ($request->artist_id as $value) {
                    Video_artist::insert([
                        'video_id' => $id,
                        'artist_id' => $value,
                    ]);
                }
            }

            if ($result) {
                return redirect()->route('admin.videos.index')->with('success', 'Sửa video thành công');
            } else {
                return redirect()->route('admin.videos.index')->with('error', 'Sửa video không thành công');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $video = Video::find($request->id);
        $code = $video->code;
        $video->delete();
        $video->artists()->detach();
        Videos_listens::where('code', '=', $code)->delete();
        return response(['errors' => false]);
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = Video::destroy($arr);
        // Videos_listens::where('code', '=', $code)->delete();
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };

    }
    public function dataSelect2(Request $request)
    {
        $term = trim($request->q);
        $data = [];
        // dd($term);
        if (strlen($term) < 3) {
            $videos = Video::limit(10)->orderBy('created_at', 'desc')->get();
            // dd($videos);
            foreach ($videos as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        } elseif (strlen($term) > 2) {
            $videos = Video::where('name', 'LIKE', "%$term%")
                ->orderBy('created_at', 'desc')
                ->get();
            foreach ($videos as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        }
    }

    public function custom()
    {
        return view('admin.videos.custom');
    }
    public function setActiveStatusAll(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Video::where('status', '<>', '1')->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
    public function multiDel(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Video::destroy($arr);

        if ($check) {
            $arr_filename = Video::select('songs.filename')->whereIn('id', $arr)->get();
            $arr_filename = array_column($arr_filename->toArray(), 'filename');
            // dd($arr_filename);
            Storage::disk('public3')->delete($arr_filename);
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr_filename,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
    public function setActiveStatus(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Video::whereIn('id', $arr)->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
}
