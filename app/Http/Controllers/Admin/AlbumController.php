<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album;
use Illuminate\Http\Request;
// use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Validator;

class AlbumController extends Controller
{
    public function index()
    {
        $search = trim(Input::get('search'));
        if ($search != '') {
            $data = Album::where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('id', '=', $search)
                ->orderBy('id', 'desc')
                ->paginate(20);
            $data->appends(['search' => $search]);
        } else {
            $data = Album::orderBy('id', 'desc')->paginate(20);
        }

        return view('admin.albums.index', ['listAlbums' => $data]);
    }

    // public function data()
    // {
    //     $data = Album::all();
    //     return Datatables::of($data)->make(true);
    // }

    public function dataSelect2(Request $request)
    {
        $term = trim($request->q);
        $data = [];
        // dd($term);

        if (strlen($term) <= 2) {
            $albums = Album::limit(20)->orderBy('created_at', 'desc')->get();
            // dd($albums);
            foreach ($albums as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        } else {
            $albums = Album::where('name', 'LIKE', "%$term%")
                ->orderBy('created_at', 'desc')
                ->get();
            foreach ($albums as $key) {
                $data[] = ['id' => $key->id, 'name' => $key->name];
            }

            return response()->json($data);
        }
    }

    public function saveAdd(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:100',
                'slug' => 'required|unique:albums,slug',
            ],
            [
                'required' => 'Không được để trống',
                'name.max' => 'Tên album tối đa 100 kí tự',
                'name.unique' => 'Tên album đã tồn tại. Vui lòng chọn tên khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng chọn slug khác',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token');
            $data['code'] = getToken(12);
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $result = Album::insert($data);

            return response(
                [
                    'errors' => false,
                    'data' => $data,
                ]
            );
        }
    }

    public function edit($id)
    {
        $album = Album::find($id);

        return response($album);
    }

    public function saveEdit(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:100',
                'slug' => 'required|unique:albums,slug,' . $request->id,
            ],
            [
                'required' => 'Không được để trống',
                'name.max' => 'Tên album tối đa 100 kí tự',
                'name.unique' => 'Tên album đã tồn tại. Vui lòng chọn tên khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng chọn slug khác',
            ]
        );

        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $data = $request->except('_token', 'id');
            $data['updated_at'] = date('Y-m-d H:i:s', time());
            $result = Album::where('id', $request->id)->update($data);

            return response(
                [
                    'errors' => false,
                    'data' => true,
                ]
            );
        }
    }

    public function delete($id)
    {
        // $id = $request->id;
        $album = Album::find($id);
        $check = $album->delete();
        if ($check == 1) {
            return redirect()->route('admin.albums.index')->with('success', 'Xoá albums thành công');
        } else {
            return redirect()->route('admin.albums.index')->with('error', 'Xoá albums không thành công');
        }
    }
}
