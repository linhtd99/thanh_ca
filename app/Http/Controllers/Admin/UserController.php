<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Yajra\Datatables\Datatables;
use App\Http\Requests\AddRequestUser;
use App\Http\Requests\EditRequestUser;
use App\Http\Requests\ProfileRequest;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view('admin.users.index');
    }

    public function data()
    {
        $data = User::all()->sortByDesc('created_at');
        return Datatables::of($data)->make(true);
    }
    public function delete($id)
    {
        $getUser = User::where('id', $id)->first();
        if ($id == Auth::user()->id) {
            return back()->with('error', 'Không thể xóa chính mình');
            die;
        };
        if (Auth::user()->role != 101) {
            return back()->with('error', 'Không đủ quyền hạn để xóa quản trị này');
            die;
        }
        $user = User::where('id', $id)->delete();
        if ($user == 1) {
            return back()->with('success', 'Xóa quản trị thành công');
        } else {
            return back()->with('error', 'Xóa quản trị không thành công');
        }
    }


    public function add()
    {
        return view('admin.users.add');
    }

    public function saveAdd(AddRequestUser $request)
    {
        if (Auth::user()->role != 101) {
            return back()->with('error', 'Không đủ quyền hạn ');
            die;
        }

        $data = $request->except('_token', 'cf_password');
        $data['password'] = bcrypt($request->password);
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $user = User::insert($data);
        if ($user == 1) {
            return redirect()->route('admin.users.index')->with('success', 'Thêm quản trị thành công');
        } else {
            return redirect()->route('admin.users.index')->with('error', 'Có lỗi xảy ra');
        }
    }

    public function edit($id)
    {
        $user = User::find($id);
        if (Auth::user()->role != 101) {
            return back()->with('error', 'Không đủ quyền hạn ');
            die;
        }

        return view('admin.users.edit', ['user' => $user]);
    }

    public function saveEdit(EditRequestUser $request)
    {
        $user = User::find($request->id)->toArray();
        $data = $request->except('_token', 'id');

        if (empty($request->password)) {
            $data['password'] = $user['password'];
        } else {
            $data['password'] = bcrypt($request->password);
        }

        $data['updated_at'] = date('Y-m-d H:i:s', time());
        if ($user['role'] == 101) {
            $data['role'] = 101;
        };

        $result = User::where('id', $request->id)
            ->update($data);

        if ($result == 1) {
            return redirect()->route('admin.users.index')->with('success','Sửa quản trị thành công');
        }else{
            return redirect()->route('admin.users.index')->with('error', 'Sửa quản trị không thành công');
        }
    }

    public function profile(){
        return view('admin.users.profile');
    }

    public function saveProfile(ProfileRequest $request){

        $user = User::find($request->id)->toArray();
        $data = $request->except('_token', 'id','cf_password');
        if (empty($request->password)) {
            $data['password'] = $user['password'];

        } else {
            $data['password'] = bcrypt($request->password);
        }

        $data['updated_at'] = date('Y-m-d H:i:s', time());


        $result = User::where('id', $request->id)
            ->update($data);

        if ($result == 1) {
            return redirect()->route('admin.users.profile')->with('success', 'Sửa thông tin thành công');
        } else {
            return redirect()->route('admin.users.profile')->with('error', 'Sửa thông tin không thành công');
        }
    }
}
