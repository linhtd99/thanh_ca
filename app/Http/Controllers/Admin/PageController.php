<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Yajra\Datatables\Datatables;
use Validator;

class PageController extends Controller
{
    public function index(){
        return view('admin.pages.index');
    }
    public function data(){
        $page = Page::all()->sortByDesc('created_at');

        return Datatables::of($page)->make(true);
    }

    public function add(){
        return view('admin.pages.add');
    }

    public function saveAdd(Request $request){
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:pages,name',
                'slug' => 'required|unique:pages,slug'
            ],
            [
                'required' => 'Không được để trống',
                'name.unique' => 'Tên trang đã tồn tại. Vui lòng điền tên trang khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác',
            ]
        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        }else{
            $data = $request->except('_token');
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $result = Page::insert($data);
            if ($result == 1) {
                return redirect()->route('admin.pages.index')->with('success', 'Thêm trang thành công');
            }else{
                return redirect()->route('admin.pages.index')->with('error', 'Thêm trang không thành công');
            }
        }
    }

    public function edit($id){
        $page = Page::find($id);
        return view('admin.pages.edit',['page' => $page]);
    }

    public function saveEdit(Request $request){
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:pages,name,'.$request->id,
                'slug' => 'required|unique:pages,slug,'.$request->id,
            ],
            [
                'required' => 'Không được để trống',
                'name.unique' => 'Tên trang đã tồn tại. Vui lòng điền tên trang khác',
                'slug.unique' => 'Slug đã tồn tại. Vui lòng điền slug khác',
            ]

        );
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $data = $request->except('_token','id');
            $data['updated_at'] = date('Y-m-d H:i:s', time());
            $result = Page::where('id',$request->id)->update($data);
            if ($result == 1) {
                return redirect()->route('admin.pages.index')->with('success', 'Sửa trang thành công');
            } else {
                return redirect()->route('admin.pages.index')->with('error', 'Sửa trang không thành công');
            }
        }
    }

    public function delete($id){
        $result = Page::where('id', $id)->delete();
        if ($result == 1) {
            return redirect()->route('admin.pages.index')->with('success', 'Xóa trang thành công');
        } else {
            return redirect()->route('admin.pages.index')->with('error', 'Có lỗi xảy ra');
        }
    }
    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Page::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }
}
