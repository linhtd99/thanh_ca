<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Topic;
use App\Models\Topic_playlist;


class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();

        return view('admin.topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        return view('admin.topics.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:191|unique:topics,title',
            ],
            [
                'required' => 'Không được để trống',
                'title.max' => 'Tên chủ đề tối đa 191 kí tự',
                'title.unique' => 'Tên chủ đề đã tồn tại. Vui lòng chọn tên khác',
            ]
        );
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $topic = new Topic();
            $topic->title = $request->title;
            $topic->banner = $request->image;
            $topic->code = getToken();
            $topic->description = $request->description;
            $topic->save();

            return response(
                [
                    'data' => $topic,
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::where('id', $id)->delete();
        // $topic->delete();

        $topic_playlist = Topic_playlist::where('topic_id', $id)->delete();

        return redirect()->route('admin.topics.index');
    }

    public function dataSelect2(Request $request)
    {
        $term = trim($request->q);
        $data = [];
        // dd($term);

        if (strlen($term) == 0) {
            $tags = Topic::limit(20)->orderBy('id', 'desc')->get();
            // dd($tags);
            foreach ($tags as $key) {
                $data[] = ['id' => $key->id, 'title' => $key->title];
            }

            return response()->json($data);
        } elseif (strlen($term) > 2) {
            $tags = Topic::where('title', 'LIKE', "%$term%")
               ->orderBy('id', 'desc')
               ->get();
            foreach ($tags as $key) {
                $data[] = ['id' => $key->id, 'title' => $key->title];
            }

            return response()->json($data);
        }
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Topic::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }

}
