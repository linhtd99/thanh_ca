<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Customer;
use App\Http\Requests\RequestAddCustomer;
use App\Http\Requests\RequestEditCustomer;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(){
        return view('admin.customers.index');
    }

    public function data(){
        $data = Customer::all()->sortByDesc('created_at');
        return Datatables::of($data)->make(true);
    }

    public function edit($id){
        $customer = Customer::find($id);
        return view('admin.customers.edit', ['customer' => $customer]);
    }

    public function saveEdit(RequestEditCustomer $request){
        // dd($request->all());

        $customerCurrent = Customer::find($request->id);
        $data = $request->except('_token','id','image');
        if (empty($request->password)) {
            $data['password'] = $customerCurrent->password;
        }else{
            $data['password'] = bcrypt($request->password);
        }

        $data['avatar'] = $request->image;

        $result = Customer::where('id',$request->id)->update($data);

        if ($result == 1) {
            return redirect()->route('admin.customers.index')->with('success', 'Sửa người dùng thành công');
        } else {
            return redirect()->route('admin.customers.index')->with('error', 'Sửa người dùng không thành công');
        }

    }

    public function add(){
        return view('admin.customers.add');
    }
    public function saveAdd(RequestAddCustomer $request){
        $data = $request->except('_token','cf_password','image');
        $data['password'] = bcrypt($request->password);
        $data['avatar'] = $request->image;
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $result = Customer::insert($data);
        if ($result == 1) {
            return redirect()->route('admin.customers.index')->with('success', 'Thêm người dùng thành công');
        } else {
            return redirect()->route('admin.customers.index')->with('error', 'Có lỗi xảy ra');
        }
    }

    public function detail($id){
        $customer = Customer::find($id);
        return view('admin.customers.detail',['customer' => $customer]);
    }

    public function delete(Request $request){
        $result = Customer::where('id',$request->id)->delete();
        if ($result == 1) {
            return redirect()->route('admin.customers.index')->with('success', 'Xóa người dùng thành công');
        } else {
            return redirect()->route('admin.customers.index')->with('error', 'Có lỗi xảy ra');
        }
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Customer::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }

}
