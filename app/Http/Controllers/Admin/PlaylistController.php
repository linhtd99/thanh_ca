<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Playlist;
use App\Models\Song_playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class PlaylistController extends Controller
{
    public function index()
    {
        $search = trim(Input::get('search'));

        if (strlen($search) > 0) {
            $arg = [];
            if (strlen($search) > 0) {
                $arg[] = ['name', 'LIKE', '%' . $search . '%'];
            }

            $playlists = Playlist::where($arg)
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        } else {
            $playlists = Playlist::orderBy('created_at', 'desc')->paginate(20);
        }

        return view('admin.playlists.index', compact('playlists'));
    }

    public function delete(Request $request)
    {
        Playlist::where('id', $request->id)->delete();

        return response(
            [true]
        );
    }

    public function add()
    {
        $category = Category::all();

        return view('admin.playlists.add', compact('category'));
    }

    public function saveAdd(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                // 'songs' => 'required',
                // 'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            ],
            [
                'name.required' => 'Không được để trống tên playlist',
                // 'songs.required' => 'Playlist bắt buộc phải có ít nhất 1 bài hát',
                // 'image.mimes' => 'Không đúng định dạng ảnh'
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            // dd($request->all());
            $data = $request->except('_token', 'songs', 'image');
            $data['customer_id'] = 0;
            $data['code'] = getToken(12);
            if ($request->image != null) {
                $data['image'] = $request->image;
            }
            $data['status']=$request->status;
            $result = Playlist::create($data);

            if (count($request->arr_id) > 0) {
                foreach ($request->arr_id as $key) {
                    Song_playlist::insert([
                        'playlist_id' => $result->id,
                        'song_id' => $key,
                    ]);
                }
            }
            if (is_array($request->tag_id) && count($request->tag_id) > 0) {
                $result->tags()->sync($request->tag_id);
            }

            if ($result) {
                return redirect()->route('admin.playlists.index')->with('success', 'Thêm playlist thành công');
            } else {
                return redirect()->route('admin.playlists.index')->with('error', 'Thêm playlist không thành công');
            }
        }
    }

    public function edit($id)
    {
        $category = Category::all();
        $playlist = Playlist::where('id', $id)->with(['songs' => function ($query) {
            $query->with('artists');
        }])->first();
        // dd($playlist->songs);
        return view('admin.playlists.edit', compact('playlist', 'category'));
    }

    public function saveEdit(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                // 'songs' => 'required',
            ],
            [
                'name.required' => 'Không được để trống tên playlist',
                // 'songs.required' => 'Playlist bắt buộc phải có ít nhất 1 bài hát',
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate->errors());
        } else {
            $data = $request->except('_token', 'songs', 'image', 'id', 'arr_id');
            if ($request->image != null) {
                $data['image'] = $request->image;
            }
            $data['status']=$request->status;
            $result = Playlist::where('id', $request->id)->update($data);

            $abc = Song_playlist::where('playlist_id', '=', $request->id)->delete();

            if (count($request->arr_id) > 0) {
                foreach ($request->arr_id as $key) {
                    Song_playlist::insert([
                        'playlist_id' => $request->id,
                        'song_id' => $key,
                    ]);
                }
            }

            if ($result) {
                return redirect()->route('admin.playlists.index')->with('success', 'Sửa playlist thành công');
            } else {
                return redirect()->route('admin.playlists.index')->with('error', 'Sửa playlist không thành công');
            }
        }
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Playlist::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }
    public function multiDel(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Playlist::destroy($arr);

        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
    public function setActiveStatus(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Playlist::whereIn('id', $arr)->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
    public function setActiveStatusAll(Request $request)
    {
        $data = $request->data;
        $arr = explode(',', $data);
        $check = true;
        $check = Playlist::where('status', '>', '1')->update(['status' => '1']);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);

        }

    }
}
