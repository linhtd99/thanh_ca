<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;
use DB;
use Illuminate\Support\Facades\Input;
class ReportController extends Controller
{
    public function index(){
        if (Input::get('type') == 'song') {
            $reports = Report::select(DB::raw('reports.*','songs.name'))
                        ->join('songs','reports.code','=','songs.code')
                        ->where('reports.provider','song')
                        ->orderBy('reports.created_at','desc')
                        ->paginate(15);
        } elseif (Input::get('type') == 'video') {
                    $reports = Report::select(DB::raw('reports.*', 'videos.name'))
                        ->join('videos', 'reports.code', '=', 'videos.code')
                        ->where('reports.provider', 'video')
                        ->orderBy('reports.created_at', 'desc')
                        ->paginate(15);
        } elseif (Input::get('type') == 'playlist') {
                    $reports = Report::select(DB::raw('reports.*', 'playlists.name'))
                        ->join('playlists', 'reports.code', '=', 'playlists.code')
                        ->where('reports.provider', 'playlist')
                        ->orderBy('reports.created_at', 'desc')
                        ->paginate(15);
        } elseif (Input::get('type') == 'album') {
                    $reports = Report::select(DB::raw('reports.*', 'albums.name'))
                        ->join('albums', 'reports.code', '=', 'albums.code')
                        ->where('reports.provider', 'album')
                        ->orderBy('reports.created_at', 'desc')
                        ->paginate(15);
        } else {
            abort(404);
        }
        // dd($reports);
        return view('admin.comments_reports.reports', compact('reports'));
    }

    public function destroy(Request $request)
    {
        Report::where('id', $request->id)->delete();
    }

    public function updateStatus(Request $request){
        Report::where('id', $request->id)->update(['status' => $request->val]);
    }

    public function deleteMulti(Request $request)
    {
        $data = $request->data;

        $arr = explode(',', $data);
        $check = Report::destroy($arr);
        if ($check) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => $arr,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Có gì đó sai sai',
                'data' => $arr,
            ]);
        };
    }
}

