<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Password_reset;
use Mail;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function login(){
        if (Auth::check()) {
            return redirect()->back();
        }else{
            return view('admin.login');

        }

    }

    public function postLogin(UserRequest $request){
        // dd($request->all());
        $data = $request->except('_token');
        if (empty($request)) {
            return redirect()->route('admin.login');
        }else{
            if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $check = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
                if ($check == true) {
                    return redirect()->route('admin.home')->with('success', 'Đăng nhập thành công');
                } else {
                    return redirect()->route('admin.login')->with('error', 'Sai mật khẩu');
                }
            } else {
                $check =  Auth::attempt(['username' => $request->email, 'password' => $request->password]);
                if ($check == true) {
                    return redirect()->route('admin.home')->with('success', 'Đăng nhập thành công');
                } else {
                    return redirect()->route('admin.login')->with('error', 'Sai mật khẩu');
                }

            }

        }
    }

    public function forgetPassword(){

        return view('admin.forgetPassword');

    }

    public function postForget(Request $request){
        $users = User::all()->toArray();
        foreach ($users as $key => $value) {
            if ($value['email'] == $request->email) {
                $token = uniqid();
                $data = [
                    'email' => $request->email,
                    'token' => $token,
                    'created_at' => date('Y-m-d H:i:s', time()),
                ];
                $email = $value['email'];
                $data_sendmail['title'] = "Thiết lập lại mật khẩu";
                $data_sendmail['name'] = $value['name'];
                $data_sendmail['email'] = $value['email'];
                $data_sendmail['token'] = $token;
                Mail::send('send_mail', $data_sendmail, function ($message) use ($email) {
                    $message->to($email)->subject('Pveser');
                });
                Password_reset::insert($data);

                return back()->with('success', 'Kiểm tra email của bạn để lấy đường dẫn xác nhận đăng ký.');

            }else{
                return back()->withErrors(['email' => 'Email không tồn tại']);
            }
        }
    }

  
    public function resetPassword($token){
        $token = Password_reset::where('token','=',$token)->get();

        if(count($token) > 0){
            return redirect('admin/changeForgetPw/'.$token[0]->email);
        }else{
            abort(404);
        }
    }

    public function changeForgetPw($email){
        $email = Password_reset::where('email', '=', $email)->get();
        if (count($email) > 0) {
            return view('admin/changeForgetPw',['email' => $email[0]->email]);
        }else{
            abort(404);
        }
    }

    public function postChangePw(Request $request){
        dd(1);
        // dd($request->all());
        $email = Password_reset::where('email','=',$request->email)->get();
        // dd($email);
        if (count($email) > 0 ) {
            // dd('â');
            if (strlen($request->password) == 0) {
                return back()->withErrors(['password' => 'Mật khẩu không được để trống']);
            } elseif (strlen($request->password) < 6) {
                return back()->withErrors(['password' => 'Có ít nhất 6 kí tự']);
            } elseif (strlen($request->cf_password) == 0) {
                return back()->withErrors(['cf_password' => 'Mật khẩu không được để trống']);
            } elseif (trim($request->password) != trim($request->cf_password)) {
                return back()->withErrors(['cf_password' => 'Mật khẩu xác nhận không trùng với mật khẩu']);
            } else {
                $user = User::where('email', $request->email)->update(['password' => bcrypt($request->password)]);
                if ($user == 1) {
                    Password_reset::where('email', $request->email)->delete();
                    return redirect()->route('admin.login')->with('success', 'Thay đổi mật khẩu thành công');
                } else {
                    return back()->with('error', 'Có lỗi xảy ra');
                }
            }
        }else{
            abort(404);
        }

    }

    public function logOut(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
