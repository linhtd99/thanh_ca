<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestEditCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:8|regex:/^[A-Za-z0-9.-_]+$/u|unique:customers,username,'.$this->id,
            'name' => 'required',
            'email' => 'required|email|unique:customers,email,'.$this->id,
            'password' => 'nullable|min:6',

        ];
    }

    public function messages()
    {
        return [
            'required' => 'Không được để trống',
            'username.unique' => 'Tên đăng nhập này đã tồn tại. Vui lòng điền tên đăng nhập khác',
            'username.min' => 'Chỉ được sử dụng chữ cái,chữ số,gạch dưới và dấu chấm và có ít nhất 8 kí tự',
            'username.regex' => 'Chỉ được sử dụng chữ cái,chữ số,gạch dưới và dấu chấm và có ít nhất 8 kí tự',
            'email.unique' => 'Email này đã tồn tại. Vui lòng chọn tên đăng nhập khác',
            'email.email' => 'Nhập đúng dịnh dạng email',
            'password.min' => 'Mật khẩu có ít nhất 6 kí tự',

        ];
    }
}
