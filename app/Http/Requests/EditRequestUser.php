<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditRequestUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'email' => [
                'required',
                'email',
                'unique:users,email,'. $this->id,
            ],
            'name' => 'required',
            'password' => 'nullable|min:6',
            // 'cf_password' => 'required|same:password',
        ];
    }

    public function messages()
    {


        return [
            'required' => 'Không được để trống',
            'email.email' => 'Vui lòng nhập đúng định dạng email',
            'email.unique' => 'Đã tồn tại email',
            'password.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            // 'cf_password.same' => 'Mật khẩu chưa trùng khớp',
        ];
    }
}
