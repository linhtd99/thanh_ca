<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class CustomerLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::guard('customers')->check() || Auth::guard('customers')->user()->username != $request->route()->parameters('username')['username'] ) {
            return redirect()->route('/');
        }
        return $next($request);
    }
}
