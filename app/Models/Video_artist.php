<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video_artist extends Model
{
    protected $table = 'video_artist';
    protected $hidden = ['pivot'];
    protected $fillable  = [
        'id',
        'video_id',
        'artist_id'
    ];
}
