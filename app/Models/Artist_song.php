<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist_song extends Model
{
    protected $table = 'artist_song';
    protected $hidden = ['pivot'];
    protected $fillable = [
        'id',
        'song_id',
        'artist_id',
    ];
}
