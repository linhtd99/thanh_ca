<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'username',
        'avatar',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'role',
    ];
}
