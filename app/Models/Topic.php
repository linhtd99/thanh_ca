<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topics';
    protected $fillable = [
        'id',
        'title',
        'code',
        'group_id',
        'description',
        'banner',
        'playlist_id', // playlist id nổi bật
    ];

    public function playlists()
    {
        return $this->hasMany('App\Models\Playlist', 'topic_id', 'id');
    }

    public function getNewPlaylist()
    {
        return Playlist::join('topic_playlist', 'playlists.id', 'topic_playlist.playlist_id')
        ->join('topics', 'topics.id', 'topic_playlist.topic_id')->where('topics.id', '=', $this->id)
        ->orderBy('playlists.created_at', 'DESC')
        ->first();
    }
}
