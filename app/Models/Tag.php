<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';
    protected $fillable = [
        'id',
        'title',
        'slug',
    ];

    public function songs()
    {
        return $this->belongsToMany('App\Models\Song', 'tag_song', 'tag_id', 'song_id');
    }
    public function getParent()
    {
        return self::where('id', '=', $this->parent_id)->first();
    }

    public function getChilden(Type $var = null)
    {
        return self::where('parent_id', '=', $this->id)
            ->orderBy('title', 'ASC')
            ->orderBy('id', 'ASC')
            ->get();
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Tag', 'parent_id', 'id');
    }

    public function getTagChildren()
    {
        return $this->tags()->orderBy('title', 'ASC');
    }

    public function existInChilden($slug)
    {
        return $this->tags()->where('slug', '=', $slug)->exists();
    }

    public function countChilden()
    {
        return $this->tags()->count();
    }

}
