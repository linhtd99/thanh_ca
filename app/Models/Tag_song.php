<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag_song extends Model
{
    protected $table = 'tag_song';
    protected $fillable = [
        'id',
        'tag_id',
        'song_id',
    ];
}
