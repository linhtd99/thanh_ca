<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Song;
use App\Models\Video;


class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = [
        'id',
        'code',
        'customer_id',
        'parent_id',
        'content',
        'created_at',
        'updated_at'
    ];

    public function getSong()
    {
        return Song::where('code' , '=', $this->code)->first();
    }

    public function getVideo()
    {
        return Video::where('code' , '=', $this->code)->first();
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer','customer_id','id');
    }

    public function getChildComment()
    {
        return Comment::select(DB::raw('comments.*,COUNT(like_comment.comment_id) AS total_like,GROUP_CONCAT(like_comment.customer_id) AS arr_customer_id'))
            ->leftJoin('like_comment', 'like_comment.comment_id', '=', 'comments.id')
            ->groupBy('comments.id')
            ->where(['parent_id' => $this->id])->limit(6)->with('customer')->orderBy('comments.created_at','desc')->get();
    }

}
