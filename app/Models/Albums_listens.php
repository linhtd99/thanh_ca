<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Albums_listens extends Model
{
    protected $table = 'albums_listens';
    protected $fillable = [
        'code',
        'user_id',
        'ip',
        'agent',
        'created_at',
    ];
}
