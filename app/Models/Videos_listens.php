<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Videos_listens extends Model
{
    protected $table = 'videos_listens';
    protected $fillable = [
        'code',
        'user_id',
        'ip',
        'agent',
        'created_at',
    ];

    
}
