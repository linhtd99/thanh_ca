<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $table = 'playlists';
    protected $hidden = ['pivot'];

    protected $fillable = [
        'code',
        'name',
        'image',
        'slug',
        'description',
        'category_id',
        'topic_id',
        'customer_id',
        'created_at',
        'updated_at',
    ];

    public function songs()
    {
        return $this->belongsToMany('App\Models\Song', 'song_playlist');
    }

    public function topic()
    {
        return $this->belongsTo('App\Models\Topic', 'topic_id', 'id');
    }

    public function listenPlaylist()
    {
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_playlist', 'playlist_id', 'tag_id');
    }
}
