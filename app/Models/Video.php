<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';
    protected $hidden = ['pivot'];
    protected $fillable = [
        'id',
        'name',
        'code',
        'description',
        'category_id',
        'album_id',
        'link',
        'status',
        'image',
        'karaoke_type',
        'beat_type'
    ];

    public function song()
    {
        return $this->belongsTo('App\Models\Song', 'song_id', 'id');
    }

    public function lyric()
    {
        return $this->hasOne('App\Models\Lyric');
    }

    public function artists()
    {
        return $this->belongsToMany('App\Models\Artist', 'video_artist');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function album(){
        return $this->belongsTo('App\Models\Album','album_id','id');
    }

    public function countListen()
    {
        return Videos_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $this->code)->groupBy('code')->first();
    }
}
