<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_history extends Model
{
    protected $table = 'user_history';
    protected $fillable = [
        'provider','code', 'customer_id'
    ];
}
