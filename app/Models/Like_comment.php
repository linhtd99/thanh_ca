<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like_comment extends Model
{
    protected $table = 'like_comment';
    protected $fillable = [
        'customer_id', 'comment_id'
    ];
}
