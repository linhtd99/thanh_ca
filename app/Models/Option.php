<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';
    protected $fillable = [
        'id',
        'name',
        'key',
        'value',
    ];

    public static function _get($key)
    {
        return self::where('key', '=', $key)->first();
    }
}
