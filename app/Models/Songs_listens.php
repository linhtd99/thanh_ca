<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Songs_listens extends Model
{
    protected $table = 'songs_listens';
    protected $fillable = [
        'code',
        'user_id',
        'ip',
        'agent',
        'created_at',
    ];
}
