<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';
    protected $fillable = [
        'name',
        'slug',
        'code',
        'description',
        'image',
        'created_at',
        'updated_at',
    ];

    public function songs()
    {
        return $this->hasMany('App\Models\Song');
    }

    public function albums()
    {
        return $this->hasMany('App\Models\Video');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Artist');
    }
}
