<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Song_category extends Model
{
    protected $table = 'song_category';
    protected $fillable = [
        'id',
        'song_id',
        'category_id',
    ];
}
