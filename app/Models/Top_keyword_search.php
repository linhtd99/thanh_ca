<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Top_keyword_search extends Model
{
    protected $table = 'top_keyword_search';
    protected $fillable = [
        'keyword'
    ];
}
