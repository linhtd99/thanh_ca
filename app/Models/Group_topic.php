<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group_topic extends Model
{
    protected $table  = 'group_topic';
    protected $fillable = [
        'id',
        'title',
        'description',
        'banner'
    ];
}
