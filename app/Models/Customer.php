<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table = 'customers';

    public $fillable = [
        'username',
        'token',
        'name',
        'email',
        'phone',
        'avatar',
        'created_at',
        'updated_at',
        'views',
    ];

    protected $hidden = [
        'password',
    ];
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'email', 'email');
    }
}
