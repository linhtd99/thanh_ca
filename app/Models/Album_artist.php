<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album_artist extends Model
{
    protected $table = 'album_artist';
    protected $fillable = [
        'id',
        'album_id',
        'artist_id',
    ];
}
