<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlists_listens extends Model
{
    protected $table = 'playlists_listens';
    protected $fillable = [
        'code',
        'user_id',
        'ip',
        'agent',
        'created_at',
    ];
}
