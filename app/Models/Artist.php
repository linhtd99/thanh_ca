<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table = 'artists';
    protected $hidden = ['pivot'];
    protected $fillable = [
        'id',
        'name',
        'gender',
        'slug',
        'image',
        'parent_id',
        'description',
    ];

    public function songs()
    {
        return $this->belongsToMany('App\Models\Song', 'artist_song');
    }

    public function videos()
    {
        return $this->belongsToMany('App\Models\Video', 'video_artist');
    }

    public function albums()
    {
        return $this->belongsToMany('App\Models\Album', 'album_artist', 'album_id', 'artist_id');
    }
}
