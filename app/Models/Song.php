<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Song extends Model
{
    protected $table = 'songs';
    protected $hidden = ['pivot'];
    // loại bỏ pivot

    protected $fillable = [
        'code',
        'name',
        'slug',
        'code',
        'description',
        'video_id',
        'album_id',
        'author_id',
        'filename',
        'image',
        'status',
    ];

    protected function getNameAuthor($id)
    {
        $customer = Customer::select('username', 'name')->where('id', $id)->first();

        return $customer;
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function albums()
    {
        return $this->belongsTo('App\Models\Album', 'album_id', 'id');
    }

    public function artists()
    {
        return $this->belongsToMany('App\Models\Artist', 'artist_song');
    }

    public function video()
    {
        return $this->belongsTo('App\Models\Video');
    }

    public function lyric()
    {
        return $this->belongsTo('App\Models\Lyric');
    }

    public function playlists()
    {
        return $this->belongsToMany('App\Models\Playlist', 'song_playlist');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_song', 'song_id', 'tag_id');
    }

    public function countListen()
    {
        return Songs_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $this->code)->groupBy('code')->first();
    }

    public function getComments()
    {
        return Comment::where(['code' => $this->code, 'status' => 1])->get();
    }

    public function getArtists()
    {
        return Artist::select('*')->join('artist_song', 'artists.id', 'artist_song.artist_id')
            ->where('artist_song.song_id', '=', $this->id)->get();
    }

    public function author()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'author_id');
    }

}
