<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'id',
        'name',
        'fullname',
        'slug',
        'birthday',
        'country',
        'gender',
        'banner',
        'status',
        'image',
        'description',
    ];

    public function songs()
    {
        return $this->hasMany('App\Models\Song');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\Video');
    }

    public function getCategory($id)
    {
        return self::find($id);
    }

    public function getChildCategory()
    {
        return self::where('parent_id', '=', $this->id)->inRandomOrder()->get();
    }
}
