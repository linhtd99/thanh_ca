<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lyric extends Model
{
    protected $table = 'lyrics';
    protected $fillable = [
        'id',
        'song_id',
        'video_id',
    ];

    public function song()
    {
        return $this->belongsTo('App\Models\Song');
    }

    public function video()
    {
        return $this->belongsTo('App\Models\Video');
    }
}
