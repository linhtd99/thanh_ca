<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tracker extends Model
{
    public $attributes = ['hits' => 1];

    protected $fillable = ['ip', 'visit_date', 'browser'];
    protected $table = 'trackers';

    public static function boot()
    {
        parent::boot();

        static::saving(function ($tracker) {
            $tracker->visit_date = date('Y-m-d');
            $tracker->browser = $_SERVER['HTTP_USER_AGENT'];
        });
    }

    public static function hit()
    {
        static::firstOrCreate([
            'browser' => $_SERVER['HTTP_USER_AGENT'],
            'ip'   => $_SERVER['REMOTE_ADDR'],
            'visit_date' => date('Y-m-d'),
        ])->save();
    }
}
