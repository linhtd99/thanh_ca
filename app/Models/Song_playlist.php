<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Song_playlist extends Model
{
    protected $table = 'song_playlist';
    
    protected $fillable = 
    [
        'playlist_id','song_id'
    ];
}
