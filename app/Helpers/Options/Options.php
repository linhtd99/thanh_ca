<?php

namespace App\Helpers\Options;

use Illuminate\Support\Facades\DB;

class Options
{
    public static function _get($key)
    {
        $option = DB::table('options')->where('key', $key)->first();

        return isset($option->value) ? $option->value : '';
    }

    public static function _all()
    {
        return DB::table('options')->all();
    }

    public static function _insert()
    {
    }
}
