<?php

use App\Models\Artist;
use App\Models\Category;
use App\Models\Option;
use App\Models\Song;
use App\Models\Topic;
use App\Models\Top_keyword_search;
use App\Models\Video;
use Illuminate\Support\Facades\DB;

class Sidebar
{
    public static function dataGenerals()
    {
        $dataGenerals = Option::where('key', '=', 'generals')->first();

        return json_decode($dataGenerals['value']);
    }

    public static function dataFooter()
    {
        $dataFooter = Option::where('key', '=', 'menu_footer')->first();

        return json_decode($dataFooter['value']);
    }

    public static function top_keyword_search()
    {
        $thisMonth = date('Y-m-d', strtotime('this month'));
        $thisDay = date('Y-m-d', strtotime('this day'));

        $top_keyword_search = Top_keyword_search::select(DB::raw('top_keyword_search.keyword,COUNT(top_keyword_search.keyword) AS total'))
            ->whereRaw("top_keyword_search.created_at between '$thisMonth 00:00:01' and '$thisDay 23:59:59'")
            ->groupBy('top_keyword_search.keyword')
            ->orderBy('total', 'desc')
            ->limit(5)->get();

        // dd($top_keyword_search);
        return $top_keyword_search;
    }

    public static function getArtists($limit = 5)
    {
        return Artist::limit($limit)->get();
    }

    public static function randomArtists($limit = 5)
    {
        return Artist::limit($limit)->get();
    }

    public static function artistTrending()
    {
        $firstDayOfTheWeek = date('Y/m/d', strtotime('this week'));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));

        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' - 1 week'));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . ' + 6 days'));

        $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfPreviousWeek . "' and '" . $lastDayOfPreviousWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 10"));
        if (count($artistTrending) == 0) {
            $artistTrending = DB::select(DB::raw("select artists.*, ( t2.listen_video + t3.listen_song ) AS total_listen FROM artists JOIN video_artist ON artists.id = video_artist.artist_id JOIN( SELECT videos.id AS video_id, COUNT(*) AS listen_video FROM videos JOIN videos_listens ON videos.code = videos_listens.code WHERE videos_listens.created_at BETWEEN '2019-09-30 00:00:01' AND '2019-10-6 23:00:00' GROUP BY videos.code ) t2 ON video_artist.video_id = t2.video_id JOIN( SELECT COUNT(*) AS listen_song, artists.id AS artists_id FROM songs JOIN songs_listens ON songs.code = songs_listens.code JOIN artist_song ON artist_song.song_id = songs.id JOIN artists ON artist_song.artist_id = artists.id WHERE songs_listens.created_at
                        BETWEEN '" . $firstDayOfTheWeek . "' and '" . $lastDayOfTheWeek . "' GROUP BY artists.id ) t3 ON artists.id = t3.artists_id GROUP BY artists.id
                        ORDER BY total_listen DESC
                        LIMIT 10"));
        }

        return $artistTrending;
    }

    public static function top100New()
    {
        $childCategories = App\Models\Category::where('parent_id', '<>', '0')
            ->inRandomOrder()
            ->limit(5)
            ->get();
        if (!empty($childCategories[0]->name)) {
            $fisrt = '<div class="top100_img"><img src="https://stc-id.nixcdn.com/v11/images/flower.png" class="new"></div><div class="top100"><a href="' . route('top100_song', ['slug_category' => name_to_slug($childCategories[0]->name)]) . '" title="Top 100  bài hát"><div class="top100_head"><span class="logo-1"></span><div class="padd"><div class="box_title"><p class="top100_title">Top 100  bài hát</p></div></div></div></a><div class="box_content"><ul>';
            $last = '</ul><div style="display: none" class="all"><a href="' . route('top100_song', ['slug_category' => name_to_slug($childCategories[0]->name)]) . '">Xem tất cả TOP 100 </a></div></div></div>';
            $content = '';
            foreach ($childCategories as $key => $value) {
                $content .= '<li>
                                <a href="' . route('top100_song', ['slug_category' => name_to_slug($value->name)]) . '">
                                    <div class="box_action">
                                        <img src="https://i.imgur.com/WS5EL09.png"
                                            alt="" title="">
                                        <div class="bg_black"></div>
                                        <span class="icon_play"></span>
                                    </div>
                                </a>
                                <a title="Top 100  ' . $value->name . ' Hay Nhất" href="' . route('top100_song', ['slug_category' => name_to_slug($value->name)]) . '">
                                    <p>Top 100  ' . $value->name . ' Hay Nhất</p>
                                </a>
                            </li>';
            }

            echo $fisrt . $content . $last;
        }
    }

    /**
     * Static functions.
     *
     * @param int $limit
     */
    public static function songChart($limit = 2)
    {
        $bxh = json_decode(Options::_get('choose_category_bxh'), true);
        $count = 0;
        $arr_cate = [];
        $data_chart = [];
        if (!empty($bxh)) {
            foreach ($bxh as $key => $value) {
                ++$count;
                $arr_cate[] = get_cat_by_id($value['id']);
                $data_chart[] = self::getSongChart($value['id']);

                if ($count == $limit) {
                    break;
                }
            }
        }

        return $data = [
            'categories' => $arr_cate,
            'charts' => $data_chart,
        ];
    }

    public static function videoChart($limit = 2)
    {
        $bxh = json_decode(Options::_get('choose_category_bxh'), true);
        $count = 0;
        $arr_cate = [];
        $data_chart = [];
        if (!empty($bxh)) {
            foreach ($bxh as $key => $value) {
                ++$count;
                $arr_cate[] = get_cat_by_id($value['id']);
                $data_chart[] = self::getVideoChart($value['id']);
                if ($count == $limit) {
                    break;
                }
            }
        }

        return $data = [
            'categories' => $arr_cate,
            'charts' => $data_chart,
        ];
    }

    public static function getTopics($limit = 5)
    {
        $topics = Topic::with([
            'playlists' => function ($query) {
                $query->orderBy('created_at', 'desc')->first();
            },
        ])->inRandomOrder()->limit($limit)->get();
        return $topics;
    }

    protected static function getSongChart($category_id, $limit = 10)
    {
        if (date('W') != 1) {
            $month = date('m') - 1;
            $year = date('Y');
        } else {
            $month = 12;
            $year = date('Y') - 1;
        }
        $currentYear = date('Y');
        $currentMonth = date('W');
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currentMonth, $currentYear);
        $category = Category::where('id', '=', $category_id)->first();
        if (!empty($category)) {
            $category_id = $category->id;
            $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')

                ->where([
                    ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                    ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                    ['.songs.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                ->groupBy('songs.code')->orderBy('listens', 'desc')
                ->limit($limit)->with('artists', 'lyric')
                ->get();
            if ($chart->count() < 1) {
                $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
                    ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')

                    ->where([
                        ['.songs.status', '=', '1'],
                    ])
                    ->where(function ($query) use ($category_id) {
                        $query->where('category_id', '=', $category_id)
                            ->orWhere('parent_id', '=', $category_id);
                    })
                    ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
                    ->groupBy('songs.code')->orderBy('listens', 'desc')
                    ->limit($limit)->with('artists', 'lyric')
                    ->get();
            }

            return $chart;
        }
    }

    protected static function getVideoChart($category_id, $limit = 10)
    {
        if (date('W') != 1) {
            $month = date('m') - 1;
            $year = date('Y');
        } else {
            $month = 12;
            $year = date('Y') - 1;
        }
        $currentYear = date('Y');
        $currenMonth = date('m');
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);
        $category = Category::where('id', '=', $category_id)->first();
        if (!empty($category)) {
            $category_id = $category->id;
            $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
                ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')

                ->where([
                    ['videos_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                    ['.videos_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                    ['.videos.status', '=', '1'],
                ])
                ->where(function ($query) use ($category_id) {
                    $query->where('category_id', '=', $category_id)
                        ->orWhere('parent_id', '=', $category_id);
                })
                ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
                ->groupBy('videos.code')->orderBy('listens', 'desc')
                ->limit($limit)->with('artists')
                ->get();
            if ($chart->count() < 1) {
                $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
                    ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')

                    ->where([
                        ['.videos.status', '=', '1'],
                    ])
                    ->where(function ($query) use ($category_id) {
                        $query->where('category_id', '=', $category_id)
                            ->orWhere('parent_id', '=', $category_id);
                    })
                    ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
                    ->groupBy('videos.code')->orderBy('listens', 'desc')
                    ->limit($limit)->with('artists')
                    ->get();

            }

            return $chart;
        }
    }
}
