<?php

use App\Models\Albums_listens;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Playlists_listens;
use App\Models\Song;
use App\Models\Songs_listens;
use App\Models\Tracker;
use App\Models\User_history;
use App\Models\Video;
use App\Models\Videos_listens;
use Illuminate\Support\Facades\DB;
//$dataGenerals = Sidebar::dataGenerals();
if (!function_exists('getToken')) {
    function getToken($length = 12)
    {
        $token = '';
        $codeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $codeAlphabet .= time();
        $codeAlphabet .= 'abcdefghijklmnopqrstuvwxyz';
        $codeAlphabet .= '0123456789';
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; ++$i) {
            $token .= $codeAlphabet[random_int(0, $max - 1)];
        }

        return $token;
    }
}

if (!function_exists('lyricToJson')) {
    function lyricToJson($lyric)
    {
        $array = explode('*', trim($lyric, '*'));
        $new_array = [];
        for ($i = 0; $i < count($array); ++$i) {
            $child_arr = explode('|', $array[$i]);
            $new_array[] = [
                'time' => trim($child_arr[0]),
                'text' => preg_replace("/[\n\r]/", '', trim($child_arr[1], '')),
            ];
        }

        return json_encode($new_array);
    }
}

if (!function_exists('get_permalink')) {
    function song_permalink($id)
    {
    }
}

if (!function_exists('to_slug')) {
    function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        // $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        // $str = preg_replace('/([\s]+)/', '', $str);

        return $str;
    }
}
if (!function_exists('name_to_slug')) {
    function name_to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;
    }
}

// lấy danh mục bằng id danh mục
if (!function_exists('get_cat_by_id')) {
    function get_cat_by_id($id)
    {
        $category = Category::where('id', $id)->first();
        if (empty($category)) {
            $category = new Category();
            $category->slug = '';
        }

        return $category;
    }
}
// lay cac danh muc cha
if (!function_exists('get_cat_parent')) {
    function get_cat_parent()
    {
        $category = Category::where('parent_id', 0)->get();

        return $category;
    }
}

// lấy tất cả video bằng id danh mục
if (!function_exists('get_videos_by_id_cat')) {
    function get_videos_by_id_cat($id)
    {
        $newData = [];
        $data = [];
        $category = Category::where('id', $id)->orWhere('parent_id', '=', $id)->with('videos')->get();
        // dd($category);
        $i = 0;
        foreach ($category as $value) {
            foreach ($value->videos as $key => $video) {
                $newData[$i] = $video;
                $newData[$i]['listens'] = $video->countListen();
                ++$i;
            }
        }
        // dd($newData);

        return $newData;
    }
}

if (!function_exists('getIdVideo')) {
    function getIdVideo($url)
    {
        $arr = [];
        $video_id = '';
        parse_str(parse_url($url, PHP_URL_QUERY), $arr);
        if (!empty($arr['v'])) {
            $video_id = $arr['v'];
        }

        return $video_id;
    }
}

if (!function_exists('getVideoYoutubeApi')) {
    function getVideoYoutubeApi($url)
    {
        $thumbnail_base = 'https://i.ytimg.com/vi/';
        $arr = [];
        parse_str(parse_url($url, PHP_URL_QUERY), $arr);
        if(!empty($arr['v'])){
            $video_id = $arr['v'];
        } else {
            $video_id = 0;
        }
        
        $vinfo['duration_s'] = 500000;
        $vinfo['duration_sec'] = '';
        $vinfo['thumbnail']['default'] = $thumbnail_base . $video_id . '/maxresdefault.jpg';
        $vinfo['thumbnail']['mqDefault'] = $thumbnail_base . $video_id . '/mqdefault.jpg';

        return $vinfo;
    }
}
function convert_to_string_time($num_seconds)
{
    $seconds = $num_seconds % 60;
    $min = floor($num_seconds / 60);
    if ($min == 0) {
        return "{$seconds}";
    } else {
        return "{$min}:{$seconds}";
    }
}

// đếm view bai hat
if (!function_exists('countListenSongHelper')) {
    function countListenSongHelper($code)
    {
        return Songs_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $code)->groupBy('code')->first();
    }
}
// đếm view video
if (!function_exists('countListenHelper')) {
    function countListenHelper($code)
    {
        return Videos_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $code)->groupBy('code')->first();
    }
}

// dem view Playlist
if (!function_exists('countListenPlaylistHelper')) {
    function countListenPlaylistHelper($code)
    {
        return Playlists_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $code)->groupBy('code')->first();
    }
}

// dem view album
if (!function_exists('countListenAlbumHelper')) {
    function countListenAlbumHelper($code)
    {
        return Albums_listens::select(DB::raw('count(*) as listen'))->where('code', '=', $code)->groupBy('code')->first();
    }
}

if (!function_exists('getNameCustomer')) {
    function getNameCustomer($id)
    {
        // dd($id);
        $customer = Customer::where('id', '=', $id)->first();
        // dd($customer);
        if (!empty($customer['name'])) {
            $name = $customer['name'];
        } else {
            $name = $customer['username'];
        }

        return $name;
    }
}

if (!function_exists('getCustomer')) {
    function getUserNameCustomer($id)
    {
        $customer = Customer::where('id', '=', $id)->first();

        $name = $customer['username'];

        return $name;
    }
}

if (!function_exists('getStartAndEndDate')) {
    function getStartAndEndDate($week, $year)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');

        return $ret;
    }
}

if (!function_exists('getStartAndEndDateOfMonth')) {
    function getStartAndEndDateOfMonth($month, $year)
    {
        $ret = [];
        $ret['start'] = date('Y-m-d', strtotime("$year-$month-01"));
        $ret['end'] = date('Y-m-t', strtotime("$year-$month-01"));

        return $ret;
    }
}

if (!function_exists('getNumberOnChart')) {
    function getNumberOnChart($code, $category_id, $month, $year)
    {
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        // $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);
        // dd($arr_date);
        // $category = Category::where('slug', '=', $slug_category)->first();
        // $category_id = $category->id;
        // dd($category->id);
        $chart = Song::join('categories', 'songs.category_id', '=', 'categories.id')
            ->join('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where([
                ['songs_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.songs_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.songs.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(songs.code) as listens, songs.*, categories.parent_id'))
            ->groupBy('songs.code')->orderBy('listens', 'desc')
            ->limit(20)->with('artists', 'lyric')
            ->get();

        $location = '';
        if (count($chart) < 20) {
            $count = count($chart);
        } else {
            $count = 20;
        }
        if (count($chart) > 0) {
            for ($i = 0; $i < $count; ++$i) {
                if ($chart[$i]->code == $code) {
                    $location = $i + 1;
                    break;
                }
            }
        }

        return $location;
    }
}

if (!function_exists('getNumberOnChartVideo')) {
    function getNumberOnChartVideo($code, $category_id, $month, $year)
    {
        $arr_date = getStartAndEndDateOfMonth($month, $year);
        // $current_date = getStartAndEndDateOfMonth($currenMonth, $currentYear);
        // dd($arr_date);
        // $category = Category::where('slug', '=', $slug_category)->first();
        // $category_id = $category->id;
        // dd($category->id);
        $chart = Video::join('categories', 'videos.category_id', '=', 'categories.id')
            ->join('videos_listens', 'videos.code', '=', 'videos_listens.code')

            ->where([
                ['videos_listens.created_at', '>=', $arr_date['start'] . ' 00:00:00'],
                ['.videos_listens.created_at', '<=', $arr_date['end'] . ' 23:59:59'],
                ['.videos.status', '=', '1'],
            ])
            ->where(function ($query) use ($category_id) {
                $query->where('category_id', '=', $category_id)
                    ->orWhere('parent_id', '=', $category_id);
            })
            ->select(DB::raw('count(videos.code) as listens, videos.*, categories.parent_id'))
            ->groupBy('videos.code')->orderBy('listens', 'desc')
            ->limit(20)
            ->get();

        $location = '';
        if (count($chart) < 20) {
            $count = count($chart);
        } else {
            $count = 20;
        }
        if (count($chart) > 0) {
            for ($i = 0; $i < $count; ++$i) {
                if ($chart[$i]->code == $code) {
                    $location = $i + 1;
                    break;
                }
            }
        }

        return $location;
    }
}
if (!function_exists('getAuth')) {
    function getAuth($customerId)
    {
        DB::select(DB::raw("delete t
            FROM user_history t JOIN
            (SELECT tt.created_at,tt.code
            FROM user_history tt
            WHERE tt.provider = 'search_history'
            AND tt.customer_id = $customerId
            ORDER BY tt.created_at DESC
            LIMIT 50
            OFFSET 5
            ) tt
            ON t.code = tt.code"));
        $history_search_by_customer = User_history::select('user_history.id', DB::raw('user_history.code AS keyword'))
            ->where(['user_history.provider' => 'search_history', 'user_history.customer_id' => $customerId])
            ->orderBy('user_history.created_at', 'desc')->limit(5)->get();

        return $history_search_by_customer;
    }
}

if (!function_exists('getInfoVideo')) {
    function getInfoVideo($code)
    {
        $video = Video::where('code', $code)->with('artists')->first();

        return $video;
    }
}

if (!function_exists('getLastWeek')) {
    function getLastWeek()
    {
        $week = date('W');
        $year = date('Y');
        if ($week == 1) {
            $lastWeek = 12;
            $year = $year - 1;
        } else {
            $lastWeek = $week - 1;
        }

        return [
            'week' => $lastWeek,
            'year' => $year,
        ];
    }
}

if (!function_exists('get_song_history_user')) {
    function get_song_history_user($id_user)
    {
        $songs = User_history::select(DB::raw('songs.*'), DB::raw('COUNT(songs_listens.code) AS listen'))
            ->join('songs', 'user_history.code', '=', 'songs.code')
            ->leftJoin('songs_listens', 'songs.code', '=', 'songs_listens.code')
            ->where(['user_history.provider' => 'song', 'user_history.customer_id' => $id_user])
            ->groupBy('songs.code')
            ->orderBy('user_history.created_at', 'desc')
            ->limit(10)
            ->get();
        return $songs;
    }
}

if (!function_exists('tracker_hit')) {
    function tracker_hit()
    {
        Tracker::hit();
    }
}

if (!function_exists('getTrack')) {
    function getTrack($song)
    {
        $new_arr = [];
        $artist_id = '';
        foreach ($song as $key => $value) {

            foreach ($value->artists as $artist) {
                $artist_id .= $artist->id;
            }
            ;
            $new_arr[$key] = [
                "track" => $key + 1,
                "name" => $value['name'],
                "code" => $value['code'],
                'image' => $value['image'],
                "singer" => $value['song'],
                "filename" => Storage::disk('public')->url('/uploads/files/' . $value->filename),
                "video" => ($value->video != null ? $value->video->link : null),
                "lyrics" => ($value->lyric_karaoke != null ? json_decode($value->lyric_karaoke, true) : ''),
            ];
        }
        // die;
        return $new_arr;
    }
}

if (!function_exists('getTrackSong')) {
    function getTrackSong($song)
    {
        $new_arr = [];
        $artist_id = '';

        if (!empty($song)) {
            foreach ($song->artists as $artist) {
                $artist_id .= $artist->id;
            }
            $new_arr[0] = [
                "track" => 1,
                "name" => $song['name'],
                'image' => $song['image'],
                "singer" => $song['song'],
                'code' => $song['code'],
                "filename" => Storage::disk('public')->url('/uploads/files/' . $song->filename),
                // "video" => ($song->video != null ? $song->video->link : null),
                "lyrics" => ($song->lyric_karaoke != null ? json_decode($song->lyric_karaoke, true) : ''),
            ];

        }

        // die;
        return $new_arr;
    }
}
