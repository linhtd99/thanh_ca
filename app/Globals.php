<?php

namespace App;

class Globals
{
    private static $_data = [];

    public static function categoriesRecursive($categories, $parent_id = 0, $step = 0, &$new = [])
    {
        foreach ($categories as $key => $item) {
            if ((int) $item['parent_id'] == $parent_id) {
                $c = $item;
                $c['step'] = $step;
                Globals::$_data['a'][] = $c;
                unset($categories[$key]);
                Globals::categoriesRecursive($categories, (int) $item['id'], $step + 1);
            }
        }

        return Globals::$_data['a'];
    }
}
