<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SidebarServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        require_once app_path().'/Helpers/Sidebar.php';
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }
}
