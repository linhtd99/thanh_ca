<?php

namespace App\Providers;

use App\Models\Option;
use App\Models\Top_keyword_search;
use DB;
use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {}

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        // $dataGenerals = Option::where('key', '=', 'generals')->first();
        // $dataFooter = Option::where('key', '=', 'menu_footer')->first();
        // $top_keyword_search = Top_keyword_search::select(DB::raw('top_keyword_search.keyword,COUNT(top_keyword_search.keyword) AS total'))
        //     ->groupBy('top_keyword_search.keyword')
        //     ->orderBy('total', 'desc')
        //     ->limit(5)->get();
        // View::share([
        //     'dataGenerals' => json_decode($dataGenerals['value']),
        //     'dataFooter' => json_decode($dataFooter['value']),
        //     'top_keyword_search' => $top_keyword_search,
        // ]);
    }
}
