<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OptionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        require_once app_path().'/Helpers/Options/Options.php';
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }
}
