<h1>Thiết Lập Lại Mật Khẩu</h1>
<p>Một ai đó vừa yêu cầu đặt lại mật khẩu cho tài khoản sau:
<br>Tên người dùng: {{ $db->name }}
<br>Email : {{ $db->email }}
<br>Nếu thấy đây là việc không cần thiết, bạn có thể bỏ qua email này và không thay đổi gì hết.
Để thiết lập lại mật khẩu của bạn, hãy truy cập vào địa chỉ sau đây:

<a href="{{url('/').'/reset_password/'.$db->token}}">Link thay đổi mật khẩu </a>
</p>