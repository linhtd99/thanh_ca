<h1>{{$title}}</h1>
<p>Một ai đó vừa yêu cầu đặt lại mật khẩu cho tài khoản sau:
<br>Tên người dùng: {{$name}}
<br>Email : {{$email}}
<br>Nếu thấy đây là việc không cần thiết, bạn có thể bỏ qua email này và không thay đổi gì hết.
Để thiết lập lại mật khẩu của bạn, hãy truy cập vào địa chỉ sau đây:
<a href="{{url('/').'/admin/resetPassword/'.$token}}"">Link thay đổi mật khẩu </a>
</p>