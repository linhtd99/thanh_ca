<h1>{{$title}}</h1>
<p>Một ai đó vừa yêu cầu đặt lại mật khẩu cho tài khoản sau:
    <br>Tên người dùng: {{$name}}
    <br>Email: {{$email}}
    <br>Mã xác thực: <span style="font-weight:bold"> {{$token}} </span>
    <br>Nếu thấy đây là việc không cần thiết, bạn có thể bỏ qua email này và không thay đổi gì hết.
</p>