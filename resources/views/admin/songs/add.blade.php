@extends('admin.layout.master')
{{-- page title --}}
@section('page_title')
Thêm Mới bài hát
@endsection
{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    b.btn {
        margin: 5px !important;
    }

    audio {
        margin: 5px 0px !important;
        display: none;
    }
    .boxs-img, .boxs-img img {
        margin-top: 10px;
    }

    .grid-container {
        display: grid;
        grid-template-columns: auto auto auto;

        grid-gap: 10px;
    }

    .item1 {
        grid-column: 1 / 12;
    }

    .add_error {}

    .item2 {
        grid-column: 12 / 12;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 17px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }

</style>
@endsection
{{-- content --}}
@section('content')
{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}
<!-- main content -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    @yield('page_title')
                </h3>
                <div class="box-tools pull-right">
                    <div class="pull-right btn-group-sm">
                        <button data-title="Thêm mới Album" data-action="add_album" data-toggle="modal"
                            data-target="#modal-default" class=" btns_add btn btn-sm btn-secondary btn-flat"><i
                                class="fa fa-list-alt"></i> Thêm Album</button>
                        <button data-title="Thêm mới nghệ sĩ" data-action="add_artist" data-toggle="modal"
                            data-target="#modal-default" class="btns_add btn btn-sm btn-secondary btn-flat"><i
                                class="fa fa-user mr-1"></i> Thêm nghệ sĩ</button>
                        <button style="display:none" data-title="Thêm mới danh mục" data-action="add_category" data-toggle="modal"
                            data-target="#modal-default" class="btns_add btn btn-sm btn-secondary btn-flat"><i
                                class="fa fa-bars"></i> Thêm danh mục</button>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form method="POST" id=create_song action="{{route('admin.customers.saveAdd')}}">
                    {{ @csrf_field() }}
                    <input type="hidden" id="action" name="action" value="save&exit">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name">Tên bài hát</label>
                                <input onkeyup="javascript: ChangeToSlug('name', 'slug')" class="form-control"
                                    type="text" name="name" id="name"
                                    value="{{ !empty(old('name')) ? old('name') : ''}}">
                                <div class="error text-danger text-bold error_name"></div>
                                @error('name')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input class="form-control" type="text" name="slug" id="slug"
                                    value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                                <div class="error text-danger text-bold error_slug"></div>
                                @error('slug')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="album_id">Album</label>
                                <select class="form-control" name="album_id" id="album_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_album_id"></div>
                                @error('album_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="category_id">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_category_id"></div>
                                @error('category_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="artist_id">Nghệ sĩ thể hiện:</label>
                                <select class="form-control" multiple name="artist_id[]" id="artist_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_artist_id"></div>
                                @error('artist_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div style="display:none" class="form-group">
                                <label for="video_id">Video đính kèm</label>
                                <div class="box-slider box-slider-video">
                                    <label class="switch switch-video">
                                        <input value="1" type="checkbox" name="check_video" checked>
                                        <span class="slider slider-video"></span>
                                    </label>
                                </div>
                                <div class="box-video">
                                    {{-- <select class="form-control" name="video_id" id="video_id"></select> --}}
                                    <input type="hidden" name="check_change_video" value="1">
                                    <input type="text" name="video" class="video form-control" id="video">
                                    <div class="error text-danger text-bold error_video"></div>
                                </div>

                                @error('video_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Trạng thái</label>
                                <select class="form-control" name="status" id="status" class="form-control">
                                    <option value="0">Không kích hoạt</option>
                                    <option selected value="1">Kích hoạt</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div style="display:none" class="form-group">
                                <div class="">
                                    <label for="tag">Tag</label>
                                </div>
                                <div class="box-tag">
                                    <div class="grid-container">
                                        <div style="" class="grid-item item1">
                                            <select disabled multiple name="tag[]" id="tag"
                                                class="tag form-control"></select>
                                        </div>
                                        <div style="" class="grid-item item2">
                                            <button data-toggle="modal" data-target="#addTag" type="button"
                                                class="btn btn-flat btn-google btn-sm">Add
                                                tag</button>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">Giới thiệu bài hát</label>
                                <textarea style="min-width:100%; max-width: 100%; min-height: 100px"
                                    class="form-control" style="width:100%" name="description" id="description" cols="5"
                                    rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <input
                                    value="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                    readonly min="0" type="hidden" name="image" class="form-control image"
                                    id="thumbnail">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-primary choose_image" id="choose_image" type="button">Chọn
                                    Ảnh</button>
                                    <div class="error text-danger text-bold error_image"></div>

                                <span style="margin-left: 10px !important" class="boxs-img" class="">
                                    <i style="display:none" class="btn btn-danger btn-xs btn-remove-img">
                                        <i class="fa fa-times"></i>
                                    </i>
                                    <img style="width: 60px; height: auto;" id="this-img"
                                        src="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                        alt="">
                                </span>
                            </div>
                            <div class="form-group">
                                <input
                                    value="{{ (old('link')) ? old('link') : ( !empty($song['link']) ? $song['link'] : '' ) }}"
                                    readonly type="hidden" name="link" class="form-control link" id="link">
                                <input readonly type="hidden" name="filename" class="form-control filename"
                                    id="filename">
                                <span>

                                    <span class="">
                                        <button id="btn-choose_file" type="button" class="btn btn-sm ">Chọn bài
                                            hát</button>
                                    </span>
                                    <span style="margin: 0px 5px !important "
                                        class="pull-right-container my-2 text-bold text-dark" id="file_name"></span>
                                    <div class="error text-danger text-bold error_song"></div>
                                    <div class="error text-danger text-bold error_link"></div>
                                    <div class="error text-danger text-bold error_filename"></div>
                                </span>
                                <div style="" id="box-mp3" class="box-mp3">
                                    <span style="margin-top: 10px !important">
                                        <audio volume="0.5" style="margin-top: 10px !important;height: 50px !important;"
                                            controls id="audio" class="same-form-control" src=""></audio>

                                    </span>
                                    <div class="btn-group-sm">
                                        <a onclick="getCurTime()" class="btn-get-time btn btn-primary btn-flat">Get
                                            time</a>
                                        <a onclick="togglePlay()" class="btn-play btn btn-info btn-flat">Play/Pause</a>
                                        <a onclick="next()" class="btn-next btn btn-info btn-flat"> &gt;&gt; Next 5s</a>
                                        <a onclick="prev()" class="btn-prev btn btn-info btn-flat"> &lt;&lt; Prev 5s</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Lời karaoke</label>
                                <div class="error text-danger text-bold error_lyric_karaoke"></div>
                                <textarea style="min-width: 100%; max-width: 100%;" name="lyric_karaoke" id="lyric_karaoke" class="lyric_karaoke form-control" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="lyric">Lời bài hát</label>
                                <div class="box-slider box-slider-lyric">
                                    <label class="switch switch-lyric">
                                        <input value="1" type="checkbox" name="check_lyric" checked>
                                        <span class="slider slider-lyric"></span>
                                    </label>
                                </div>
                                <div class="box-lyric">
                                    <input type="hidden" name="check_change_lyric" value="1">
                                    <div class="error text-danger text-bold error_lyric"></div>
                                    <textarea style="min-width: 100%; max-width:100%; min-height:200px" name="lyric"
                                        class="col-sm-12 lyric form-control" id="lyric" cols="10" rows="10"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" value="0" name="author_id">
                                <button class=" btn btn-success" type="submit">Lưu</button>

                            </div>
                        </div>
                    </div>
                </form>
                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->

<!--  Modal -->
<div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="addTag" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content box box-danger">
            <div class="modal-header">
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Thêm mới Tag</b></h4>
                </div>
                <div id="show-alert">

                </div>
            </div>
            <div class="modal-body">
                <form id=add-new-tags action="" method="post">
                    <div class="form-group">
                        <input style="min-width: 100%" class="form-control" id="input-tag" type="text" value=""
                            data-role="tagsinput">
                    </div>
                    <div class="form-group">
                        <button id="btn-save" class="btn btn-flat btn-success">Lưu</button>

                    </div>
                </form>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div> --}}
        </div>
    </div>
</div>
<!-- end Modal -->


<!-------------------   Modal add new    --------------------->
<div class="modal fade modal-add" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modal-form">

                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-------------------  .end Modal add new    --------------------->



@endsection
<!----- end javascript here -------->
@section('js')
<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.js') }}"></script>
<script>
    $("#input-tag").val()

</script>
<script>
    CKFinder.start( {
    skin: 'jquery-mobile',
    dialogOverlaySwatch: 'b'
} );

CKFinder.start( {
    skin: 'jquery-mobile',
    swatch: 'c',
    dialogOverlaySwatch: true // Will apply 'c' swatch.
} );
</script>
<script>
    $(function () {
        /**
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
        **/



        $('#create_song').on('submit', function (e) {

            e.preventDefault();
            console.log($('#action').val());
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            //console.log(dataForm);
            $.ajax({
                url: "{{route('admin.songs.postSong')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    //console.log(msg);
                    if (result.errors) {
                        console.log(msg);
                        if (msg.name) {
                            form.find('.error_name').html(msg.name);
                        } else {
                            form.find('.error_name').html(' ');
                        }
                        if (msg.slug) {
                            form.find('.error_slug').html(msg.slug);
                        } else {
                            form.find('.error_slug').html('');
                        }
                        if (msg.video) {
                            form.find('.error_video').html(msg.video);
                        } else {
                            form.find('.error_video').html('');
                        }

                        if (msg.song) {
                            form.find('.error_song').html(msg.song);
                        } else {
                            form.find('.error_song').html('');
                        }

                        if (msg.link) {
                            form.find('.error_link').html(msg.link);
                        } else {
                            form.find('.error_link').html('');
                        }

                        if (msg.filename) {
                            form.find('.error_filename').html(msg.filename);
                        } else {
                            form.find('.error_filename').html('');
                        }
                        if (msg.lyric) {
                            form.find('.error_lyric').html(msg.lyric);
                        } else {
                            form.find('.error_lyric').html('');
                        }
                        if (msg.category_id) {
                            form.find('.error_category_id').html(msg.category_id);
                        } else {
                            form.find('.error_category_id').html('');
                        }
                        if (msg.image) {
                            form.find('.error_image').html(msg.image);
                        } else {
                            form.find('.error_image').html('');
                        }
                        if (msg.lyric_karaoke) {
                            form.find('.error_lyric_karaoke').html(msg.lyric_karaoke);
                        } else {
                            form.find('.error_lyric_karaoke').html('');
                        }
                    } else {
                        form.find('.error').html('');
                        // $('#create_song').trigger("reset");
                        // $('#this-img').attr('src', '');
                        console.log(result);
                        swal({
                            title: "Thông báo",
                            text: "Thêm bài hát thành công!",
                            icon: "success",
                            button: "OK",
                        }).then((value) => {
                            window.location.href = "{{ route('admin.songs.index') }}";
                        })

                        // table.ajax.reload();
                        // window.location.reload();
                    }
                    // table.ajax.reload();
                });
        })
    })

</script>

<!------ Slider toggle ------>
<script>
    $(function () {
        /**
         *  toggle video
         **/
        $('body').on('click', '.slider-video', function (e) {
            var check = $('input[name="check_video"]').is(':checked');
            if (check == true) {
                $('input[name="check_video"]').val(2);
                $('body .box-video').html(
                    `<input type="hidden" name="check_change_video" value="2">
                    <select class="form-control" name="video_id" id="video_id"></select>
                    <div class="error text-danger text-bold error_video"></div>`
                );
                // ******
                var videos_select2 = $("#video_id").select2({
                    ajax: {
                        url: "{{ route('admin.videos.data_select2') }}",
                        type: "post",
                        dataType: 'json',
                        delay: 1000,
                        data: function (params) {
                            //console.log(params);
                            return {
                                q: $.trim(params.term),
                                _token: "{{csrf_token()}}"
                            };
                        },
                        processResults: function (data) {
                            //console.log(data);
                            var empt = [{
                                id: 0,
                                name: "--- Unknown ---"
                            }];
                            data = empt.concat(data);

                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                        value: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });


                // ******
            } else {
                $('input[name="check_video"]').val(1);
                $('body .box-video').html(
                    `<input type="hidden" name="check_change_video" value="1">
                    <input type="text" name="video" class="video form-control" id="video">
                    <div class="error text-danger text-bold error_video"></div>`
                );
            }
        })
        /**
         *  toggle lyric
         **/
        $('body').on('click', '.slider-lyric', function (e) {
            var check = $('input[name="check_lyric"]').is(':checked');
            var check_change = $('body').find('input[name="check_change_lyric"]').val();
            if (check == true && parseInt(check_change) == 1) {
                $('input[name="check_video"]').val(1);
                $('body .box-lyric').html(
                    `<input type="hidden" name="check_change_lyric" value="2">
                    <select class="form-control" name="lyric_id" id="lyric_id"></select>
                    <div class="error text-danger text-bold error_lyric"></div>`
                );
                var lyrics_select2 = $("#lyric_id").select2({
                    ajax: {
                        url: "{{ route('admin.lyrics.data_select2') }}",
                        type: "post",
                        dataType: 'json',
                        delay: 10,
                        data: function (params) {
                            //console.log(params);
                            return {
                                q: $.trim(params.term),
                                _token: "{{csrf_token()}}"
                            };
                        },
                        processResults: function (data) {
                            //console.log(data);
                            var empt = [{
                                id: 0,
                                name: "--- Unknown ---"
                            }];
                            data = empt.concat(data);

                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                        value: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });
            } else {
                $('body .box-lyric').html(
                    `<input type="hidden" name="check_change_lyric" value="1">
                    <div class="error text-danger text-bold error_lyric"></div>
                    <textarea style="min-width: 100%; max-width:100%" name="lyric" class="col-sm-12 lyric form-control"
                                    id="lyric" cols="10" rows="10"></textarea>`
                );
            }
        })
    })

</script>


<script>
    // music
    var myAudio = document.getElementById("audio");

    var isPlaying = false;
    myAudio.onplaying = function () {
        isPlaying = true;
    };
    myAudio.onpause = function () {
        isPlaying = false;
    };

    function togglePlay() {
        if (isPlaying) {
            myAudio.pause()
        } else {
            myAudio.play();
        }
    }

    function getCurTime() {
        myAudio.pause()
        var time = myAudio.currentTime;
        var format_time = ('*' + parseInt(time / 60)) + ':' + parseFloat((time % 60)).toFixed(2) + '|';
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(format_time).select();
        document.execCommand("copy");
        $temp.remove();
        return time;
    }

    function next() {
        var time = getCurTime();
        myAudio.currentTime = time + 5;
    }

    function prev() {
        var time = getCurTime();
        myAudio.currentTime = time - 5;
    }

</script>
{{-- ckfinder --}}
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script src="{{ asset('js/typeahead.jquery.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.js') }}"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>
<!----************************************************ --->
<script>
    $(function () {

        // ******

        var videos_select2 = $("#video_id").select2({
            ajax: {
                url: "{{ route('admin.videos.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 1000,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });


        // ******
        // ******

        var lyrics_select2 = $("#lyric_id").select2({
            ajax: {
                url: "{{ route('admin.lyrics.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });


        // ******
        // ******
        var albums_select2 = $("#album_id").select2({
            ajax: {
                url: "{{ route('admin.albums.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var artists_select2 = $("#artist_id").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    // var empt = [{
                    //     id: 0,
                    //     name: "--- Unknown ---"
                    // }];
                    // data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var tags_select2 = $("#tag").select2({
            ajax: {
                url: "{{ route('admin.tags.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 1,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var categories_select2 = $('body').find("#category_id").select2({
            ajax: {
                url: "{{ route('admin.categories.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        function func_video_select2() {
            var tags_select2 = $("#tag").select2({
                ajax: {
                    url: "{{ route('admin.tags.data_select2') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 1,
                    data: function (params) {
                        //console.log(params);
                        return {
                            q: $.trim(params.term),
                            _token: "{{csrf_token()}}"
                        };
                    },
                    processResults: function (data) {
                        //console.log(data);
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.title,
                                    id: item.id,
                                    value: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        }
        //*******
    })

</script>
<!----************************************************ --->
{{-- js --}}
@if ((old('image')))
<script>
    $('img#this-img').show();

</script>
@else
@if (!empty($song['image']))
<script>
    $('img#this-img').show();

</script>
@else
<script>
    $('img#this-img').hide();

</script>
@endif
@endif
<script>
    var button1 = document.getElementById('choose_image');
    button1.onclick = function () {
        selectFileWithCKFinder('thumbnail');
    };

    function selectFileWithCKFinder(elementId, elementImg) {
        CKFinder.popup({
            resourceType: 'Images',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function (finder) {
                finder.on('files:choose', function (evt) {
                    var file = evt.data.files.first();
                    var output = document.getElementById(elementId);
                    output.value = file.getUrl();
                    $('img#this-img').show();
                    $('img#this-img').attr('src', file.getUrl());
                });
                finder.on('file:choose:resizedImage', function (evt) {
                    var output = document.getElementById(elementId);
                    output.value = evt.data.resizedUrl;
                });
            }
        });


    }

</script>
<script>
    chooseFile('btn-choose_file', 'link', 'audio', 'box-mp3', 'filename');
    var link = $('#link').val();
    if (link.length == 0) {
        $('#audio').hide();
    } else {
        $('#audio').show();
    }

</script>

<script>
    $(function () {
        var modal_add = $('body').find('.modal-add');
        $('body').on('click', '.btns_add', function (e) {
            var title = $(this).data('title');
            var action = $(this).data('action');
            modal_add.find('.modal-title').html(title);
            if (action == 'add_album') {
                modal_add.find('.modal-form').html(`
                        <form id="form_add_album">
                        <div class="form-group">
                            <label for="">Tên album <i class="add_error text-danger error_name"></i></label>
                            <input type="text" id="album_name" name="name" class="form-control">

                        </div>
                        <div class="form-group">
                            <label for="">Slug <i class="add_error text-danger error_slug"></i></label>
                            <input type="text" id="album_slug" name="slug" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Mô tả <i class="add_error text-danger error_description"></i></label>
                            <textarea style="height:100px" name="" id="album_description" cols="20" class="form-control"
                                rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Ảnh <i class="add_error text-danger error_image"></i></label>
                            <input value="" readonly="" min="0" type="hidden" name="image" class="form-control image" id="thumbnail99">
                        </div>
                        <button class="btn btn-primary choose_image99" id="choose_image99" type="button">Chọn ảnh</button>
                        <div class="form-group">
                            <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img99" src="" alt="image">
                        </div>
                        <br>
                        <div class="pull-left">
                            <button class=" btn btn-success" type="submit">Lưu</button>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div style="clear: both">
                        </div>
                    </form>
                        `);
                $('body').find('#this-img99').css('display', 'none');
            } else if (action == 'add_artist') {
                modal_add.find('.modal-form').html(`
                        <form id="form_add_artist">
                        <div class="row">
                            <div class="form-group col-sm-6">
                            <label for="">Tên artist <i class="add_error text-danger error_name"></i></label>
                            <input type="text" name="name" id="artist_name"  class="form-control">
                            <input type="hidden" name="type" value="ajax"  class="form-control">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Slug <i class="add_error text-danger error_slug"></i></label>
                            <input type="text" name="slug" id="artist_slug" class="form-control">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Họ và tên <i class="add_error text-danger error_fullname"></i></label>
                            <input type="text" name="fullname" class="form-control">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="birthday">Sinh nhật</label>
                            <input type="text" placeholder="20/11/1999" name="birthday" id="birthday" class="form-control birthday" autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="">Mô tả <i class="add_error text-danger error_description"></i></label>
                            <textarea style="height:100px" name="" id="artist_description" cols="20" class="form-control"
                                rows="10"></textarea>
                        </div>
                        <div class="col-sm-12">
                            <label for="image">Ảnh <i class="add_error text-danger error_image"></i></label>
                        </div>
                        <div class="form-group col-sm-12">

                            <input value="" readonly="" min="0" type="hidden" name="image" class="form-control image" id="thumbnail99">
                        </div>

                        <div class="form-group col-sm-6">
                            <button class="btn btn-primary choose_image99" id="choose_image99" type="button">Chọn ảnh</button>
                            <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img99" src="" alt="image">
                        </div>
                        <br>
                        <div class="col-sm-12">
                            <div class="pull-left">
                            <button class=" btn btn-success" type="submit">Lưu</button>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div style="clear: both">
                        </div>
                        </div>
                        </div>
                    </form>
                        `);
                $('body').find('#this-img99').css('display', 'none');
                $(".birthday").datepicker({
                    format: "dd/mm/yyyy",
                    weekStart: 0,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true,
                    rtl: true,
                    language: 'vi',
                    orientation: "auto"
                });

            } else if (action = 'add_category') {

            }

        })
    })

</script>

<script>
    $(function () {

        $('body').on('submit', '#form_add_album', function (e) {
            e.preventDefault();

            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            $.ajax({
                url: "{{route('admin.albums.saveAdd')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    if(result.errors == true){
                        msg = result.data;
                        if (msg.name) {
                            form.find('.error_name').html(msg.name);
                        } else {
                            form.find('.error_name').html('');
                        }
                        if (msg.slug) {
                            form.find('.error_slug').html(msg.slug);
                        } else {
                            form.find('.error_slug').html('');
                        }
                    } else {
                        $('#modal-default').modal('toggle');
                    }
                }
            )
        })
    })

</script>

<script>
    $(function () {
        $('body').on('submit', '#form_add_artist', function (e) {
            e.preventDefault();
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            $.ajax({
                url: "{{route('admin.artists.saveAdd')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    console.log(result);
                    if(result.errors == true){
                        msg = result.data;
                        if (msg.name) {
                            form.find('.error_name').html(msg.name);
                        } else {
                            form.find('.error_name').html('');
                        }
                        if (msg.slug) {
                            form.find('.error_slug').html(msg.slug);
                        } else {
                            form.find('.error_slug').html('');
                        }
                    } else {
                        $('#modal-default').modal('toggle');
                    }
                }
            )
        })
    })

</script>

<script>
    $('body').on('keyup', '#album_name', function () {
        ChangeToSlug('album_name', 'album_slug');
    })
    $('body').on('keyup', '#artist_name', function () {
        ChangeToSlug('artist_name', 'artist_slug');
    })

    $(function () {
        $('body').on('click', '#choose_image99', function () {
            CKFinder.popup({
                resourceType: 'Images',
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        var file_url = file.getUrl();
                        $('body').find('#thumbnail99').val(file_url);
                        //input.val(file_url);
                        $('body').find('#this-img99').css('display', 'block');
                        $('body').find('#this-img99').attr("src", file_url);
                        //document.getElementById(idimg).style.display = "block";
                        //document.getElementById(idimg).src = file_url;
                    });
                }
            });
        })
    })





    //  chooseImg('choose_banner', 'banner', 'this-banner');

</script>
<script>
    $(function(){
        $("ul.select2-selection__rendered").sortable({
        containment: 'parent'
        });
    })
</script>




@endsection
