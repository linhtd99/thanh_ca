@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách bài hát
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
    table.dataTable thead .sorting_asc:first-child:after {
        content: "";
    }
    input[type=checkbox] {
        cursor: pointer;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-xs-12">
        <div class="box box-primary">

            <div class="box-header">
                <div style="margin-right: 30px !important;" class="pull-left ml-4">
                    <a href="{{route('admin.songs.add')}}" class="btn btn-primary">Thêm bài hát</a>
                </div>

            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="col-xs-12 ">
                    <form class="form-action" id="form-action">
                        <div style="margin-right: 5px !important;" class="pull-left">
                            <select name="action" id="action" class="form-control col-sm-12 input-sm action "
                                field_signature="2473543864">
                                <option value="0">Tác vụ</option>
                                <option value="delete">Xoá</option>
                                <option value="agree">Duyệt</option>
                                <option value="agreeAll">Duyệt tất cả</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <button id="btn-action" class="btn btn-secondary btn-sm btn-flat btn-action">Áp
                                dụng</button>
                        </div>
                    </form>
                    <form action="" method="get">
                        <div class="search">

                            <div class="pull-right ">
                                <button class="btn btn-flat btn-primary" type="submit">Tìm</button>
                                <a class="btn btn-flat btn-info" href="{{ route('admin.songs.index') }}">Huỷ lọc</a>
                            </div>
                            <div class="pull-right ">
                                <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" type="search"
                                    name="search" placeholder="Tên bài hát" id="search" class="form-control search">
                            </div>
                        </div>
                        <div class="pull-right ">
                            {{-- <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" required
                            type="search" name="search" id="search" class="form-control search"> --}}
                            <select name="status" id="status" class="form-control">
                                <option
                                    {{ !empty($_GET['status']) && ( $_GET['status'] != 0 && $_GET['status'] != 1 && $_GET['status'] != 2) ? 'selected' : '' }}
                                    value="">Trạng
                                    thái</option>
                                <option {{ isset($_GET['status']) && (int)$_GET['status'] == 1 ? 'selected' : '' }}
                                    value="1">Kích hoạt
                                </option>
                                <option
                                    {{ isset($_GET['status'])&& is_numeric($_GET['status']) && ((int)$_GET['status'] == 0) ? 'selected' : '' }}
                                    value="0">Vô hiệu hoá
                                </option>
                                <option {{ isset($_GET['status']) && (int)$_GET['status'] == 2 ? 'selected' : '' }}
                                    value="2">Chờ duyệt
                                </option>
                            </select>
                        </div>
                        <div style="width: 160px !important" class="pull-right">
                            <select name="category" id="category" style="with: 250px !important" class="form-control col-md-12 col-sm-12">
                                <option value="">-- Tất cả danh mục --</option>
                                @if (!empty($category_search->id))
                                <option selected value="{{ $category_search->id }}">{{ $category_search->name }}
                                </option>
                                @endif
                            </select>
                        </div>
                        <div style="width: 150px !important" class="pull-right">
                            <select name="artist" id="artist" style="with: 250px !important" class="form-control col-md-12 col-sm-12">
                                <option value="">-- Tất cả nghệ sĩ --</option>
                                @if (!empty($artist_search->id))
                                <option selected value="{{ $artist_search->id }}">{{ $artist_search->name }}</option>
                                @endif
                            </select>
                        </div>

                    </form>
                </div>
                <br>
                <div class="col-xs-12">
                    <div class="table-responsive ">
                        <table class="table table-hover table-striped" id="table-songs">
                            <thead>
                                <tr>
                                    <th class="no-sort">
                                        <input id="checkAll" value="all" class="checkAll" type="checkbox"
                                            name="checkAll">
                                    </th>
                                    <th scope="col">Tên bài hát</th>
                                    <th scope="col">Danh mục bài hát</th>
                                    <th scope="col">Nghệ sĩ</th>
                                    <th scope="col">Lượt nghe</th>
                                    <th>Trạng thái</th>
                                    <th scope="col">Tác giả đăng</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($listSongs) || count($listSongs) != 0)
                                @foreach ($listSongs as $item)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="checkbox" value="{{  $item->id  }}"
                                            name="checkbox">
                                    </td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        {{ $item->categories['name'] }}
                                    </td>
                                    <td>
                                        @forelse ($item->artists as $artist)
                                        <span class="label label-default">{{ $artist->name }}</span>
                                        @empty - @endforelse

                                    </td>
                                    <td>
                                        {{ (!empty($item->countListen()->listen)) ? $item->countListen()->listen : 0 }}
                                    </td>

                                    <td>
                                        @if ($item->status == 0)
                                        <span class="label label-danger w-100">Vô hiệu hoá</span>
                                        @elseif($item->status == 1)
                                        <span class="label label-success w-100">Kích hoạt</span>
                                        @elseif($item->status == 2)
                                        <span class="label label-warning w-100">Chờ duyệt</span>
                                        @else
                                        {{ $item->status }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ !empty($item->author->name) ? $item->author->name : 'Admin' }}
                                    </td>
                                    <td class="btn-group-xs">
                                        <a href="{{route('song',['name' => name_to_slug($item->name) ,'code' => $item->code])}}"
                                            target="_blank" data-toggle="modal" class="btn btn-primary btn-xs"><i
                                                class="fa fa-info-circle"></i></a>
                                        <a href="{{ route('admin.songs.edit', $item->id) }}" class="btn btn-warning"><i
                                                class="fa fa-pencil"></i></a>
                                        <button data-remove="{{ $item->id }}" class="btn btn-danger btn-remove"><i
                                                class="fa fa-remove "></i></button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                Không có dữ liệu
                                @endif
                            </tbody>
                        </table>
                        <div class="pagination">
                            {{ $listSongs->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here -->
        </div>
    </div>
</div>

<!-- end main content -->








<!----- javascript here -------->
@section('js')
<script>
    $(function () {
        var categories_select2 = $("#category").select2({
            ajax: {
                url: "{{ route('admin.categories.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 1000,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    let empt = [{
                        created_at: "1999-11-27 02:02:41",
                        description: '',
                        id: '',
                        name: "-- Tất cả danh mục --",
                        parent_id: 0,
                        slug: "Tất cả danh mục",
                        updated_at: "1999-11-27 02:02:41",
                    }];
                    data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        var artists_select2 = $("#artist").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: '',
                        name: "-- Tất cả ca sĩ --"
                    }];
                    data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    })

</script>
<script>
    $(function () {
        $('body').on('click', '.btn-remove', function (e) {
            // alert('ok');

            var id = $(this).data('remove');
            // alert(removeUrl);
            swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn muốn xoá mục này không?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.songs.delete')}}",
                            method: 'POST',
                            data: {
                                id: id,
                                _token: '{{csrf_token()}}'
                            }
                        }).done(result => {
                            if (result) {
                                swal("Deleted!",
                                    "Danh Mục đã bị xoá.",
                                    "success");
                            };
                            setTimeout(function () {
                                location.reload();
                            }, 1500);


                        });
                    }
                });
        });
    })

</script>


<!---  xoá nhiều --------->

<script>
    $(function () {
        $('body').on('submit', '#form-action', function (e) {
            e.preventDefault();
            var action = $('body').find('#action').val();
            if (parseInt(action) != 0) {
                var data = [];
                $.each($("input[name='checkbox']:checked"), function () {
                    data.push($(this).val());
                });
                var formData = new FormData();
                formData.append('data', data);
                if (action == 'agreeAll') {
                    var c = confirm("Bạn có chắc chắn muốn duyệt các mục đã chọn?");
                    if (c === true) {
                        ajaxFunc("{{ route('admin.songs.setActiveStatusAll') }}", formData);
                        swal({
                            title: "Thông báo",
                            text: "Duyệt bài thành công!",
                            icon: "success",
                            button: "OK",
                        }).then((value) => {
                            window.location.reload();

                        })

                    }
                    return false;
                }
                if (data.length != 0) {
                    if (action == 'delete') {
                        var c = confirm("Bạn có chắc chắn muốn xoá mục đã chọn?");
                        if (c === true) {
                            ajaxFunc("{{ route('admin.songs.multiDel') }}", formData);
                            swal({
                                title: "Thông báo",
                                text: "Xoá bài hát thành công!",
                                icon: "success",
                                button: "OK",
                            }).then((value) => {
                                window.location.reload();
                            })

                        }
                    }
                    if (action == 'agree') {
                        var c = confirm("Bạn có chắc chắn muốn duyệt các mục đã chọn?");
                        if (c === true) {
                            ajaxFunc("{{ route('admin.songs.setActiveStatus') }}", formData);
                            swal({
                                title: "Thông báo",
                                text: "Duyệt bài thành công!",
                                icon: "success",
                                button: "OK",
                            }).then((value) => {
                                window.location.reload();
                            })

                        }
                    }

                } else {
                    alert('Vui lòng chọn danh mục!')
                }

            }
        })
    })

</script>
<script>
    $('#table-songs').DataTable({
        'paging': false,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': false,
        'autoWidth': true,
        "columnDefs": [
            {
            "targets": 0,
            "orderable": false
            },
            {
            "targets": 7,
            "orderable": false
            },
        ]
    })

</script>

@endsection
<!----- end javascript here -------->

@endsection
