@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách người dùng
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <a href="{{route('admin.customers.add')}}" class="btn btn-primary">Thêm</a>

                    {{-- Danh sách quản trị --}}
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12">
                    <form class="form-action" id="form-action">
                        <div style="margin-right: 5px !important;" class="pull-left">
                            <select name="action" id="action" class="form-control input-sm action" field_signature="2473543864">
                                <option value="0">Tác vụ</option>
                                <option value="delete">Xoá</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <button id="btn-action" class="btn btn-secondary btn-sm btn-flat btn-action">Áp
                                dụng</button>
                        </div>
                    </form>
                </div>
                <!--  content here -->
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="table-customers"></table>
                    </div>
                </div>



                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->








<!----- javascript here -------->
@section('js')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {

        var table = $('#table-customers').DataTable({

            serverSide: true,
            processing: true,
            responsive: true,
            "bLengthChange" : false,
            ajax: "{{ route('admin.customers.data') }}",
            // order: [
            //     [0, 'desc']
            // ],
            lengthMenu: [20, 30, 50, 100],
            iDisplayLength: 20,
            columnDefs: [{
                    "targets": [0],
                    "searchable": false,
                    "orderable": false,
                },
                {
                    "targets": [1],
                    "searchable": false,
                    "orderable": false,
                },
                {
                    "targets": [2],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [3],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [4],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [5],
                    "searchable": true,
                    "orderable": true,
                }
            ],
            columns: [
                {
                    data: 'id',
                    title: `<input id="checkAll" value="all" class="checkAll" type="checkbox" name="checkAll"> `,
                    render:function (data) {
                        var html = `<td>
                                        <input type="checkbox" class="checkbox" value="${data}" name="checkbox">
                                    </td>`;
                        return html;
                    }
                },
                {
                    data: 'id',
                    title: "ID"
                },
                {
                    data: 'username',
                    title: "Tên đăng nhập",
                    autoWidth: true
                },
                {
                    data: 'name',
                    title: "Tên người dùng",
                    autoWidth: true
                },
                {
                    data: 'email',
                    title: "Email",
                    autoWidth: true
                },
                {
                    data: 'status',
                    title: "Trạng thái",
                    autoWidth: true,
                    render: function (data) {
                    html = `${data}` == 1 ? 'Kích hoạt' : 'Không kích hoạt'
                    ;
                    return html;
                    }
                },
                {
                    data: 'id',
                    title: 'Hành động',
                    autoWidth: true,
                    render: function (data) {
                        // console.log(data);
                        var html =
                            `
                            <a href="{{url('/').'/admin/customers/detail/'}}${data}" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i></a>
                            <a href="{{url('/').'/admin/customers/edit/'}}${data}" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                            <a url="{{url('/').'/admin/customers/delete/'}}${data}" data-toggle="modal" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-remove"></i></a>
                            `;
                        return html;
                    }
                }

            ],
            "language": {
                "lengthMenu": "Hiển thị _MENU_ bản ghi trên trang",
                "zeroRecords": "Không có dữ liệu để hiển thị",
                "info": "Trang hiển thị _PAGE_ / _PAGES_",
                "infoEmpty": "Không có dữ liệu để hiển thị",
                "infoFiltered": "(được lọc từ _MAX_ tổng số hồ sơ)",
                "search": 'Tìm kiếm:   ',
                "paginate": {
                    "first": "Trang đầu",
                    "last": "Trang cuối",
                    "next": "Trang sau",
                    "previous": "Trang trước"
                },
            },
            initComplete: function() {
            $('body').on('click', '.btn-remove', function() {
                var id = $(this).data('remove');
                swal({
                    title: "Bạn có chắc chắn?",
                    text: "Sau khi xóa, bạn sẽ không thể khôi phục danh mục này!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.customers.delete')}}",
                            method: 'POST',
                            data: {
                                id: id,
                                _token: '{{csrf_token()}}'
                            }
                        }).done(result => {
                            table.ajax.reload();
                        });
                    } else {

                    }
                    })
                })
            }


        })
    })
    $(function () {
        $('body').on('click', '#btn-action', function (e) {
            // console.log('a');
            e.preventDefault();
            var action = $('body').find('#action').val();
            if (parseInt(action) != 0) {
                if (action == 'delete') {
                    var data = [];
                    $.each($("input[name='checkbox']:checked"), function () {
                        data.push($(this).val());
                    });
                    // console.log(data);
                    if (data.length != 0) {
                        var c = confirm("Bạn có chắc chắn muốn xoá mục đã chọn?");
                        if (c === true) {
                            var formData = new FormData();
                            formData.append('data', data);
                            ajaxFunc("{{route('admin.customers.deleteMulti')}}", formData);
                        }
                    } else {
                        alert('Vui lòng chọn !')
                    }
                }
            }
        })
    })




</script>


@endsection
<!----- end javascript here -------->

@endsection
