@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách người dùng
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Sửa người dùng
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form method="POST" action="{{route('admin.customers.saveEdit')}}">
                    {{@csrf_field()}}
                    <input type="hidden" name="id" value="{{$customer->id}}">
                    <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="name">Tài khoản đăng nhập</label>
                            <input class="form-control" type="text" name="username" id="username" value="{{ !empty(old('username')) ? old('username') : $customer->username}}">
                            @error('username')
                            <span style="color:red">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">Tên người dùng</label>
                            <input class="form-control" type="text" name="name" id="name"
                                value="{{ !empty(old('name')) ? old('name') : $customer->name}}">
                            @error('name')
                            <span style="color:red">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input class="form-control" type="text" name="email" id="email"
                                value="{{ !empty(old('email')) ? old('email') : $customer->email}}">
                            @error('email')
                            <span style="color:red">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="phone">Số điện thoại</label>
                            <input class="form-control" type="number" name="phone" id="phone" value="{{ !empty(old('phone')) ? old('phone') : $customer->phone}}">

                        </div>
                        <div class="form-group">
                            <label for="password">Mật khẩu</label>
                            <input class="form-control" type="password" name="password" id="password"
                                value="{{ !empty(old('password')) ? old('password') : ''}}">
                            @error('password')
                            <span style="color:red">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="birthday">Ngày sinh</label>
                            <input class="form-control" type="date" name="birthday"  id="birthday"
                                value="{{ !empty(old('birthday')) ? old('birthday') : $customer->birthday }}">
                        </div>
                        <div class="form-group">
                            <label for="sex">Giới tính</label>
                            <select name="sex" id="sex" class="form-control">
                                <option {{ $customer->sex == -1 ? 'selected' : '' }} value="-1">Khác</option>
                                <option {{ $customer->sex == 0 ? 'selected' : '' }} value="0">Nam</option>
                                <option {{ $customer->sex == 1 ? 'selected' : '' }} value="1">Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select name="status" id="status" class="form-control">
                                <option {{$customer->status == 0 ? 'selected' : ''}} value="0">Không kích hoạt</option>
                                <option {{$customer->status == 1 ? 'selected' : ''}} value="1">Kích hoạt</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="image">Avatar</label>
                            <input  value="{{ (old('image')) ? old('image') : ( !empty($customer['avatar']) ? $customer['avatar'] : '' ) }}" readonly
                                min="0" type="hidden" name="image" class="form-control image" id="thumbnail">
                        </div>
                        <button class="btn btn-primary choose_image" id="choose_image" type="button">Choose Image</button>
                        <br>
                        <div class=form-group>
                            <img style="width: auto; height: 350px; padding-top: 10px !important" id="this-img"
                                src="{{ (old('image')) ? old('image') : ( !empty($customer['avatar']) ? $customer['avatar'] : '' ) }}" alt="">
                        </div>
                    </div>
                    </div>
                    <div class="form-group">
                        <button class=" btn btn-success" type="submit">Lưu</button>
                    </div>
                </form>
                <!-- end content here -->
            </div>
        </div>
    </div>
    {{-- /////////////////// --}}

</div>
<!-- end main content -->




@endsection
<!----- end javascript here -------->
@section('js')

{{-- ckfinder --}}
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config( { connectorPath: '/ckfinder/connector' } );
</script>

{{-- js --}}
@if ((old('image')))
<script>
    $('img#this-img').show();
</script>
@else
@if (!empty($customer['avatar']))
<script>
    $('img#this-img').show();
</script>
@else
<script>
    $('img#this-img').hide();
</script>
@endif
@endif
<script>
    var button1 = document.getElementById('choose_image');

    button1.onclick = function() {
        selectFileWithCKFinder( 'thumbnail' );
    };

    function selectFileWithCKFinder( elementId, elementImg ) {
        CKFinder.popup( {
            resourceType: 'Images',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();
                    var output = document.getElementById( elementId );
                    output.value = file.getUrl();
                    $('img#this-img').show();
                    $('img#this-img').attr('src', file.getUrl());
                } );

                finder.on( 'file:choose:resizedImage', function( evt ) {
                    var output = document.getElementById( elementId );
                    output.value = evt.data.resizedUrl;
                } );
            }
        } );
    }
</script>
@endsection
