@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Người dùng chi tiết
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="box box-danger">
    <div class="box-header">
        <h3 class="box-title">
            <a class="btn btn-primary" href="{{route('admin.customers.index')}}">Quay lại danh sách</a>


        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-3 col-xs-3">
                <div class="img-responsive">
                    <img width="100%"
                        src="{{ ($customer['avatar']) ? $customer['avatar'] : 'https://x.kinja-static.com/assets/images/logos/placeholders/default.png' }}"
                        alt="">
                </div>
            </div>
            <div class="col-sm-9 col-xs-9">
                <div class="card card-group">
                    <div class="card-body">
                        <div>
                            <h3>Tên người dùng: {{$customer['name']}}</h3>
                        </div>
                        <div>
                        <h3>Tài khoản đăng nhập: {{$customer['username']}}</h3>
                        </div>
                        
                        <div>
                            <div>Mô tả:</div>
                            @php
                            echo ($customer['description']) ? trim($customer['description']) : ''
                            @endphp


                        </div>
                        <div>
                            <h5>Trạng thái:
                                <span>{{ ($customer['status'] == 0 ) ? 'Không hiển thị'  : 'Không hiển thị' }}</span>
                            </h5>
                        </div>

                        <div>
                            <h5>Thời gian thêm :
                                <span>{{ (!empty($customer['created_at'])) ? date_format($customer['created_at'],"d/m/Y H:i:s")  : 'N/A' }}</span>
                            </h5>
                        </div>
                        <div>
                            <h5>Thời gian sửa:
                                <span>{{ (!empty($customer['updated_at'])) ? date_format($customer['updated_at'],"d/m/Y H:i:s")  : 'N/A' }}</span>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /row -->
</div>






@endsection
<!----- end javascript here -------->