{{-- {{ dd($song->name) }} --}}

@extends('admin.layout.master')
{{-- page title --}}
@section('page_title')
Sửa bài hát
@endsection
{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    b.btn {
        margin: 5px !important;
    }

    audio {
        margin: 5px 0px !important;
        display: none;
    }

    .grid-container {
        display: grid;
        grid-template-columns: auto auto auto;

        grid-gap: 10px;
    }

    .item1 {
        grid-column: 1 / 12;
    }

    .item2 {
        grid-column: 12 / 12;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 17px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }

</style>
@endsection
{{-- content --}}
@section('content')
{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}
<!-- main content -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    @yield('page_title')
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form method="POST" id=create_song action="{{route('admin.customers.saveAdd')}}">
                    {{ @csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name">Tên bài hát</label>
                                <input type="hidden" value="{{ $song->id }}" name="id">
                                <input onkeyup="javascript: ChangeToSlug('name', 'slug')" class="form-control"
                                    type="text" name="name" id="name" value="{{ $song->name }}">
                                <div class="error text-danger text-bold error_name"></div>
                                @error('name')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input class="form-control" type="text" name="slug" id="slug" value="{{ $song->slug }}">
                                <div class="error text-danger text-bold error_slug"></div>
                                @error('slug')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="album_id">Album</label>
                                <select class="form-control" name="album_id" id="album_id">
                                    <option value="{{ $song->albums['id'] }}" selected="selected">
                                        {{ $song->albums['name'] }}</option>
                                </select>
                                <div class="error text-danger text-bold error_album_id"></div>
                                @error('album_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="category_id">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <option value="{{ $song->categories['id'] }}" selected="selected">
                                        {{ $song->categories['name'] }}</option>

                                </select>
                                <div class="error text-danger text-bold error_category_id"></div>
                                @error('category_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="artist_id">Nghệ sĩ thể hiện:</label>
                                <select class="form-control" multiple name="artist_id[]" id="artist_id">
                                    @foreach ($song->artists as $artist)
                                    <option value="{{ $artist['id'] }}" selected="selected">
                                        {{ $artist['name'] }}
                                    </option>
                                    @endforeach

                                </select>
                                <div class="error text-danger text-bold error_artist_id"></div>
                                @error('artist_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="video_id">Video đính kèm</label>
                                <div class="box-slider box-slider-video">
                                    <label class="switch switch-video">
                                        <input value="1" type="checkbox" name="check_video">
                                        <span class="slider slider-video"></span>
                                    </label>


                                </div>
                                <div class="box-video">
                                    <input type="hidden" name="check_change_video" value="2">
                                    <select class="form-control" name="video_id" id="video_id">
                                        @if ($song->video_id)
                                            <option selected value="{{ $song->video['id'] }}">{{ $song->video['name'] }}</option>
                                        @endif
                                    </select>
                                    <div class="error text-danger text-bold error_video"></div>
                                </div>

                                @error('video_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Trạng thái</label>
                                <select class="form-control" name="status" id="status" class="form-control">
                                    <option {{ ($song->status == 1)? 'selected' : '' }} value="1">Kích hoạt</option>
                                    <option {{ ($song->status == 0)? 'selected' : '' }} value="0">Không kích hoạt
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="">
                                    <label for="tag">Tag</label>
                                </div>
                                <div class="box-tag">
                                    <div class="grid-container">
                                        <div style="" class="grid-item item1">
                                            <select multiple name="tag[]" id="tag" class="tag form-control">
                                                @foreach ($song->tags as $tag)
                                                <option value="{{ $tag['id'] }}" selected="selected">
                                                    {{ $tag['title'] }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div style="" class="grid-item item2">
                                            <button data-toggle="modal" data-target="#addTag" type="button"
                                                class="btn btn-flat btn-google btn-sm">Add
                                                tag</button>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">Giới thiệu bài hát</label>
                                <textarea style="min-width:100%; max-width: 100%; min-height: 100px"
                                    class="form-control" style="width:100%" name="description" id="description" cols="5"
                                    rows="5">{{ $song->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <input
                                    value="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                    readonly min="0" type="hidden" name="image" class="form-control image"
                                    id="thumbnail">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-primary choose_image" id="choose_image" type="button">Chọn
                                    Ảnh</button>
                                <span style="margin-left: 10px !important" class="boxs-img" class="">
                                    <i style="display:none" class="btn btn-danger btn-xs btn-remove-img">
                                        <i class="fa fa-times"></i>
                                    </i>
                                    <img style="width: 60px; height: auto;" id="this-img"
                                        src="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                        alt="">
                                </span>
                            </div>
                            <div class="form-group">
                                <input
                                    value="{{ (old('link')) ? old('link') : ( !empty($song['link']) ? $song['link'] : '' ) }}"
                                    readonly type="hidden" name="link" class="form-control link" id="link">
                                <input readonly type="hidden" name="filename" class="form-control filename"
                                    id="filename" value="{{ $song->filename }}">
                                <span>
                                    <span class="">
                                        <button id="btn-choose_file" type="button" class="btn btn-sm ">Chọn bài
                                            hát</button>
                                    </span>
                                    <span style="margin: 0px 5px !important "
                                        class="pull-right-container my-2 text-bold text-dark"
                                        id="file_name">{{ $song->filename }}</span>
                                    <div class="error text-danger text-bold error_song"></div>
                                    <div class="error text-danger text-bold error_link"></div>
                                    <div class="error text-danger text-bold error_filename"></div>
                                </span>
                                <div style="" id="box-mp3" class="box-mp3">
                                    <span style="margin-top: 10px !important">
                                        <audio volume="0.5" style="margin-top: 10px !important;height: 50px !important;"
                                            controls id="audio" class="same-form-control"
                                            src="{{ (old('link')) ? old('link') : ( !empty($song['link']) ? $song['link'] : '' ) }}"></audio>

                                    </span>
                                    <div class="btn-group-sm">
                                        <a onclick="getCurTime()" class="btn-get-time btn btn-primary btn-flat">Get
                                            time</a>
                                        <a onclick="togglePlay()" class="btn-play btn btn-info btn-flat">Play/Pause</a>
                                        <a onclick="next()" class="btn-next btn btn-info btn-flat"> &gt;&gt; Next 5s</a>
                                        <a onclick="prev()" class="btn-prev btn btn-info btn-flat"> &lt;&lt; Prev 5s</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lyric">Lời bài hát</label>
                                <div class="box-slider box-slider-lyric">
                                    <label class="switch switch-lyric">
                                        <input value="1" type="checkbox" name="check_lyric">
                                        <span class="slider slider-lyric"></span>
                                    </label>
                                </div>
                                <div class="box-lyric">
                                    <input type="hidden" name="check_change_lyric" value="2">
                                    <select class="form-control" name="lyric_id" id="lyric_id"></select>
                                    <div class="error text-danger text-bold error_lyric"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" value="0" name="author_id">
                                <button class=" btn btn-success" type="submit">Lưu</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->

<!--  Modal -->
<div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="addTag" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content box box-danger">
            <div class="modal-header">
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Sửa Tag</b></h4>
                </div>
                <div id="show-alert">

                </div>
            </div>
            <div class="modal-body">
                <form id=add-new-tags action="" method="post">
                    <div class="form-group">
                        <input style="min-width: 100%" class="form-control" id="input-tag" type="text" value=""
                            data-role="tagsinput">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-flat btn-success">Lưu</button>
                    </div>
                </form>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div> --}}
        </div>
    </div>
</div>
<!-- end Modal -->
@endsection
<!----- end javascript here -------->
@section('js')
<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.js') }}"></script>
<script>
    $("#input-tag").val()

</script>
<script>



</script>
<script>
    $(function () {
        var videos_select2 = $("#video_id").select2({
        ajax: {
            url: "{{ route('admin.videos.data_select2') }}",
            type: "post",
            dataType: 'json',
            delay: 1000,
            data: function (params) {
                //console.log(params);
                return {
                    q: $.trim(params.term),
                    _token: "{{csrf_token()}}"
                };
            },
            processResults: function (data) {
                //console.log(data);
                var empt = [{
                    id: 0,
                    name: "--- Unknown ---"
                }];
                data = empt.concat(data);

                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id,
                            value: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
    var lyrics_select2 = $("#lyric_id").select2({
        ajax: {
            url: "{{ route('admin.lyrics.data_select2') }}",
            type: "post",
            dataType: 'json',
            delay: 10,
            data: function (params) {
                //console.log(params);
                return {
                    q: $.trim(params.term),
                    _token: "{{csrf_token()}}"
                };
            },
            processResults: function (data) {
                //console.log(data);
                var empt = [{
                    id: 0,
                    name: "--- Unknown ---"
                }];
                data = empt.concat(data);

                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id,
                            value: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
        $('body').on('submit', '#add-new-tags', function (e) {
            e.preventDefault();
            var tags = $("#input-tag").tagsinput('items');
            //console.log(tags);
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            dataForm.set('tags', tags);
            if (Array.isArray(tags) && tags.length > 0) {
                $.ajax({
                    url: "{{ route('admin.tags.saveNewTags') }}",
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: dataForm,
                }).done(
                    result => {
                        $('#input-tag').tagsinput('removeAll');
                        $('#input-tag').tagsinput('destroy');
                        $('.modal').modal('show');
                    }
                )
            }

        })
        $('#create_song').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            //console.log(dataForm);
            $.ajax({
                url: "{{route('admin.songs.updateSong')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    //console.log(msg);
                    if (result.errors) {
                        console.log(msg);
                        if (msg.name) {
                            form.find('.error_name').html(msg.name);
                        } else {
                            form.find('.error_name').html(' ');
                        }
                        if (msg.slug) {
                            form.find('.error_slug').html(msg.slug);
                        } else {
                            form.find('.error_slug').html('');
                        }
                        if (msg.video) {
                            form.find('.error_video').html(msg.video);
                        } else {
                            form.find('.error_video').html('');
                        }

                        if (msg.song) {
                            form.find('.error_song').html(msg.song);
                        } else {
                            form.find('.error_song').html('');
                        }

                        if (msg.link) {
                            form.find('.error_link').html(msg.link);
                        } else {
                            form.find('.error_link').html('');
                        }

                        if (msg.filename) {
                            form.find('.error_filename').html(msg.filename);
                        } else {
                            form.find('.error_filename').html('');
                        }
                        if (msg.lyric) {
                            form.find('.error_lyric').html(msg.lyric);
                        } else {
                            form.find('.error_lyric').html('');
                        }
                    } else {
                        form.find('.error').html('');
                        // $('#create_song').trigger("reset");
                        // $('#this-img').attr('src', '');
                        console.log(result);
                        window.location.href = "{{ route('admin.songs.index') }}";
                        // table.ajax.reload();
                        // window.location.reload();
                    }
                    // table.ajax.reload();
                });
        })
    })

</script>
<!------ Slider toggle ------>
<script>
    $(function () {
        /**
         *  toggle video
         **/
        $('body').on('click', '.slider-video', function (e) {
            var check = $('input[name="check_video"]').is(':checked');

            if (check == true) {
                $('input[name="check_video"]').val(2);
                $('body .box-video').html(
                    `<input type="hidden" name="check_change_video" value="2">
                    <select class="form-control" name="video_id" id="video_id"></select>
                    <div class="error text-danger text-bold error_video"></div>`
                );
                var videos_select2 = $("#video_id").select2({
                    ajax: {
                        url: "{{ route('admin.videos.data_select2') }}",
                        type: "post",
                        dataType: 'json',
                        delay: 1000,
                        data: function (params) {
                            //console.log(params);
                            return {
                                q: $.trim(params.term),
                                _token: "{{csrf_token()}}"
                            };
                        },
                        processResults: function (data) {
                            //console.log(data);
                            var empt = [{
                                id: 0,
                                name: "--- Unknown ---"
                            }];
                            data = empt.concat(data);

                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                        value: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });

            } else {
                $('input[name="check_video"]').val(1);
                $('body .box-video').html(
                    `<input type="hidden" name="check_change_video" value="1">
                    <input type="text" name="video" class="video form-control" id="video">
                    <div class="error text-danger text-bold error_video"></div>`
                );
            }
        })
        /**
         *  toggle lyric
         **/
        $('body').on('click', '.slider-lyric', function (e) {
            var check = $('input[name="check_lyric"]').is(':checked');
            var check_change = $('body').find('input[name="check_change_lyric"]').val();
            if (check == true && parseInt(check_change) == 1) {
                $('input[name="check_video"]').val(1);
                $('body .box-lyric').html(
                    `<input type="hidden" name="check_change_lyric" value="2">
                    <select class="form-control" name="lyric_id" id="lyric_id"></select>
                    <div class="error text-danger text-bold error_lyric"></div>`
                );
                var lyrics_select2 = $("#lyric_id").select2({
                    ajax: {
                        url: "{{ route('admin.lyrics.data_select2') }}",
                        type: "post",
                        dataType: 'json',
                        delay: 10,
                        data: function (params) {
                            //console.log(params);
                            return {
                                q: $.trim(params.term),
                                _token: "{{csrf_token()}}"
                            };
                        },
                        processResults: function (data) {
                            //console.log(data);
                            var empt = [{
                                id: 0,
                                name: "--- Unknown ---"
                            }];
                            data = empt.concat(data);
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                        value: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });

            } else {
                $('body .box-lyric').html(
                    `<input type="hidden" name="check_change_lyric" value="1">
                    <div class="error text-danger text-bold error_lyric"></div>
                    <textarea style="min-width: 100%; max-width:100%" name="lyric"
                            class="col-sm-12 lyric form-control"
                                    id="lyric" cols="10" rows="10"></textarea>`
                );
            }
        })
    })

</script>
<script>
    // music
    var myAudio = document.getElementById("audio");
    var isPlaying = false;
    myAudio.onplaying = function () {
        isPlaying = true;
    };
    myAudio.onpause = function () {
        isPlaying = false;
    };

    function togglePlay() {
        if (isPlaying) {
            myAudio.pause()
        } else {
            myAudio.play();
        }
    }

    function getCurTime() {
        myAudio.pause()
        var time = myAudio.currentTime;
        var format_time = ('*' + parseInt(time / 60)) + ':' + parseFloat((time % 60)).toFixed(2) + '|';
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(format_time).select();
        document.execCommand("copy");
        $temp.remove();
        return time;
    }

    function next() {
        var time = getCurTime();
        myAudio.currentTime = time + 5;
    }

    function prev() {
        var time = getCurTime();
        myAudio.currentTime = time - 5;
    }

</script>
{{-- ckfinder --}}
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script src="{{ asset('js/typeahead.jquery.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.js') }}"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>
<!----************************************************ --->
<script>
    $(function () {
        // ******
        var albums_select2 = $("#album_id").select2({
            ajax: {
                url: "{{ route('admin.albums.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var artists_select2 = $("#artist_id").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var tags_select2 = $("#tag").select2({
            ajax: {
                url: "{{ route('admin.tags.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 1,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        var categories_select2 = $("#category_id").select2({
            ajax: {
                url: "{{ route('admin.categories.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    /**
                    var empt = [{
                        id: 0,
                        name: "-- không --"
                    }];
                    data = empt.concat(data);

                    **/

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        function func_tags_select2() {
            var tags_select2 = $("#tag").select2({
                ajax: {
                    url: "{{ route('admin.tags.data_select2') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 1,
                    data: function (params) {
                        //console.log(params);
                        return {
                            q: $.trim(params.term),
                            _token: "{{csrf_token()}}"
                        };
                    },
                    processResults: function (data) {
                        //console.log(data);
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.title,
                                    id: item.id,
                                    value: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        }
        //*******
    })

</script>
<!----************************************************ --->
{{-- js --}}
@if ((old('image')))
<script>
    $('img#this-img').show();

</script>
@else
@if (!empty($song['image']))
<script>
    $('img#this-img').show();

</script>
@else
<script>
    $('img#this-img').hide();

</script>
@endif
@endif
<script>
    var button1 = document.getElementById('choose_image');
    button1.onclick = function () {
        selectFileWithCKFinder('thumbnail');
    };

    function selectFileWithCKFinder(elementId, elementImg) {
        CKFinder.popup({
            resourceType: 'Images',
            startupPath: 'Images:/images/',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function (finder) {
                finder.on('files:choose', function (evt) {
                    var file = evt.data.files.first();
                    var output = document.getElementById(elementId);
                    output.value = file.getUrl();
                    $('img#this-img').show();
                    $('img#this-img').attr('src', file.getUrl());
                });
                finder.on('file:choose:resizedImage', function (evt) {
                    var output = document.getElementById(elementId);
                    output.value = evt.data.resizedUrl;
                });
            }
        });
    }

</script>
<script>
    chooseFile('btn-choose_file', 'link', 'audio', 'box-mp3', 'filename');
    var link = $('#link').val();
    if (link.length == 0) {
        $('#audio').hide();
        $('#box-mp3').hide();
    } else {
        $('#audio').show();
        $('#box-mp3').show();
    }


</script>
{{-- tag js --}}




@endsection
