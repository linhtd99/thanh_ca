@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách bài hát
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    <div class="col-sm-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    Thêm mới chủ đề
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="">
                    <form id="form_add">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Tên nhóm</label>
                            <input type="text" class="form-control title" id="title" name="title">
                            <div class="error text-danger text-bold error_title"></div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Mô tả</label>
                            <textarea style="max-width: 100%;" class="form-control description" id="description"
                                name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Ảnh</label>
                            <input
                                value="{{ (old('image')) ? old('image') : ( !empty($product['image']) ? $product['image'] : '' ) }}"
                                readonly min="0" type="hidden" name="image" class="form-control image" id="thumbnail1">
                        </div>
                        <button class="btn btn-primary choose_image1" id="choose_image1" type="button">Chọn
                            ảnh</button>
                        <br>
                        <div class=form-group>
                            <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img1"
                                src="{{ (old('image')) ? old('image') : ( !empty($product['image']) ? $product['image'] : '' ) }}"
                                alt="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success submit_add">Lưu</button>
                        </div>

                    </form>
                </div>
                <!-- end content here -->
            </div>
        </div>
    </div>
    {{-- /////////////////// --}}
    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    Danh sách chủ đề
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="col-xs-12 ">
                    <form class="form-action" id="form-action">
                        <div style="margin-right: 5px !important;" class="pull-left">
                            <select name="action" id="action" class="form-control input-sm action" field_signature="2473543864">
                                <option value="0">Tác vụ</option>
                                <option value="delete">Xoá</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <button id="btn-action" class="btn btn-secondary btn-sm btn-flat btn-action">Áp
                                dụng</button>
                        </div>
                    </form>
                    <form action="" method="get">
                        <div class="search">
                            <div class="pull-right ">
                                <button class="btn btn-flat btn-primary" type="submit">Tìm</button>
                            </div>
                            <div class="pull-right ">
                                <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" type="search"
                                    name="search" id="search" class="form-control search">
                            </div>
                        </div>
                        <div style="display:none" class="pull-right ">
                            {{-- <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" required
                            type="search" name="search" id="search" class="form-control search"> --}}
                            <select name="status" id="status" class="form-control">
                                <option
                                    {{ !empty($_GET['status']) && ( $_GET['status'] != 0 && $_GET['status'] != 1 && $_GET['status'] != 2) ? 'selected' : '' }}
                                    value="">Trạng
                                    thái</option>
                                <option {{ isset($_GET['status']) && (int)$_GET['status'] == 1 ? 'selected' : '' }}
                                    value="1">Kích hoạt
                                </option>
                                <option
                                    {{ isset($_GET['status'])&& is_numeric($_GET['status']) && ((int)$_GET['status'] == 0) ? 'selected' : '' }}
                                    value="0">Vô hiệu hoá
                                </option>
                                <option {{ isset($_GET['status']) && (int)$_GET['status'] == 2 ? 'selected' : '' }}
                                    value="2">Chờ duyệt
                                </option>
                            </select>
                        </div>

                    </form>
                </div>
                <br>
                <div class="col-xs-12">
                    <div class="table-responsive ">
                        <table class="table table-hover table-striped  " id="table-songs">
                            <thead>
                                <tr>
                                    <th>
                                        <input id="checkAll" value="all" class="checkAll" type="checkbox" name="checkAll">
                                    </th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Tên chủ đề</th>
                                    <th style="width: 100px" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($topics))
                                    @foreach ($topics as $item)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="checkbox" value="{{  $item->id  }}" name="checkbox">
                                            </td>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>
                                                <button data-edit="{{ $item->id }}" class="btn-update btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>
                                                <button url="{{ route('admin.topics.delete', $item->id) }}" class="btn btn-delete btn-danger btn-xs"><i class="fa fa-remove "></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                            </tbody>
                        </table>
                        <div class="pagination">

                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here -->
        </div>
    </div>
</div>

<!-- end main content -->








<!----- javascript here -------->
@section('js')
<script>
    $(function () {
        $('body').on('click', '.btn-remove', function (e) {
            // alert('ok');

            var id = $(this).data('remove');
            // alert(removeUrl);
            swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn muốn xoá mục này không?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.songs.delete')}}",
                            method: 'POST',
                            data: {
                                id: id,
                                _token: '{{csrf_token()}}'
                            }
                        }).done(result => {
                            if (result) {
                                swal("Deleted!",
                                    "Danh Mục đã bị xoá.",
                                    "success");
                            };
                            setTimeout(function () {
                                location.reload();
                            }, 1000);


                        });
                    }
                });
        });
    })

</script>


<script>
    // function name(button id, input id, img id);
    chooseImg('choose_image1', 'thumbnail1', 'this-img1');

</script>



<script>
    $(function () {
        $('body').on('submit', '#form_add', function (e) {
            e.preventDefault();
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            console.log(dataForm);
            $.ajax({
                url: "{{route('admin.topics.saveAdd')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    //console.log(msg);
                    if (result.errors) {
                        console.log(msg);
                        if (msg.title) {
                            form.find('.error_title').html(msg.title);
                        } else {
                            form.find('.error_title').html(' ');
                        }
                    } else {
                        form.find('.error').html('');
                        console.log(result);
                        window.location.href = "{{ route('admin.topics.index') }}";
                    }
                });
        })
    })
    $(function () {
        $('body').on('click', '#btn-action', function (e) {
            // console.log('a');
            e.preventDefault();
            var action = $('body').find('#action').val();
            if (parseInt(action) != 0) {
                if (action == 'delete') {
                    var data = [];
                    $.each($("input[name='checkbox']:checked"), function () {
                        data.push($(this).val());
                    });
                    // console.log(data);
                    if (data.length != 0) {
                        var c = confirm("Bạn có chắc chắn muốn xoá mục đã chọn?");
                        if (c === true) {
                            var formData = new FormData();
                            formData.append('data', data);
                            ajaxFunc("{{route('admin.topics.deleteMulti')}}", formData);
                        }
                    } else {
                        alert('Vui lòng chọn danh mục!')
                    }
                }
            }
        })
    })

</script>

@endsection
<!----- end javascript here -------->

@endsection
