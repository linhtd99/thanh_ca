@extends('admin.layout.master')
{{-- page title --}}
@section('page_title')
Thêm mới chủ đề
@endsection
{{-- style --}}
@section('style')

@endsection
{{-- content --}}
@section('content')
{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}
<!-- main content -->
<div class="row">
    <div class="col-sm-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    {{-- <button class="btn btn-sm btn-primary add-group" id="add-group" data-toggle="modal"
                        data-target=".bd-example-modal-lg">Thêm mới nhóm chủ đề</button> --}}
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="">
                    <form id="form_add">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Tên nhóm</label>
                            <input type="text" class="form-control title" id="title" name="title">
                            <div class="error text-danger text-bold error_title"></div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Mô tả</label>
                            <textarea style="max-width: 100%;" class="form-control description" id="description"
                                name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Ảnh</label>
                            <input
                                value="{{ (old('image')) ? old('image') : ( !empty($product['image']) ? $product['image'] : '' ) }}"
                                readonly min="0" type="hidden" name="image" class="form-control image" id="thumbnail1">
                        </div>
                        <button class="btn btn-primary choose_image1" id="choose_image1" type="button">Chọn
                            ảnh</button>
                        <br>
                        <div class=form-group>
                            <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img1"
                                src="{{ (old('image')) ? old('image') : ( !empty($product['image']) ? $product['image'] : '' ) }}"
                                alt="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary submit_add">Lưu</button>
                        </div>

                    </form>
                </div>
                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->
<!--  Modal -->
<!-- end Modal -->
@endsection
<!----- end javascript here -------->
@section('js')
<style>
    .img-responsive {
        position: relative;
    }

    .btn-remove-img {
        position: absolute;
        top: 11px !important;
        left: 1px !important;
    }

</style>
<script>
    $('body').find('.box_img2').hide();
    chooseImg('choose_image', 'thumbnail', 'this-img');
    $(function () {
        $('body').on('click', '.btn-remove-img', function () {
            $('body').find('input[name=image]').val('');
            $('body').find('#this-img').attr('src', '');
            $('body').find('.box_img2').hide();

        });

        $('body').on('change', '#thumbnail', function () {
            alert('aaaaaaa');
            if ($(this).val().length != 0) {

                $('body').find('.box_img2').show();
            }
        })


    })

</script>

<script>
    // function name(button id, input id, img id);
    chooseImg('choose_image1', 'thumbnail1', 'this-img1');

</script>



<script>
    $(function () {
        $('body').on('submit', '#form_add', function (e) {
            e.preventDefault();
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            console.log(dataForm);
            $.ajax({
                url: "{{route('admin.topics.saveAdd')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    //console.log(msg);
                    if (result.errors) {
                        console.log(msg);
                        if (msg.title) {
                            form.find('.error_title').html(msg.title);
                        } else {
                            form.find('.error_title').html(' ');
                        }
                    } else {
                        form.find('.error').html('');
                        console.log(result);
                         // window.location.href = "{{ route('admin.topics.index') }}";
                    }
                });
        })
    })

</script>

@endsection
