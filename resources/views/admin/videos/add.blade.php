@extends('admin.layout.master')
{{-- page title --}}
@section('page_title')
Thêm video mới
@endsection
{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    b.btn {
        margin: 5px !important;
    }

    audio {
        margin: 5px 0px !important;
        display: none;
    }

    .grid-container {
        display: grid;
        grid-template-columns: auto auto auto;

        grid-gap: 10px;
    }

    .item1 {
        grid-column: 1 / 12;
    }

    .item2 {
        grid-column: 12 / 12;
    }

    .switch {
    position: relative;
    display: inline-block;
    width: 30px;
    height: 17px;
    }

    .switch input {
    opacity: 0;
    width: 0;
    height: 0;
    }

    .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
    }

    .slider:before {
    position: absolute;
    content: "";
    height: 13px;
    width: 13px;
    left: 2px;
    bottom: 2px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
    }

    input:checked+.slider {
    background-color: #2196F3;
    }

    input:focus+.slider {
    box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
    -webkit-transform: translateX(13px);
    -ms-transform: translateX(13px);
    transform: translateX(13px);
    }
    .switch {
    position: relative;
    display: inline-block;
    width: 50px;
    height: 25px;
    }

    .switch input {
    opacity: 0;
    width: 0;
    height: 0;
    }

    .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
    }

    .slider:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    /* left: 4px; */
    bottom: 3px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
    }

    input:checked + .slider {
    background-color: #2196F3;
    }

    input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
    }
    /* Rounded sliders */
    .slider.round {
    border-radius: 34px;
    }

    .slider.round:before {
    border-radius: 50%;
    }

</style>
<script src="{{ asset('js/jwplayer.js') }}"></script>
<script>jwplayer.key="YgtWotBOi+JsQi+stgRlQ3SK21W2vbKi/K2V86kVbwU=";</script>
@endsection
{{-- content --}}
@section('content')
{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}
<!-- main content -->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    @yield('page_title')
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form method="POST" action="{{route('admin.videos.saveAdd')}}">
                    {{ @csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name">Tên bài hát</label>
                                <input class="form-control"
                                    type="text" name="name" id="name"
                                    value="{{ !empty(old('name')) ? old('name') : ''}}">
                                <div class="error text-danger text-bold error_name"></div>
                                @error('name')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Link video ( *Link youtube)</label>
                                <div class="box-video">

                                    <input type="text" name="link" class="video form-control" id="video">
                                    <div class="error text-danger text-bold error_video"></div>
                                </div>
                                @error('link')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="album_id">Album</label>
                                <select class="form-control" name="album_id" id="album_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_album_id"></div>
                                @error('album_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="category_id">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_category_id"></div>
                                @error('category_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="artist_id">Nghệ sĩ thể hiện:</label>
                                <select class="form-control" multiple name="artist_id[]" id="artist_id">
                                    {{-- <option value="">--- Unknown ---</option> --}}
                                </select>
                                <div class="error text-danger text-bold error_artist_id"></div>
                                @error('artist_id')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="col-sm-6">
                                    <span style="font-weight:400">Karaoke</span>
                                    <label class="switch">
                                        <input type="checkbox" name="karaoke">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <span style="font-weight:400">Beat</span>
                                    <label class="switch">
                                        <input type="checkbox" name="beat">
                                        <span class="slider round"></span>
                                    </label>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="status">Trạng thái</label>
                                <select class="form-control" name="status" id="status" class="form-control">
                                    <option value="0">Không kích hoạt</option>
                                    <option selected value="1">Kích hoạt</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-8">

                            <div class="form-group">
                                <label for="description">Giới thiệu bài hát</label>
                                <textarea style="min-width:100%; max-width: 100%; min-height: 100px"
                                    class="form-control" style="width:100%" name="description" id="description" cols="5"
                                    rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <input
                                    value="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                    readonly min="0" type="hidden" name="image" class="form-control image"
                                    id="thumbnail">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-primary choose_image" id="choose_image" type="button">Chọn
                                    Ảnh</button><br>
                                <span style="margin: 10px !important" class="boxs-img" class="">
                                    <i style="display:none" class="btn btn-danger btn-xs btn-remove-img">
                                        <i class="fa fa-times"></i>
                                    </i>
                                    <img style="width: 250px; height: auto;margin: 10px" id="this-img"
                                        src="{{ (old('image')) ? old('image') : ( !empty($song['image']) ? $song['image'] : '' ) }}"
                                        alt="">
                                </span>
                            </div>

                            <div class="form-group">
                                <input value="{{ (old('video')) ? old('video') : ( !empty($song['video']) ? $song['video'] : '' ) }}" readonly
                                    min="0" type="hidden" name="video" class="form-control video" id="video_output">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-primary choose_video" id="choose_video" type="button">Chọn video</button><br>

                                <div style="margin-top: 20px" class="box-player" id="box-player">

                                    <div id=player class=player>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" value="0" name="author_id">
                                <button class=" btn btn-success" type="submit">Lưu</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->


@endsection
<!----- end javascript here -------->
@section('js')


<script src="{{ asset('js/typeahead.jquery.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>
<!----************************************************ --->
<script>
    $(function () {
        // ******
        var albums_select2 = $("#album_id").select2({
            ajax: {
                url: "{{ route('admin.albums.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // ******
        // ******

        var artists_select2 = $("#artist_id").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // *****
        var categories_select2 = $("#category_id").select2({
            ajax: {
                url: "{{ route('admin.categories.data_select2') }}",
                type: "post",
                dataType: 'json',
                delay: 10,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

    })

</script>
<!----************************************************ --->
{{-- js --}}
@if ((old('image')))
<script>
    $('img#this-img').show();

</script>
@else
@if (!empty($song['image']))
<script>
    $('img#this-img').show();

</script>
@else
<script>
    $('img#this-img').hide();

</script>
@endif
@endif


<script>


$(window).resize(function() {
    var $width = $(".box-player").width();
    var $height = $width/1.5;
    jwplayer().resize($width,$height);
});
</script>
<script>
    var content = CKEDITOR.replace('description');
    var button1 = document.getElementById('choose_image');
    button1.onclick = function () {
        selectFileWithCKFinder('thumbnail');
    };
    function selectFileWithCKFinder(elementId, elementImg) {
        CKFinder.popup({
            resourceType: 'Images',
            startupPath: 'Images:/images/',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function (finder) {
                finder.on('files:choose', function (evt) {
                    var file = evt.data.files.first();
                    var output = document.getElementById(elementId);
                    output.value = file.getUrl();
                    $('img#this-img').show();
                    $('img#this-img').attr('src', file.getUrl());
                });
                finder.on('file:choose:resizedImage', function (evt) {
                    var output = document.getElementById(elementId);
                    output.value = evt.data.resizedUrl;
                });
            }
        });
    }


    // $('#this_video').hide();
    var button2 = document.getElementById('choose_video');
    button2.onclick = function () {
        selectFileWithCKFinderVideo('videos');
    };
    function selectFileWithCKFinderVideo(elementId, elementImg) {
        CKFinder.popup({
            resourceType: 'Videos',
            startupPath: 'Videos:/videos',
            chooseFiles: true,
            onInit: function (finder) {
                finder.on('files:choose', function (evt) {
                    var file = evt.data.files.first();

                    var output = $('#video_output');

                    output.val(file.getUrl());
                    $('#this_video').show();
                    $('#this_video').attr('src', file.getUrl());
                        var $width = $(".box-player").width();
                        var $height = $width/1.5;
                        jwplayer("player").setup({
                        "file": file.getUrl(),
                        "width" : $width,
                        "height" : $height,
                        "autostart": "viewable",
                        });
                });
                //finder.on('file:choose:resizedImage', function (evt) {
                   // var output = $('#video_output');
                    //output.val(evt.data.resizedUrl);
                //});
            }
        });
    }
</script>


{{-- tag js --}}




@endsection
