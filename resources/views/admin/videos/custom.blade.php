@extends('admin.layout.master')
{{-- page title --}}
@section('page_title')
Custom
@endsection
{{-- style --}}
@section('style')
<style>
    .jw-icon-hd {
        display: none !important;
    }
    div.ytp-pause-overlay.ytp-scroll-min {
         display: none !important;
    }
    a.ytp-watermark.yt-uix-sessionlink {
        display: none !important;
    }
    .ytp-chrome-top-buttons {
        display: none !important;
    }
</style>
@endsection
{{-- content --}}
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">
                    @yield('page_title')
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <input value="{{ (old('link')) ? old('link') : ( !empty($song['link']) ? $song['link'] : '' ) }}"
                        readonly type="hidden" name="link" class="form-control link" id="link">
                    <input readonly type="hidden" name="filename" class="form-control filename" id="filename">
                    <span>

                        <span class="">
                            <button id="btn-choose_file" type="button" class="btn btn-sm ">Chọn bài
                                hát</button>
                        </span>
                        <span style="margin: 0px 5px !important " class="pull-right-container my-2 text-bold text-dark"
                            id="file_name"></span>
                    </span>

                </div>
                <br>
                <div class="box-player" id="box-player">
                    <div id=player class=player>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end main content -->


@endsection
<!----- end javascript here -------->
@section('js')


<script src="{{ asset('js/typeahead.jquery.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/jwplayer.js') }}"></script>
<script>jwplayer.key="YgtWotBOi+JsQi+stgRlQ3SK21W2vbKi/K2V86kVbwU=";</script>
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    var $width = $(".box-player").width();
    var $height = $width/1.5;
jwplayer("player").setup({
  "file": "https://www.youtube.com/watch?v=KKc_RMln5UY",
  "width" : $width,
  "height" : $height,
  "autostart": "viewable",
});

$(window).resize(function() {
    var $width = $(".box-player").width();
    var $height = $width/1.5;
    jwplayer().resize($width,$height);
});
</script>
<script>
    function chooseFile(btnChooseImgID, inputID, imgID, boxID, inputName) {
        var idimg = imgID;
        document.getElementById(imgID).style.display = "none";
        document.getElementById(boxID).style.display = "none";
        var choose = document.getElementById(btnChooseImgID);
        choose.onclick = function () {
            CKFinder.popup({
                resourceType: 'Videos',
                chooseFiles: true,
                startupPath: 'Files: ./files/',
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        var file_name = file.getUrl();
                        var filename = file.attributes.name
                        console.log(file);
                        console.log(filename);
                        var input = document.getElementById(inputID);
                        var name = document.getElementById(inputName);
                        input.value = file_name;
                        name.value = filename;
                        document.getElementById(idimg).style.display = "block";
                        document.getElementById(boxID).style.display = "block";
                        document.getElementById(idimg).src = file_name;
                        document.getElementById('file_name').innerHTML = filename;
                    });
                }
            });
        };
    }

</script>



@endsection
