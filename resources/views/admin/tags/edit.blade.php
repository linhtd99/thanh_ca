@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Tag bài hát
@endsection


{{-- style --}}
@section('style')
<style>
    .select2 {
        width: 100% !important;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">
    <div class="col-sm-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Sửa tag
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form id=create_category>
                    {{-- {{@csrf_field()}} --}}
                    <div class="form-group">
                        <label for="name">Tên tag</label>
                        <input class="form-control" type="text" name="title" id="title"
                            value="{{ $tag->title }}">
                        <div class="text-danger error_title" id="error_title"></div>
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input class="form-control" type="text" name=slug id=slug
                            value="{{ $tag->slug }}">
                        <div class="text-danger error_slug" id="error_slug"></div>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Tag cha</label>
                        <select name="parent_id" id="parent_id" class="form-control parent_id">
                        </select>
                    </div>
                    <div class="form-group">
                        <button class=" btn btn-success" type="submit">Lưu</button>
                    </div>
                </form>
                <!-- end content here -->
            </div>
        </div>
    </div>

</div>
<!-- end main content -->
<!----- javascript here -------->
@section('js')

<script>
    $(function () {
        $('#title').on('keyup', function () {
            ChangeToSlug('title', 'slug');
        })
        $('#title2').on('keyup', function () {
            ChangeToSlug('title2', 'slug2');
        })
        function ChangeToSlug(n, s) {
            var title, slug;
            //Lấy text từ thẻ input title
            title = document.getElementById(n).value;
            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();
            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(
                /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById(s).value = slug;
        }
        // select2
        var select2 = $("#parent_id").select2({
            ajax: {
                url: "{{ route('admin.tags.data_select2') }}",
                type: "get",
                dataType: 'json',
                delay: 50,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    var empt = [{
                        id: 0,
                        name: "-- không --"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        // thêm mới tag
        $('#create_category').on('submit', function (e) {
            e.preventDefault();
            // alert('ịijiij');
            var form = $(this);
            let dataForm = new FormData(form[0]);
            dataForm.set('_token', '{{csrf_token()}}');
            // console.log(dataForm);
            $.ajax({
                url: "{{route('admin.tags.saveEdit', ['id' => $tag->id])}}",
                // url: window.location.href,
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    if (result.errors) {
                        if (msg.title) {
                            form.find('#error_title').html(msg.title);
                            // console.log(msg.title);
                        }
                        if (msg.slug) {
                            form.find('#error_slug').html(msg.slug);
                            // console.log(msg.slug);
                        }
                    } else {
                        swal({
                            title: "Thông báo!",
                            text: "Cập nhật tag thành công!",
                            icon: "sussess",
                        }).then((all) => {
                            window.location.href = "{{ route('admin.tags.index') }}";
                        })
                    }
                });
        })
    })

</script>
@endsection
<!----- end javascript here -------->
@endsection
