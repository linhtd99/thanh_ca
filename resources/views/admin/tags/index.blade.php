@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
tag bài hát
@endsection


{{-- style --}}
@section('style')
<style>
    .select2 {
        width: 100% !important;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">
    <div class="col-sm-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Thêm mới tag
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form id=create_category>
                    {{-- {{@csrf_field()}} --}}
                    <div class="form-group">
                        <label for="name">Tên tag</label>
                        <input class="form-control" type="text" name="title" id="title"
                            value="{{ !empty(old('title')) ? old('title') : ''}}">
                        <div class="text-danger error_title" id="error_title"></div>
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input class="form-control" type="text" name=slug id=slug
                            value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                        <div class="text-danger error_slug" id="error_slug"></div>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Tag cha</label>
                        <select name="parent_id" id="parent_id" class="form-control parent_id">
                            <option value="0">-- không --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class=" btn btn-success" type="submit">Lưu</button>
                    </div>
                </form>



                <!-- end content here -->
            </div>
        </div>
    </div>
    {{-- /////////////////// --}}
    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    Danh sách tag
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="table-categories"></table>
                </div>



                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->



<!--  Modal -->
<div class="modal fade" id="updateCategory" tabindex="-1" role="dialog" aria-labelledby="updateCategoryTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content box box-danger">
            <div class="modal-header">
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Sửa tag</b></h4>
                </div>
                <div id="show-alert">

                </div>
            </div>
            <div class="modal-body">
                <form id="update-category">
                    {{ @csrf_field() }}
                    <input type="hidden" name="id">
                    <div class="">
                        <div class="form-group">
                            <label for="name">Tên tag</label>
                            <input class="form-control" type="text" name="title" id="title2"
                                value="{{ !empty(old('title')) ? old('title') : ''}}">
                            <div class="text-danger error_title2" id="error_title2"></div>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input class="form-control" type="text" name=slug id=slug2
                                value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                            <div class="text-danger error_slug2" id="error_slug2"></div>
                        </div>
                        <div class="form-group">
                            <div>
                                <label for="parent_id">tag cha</label>
                            </div>
                            <select name="parent_id" id="parent_id2" class="form-control">
                                <option value="0">-- không --</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                            <button type="submit" class="btn btn-success">Lưu</button>
                            {{-- <button class=" btn btn-success" type="submit">Lưu</button> --}}
                        </div>
                    </div>
                </form>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div> --}}
        </div>
    </div>
</div>
<!-- end Modal -->




<!----- javascript here -------->
@section('js')

<script>
    $(document).ready(function () {
        $("#updateCategory").on("hidden.bs.modal", function () {
            $("#show-alert").html("");
        });
    });

</script>
<script>
    $(function () {
        $('#title').on('keyup', function () {
            ChangeToSlug('title', 'slug');
        })
        $('#title2').on('keyup', function () {
            ChangeToSlug('title2', 'slug2');
        })

        function ChangeToSlug(n, s) {
            var title, slug;

            //Lấy text từ thẻ input title
            title = document.getElementById(n).value;

            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();

            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(
                /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById(s).value = slug;
        }

        // select2
        var select2 = $("#parent_id").select2({
            ajax: {
                url: "{{ route('admin.tags.data_select2') }}",
                type: "get",
                dataType: 'json',
                delay: 50,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    var empt = [{
                        id: 0,
                        name: "-- không --"
                    }];
                    data = empt.concat(data);

                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        // select2
        // var select2 = $("#parent_id2").select2({
        //     ajax: {
        //     url: "{{ route('admin.categories.data_select2') }}",
        //     type: "post",
        //     dataType: 'json',
        //     delay: 50,
        //     data: function (params) {
        //         return {
        //            q: $.trim(params.term),
        //            _token : "{{csrf_token()}}"
        //        };
        //     },
        //     processResults: function (data) {
        //         var empt = [{id: 0, name: "-- không --"}];
        //         data = empt.concat(data);

        //         return {
        //         results:  $.map(data, function (item) {
        //                 return {
        //                     text: item.name,
        //                     id: item.id,
        //                     value: item.id
        //                 }
        //             })
        //         };
        //     },
        //     cache: true
        //     }
        // });
        // datatable

        var table = $('#table-categories').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            stateSave: true,
            ajax: "{{ route('admin.tags.dataTable') }}",
            // order: [
            //     [0, 'desc']
            // ],
            lengthMenu: [20, 30, 50, 100],
            iDisplayLength: 20,
            columnDefs: [{
                    "targets": [0],
                    "searchable": false,
                    "orderable": false,
                },
                {
                    "targets": [1],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [2],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [3],
                    "searchable": false,
                    "orderable": false,
                },


            ],
            columns: [{
                    data: 'id',
                    title: "ID"
                },
                {
                    data: 'title',
                    title: "Tên tag",
                    autoWidth: true
                },
                {
                    data: 'parent_title',
                    title: "tag gốc",
                    autoWidth: true
                },

                {
                    data: 'id',
                    title: 'Hành động',
                    autoWidth: true,
                    render: function (data) {
                        var html =
                            `<a class=" btn btn-warning btn-xs" href="{{ url('admin/tags/edit/') }}/${data}"><i class="fa fa-pencil"></i></a>
                                        <button data-remove="${data}" class="btn btn-remove btn-danger btn-xs"><i class="fa fa-remove "></i></button>`;
                        return html;
                    }
                }

            ],
            "language": {
                "lengthMenu": "_MENU_ bản ghi/trang",
                "zeroRecords": "Không có dữ liệu để hiển thị",
                "info": "_PAGE_/_PAGES_",
                // "infoEmpty": "No records available",
                "infoEmpty": "Không có dữ liệu để hiển thị",
                // "infoFiltered": "(filtered from _MAX_ total records)",
                "infoFiltered": "(_MAX_/tổng số)",
                "search": 'Tìm kiếm: ',
                "paginate": {
                    "first": "Trang đầu",
                    "last": "Trang cuối",
                    "next": "Trang sau",
                    "previous": "Trang trước"
                },
            },
            initComplete: function () {
                $('body').on('click', '.btn-remove', function () {
                    var id = $(this).data('remove');
                    swal({
                        title: "Bạn có chắc chắn?",
                        text: "Sau khi xóa, bạn sẽ không thể khôi phục tag này!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.tags.remove')}}",
                                method: 'POST',
                                data: {
                                    id: id,
                                    _token: '{{csrf_token()}}'
                                }
                            }).done(result => {

                                if (result) {
                                    swal("Deleted!",
                                        "tag đã bị xoá.",
                                        "success");
                                }
                                setTimeout(function () {
                                    table.ajax.reload();
                                }, 500);

                            });
                        } else {

                        }
                    })
                })
            }


        })


        // thêm mới tag
        $('#create_category').on('submit', function (e) {
            e.preventDefault();
            // alert('ịijiij');
            var form = $(this);
            let dataForm = new FormData(form[0]);
            // dataForm.set('description', content.getData());
            dataForm.set('_token', '{{csrf_token()}}');
            // console.log(dataForm);
            $.ajax({
                url: "{{route('admin.tags.saveAdd')}}",
                // url: window.location.href,
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {
                    var msg = result.data;
                    if (result.errors) {
                        if (msg.title) {
                            form.find('#error_title').html(msg.title);
                            // console.log(msg.title);
                        }
                        if (msg.slug) {
                            form.find('#error_slug').html(msg.slug);
                            // console.log(msg.slug);
                        }
                    } else {
                        form.find('#error_title').html('');
                        form.find('#error_slug').html('');
                        $('#create_category').trigger("reset");
                        // content.setData('');
                        $('input[name=parent_id]').prop('selectedIndex', 0);
                        // console.log(msg);
                        table.ajax.reload();
                        $("input[name=parent_id]").select2("val", "");
                        $('#select2-parent_id-container').html('-- không --');
                        $('#select2-parent_id-container').attr('title', '-- không --');
                    }
                    // table.ajax.reload();

                });
        })

        // sửa tag


        // update cate
        $('body').on('click', '.btn-update', function (e) {
            e.preventDefault();
            $('#parent_id2').select2();
            var id = $(this).data('edit');
            var p_id = 0;
            $.ajax({
                url: `{{route('admin.tags.edit')}}/${id}`,
                method: 'get'
            }).done(
                result => {
                    var data = result;
                    p_id = data.parent_id;
                    $('#updateCategory input[name=title]').val(data.title);
                    $('#updateCategory input[name=id]').val(data.id);
                    $('#updateCategory input[name=slug]').val(data.slug);
                    $('#updateCategory select[name=parent_id]').val(data.parent_id);
                    $('#parent_id2').val(data.parent_id);
                    // console.log(data.parent_id);
                    //update_description.setData(data.description);
                    $('#parent_id2').val(id);
                    $('#updateCategory .select2-selection__rendered').html(data.pn);
                    $('#updateCategory .select2-selection__rendered').attr('title', data.pn);
                    $.ajax({
                        url: "{{ route('admin.categories.data_select2') }}",
                        type: "post",
                        dataType: 'json',
                        data: {
                            q: '',
                            _token: "{{ csrf_token() }}",
                        }
                    }).done(
                        result => {
                            var data = result;
                            // console.log(data);
                            var str = '<option selected value="0">-- không --</option>';

                            $.each(data, function (item, value) {
                                if (value.id == id) {
                                    str +=
                                        `<option disabled value="${value.id}">${value.name}</option>`;
                                } else {
                                    if (p_id == value.id) {
                                        str +=
                                            `<option selected value="${value.id}">${value.name}</option>`;
                                    } else {
                                        str +=
                                            `<option value="${value.id}">${value.name}</option>`;
                                    }
                                }
                            })
                            $('#parent_id2').html(str);
                        }
                    )
                    $('#updateCategory').modal('show');



                }
            )
        })
        // ckeditor



    })

    // submit form

</script>

@endsection
<!----- end javascript here -------->

@endsection
