@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách quản trị
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">


                @if (Auth::user()->role == 101 )
                    <a href="{{route('admin.users.add')}}" class="btn btn-primary">Thêm</a>
                @endif
                    {{-- Danh sách quản trị --}}
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="table-users"></table>
                </div>



                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->








<!----- javascript here -------->
@section('js')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {

        var table = $('#table-users').DataTable({

            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('admin.users.data') }}",
            // order: [
            //     [0, 'desc']
            // ],
            lengthMenu: [20, 30, 50, 100],
            iDisplayLength: 20,
            columnDefs: [{
                    "targets": [0],
                    "searchable": false,
                    "orderable": false,
                },
                {
                    "targets": [1],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [2],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [3],
                    "searchable": true,
                    "orderable": true,
                },
                {
                    "targets": [4],
                    "searchable": true,
                    "orderable": true,
                }
            ],
            columns: [{
                    data: 'id',
                    title: "ID"
                },
                {
                    data: 'name',
                    title: "Tên người dùng",
                    autoWidth: true
                },
                {
                    data: 'email',
                    title: "Email",
                    autoWidth: true
                },
                {
                    data: 'role',
                    title: "Chức vụ",
                    autoWidth: true,
                    render: function (data) {
                    // console.log(data);
                    var html = `${data}` > 50 ? 'Quản trị viên' : 'Biên tập viên';
                    return html;
                    }
                },
                {
                    data: 'id',
                    title: 'Hành động',
                    autoWidth: true,
                    render: function (data) {
                        // console.log(data);
                        var html =
                            `<a href="{{url('/').'/admin/users/edit/'}}${data}" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                        <button url="{{url('/').'/admin/users/delete/'}}${data}" class="btn btn-remove btn-danger btn-xs btn-delete"><i class="fa fa-remove "></i></button>`;
                        return html;
                    }
                }

            ],
            "language": {
                "lengthMenu": "Hiển thị _MENU_ bản ghi trên trang",
                "zeroRecords": "Không có dữ liệu để hiển thị",
                "info": "Trang hiển thị _PAGE_ / _PAGES_",
                "infoEmpty": "Không có dữ liệu để hiển thị",
                "infoFiltered": "(được lọc từ _MAX_ tổng số hồ sơ)",
                "search": 'Tìm kiếm:   ',
                "paginate": {
                    "first": "Trang đầu",
                    "last": "Trang cuối",
                    "next": "Trang sau",
                    "previous": "Trang trước"
                },
            },


        })
    })





</script>


@endsection
<!----- end javascript here -------->

@endsection
