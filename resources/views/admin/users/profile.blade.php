@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Profile
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->



<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-2">

            <!-- Profile Image -->

            <!-- /.box -->

            <!-- About Me Box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="#activity" data-toggle="tab">Thông tin</a></li>
                    {{-- <li><a href="#timeline" data-toggle="tab">Timeline</a></li> --}}
                    <li class="active" ><a href="#settings" data-toggle="tab">Thay đổi thông tin</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="activity">
                        <div class="box box-primary">
                            {{-- <div class="box-body box-profile"> --}}
                            {{-- <img class="profile-user-img img-responsive img-circle"
                                                                    src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> --}}

                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>

                            {{-- <p class="text-muted text-center">Software Engineer</p> --}}

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Email</b> <a class="pull-right">{{Auth::user()->email}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Chức vụ</b> <a
                                        class="pull-right">{{Auth::user()->role > 50 ? 'Quản trị viên' : 'Biên tập viên'}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Ngày tạo</b> <a
                                        class="pull-right">{{!empty(Auth::user()->created_at) ? date_format(Auth::user()->created_at,"d-m-Y") : 'N/A' }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Ngày sửa</b> <a
                                        class="pull-right">{{ !empty(Auth::user()->update_at)?date_format(Auth::user()->update_at,"d-m-Y"):'N/A' }} </a>
                                </li>

                            </ul>


                            {{-- </div> --}}
                            <!-- /.box-body -->
                        </div>
                    </div>



                    <div class="active tab-pane" id="settings">
                        <form method="POST" action="{{route('admin.users.saveProfile')}}" enctype="multipart/form-data">
                            {{@csrf_field()}}

                            <input type="hidden" value="{{Auth::user()->id}}" name="id">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input class="form-control" type="text" name="email" id="email"
                                    value="{{ !empty(Auth::user()->email) ? Auth::user()->email : ''}}">
                                @error('email')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Username</label>
                                <input class="form-control" type="text" name="username" id="username"
                                    value="{{ !empty(Auth::user()->username) ? Auth::user()->username : ''}}">
                                @error('username')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="name">Tên</label>
                                <input class="form-control" type="text" name="name" id="name"
                                    value="{{ !empty(Auth::user()->name) ? Auth::user()->name : ''}}">
                                @error('name')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Mật khẩu</label>
                                <input class="form-control" type="password" name="password" id="password"
                                    value="{{ !empty(old('password')) ? old('password') : ''}}">
                                @error('password')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="cf_password">Nhập lại mật khẩu</label>
                                <input class="form-control" type="password" name="cf_password" id="cf_password"
                                    value="">
                                @error('cf_password')
                                <span style="color:red">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Avatar</label>
                                <input value="{{ !empty(Auth::user()->avatar) ? Auth::user()->avatar : '' }}" readonly min="0" type="hidden" name="avatar"
                                    class="form-control image" id="thumbnail">
                            </div>
                            <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn ảnh</button>
                            <br>
                            <div class=form-group>
                                <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img"
                                    src="{{ !empty(Auth::user()->avatar) ? Auth::user()->avatar : '' }}" alt="">
                            </div>
                            {{-- <div class="form-group">
                                            <label for="role">Chức vụ</label>
                                            <select name="role" id="role" class="form-control">
                                                <option {{Auth::user()->role == 50 ? 'selected' : ''}} value="50">Biên
                            tập viên</option>
                            <option {{Auth::user()->role > 50 ? 'selected' : ''}} value="100">Quản trị viên</option>
                            </select>
                    </div> --}}
                    <div class="form-group">
                        <button class=" btn btn-danger" type="submit">Lưu</button>
                    </div>
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <div class="col-md-2"></div>
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->


<!-- end main content -->

@endsection
@section('js')
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config( { connectorPath: '/ckfinder/connector' } );
    chooseImg('choose_image', 'thumbnail', 'this-img');
</script>


@endsection
<!----- end javascript here -------->
