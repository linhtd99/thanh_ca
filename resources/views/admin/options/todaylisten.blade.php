@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Sắp xếp danh mục
@endsection


{{-- style --}}
@section('style')

@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title">
                    <h4>Chọn chủ đề hôm nay nghe gì</h4>
                </div>
            </div>
            <div class="box-body">
                <div class="col-sm-6 form-group">
                    <label for="chude">Chủ đề</label>
                    <select class="form-control" name="chude" id="chude">
                        @forelse ($topics as $item)
                        <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('js')
<script>

</script>

@endsection
