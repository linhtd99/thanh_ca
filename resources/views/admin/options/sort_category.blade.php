@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Sắp xếp danh mục
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
            <form class="repeater" method="POST" action="{{route('admin.options.save_sort_category')}}">
                @csrf
                    <div data-repeater-list="outer_list" class="abcd">
                        <div data-repeater-item class="repeater_div">

                            <div class="form-group row">
                                <div class="col-xs-10">
                                    <label>Danh mục chính</label>
                                    <select name="category" id="category" class="form-control category">
                                        @foreach ($categories as $item)
                                             <option value="{{$item->id}}">
                                                <?php for ($i=0; $i < $item['step']; $i++) {
                                                    echo "- ";
                                                }?>
                                                {{$item->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-2">

                                    <a class="btn btn-info up" style="margin-top:25px"><i class="fa fa-arrow-up"></i></a>
                                    <a class="btn btn-success down" style="margin-top:25px"><i class="fa fa-arrow-down"></i></a>
                                    <input data-repeater-delete type="button" value="Xóa" class="btn btn-danger" style="margin-top:25px"/>
                                </div>
                            </div>


                            <!-- innner repeater -->
                            <div class="inner-repeater">
                                <div >
                                    <div>
                                        <div class="form-group">
                                            <label>Danh mục phụ</label>
                                            <select name="categories[]" multiple="multiple"  class="form-control categories">
                                                @foreach ($categories as $item)
                                                 <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <input data-repeater-delete type="button" value="Delete" /> --}}
                                    </div>
                                </div>
                                {{-- <input data-repeater-create type="button" value="Add" /> --}}
                            </div>

                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Lưu</button>
                    <input data-repeater-create type="button" value="Thêm" class="btn btn-primary callBackSelect2" />
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('js')

<script src="{{asset('js/repeater.jquery.js')}}"></script>
@if (!empty(json_decode($options['value'])))
    <script>
         jQuery(document).ready(function(){
            var categories = `<?= json_encode($categories) ?>`;
            var count = 99999;
            var data = `<?= $options['value'] ?>`;
            // console.log(JSON.parse(data.category));
            var str = '';
                $.each( JSON.parse(data), function( i, v ){
                    // console.log(v.category);
                count++;
                str += `<div data-repeater-item class="repeater_div">

                    <div class="form-group row">
                        <div class="col-xs-10">
                            <label>Danh mục chính</label>
                            <select name="outer_list[${count}][category]" id="category" class="form-control category">`;
                                $.each( JSON.parse(categories), function( key, valueCategory ){
                                    let selected = valueCategory.id == v.category ? 'selected' : '';
                                    str += `<option value="${valueCategory.id}" ${selected}>`;
                                    for (let i = 0; i < valueCategory.step; i++) { str += '- ' ; }
                                    str +=  `${valueCategory.name}</option>`;
                                });
                          str += `</select>
                        </div>
                        <div class="col-xs-2">

                            <a class="btn btn-info up" style="margin-top:25px"><i class="fa fa-arrow-up"></i></a>
                            <a class="btn btn-success down" style="margin-top:25px"><i class="fa fa-arrow-down"></i></a>
                            <input data-repeater-delete type="button" value="Xóa" class="btn btn-danger" style="margin-top:25px" />
                        </div>
                    </div>


                    <!-- innner repeater -->
                    <div class="inner-repeater">
                        <div>
                            <div>
                                <div class="form-group">
                                    <label>Danh mục phụ</label>
                                    <select name="outer_list[${count}][categories[]][]" multiple="multiple" class="form-control categories">`
                                        $.each( JSON.parse(categories), function( key, valueCategory ){

                                            if(jQuery.inArray(''+valueCategory.id , v.categories) != -1){

                                                    str += `<option value="${valueCategory.id}" selected>`
                                                    for (let i = 0; i < valueCategory.step; i++) { str +='- ' ; }
                                                    str += `${valueCategory.name}</option>`;
                                            }else{
                                                // console.log(false);

                                                    str += `<option value="${valueCategory.id}">`;
                                                    for (let i = 0; i < valueCategory.step; i++) {
                                                        str += '- ';

                                                    }
                                                    str += `${valueCategory.name}</option>`;
                                            }

                                            });

                                str += `</select>
                                </div>
                                {{-- <input data-repeater-delete type="button" value="Delete" /> --}}
                            </div>
                        </div>
                        {{-- <input data-repeater-create type="button" value="Add" /> --}}
                    </div>

                </div>`;
                });

                $('.abcd').html(str);
            });
    </script>
@else
<script>aaaa</script>
@endif
<script>
$(document).ready(function () {
    jQuery('.repeater').repeater({});
    $('body').find('.categories').select2();
    $('.callBackSelect2').click(function(){
        $('body').find(".categories:last").select2({});
    });

    jQuery('body').on('click','.up',function(){
        divCurrent = $(this).parents('.repeater_div');
        var wrapper = $(this).closest('.repeater_div');

        var selectEq1_1 = $(divCurrent.prev()).find('select:eq(0)').attr('name');
        var selectEq1_2 = $(divCurrent).find('select:eq(0)').attr('name');

        var selectEq2_1 = $(divCurrent.prev()).find('select:eq(1)').attr('name');
        var selectEq2_2 = $(divCurrent).find('select:eq(1)').attr('name');

        $(wrapper).find('select:eq(0)').attr('name',selectEq1_1);
        $(divCurrent.prev()).find('select:eq(0)').attr('name',selectEq1_2);

        $(wrapper).find('select:eq(1)').attr('name',selectEq2_1);
        $(divCurrent.prev()).find('select:eq(1)').attr('name',selectEq2_2);

        wrapper.insertBefore(divCurrent.prev());
    });

    jQuery('body').on('click','.down',function(){
        divCurrent = $(this).parents('.repeater_div');
        var wrapper = $(this).closest('.repeater_div');

        var selectEq1_1 = $(divCurrent.next()).find('select:eq(0)').attr('name');
        var selectEq1_2 = $(divCurrent).find('select:eq(0)').attr('name');

        var selectEq2_1 = $(divCurrent.next()).find('select:eq(1)').attr('name');
        var selectEq2_2 = $(divCurrent).find('select:eq(1)').attr('name');

        $(wrapper).find('select:eq(0)').attr('name',selectEq1_1);
        $(divCurrent.next()).find('select:eq(0)').attr('name',selectEq1_2);

        $(wrapper).find('select:eq(1)').attr('name',selectEq2_1);
        $(divCurrent.next()).find('select:eq(1)').attr('name',selectEq2_2);


        wrapper.insertAfter(divCurrent.next());
    });
});

</script>
@endsection
