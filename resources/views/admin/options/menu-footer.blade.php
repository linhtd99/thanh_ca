@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Thiếp lập chân trang
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}

{{-- {{dd($options)}} --}}
<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <form action="{{route('admin.options.save_menu_footer')}}" method="post">
        <div class="col-xs-10">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
                        Cột
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                            data-original-title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                        @csrf
                        @if (!empty($options[0]))
                            @foreach ($options[0] as $key => $item)
                                <div class="form-group">
                                <label>Cột {{$key+1}}</label>
                                <input type="text" name="footer_title[]" value="{{$item->title}}" class="form-control" placeholder="Tiêu đề">
                                    <textarea name="footer_content[]" id="ftc{{$key+1}}" rows="10" placeholder="Nội dung" class="form-control">{{$item->content}}</textarea>
                                </div>
                            @endforeach
                        @else
                                <div class="form-group">
                                    <label>Cột 1</label>
                                    <input type="text" name="footer_title[]" class="form-control" placeholder="Tiêu đề">
                                    <textarea name="footer_content[]" id="ftc1" rows="10" placeholder="Nội dung" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Cột 2</label>
                                    <input type="text" name="footer_title[]" class="form-control" placeholder="Tiêu đề">
                                    <textarea name="footer_content[]" id="ftc2" rows="10" placeholder="Cột" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Cột 3</label>
                                    <input type="text" name="footer_title[]" class="form-control" placeholder="Tiêu đề">
                                    <textarea name="footer_content[]" id="ftc3" rows="10" placeholder="Nội dung" class="form-control"></textarea>
                                </div>
                        @endif
                        @if (!empty($options[1]))
                            <div class="form-group">
                                <label>Cột 4</label>
                                <input type="text" name="footer_title4" value="{{$options[1]->title}}" class="form-control" placeholder="Tiêu đề">
                                <input type="text" name="footer_content4" value="{{$options[1]->content}}" placeholder="Link facebook" class="form-control">
                            </div>
                        @else
                            <div class="form-group">
                                <label>Cột 4</label>
                                <input type="text" name="footer_title4" class="form-control" placeholder="Tiêu đề">
                                <input type="text" name="footer_content4" placeholder="Link facebook" class="form-control">
                            </div>
                        @endif

                        @if (!empty($options[2]))
                            <div class="form-group">
                                <label>Cột 5</label>
                                <input type="text" name="footer_title5" value="{{$options[2]->title}}" class="form-control" placeholder="Tiêu đề">
                                <input type="text" name="link_down1"    value="{{$options[2]->link_down1}}" placeholder="Link tải iphone" class="form-control">
                                <input type="text" name="link_down2"    value="{{$options[2]->link_down2}}" placeholder="Link tải android" class="form-control">
                            </div>
                        @else
                            <div class="form-group">
                                <label>Cột 5</label>
                                <input type="text" name="footer_title5" class="form-control" placeholder="Tiêu đề">
                                <input type="text" name="link_down1" placeholder="Link tải iphone" class="form-control">
                                <input type="text" name="link_down2" placeholder="Link tải android" class="form-control">
                            </div>
                        @endif

                        @if (!empty($options[3]))
                            <div class="form-group">
                                <label>Thông tin</label>
                                <textarea name="info" id="ftc4" rows="10" placeholder="Nội dung thông tin" class="form-control">{{$options[3]}}</textarea>
                            </div>
                        @else
                            <div class="form-group">
                                <label>Thông tin</label>
                                <textarea name="info" id="ftc4" rows="10" placeholder="Nội dung thông tin" class="form-control"></textarea>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        <div class="col-xs-2" style="position: fixed;left:85%">
            <button class="btn btn-primary btn-lg">Lưu</button>
        </div>
    </form>
</div>


@endsection
@section('js')
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    CKEDITOR.replace('ftc1');
	CKEDITOR.replace('ftc2');
	CKEDITOR.replace('ftc3');
    CKEDITOR.replace('ftc4');
</script>
@endsection
