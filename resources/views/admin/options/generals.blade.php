@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Thiết lập chung
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">
    <form action="{{route('admin.options.save_general')}}" method="post">
        @csrf
        <div class="col-xs-9">
            <div class="box box-primary">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#home">Logo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Slide Show</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu2">Hôm nay nghe gì</a>
                        </li>
                        {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu2">Menu 2</a>
                            </li> --}}
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="tab-pane active"><br>
                            <div class="form-group">
                                <label for="name">Tên website</label><br>
                                <input type="text" name="name" class="form-control"
                                    value="{{ !empty($options->name) ? $options->name : '' }}">
                            </div>
                            <div class="form-group">
                                <label for="image">Ảnh</label>
                                <input value="{{ !empty($options->logo) ? $options->logo : '' }}" readonly min="0"
                                    type="hidden" name="image" class="form-control image" id="thumbnail">
                                <br>
                                <img style="width: auto; padding-top: 10px !important" id="this-img"
                                    src="{{ !empty($options->logo) ? $options->logo : '' }}" alt="">
                            </div>
                            <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn
                                ảnh</button>
                            <br>
                        </div>
                        <div id="menu1" class="tab-pane fade repeater"><br>
                            <div data-repeater-list='slide' class="abc">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-xs-10">
                                            <label>Link</label>
                                            <input type="text" name="link" class="form-control" value="#">
                                            <div class="form-group">
                                                <label for="image">Chọn ảnh</label>
                                                <input value="{{ !empty(old('image')) ? old('image') : '' }}" readonly
                                                    min="0" type="hidden" name="slide"
                                                    class="form-control image thumbnail1">

                                                <img style="max-width: 100%; padding-top: 10px !important"
                                                    class="this-img1"
                                                    src="{{ !empty(old('image')) ? old('image') : '' }}" alt="">
                                                <br>
                                                <a class="btn btn-primary choose_image1" type="button">Chọn ảnh</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <input data-repeater-delete type="button" value="Xóa" class="btn btn-danger"
                                                style="margin-top:25px;" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <input data-repeater-create type="button" class="btn btn-success" value="Thêm" />
                        </div>
                        <div id="menu2" class="tab-pane"><br>
                            <div class="form-group">
                                <label for="name">Chủ đề</label><br>
                                <select class="form-control" name="topic" id="topic">
                                    @forelse ($topic as $item)
                                    <option {{  $options->topic == $item->id  ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->title }}</option>
                                    @empty
                                    @endforelse
                                </select>

                            </div>
                            <br>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3" style="position:fixed;left:80%">
            <button class="btn btn-primary">Lưu</button>
        </div>
    </form>
</div>


@endsection

@section('js')
@if(!empty($options->slideshow))
<script>
    jQuery(document).ready(function () {
        // var count = 99999;
        var data = @php echo json_encode($options->slideshow) @endphp;
        var str = '';
        $.each(data, function (i, v) {

            str += `
                    <div data-repeater-item>
                        <div class="form-group row">
                            <div class="col-xs-10">
                                <label>Link</label>
                                <input type="text" name="slide[${i}][link]" class="form-control" value="${v.link}">
                                <div class="form-group">
                                    <label for="image">Chọn ảnh</label><br>
                                    <input value="${v.slide}" readonly="" min="0" type="hidden" name="slide[${i}][slide]"
                                        class="form-control image thumbnail1">

                                    <img style="max-width: 100%; padding-top: 10px !important" class="this-img1" src="${v.slide}" alt="">
                                    <br>
                                    <a class="btn btn-primary choose_image1" type="button">Chọn ảnh</a>
                                </div>
                            </div>
                            <div class="col-xs-2">`;
            if (v == 0) {
                str += '';
            } else {
                str +=
                    `<input data-repeater-delete type="button" value="Xóa" class="btn btn-danger" style="margin-top:25px;" />`;
            };
            str += `
                            </div>
                        </div>
                    </div>

                `;
        });
        $('.abc').html(str);
    });

</script>
@endif


<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/repeater.jquery.js')}}"></script>

<script>
    $('.repeater').repeater({
        defaultValues: {
            'link': '#'
        },

        show: function () {
            $(this).slideDown();
        },

        hide: function (deleteElement) {
            if (confirm('Chắc chắn xóa ???')) {
                $(this).slideUp(deleteElement);
            }
        },

        isFirstItemUndeletable: true
    });
    chooseImg('choose_image', 'thumbnail', 'this-img');

    $('body').on('click', '.choose_image1', function () {
        scope = $(this);
        CKFinder.popup({
            resourceType: 'Images',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function (finder) {
                finder.on('files:choose', function (evt) {
                    var file = evt.data.files.first();
                    var file_url = file.getUrl();
                    scope.parents('.form-group').find('.thumbnail1').val(file_url);
                    scope.parents('.form-group').find('.this-img1').attr('src', file_url);
                    scope.parents('.form-group').find('.this-img1').css('display', 'block');
                });
            }
        });
    })

</script>
@endsection
