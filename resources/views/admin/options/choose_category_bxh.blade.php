@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Chọn danh mục bảng xếp hạng
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }

    .dd-empty {
        display: none
    }

    #nestable3 {
        padding: 4px 8px !important;
        border: 1px #e0e0e0 solid;
        min-height: 40px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div style="display: none" class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <form id="form_test" class="repeater" method="POST"
                    action="{{route('admin.options.save_choose_category')}}">
                    @csrf
                    <div>
                        <div>
                            <div class="form-group row">
                                <div class="col-xs-10">
                                    <label>Chọn danh mục</label>
                                    <select name="categories[]" multiple="multiple" id="categories"
                                        class="form-control categories">
                                        {{-- @foreach ($categories as $item)
                                        <option
                                            {{!empty($options['value']) && in_array($item->id,json_decode($options['value'])) ? 'selected' : ''}}
                                        value="{{$item->id}}">
                                        {{$item->name}}
                                        </option>
                                        @endforeach --}}
                                    </select>
                                </div>

                            </div>


                            <!-- innner repeater -->


                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Lưu</button>

                    {{-- <input data-repeater-create type="button" value="Thêm" class="btn btn-primary callBackSelect2" /> --}}
                </form>
                <button class="btn_test">TEST</button>
            </div>
        </div>
    </div>


    {{-- ****************** --}}

    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">

                {{-- ************** --}}
                {{-- ----------------- --}}
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Thêm danh mục</label>
                        <select name="categories" id="categories2" class="form-control categories2">
                            @forelse ($categories as $item)
                            <option value="{{$item->id}}">
                                {{$item->name}}
                            </option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                    <div style="margin: 10px 0px">
                        <button style="margin: 5px 0px" class="btn btn-bitbucket btn-sm btn_add_element">Thêm <i
                                class="fa fa-long-arrow-right"></i></button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div>
                        <label for="">Thứ tự danh mục</label>
                    </div>
                    <div class="dd" id="nestable3">
                        <ol class="dd-list">
                            @php
                                $arr = json_decode($options['value'], true);

                            @endphp
                            @if (is_array($arr) && count($arr) > 0)
                            @foreach ($arr as $key => $item)
                            @php
                                $category = get_cat_by_id($item['id']);
                            @endphp
                            <li class="dd-item dd3-item" data-id="{{ $category->id }}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="text-box">
                                        {{ $category->name }}
                                    </div>
                                </div>
                                <div class="pull-right2">
                                    <span style="cursor: pointer" class="btn_remove_element">
                                        <i class="fa fa-close"></i>
                                    </span>
                                </div>
                            </li>
                            @endforeach
                            @endif

                        </ol>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div style="margin: 20px 0px" class="">
                        <button class="btn btn-success btn_save">Lưu</button>
                    </div>
                </div>
                {{-- ----------------- --}}


                {{-- **************** --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

{{-- <script src="{{asset('js/repeater.jquery.js')}}"></script> --}}

<script>
    $(document).ready(function () {
        $('.categories').select2();
    });

</script>
<script>
    $('.dd').nestable({
        maxDepth: 1,
        group: 2
    });
    $('.btn_save').click(function () {
        var a = $('.dd').nestable('serialize');
        //let result = a.map(a => a.id);
        var myJSON = JSON.stringify(a);
        console.log(myJSON);
        var formData = new FormData();

        $.ajax({
            url: "{{route('admin.options.save_choose_category2')}}",
            method: 'post',
            //processData: false,
            //contentType: false,
            type: 'json',
            dataType: 'json',
            data: {
                categories: myJSON,
                _token: '{{ csrf_token() }}'
            },
        }).done(
            result => {
                var msg = result;
                console.log(msg);
                if (result.error == false) {
                    swal("Lưu thành công!")

                } else {
                    //form.find('.error').html('');
                    // $('#create_song').trigger("reset");
                    // $('#this-img').attr('src', '');
                    //console.log(result);
                    //window.location.href = "{{ route('admin.songs.index') }}";
                    // table.ajax.reload();
                    // window.location.reload();
                }
                // table.ajax.reload();
            });
    })

</script>
<script>
    function buildItem(text, value) {

        var html = `<li class="dd-item dd3-item" data-id="${value}">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content">${text}</div>
                        <div class="pull-right2">
                            <span style="cursor: pointer" class="btn_remove_element">
                                <i class="fa fa-close"></i>
                            </span>
                        </div>
                    </li>`;
        return html;
    }
    $(function () {
        $('body').on('click', '.btn_add_element', function () {
            let text = $("#categories2 option:selected").text();
            text = $.trim(text);
            let value = $("#categories2").val();
            console.log(text);
            console.log(value);
            $('.dd-list').append(buildItem(text, value));

        })
        $('body').on('click', '.fa-close', function (e) {
            $(this).parents('.dd3-item').remove();
        })
    })

</script>
@endsection
