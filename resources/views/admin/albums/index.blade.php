@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Quản lý album
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .modal .cke_contents {
        height: 100px !important;
    }

    .select2 {
        width: 100% !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">
    <div class="col-sm-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Thêm mới album
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form id=create_album>
                    {{-- {{@csrf_field()}} --}}
                    <div class="form-group">
                        <label for="name">Tên album</label>
                        <input class="form-control" type="text" name="name" id="name"
                            value="{{ !empty(old('name')) ? old('name') : ''}}">
                        <div class="text-danger error_name" id="error_name"></div>
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input class="form-control" type="text" name=slug id=slug
                            value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                        <div class="text-danger error_slug" id="error_slug"></div>
                    </div>

                    <div class="form-group">
                        <label for="description">Mô tả</label>
                        <textarea class="form-control description" name="description" id="description" cols="30"
                            rows="10"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="image">Ảnh</label>
                        <input value="{{ (old('image') ? old('image') :  '' ) }}" readonly min="0" type="hidden"
                            name="image" class="form-control image" id="thumbnail">
                    </div>
                    <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn ảnh</button>
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img"
                            src="{{ (old('image') ? old('image') : '' ) }}" alt="">
                    </div>

                    <div class="form-group">
                        <button class=" btn btn-success" type="submit">Lưu</button>
                    </div>
                </form>



                <!-- end content here -->
            </div>
        </div>
    </div>
    {{-- /////////////////// --}}
    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    Danh mục album
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <div class="col-xs-12 ">
                    <form action="" method="get">
                        <div class="search">
                            <div class="pull-right ">
                                <button class="btn btn-flat btn-primary" type="submit">Tìm</button>
                            </div>
                            <div class="pull-right ">
                                <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" required type="search" name="search" id="search" class="form-control search">
                            </div>
                        </div>

                    </form>
                </div>
                <div class="table-responsive col-xs-12">
                    {{-- <table class="table table-hover table-striped" id="table-albums"></table> --}}
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Tên album</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($listAlbums as $item)
                            <tr>
                                <th scope="row">{{$item['id']}}</th>
                                <td>{{$item['name']}}</td>
                                <td>{{$item['slug']}}</td>
                                <td><button data-edit="{{$item['id']}}"
                                        class="btn-update btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>
                                    <button url="{{url('/').'/admin/albums/delete/'.$item['id']}}" class="btn btn-delete btn-danger btn-xs"><i
                                            class="fa fa-remove "></i></button></td>
                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                    <div class="pagination">
                        {{ $listAlbums->links() }}
                    </div>
                </div>



                <!-- end content here -->
            </div>
        </div>
    </div>
</div>
<!-- end main content -->



<!--  Modal -->




<!-- Modal -->
<div class="modal fade" id="updateAlbum" tabindex="-1" role="dialog" aria-labelledby="updateCategoryTitle"
    aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Sửa album</b></h4>
                </div>
                <div id="show-alert">

                </div>
            </div>
            <div class="modal-body">
                <form id="edit-album">
                    {{ @csrf_field() }}
                    <input type="hidden" name="id">
                    <div class="">
                        <div class="form-group">
                            <label for="name">Tên danh mục</label>
                            <input class="form-control" type="text" name="name" id="name2"
                                value="{{ !empty(old('name')) ? old('name') : ''}}">
                            <div class="text-danger error_name2" id="error_name2"></div>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input class="form-control" type="text" name=slug id=slug2
                                value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                            <div class="text-danger error_slug2" id="error_slug2"></div>
                        </div>

                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control description" name="description" id="update_description"
                                cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Ảnh</label>
                            <input value="{{ (old('image') ? old('image') :  '' ) }}" readonly min="0" type="hidden"
                                name="image" class="form-control image" id="thumbnail1">
                        </div>
                        <button class="btn btn-primary choose_image" id="choose_image1" type="button">Chọn ảnh</button>
                        <br>
                        <div class=form-group>
                            <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img1"
                                src="{{ (old('image') ? old('image') : '' ) }}" alt="">
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                            <button type="submit" class="btn btn-success">Lưu</button>
                            {{-- <button class=" btn btn-success" type="submit">Lưu</button> --}}
                        </div>
                    </div>
                </form>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div> --}}
        </div>
  </div>
</div>



<!-- end Modal -->




<!----- javascript here -------->
@section('js')
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config( { connectorPath: '/ckfinder/connector' } );
</script>

{{-- js --}}
{{-- @if ((old('image')))
<script>
    $('img#this-img').show();
</script>
@else
@if (!empty($product['image']))
<script>
    $('img#this-img').show();
</script>
@else
<script>
    $('img#this-img').hide();
</script>
@endif
@endif --}}
<script>
    $('body').on('click', '#choose_image', function(){
        // alert('msg');
        var choose = $(this);
        CKFinder.popup( {
            resourceType: 'Images',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();

                    var file_name = file.getUrl();
                    $('#thumbnail').val(file_name);

                    $('#this-img').show();
                    $('img#this-img').attr('src',file_name);
                } );
            }
        } );

    })
</script>
<script>
    $('body').on('click', '#choose_image1', function(){
        // alert('msg');
        var choose = $(this);
        CKFinder.popup( {
            resourceType: 'Images',
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();

                    var file_name = file.getUrl();
                    $('#thumbnail1').val(file_name);

                    $('#this-img1').show();
                    $('img#this-img1').attr('src',file_name);
                } );
            }
        } );

    })
</script>

{{-- asda --}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $(document).ready(function() {
  $("#updateCategory").on("hidden.bs.modal", function() {
    $("#show-alert").html("");
  });
});
</script>
<script>
    $(function () {
        $('#name').on('keyup', function () {
                ChangeToSlug('name', 'slug');
            })
        $('#name2').on('keyup', function () {
                ChangeToSlug('name2', 'slug2');
            })







// thêm mới danh mục
 $('#create_album').on('submit', function(e){
        e.preventDefault();

        var form = $(this);
        let dataForm = new FormData(form[0]);
        dataForm.set('description', content.getData());
        dataForm.set('_token', '{{csrf_token()}}');
        console.log(dataForm);
        $.ajax({
            url: "{{route('admin.albums.saveAdd')}}",
            // url: window.location.href,
            method: 'post',
            processData: false,
            contentType: false,
            data: dataForm,
        }).done(
            result => {
            var msg = result.data;
            if (result.errors) {
                if (msg.name) {
                    form.find('#error_name').html(msg.name);
                    // console.log(msg.name);
                }
                if (msg.slug) {
                    form.find('#error_slug').html(msg.slug);
                    // console.log(msg.slug);
                }
            } else {
                form.find('#error_name').html('');
                form.find('#error_slug').html('');
                $('#create_album').trigger("reset");
                $('#this-img').attr('src','');
                content.setData('');

                // console.log(msg);
                // table.ajax.reload();
                window.location.reload();


            }
            // table.ajax.reload();

        });
    })

    // sửa danh mục
     $('#edit-album').on('submit', function(e){
        e.preventDefault();
        // alert('ịijiij');
        var form = $(this);
        let dataForm = new FormData(form[0]);
        dataForm.set('description', update_description.getData());
        dataForm.set('_token', '{{ csrf_token() }}');
        // console.log(dataForm);
        $.ajax({
            url: "{{route('admin.albums.saveEdit')}}",
            // url: window.location.href,
            method: 'post',
            processData: false,
            contentType: false,
            data: dataForm,
        }).done(
            result => {
            var msg = result.data;
            if (result.errors) {
                if (msg.name) {
                    form.find('#error_name2').html(msg.name);
                    // console.log(msg.name);
                }
                if (msg.slug) {
                    form.find('#error_slug2').html(msg.slug);
                    // console.log(msg.slug);
                }
            } else {
                // form.find('#error_name2').html('');
                // form.find('#error_slug2').html('');
                // $('#edit-album').trigger("reset");
                // // $('#this-img1').attr('src','');
                // content.setData('');
                // table.ajax.reload();

                window.location.reload();
                if (result.data == true) {

                    $('#show-alert').html(`
                <div style="padding: 5px  35px 5px 15px !important; margin-bottom: 0px !important;" class="alert  alert-success alert-dismissible show" role="alert">
                    <strong>Thông báo!</strong> Sửa thành công
                    <button type="button" class="close" style="top: 0px !important" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>`);
                }


            }
            // table.ajax.reload();

        });
    })

    // update cate
    $('body').on('click', '.btn-update', function (e) {
        e.preventDefault;
        var id = $(this).data('edit');
        var p_id = 0;
        $.ajax({
            url: `{{route('admin.albums.edit')}}/${id}`,
            method: 'get'
        }).done(
            result => {
                var data = result;
                p_id = data.parent_id;
                $('#edit-album input[name=name]').val(data.name);
                $('#edit-album input[name=id]').val(data.id);
                $('#edit-album input[name=slug]').val(data.slug);
                // console.log(data.image);
                $('#edit-album').find('#thumbnail1').val(data.image);
                $('#edit-album').find('#this-img1').attr('src',data.image);

                // console.log(data.parent_id);
                update_description.setData(data.description);



                $('#updateAlbum').modal();
                $( "#updateAlbum" ).removeClass( "in" )


            }
        )
    })


    // ckeditor
    var options = {
        filebrowserUploadUrl: "{{route('admin.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        toolbar: 'Basic',
        /* this does the magic */
        uiColor: '#ecf0f5',
        toolbarGroups: [{
                name: 'document',
                groups: ['mode', 'doctools', 'document']
            },
            {
                name: 'clipboard',
                groups: ['clipboard', 'undo']
            },
            {
                name: 'editing',
                groups: ['find', 'selection', 'spellchecker', 'editing']
            },
            {
                name: 'forms',
                groups: ['forms']
            },
            {
                name: 'styles',
                groups: ['styles']
            },
            {
                name: 'basicstyles',
                groups: ['basicstyles', 'cleanup']
            },
            {
                name: 'paragraph',
                groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']
            },
            {
                name: 'links',
                groups: ['links']
            },
            {
                name: 'insert',
                groups: ['insert']
            },
            {
                name: 'colors',
                groups: ['colors']
            },
            {
                name: 'others',
                groups: ['others']
            },
            {
                name: 'tools',
                groups: ['tools']
            },
            {
                name: 'about',
                groups: ['about']
            }
        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'Find,Replace,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Flash,HorizontalRule,About,Save,NewPage,Preview,Print,Cut,Copy,Paste,Redo,Undo,SelectAll,Scayt,Form,Radio,Textarea,TextField,Select,Button,ImageButton,HiddenField,Anchor,Font,Templates,Checkbox,BulletedList,NumberedList,Outdent,Indent',

    }
    var content = CKEDITOR.replace('description', options);
    var update_description = CKEDITOR.replace('update_description', options);


    })

    // submit form



</script>

<script>
    $('#updateAlbum').css( "overflow", "dcm !important");
</script>


@endsection
<!----- end javascript here -------->

@endsection
