@extends('admin.layout.master')

{{-- page title --}}
@section('page_title')
Quản lý nghệ sĩ
@endsection


{{-- style --}}
@section('style')
<style>
    .box-header,
    label {
        cursor: pointer;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}

{{-- end alert --}}


<!-- main content -->
<div class="box box-danger">
    <div class="box-header" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
        <h3 class="box-title">
            Sửa nghệ sĩ
        </h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-plus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <form method="POST" action="{{route('admin.artists.saveEdit')}}">
            {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$artist->id}}">
            <!--  content here -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nghệ danh</label>
                          <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{ old('name') ? old('name') : $artist->name}}">
                        @error('name')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" id="slug" class="form-control" autocomplete="off" value="{{ old('slug') ? old('slug') : $artist->slug}}">
                        @error('slug')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="fullname">Họ tên thật</label>
                        <input type="text" name="fullname" id=fullname class="form-control" autocomplete="off" value="{{ old('fullname') ? old('fullname') : $artist->fullname}}">
                    </div>
                    <div class="form-group">
                        <label for="birthday">Sinh nhật</label>
                        @php
                            $date = date_create($artist->birthday);
                            $date = date_format($date,"d/m/Y");
                        @endphp
                        <input type="text" name="birthday" id="birthday" class="form-control birthday"
                            autocomplete="off" value="{{ old('birthday') ? old('birthday') : $date}}">
                    </div>
                    <div class="form-group">
                        <label for="country">Quốc gia</label>
                        <input type="text" name="country" id=country class="form-control" autocomplete="off" value="{{ old('country') ? old('country') : $artist->country}}"> 
                    </div>
                    <div class="form-group">
                        <label for="">Giới tính</label>
                        <div>
                            <label style="margin-right:10px; min-width: 80px" for="nam"><input name=gender type="radio"
                                    value="0" id="nam" {{$artist->gender == 0 ? 'checked' : '' }}>Nam</label>
                            <label style="margin-right:10px; min-width: 80px" for="Nữ"><input name=gender type="radio"
                                    value="1" id="Nữ" {{$artist->gender == 1 ? 'checked' : '' }}>Nữ</label>
                            <label style="margin-right:10px; min-width: 80px" for="Chưa xác định"><input name=gender
                                    type="radio" id="Chưa xác định" value="-1" {{$artist->gender == -1 ? 'checked' : '' }}>Chưa xác định</label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="status">Trạng thái</label>
                        <select name="status" id="status" class="form-control">
                            <option {{$artist->status == 1 ? 'selected' : '' }} value="1">Kích hoạt</option>
                            <option {{$artist->status == 0 ? 'selected' : '' }} value="0">Vô hiệu hóa</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="image">Ảnh</label>
                        <input
                            value="{{ (old('image')) ? old('image') : ( !empty($artist->image) ? $artist->image : '' ) }}"
                            readonly min="0" type="hidden" name="image" class="form-control image" id="thumbnail">
                    </div>
                    <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn ảnh</button>
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img"
                            src="{{ (old('image')) ? old('image') : ( !empty($artist->image) ? $artist->image : '' ) }}"
                            alt="">
                    </div>

                    <div class="form-group">
                        <label for="image">Banner</label>
                        <input value="{{ (old('banner')) ? old('banner') : ( !empty($artist->banner) ? $artist->banner : '' ) }}" readonly min="0" type="hidden" name="banner"
                            class="form-control image" id="banner">
                    </div>
                    <button class="btn btn-primary choose_banner" id="choose_banner" type="button">Chọn ảnh</button>
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-banner"
                            src="{{ (old('banner')) ? old('banner') : ( !empty($artist->banner) ? $artist->banner : '' ) }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="description">Tiểu sử</label>
                        <textarea name="description" id="description" class="description form-control" cols="30"
                            rows="10">{{ old('description') ? old('description') : $artist->description}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class=" btn btn-success" type="submit">Lưu</button>
            </div>
        </form>


        <!-- end content here -->
    </div>
</div>


{{-- <div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">
            Danh sách nghệ sĩ
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">

    </div>
    <div class="box-footer">

    </div>
</div> --}}
<!-- end main content -->
@endsection

<!----- javascript here -------->
@section('js')

<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config( { connectorPath: '/ckfinder/connector' } );
</script>


<script>
    $(".birthday").datepicker({
            format: "dd/mm/yyyy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            // todayHighlight: true,
            rtl: true,
            language: 'vi',
            orientation: "auto"});
    
    $('#name').on('keyup', function () {
    ChangeToSlug('name', 'slug');
    })
    

    
    chooseImg('choose_image', 'thumbnail', 'this-img');
    chooseImg('choose_banner', 'banner', 'this-banner');
</script>

@endsection
<!----- end javascript here -------->