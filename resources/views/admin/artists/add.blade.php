@extends('admin.layout.master')

{{-- page title --}}
@section('page_title')
Quản lý nghệ sĩ
@endsection


{{-- style --}}
@section('style')
<style>
    .box-header,
    label {
        cursor: pointer;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="box box-danger">
    <div class="box-header" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
        <h3 class="box-title">
            Thêm mới nghệ sĩ
        </h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-plus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <form method="POST" action="{{route('admin.artists.saveAdd')}}">
            {{ csrf_field() }}
            <!--  content here -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nghệ danh</label>
                    <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{ !empty(old('name')) ? old('name') : ''}}">
                        @error('name')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" id="slug" class="form-control" autocomplete="off" value="{{ !empty(old('slug')) ? old('slug') : ''}}">
                        @error('slug')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="fullname">Họ tên thật</label>
                        <input type="text" name="fullname" id=fullname class="form-control" autocomplete="off" value="{{ !empty(old('fullname')) ? old('fullname') : ''}}">
                    </div>
                    <div class="form-group">
                        <label for="birthday">Sinh nhật</label>
                        <input type="text" name="birthday" id="birthday" class="form-control birthday"
                            autocomplete="off" value="{{ !empty(old('birthday')) ? old('birthday') : ''}}"">
                    </div>
                    <div class="form-group">
                        <label for="country">Quốc gia</label>
                        <input type="text" name="country" id=country class="form-control" autocomplete="off" value="{{ !empty(old('country')) ? old('country') : ''}}">
                    </div>
                    <div class="form-group">
                        <label for="">Giới tính</label>
                        <div>

                            <label style="margin-right:10px; min-width: 80px" for="nam"><input name=gender type="radio"
                                    value="0" id=nam {{ old('gender') == '0' ?  'checked' : ''}}>Nam</label>
                            <label style="margin-right:10px; min-width: 80px" for="nu"><input name=gender type="radio"
                                    value="1" id=nu {{ old('gender') == '1' ?  'checked' : ''}}>Nữ</label>
                            <label style="margin-right:10px; min-width: 80px" for="chuaxacdinh"><input name=gender
                                    type="radio" id=chuaxacdinh value="-1" {{ old('gender') == '-1' ?  'checked' : ''}}>Chưa xác định</label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="status">Trạng thái</label>
                        <select name="status" id="status" class="form-control">

                            <option {{ old('status') == '1' ? 'selected' : '' }} value="1">Kích hoạt</option>
                            <option {{ old('status') == '0' ? 'selected' : '' }} value="0">Vô hiệu hóa</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="image">Ảnh</label>
                        <input
                            value="{{ !empty(old('image')) ? old('image') : '' }}"
                            readonly min="0" type="hidden" name="image" class="form-control image" id="thumbnail">
                    </div>
                    <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn ảnh</button>
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img"
                            src="{{ !empty(old('image')) ? old('image') : '' }}"
                            alt="">
                    </div>

                    <div class="form-group">
                        <label for="image">Banner</label>
                        <input value="{{ !empty(old('banner')) ? old('banner') : '' }}" readonly min="0" type="hidden" name="banner"
                            class="form-control image" id="banner">
                    </div>
                    <button class="btn btn-primary choose_banner" id="choose_banner" type="button">Chọn ảnh</button>
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-banner"
                            src="{{ !empty(old('banner')) ? old('banner') : '' }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="description">Tiểu sử</label>
                        <textarea name="description" id="description" class="description form-control" cols="5"
                            rows="5">{{!empty(old('description')) ? old('description') : ''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class=" btn btn-success" type="submit">Lưu</button>
            </div>
        </form>


        <!-- end content here -->
    </div>
</div>


{{-- <div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">
            Danh sách nghệ sĩ
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">

    </div>
    <div class="box-footer">

    </div>
</div> --}}
<!-- end main content -->
@endsection

<!----- javascript here -------->
@section('js')

<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config( { connectorPath: '/ckfinder/connector' } );
</script>

{{-- js --}}
{{-- @if ((old('image')))
<script>
    $('img#this-img').show();
</script> --}}
{{-- @else --}}
{{-- @if (!empty($product['image']))
<script>
    $('img#this-img').show();
</script>
@else
<script>
    $('img#this-img').hide();
</script>
@endif --}}
{{-- @endif --}}
<script>
    $(".birthday").datepicker({
            format: "dd/mm/yyyy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            language: 'vi',
            orientation: "auto"});

    $('#name').on('keyup', function () {
    ChangeToSlug('name', 'slug');
    })



    chooseImg('choose_image', 'thumbnail', 'this-img');
    chooseImg('choose_banner', 'banner', 'this-banner');

</script>

@endsection
<!----- end javascript here -------->
