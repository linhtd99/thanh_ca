@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách bình luận
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->

                <div class="col-xs-12 ">
                    <form class="form-action" id="form-action">
                        <div style="margin-right: 5px !important;" class="pull-left">
                            <select name="action" id="action" class="form-control input-sm action" field_signature="2473543864">
                                <option value="0">Tác vụ</option>
                                <option value="duyet">Duyệt</option>
                                <option value="duyetAll">Duyệt tất</option>
                                <option value="delete">Xoá</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <button id="btn-action" class="btn btn-secondary btn-sm btn-flat btn-action">Áp
                                dụng</button>
                        </div>
                    </form>
                    <form action="" method="get">
                        <div class="search">
                            <div class="pull-right ">
                                <button class="btn btn-flat btn-primary" type="submit">Tìm</button>
                            </div>
                            <div class="pull-right ">
                                <select name="type" id="type" class="form-control">
                                    <option {{!empty($_GET['type']) && $_GET['type'] == "song" ? 'selected' : '' }} value="song">Bài hát</option>
                                    <option {{!empty($_GET['type']) && $_GET['type'] == "video" ? 'selected' : '' }} value="video">Video</option>
                                    <option {{!empty($_GET['type']) && $_GET['type'] == "playlist" ? 'selected' : '' }} value="playlist">Playlist</option>
                                    <option {{!empty($_GET['type']) && $_GET['type'] == "bxh-video" ? 'selected' : '' }} value="bxh-video">BXH Video</option>
                                </select>
                            </div>
                        </div>


                    </form>
                </div>
                <br>
                <div class="col-xs-12">
                    <div class="table-responsive ">
                        <table class="table table-hover table-striped" id="table-songs">
                            <thead>
                                <tr>
                                    <th>
                                        <input id="checkAll" value="all" class="checkAll" type="checkbox" name="checkAll">
                                    </th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nội dung</th>
                                    <th scope="col">Người bình luận</th>
                                    <th scope="col">Luợt like</th>
                                    <th scope="col">Trạng thái</th>

                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($comments) || count($comments) != 0)
                                @foreach ($comments as $item)
                                <tr>
                                    <input type="hidden" id="content_comment" value="{{$item->content}}"">
                                    <td>
                                        <input type="checkbox" class="checkbox" value="{{  $item->id  }}" name="checkbox">
                                    </td>
                                    <td>
                                        {{ $item->id }}
                                    </td>
                                    <td>
                                        @php
                                            if (strlen($item->content) > 100) {
                                                echo substr($item->content,0,100);
                                                echo '<a style="cursor: pointer" class="read_more" data-toggle="modal" data-target="#exampleModalCenter"> Xem tiếp </a>';
                                            }else{
                                                echo $item->content;
                                            }
                                        @endphp
                                    </td>

                                    <td>{{ $item->customer_id == 0 ? 'admin' : getNameCustomer($item->customer_id)}}</td>

                                    <td>
                                        {{ $item->total }}
                                    </td>

                                    <td class="status" attr-id="{{$item->id}}" val="{{$item->status}}">
                                        <?php echo $item->status == -1 ? '<span class="label label-warning">Chờ duyệt</span>' : '<span class="label label-primary">Đã duyệt</span>' ?>
                                    </td>

                                    <td class="btn-group-xs">
                                    @php
                                        if (!empty($_GET['type'])) {
                                            if ($_GET['type'] == 'song') {
                                                $type = 'song';
                                            }elseif($_GET['type'] == 'video'){
                                                $type = 'detailVideo';
                                            }elseif($_GET['type'] == 'playlist'){
                                                $type = 'playlist';
                                            }elseif($_GET['type'] == 'bxh-video'){
                                                $type = 'playlist_bxh_video';
                                            };
                                        } else {
                                            $type = '';
                                        }

                                    @endphp
                                    @if (!empty($_GET['type']) && $_GET['type'] == 'bxh-video')
                                        @php
                                            $arr_code =  explode('.',$item->code);

                                        @endphp
                                        <a href="{{route($type,['slug' => $arr_code[count($arr_code) - 3],'week' => $arr_code[count($arr_code) - 2], 'year' => $arr_code[count($arr_code) - 1] ])}}" target="_blank" data-toggle="modal"
                                            class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i></a>
                                    @else
                                        <a href="{{route($type,['name' => 'preview','code' => $item->code ])}}" target="_blank" data-toggle="modal" class="btn btn-primary btn-xs"><i
                                                class="fa fa-info-circle"></i></a>
                                    @endif

                                        <button data-remove="{{ $item->id }}" class="btn btn-danger btn-remove"><i
                                                class="fa fa-remove "></i></button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                Không có dữ liệu
                                @endif
                            </tbody>

                        </table>
                        <div class="pagination">
                            {{ $comments->appends(['type' => $_GET['type']])->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here -->
        </div>
    </div>
</div>

<!-- end main content -->

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nội dung</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="word-wrap: break-word;">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>






<!----- javascript here -------->
@section('js')
<script>

    $(function () {
        $('.read_more').on('click',function(){
           var content = $(this).parents('tr').find('#content_comment').val();
           $('.modal-body').html(content);
        });
        $('body').on('click', '.btn-remove', function (e) {
            // alert('ok');

            var id = $(this).data('remove');
            // alert(removeUrl);
            swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn muốn xoá mục này không?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.comments.delete')}}",
                            method: 'POST',
                            data: {
                                id: id,
                                _token: '{{csrf_token()}}'
                            }
                        }).done(result => {
                            if (result) {
                                swal("Deleted!",
                                    "Video đã bị xoá.",
                                    "success");
                            };
                            setTimeout(function () {
                                location.reload();
                            }, 500);


                        });
                    }
                });
        });
    })
    $(function () {
            $('body').on('click', '#btn-action', function (e) {
                // console.log('a');
                e.preventDefault();
                var action = $('body').find('#action').val();
                if (parseInt(action) != 0) {
                    if (action == 'delete') {
                        var data = [];
                        $.each($("input[name='checkbox']:checked"), function () {
                            data.push($(this).val());
                        });
                        // console.log(data);
                        if (data.length != 0) {
                            var c = confirm("Bạn có chắc chắn muốn xoá mục đã chọn?");
                            if (c === true) {
                                var formData = new FormData();
                                formData.append('data', data);
                                ajaxFunc("{{route('admin.comments.deleteMulti')}}", formData);
                                window.location.reload();
                            }
                        } else {
                            alert('Vui lòng chọn!')
                        }
                    }
                    if (action == 'duyet') {
                        var data = [];
                        $.each($("input[name='checkbox']:checked"), function () {
                            data.push($(this).val());
                        });
                        if (data.length != 0) {
                            var c = confirm("Bạn có chắc chắn muốn duyệt mục đã chọn?");
                            if (c === true) {
                                var formData = new FormData();
                                formData.append('data', data);
                                ajaxFunc("{{route('admin.comments.accept')}}", formData);
                                window.location.reload();
                            }
                        } else {
                            alert('Vui lòng chọn!')
                        }
                    }
                    if (action == 'duyetAll') {
                        var c = confirm("Bạn có chắc chắn muốn duyệt tất cả bình luận?");
                        if (c === true) {
                            var data = {
                                action : acceptAll = null,
                            };
                            ajaxFunc("{{route('admin.comments.acceptAll')}}", data);
                            window.location.reload();
                        }
                    }
                }
            })
    })

    $('.status').on('click',function(){
            id = $(this).attr('attr-id');
            console.log($(this).attr('val'));
            if ($(this).attr('val') == '-1') {
                $.ajax({
                    url : "{{route('admin.comments.updateStatus')}}",
                    method: 'post',

                    data: {
                        id:id,
                        val:1,
                        _token: '{{csrf_token()}}'
                    },
                }).done(
                    result => {

                    window.location.reload();

                });
            }else{
                 $.ajax({
                    url : "{{route('admin.comments.updateStatus')}}",
                    method: 'post',

                    data: {
                        id:id,
                        val:-1,
                        _token: '{{csrf_token()}}'
                    },
                }).done(
                    result => {

                    window.location.reload();

                });
            }
    });
</script>

@endsection
<!----- end javascript here -------->

@endsection
