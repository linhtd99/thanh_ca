<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel" style="">
            <div class="pull-left image">
                <img style="height: auto !important; width:100% !important" src="{{ !empty(Auth::user()->avatar) ? Auth::user()->avatar : '' }} " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Request::segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{ route('admin.home') }}"><i class="fa  fa-dashboard"></i><span>Dashboard</span></a>
            </li>


            {{-- Danh mục --}}
            {{-- <li class="{{ Request::segment(2) == 'categories' ? 'active' : '' }}">
                <a href="{{ route('admin.categories.index') }}">
                <i class="fa fa-list-alt"></i>
                <span>Danh mục bài hát</span>
                </a>
            </li> --}}


            {{-- Album --}}
            {{-- <li class="{{ Request::segment(2) == 'albums' ? 'active' : '' }}">
                <a href="{{ route('admin.albums.index') }}">
                    <i class="fa fa-list-alt"></i>
                    <span>Danh sách album</span>
                </a>
            </li> --}}


            {{-- Bài hát  --}}
            <li class="treeview {{ Request::segment(2) == 'songs' ? 'active' : '' }} {{ Request::segment(2) == 'albums' ? 'active' : '' }} {{ Request::segment(2) == 'categories' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-music"></i> <span>Quản lý bài hát</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ Request::segment(2) == 'songs' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a href="{{ route('admin.songs.add') }}"><i class="fa fa-circle-o"></i> Thêm Bài hát</a></li>
                    <li class="{{ Request::segment(2) == 'songs' && Request::segment(3) == '' ? 'active' : ''  }}"><a href="{{ route('admin.songs.index') }}"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'categories' ? 'active' : '' }}"><a href="{{ route('admin.categories.index') }}"><i class="fa fa-list-alt"></i> Danh mục bài hát</a></li>

                    <li class="{{ Request::segment(2) == 'albums' ? 'active' : '' }}"><a href="{{ route('admin.albums.index') }}"><i class="fa fa-list-alt"></i> Album bài hát</a></li>
                </ul>

            </li>
            <li class="treeview {{ Request::segment(2) == 'videos' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-youtube"></i> <span>Quản lý video</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'videos' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.videos.index') }}"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'videos' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a
                            href="{{ route('admin.videos.add') }}"><i class="fa fa-circle-o"></i> Thêm Bài hát</a></li>
                </ul>

            </li>

            {{-- Nghe si  --}}
            <li class="treeview {{ Request::segment(2) == 'artists' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-user"></i> <span>Quản lý nghệ sĩ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'artists' && Request::segment(3) == '' ? 'active' : ''  }}"><a href="/admin/artists/"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'artists' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a href="/admin/artists/add"><i class="fa fa-circle-o"></i> Thêm nghệ sĩ</a></li>
                </ul>

            </li>

            <li class="treeview {{ Request::segment(2) == 'playlists' || Request::segment(2) == 'tags' ||  Request::segment(2) == 'topics' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-headphones"></i> <span>Quản lý playlist</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::segment(2) == 'playlists') && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.playlists.index') }}"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'playlists' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a
                            href="{{ route('admin.playlists.add') }}"><i class="fa fa-circle-o"></i> Thêm playlist mới</a></li>
                    <li class="{{ Request::segment(2) == 'tags' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.tags.index') }}"><i class="fa fa-circle-o"></i> Quản lý tags</a></li>
                    <li class="{{ Request::segment(2) == 'topics' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.topics.index') }}"><i class="fa fa-circle-o"></i>Quản lý chủ đề</a></li>
                </ul>

            </li>
            <li class="treeview {{ Request::segment(2) == 'pages' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-file"></i> <span>Quản lý trang</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'pages' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.pages.index') }}"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'pages' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a
                            href="{{ route('admin.pages.add') }}"><i class="fa fa-circle-o"></i> Thêm trang mới</a></li>
                </ul>

            </li>
            <li class="treeview {{ Request::segment(2) == 'posts' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-file"></i> <span>Quản lý tin tức</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'posts' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.posts.index') }}"><i class="fa fa-circle-o"></i>Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'posts' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a
                            href="{{ route('admin.posts.add') }}"><i class="fa fa-circle-o"></i> Thêm trang mới</a></li>
                </ul>

            </li>


            {{-- Người dùng --}}
            <li class="treeview {{ Request::segment(2) == 'customers' ? 'active' : '' }}">
                <a>
                    <i class="fa  fa-users"></i> <span>Người dùng</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'customers' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="/admin/customers/"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'customers' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a
                            href="/admin/customers/add"><i class="fa fa-circle-o"></i> Thêm người dùng</a></li>
                </ul>

            </li>


            {{-- Quản trị viên --}}
            <li class="treeview {{ Request::segment(2) == 'users' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-user-secret"></i> <span>Quản trị</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'users' && Request::segment(3) == '' ? 'active' : ''  }}"><a href="/admin/users/"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                    <li class="{{ Request::segment(2) == 'users' && Request::segment(3) == 'add' ? 'active' : ''  }}"><a href="/admin/users/add"><i class="fa fa-circle-o"></i> Thêm quản trị</a></li>
                </ul>

            </li>
            <li class="treeview {{ Request::segment(2) == 'comments' | Request::segment(2) == 'reports' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-comments"></i> <span>Bình luận và báo lỗi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'comments' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.comments.index',['type' => 'song']) }}"><i class="fa fa-circle-o"></i>Danh sách bình luận</a></li>
                    <li class="{{ Request::segment(2) == 'reports' && Request::segment(3) == '' ? 'active' : ''  }}"><a
                            href="{{ route('admin.reports.index',['type' => 'song']) }}"><i class="fa fa-circle-o"></i>Danh sách báo lỗi</a></li>
                </ul>
            </li>
            <!--------------------------------------------------->

            <li class="treeview {{ Request::segment(2) == 'options' ? 'active' : '' }}">
                <a>
                    <i class="fa fa-cogs"></i> <span>Thiết lập chung</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'options' && Request::segment(3) == 'sort_category' ? 'active' : ''  }}"><a
                            href="{{ route('admin.options.sort_category') }}"><i class="fa fa-circle-o"></i>Sắp xếp danh mục</a></li>
                    <li class="{{ Request::segment(2) == 'options' && Request::segment(3) == 'choose_category_bxh' ? 'active' : ''  }}"><a
                            href="{{ route('admin.options.choose_category_bxh') }}"><i class="fa fa-circle-o"></i>Thiết lập bảng xếp
                            hạng</a></li>
                    <li class="{{ Request::segment(2) == 'options' && Request::segment(3) == 'menu_footer' ? 'active' : ''  }}"><a
                            href="{{ route('admin.options.menu_footer') }}"><i class="fa fa-circle-o"></i>Thiết lập chân trang</a></li>
                    <li class="{{ Request::segment(2) == 'options' && Request::segment(3) == 'generals' ? 'active' : ''  }}"><a
                            href="{{ route('admin.options.generals') }}"><i class="fa fa-circle-o"></i>Thiết lập chung</a></li>
                </ul>

            </li>
        </ul>

        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
