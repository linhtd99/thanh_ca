@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách quản trị
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->

<div class="row">
    <div class="col-sm-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Sửa quản trị
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--  content here -->
                <form method="POST" action="{{route('admin.posts.saveEdit')}}">
                    {{@csrf_field()}}

                    <input type="hidden" value="{{$post->id}}" name="id">

                    <div class="form-group">
                        <label for="name">Tên</label>
                        <input class="form-control" type="text" name="name" id="name"
                            value="{{ !empty($post->name) ? $post->name : ''}}">
                        @error('name')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input class="form-control" type="text" name="slug" id="slug"
                            value="{{ !empty($post->slug) ? $post->slug : ''}}">
                        @error('slug')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input value="{{  !empty($post['image']) ? $post['image'] : ''  }}" readonly
                            min="0" type="hidden" name="image" class="form-control image" id="thumbnail">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-sm btn-primary choose_image" id="choose_image" type="button">Chọn
                            Ảnh</button><br>
                        <span style="margin-left: 10px !important" class="boxs-img" class="">
                            <i style="display:none" class="btn btn-danger btn-xs btn-remove-img">
                                <i class="fa fa-times"></i>
                            </i>
                            <img style="width: 50%; height: auto;" id="this-img"
                                src="{{ (old('image')) ? old('image') : ( !empty($post['image']) ? $post['image'] : '' ) }}" alt="">
                        </span>
                    </div>

                    <div class="form-group">
                        <label for="content">Nội dung</label>
                        <textarea class="form-control content" name="content" id="content" cols="30"
                            rows="10">{{ !empty($post->content) ? $post->content : ''}}</textarea>
                    </div>

                    {{-- <div class="form-group">
                        <label for="cf_password">Nhập lại mật khẩu</label>
                        <input class="form-control" type="password" name="new_password" id="new_password" value="">
                        @error('new_password')
                        <span style="color:red">{{$message}}</span>
                    @enderror
            </div> --}}
            <div class="form-group">
                <label for="status">Trạng thái</label>
                <select name="status" id="status" class="form-control">
                    <option {{$post->status == 0 ? 'selected' : ''}} value="0">Đóng</option>
                    <option {{$post->status == 1 ? 'selected' : ''}} value="1">Mở</option>
                </select>
            </div>
            <div class="form-group">
                <button class=" btn btn-success" type="submit">Lưu</button>
            </div>
            </form>



            <!-- end content here -->
        </div>
    </div>
</div>
{{-- /////////////////// --}}

</div>
<!-- end main content -->




@endsection
<!----- end javascript here -------->
@section('js')
<script>
    $('#name').on('keyup', function () {
        ChangeToSlug('name', 'slug');
    })
    
</script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>
<script>
    chooseImg('choose_image', 'thumbnail', 'this-img');
</script>
<script>
    var options = {
        filebrowserUploadUrl: "{{route('admin.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
    }
        var content = CKEDITOR.replace('content', options);
        



</script>
@endsection