@extends('admin.layout.master')

{{-- page title --}}
@section('page_title')
Thêm playlist
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/playlist.css') }}">
<style>
    .box-header,
    label {
        cursor: pointer;
    }

    .item_content a {
        color: black;
    }

    .disable {
        color: gray;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        display: block !important;
        float: none !important;
    }

    /* css luong */
    ul.list_song_in_album li .button_downsort:hover {
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -195px -95px no-repeat;
    }

    ul.list_song_in_album li .button_downsort {
        width: 14px;
        height: 14px;
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -195px -81px no-repeat;
        display: block;
        float: right;
        margin: 10px 5px 0px 0px;
    }

    ul.list_song_in_album li .button_delete {
        width: 14px;
        height: 14px;
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -178px -81px no-repeat;
        display: block;
        float: right;
        margin: 10px 10px 0px 0px;
    }

    ul.list_song_in_album li .button_delete:hover {
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -178px -95px no-repeat;
    }

    ul.list_song_in_album li .button_upsort:hover {
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -210px -95px no-repeat;
    }

    ul.list_song_in_album li .button_upsort {
        width: 14px;
        height: 14px;
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -210px -81px no-repeat;
        display: block;
        float: right;
        margin: 10px 5px 0px 0px;
    }

    ul.list_song_in_album li .button_playing:hover {
        cursor: pointer;
    }

    ul.list_song_in_album li .button_playing:hover {
        background: url(https://stc-id.nixcdn.com/v11/images/icon.png) -30px -14px no-repeat;
    }

    #idScrllSongInAlbumEditPlaylist .button_select {
        display: block;
        float: right;
        margin: 0px 10px 0px 0px;
        color: #0689ba;
    }

    ul.list_song_in_album li .item_content {
        font-size: 14px;
        float: left;
        float: left;
        width: 550px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        color: #999999;
    }

    #listSong li {
        width: 350px;
    }

</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="box box-danger">
    <div class="box-header" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
        <h3 class="box-title">
            Thêm mới playlist
        </h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-plus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <form method="POST" action="{{route('admin.playlists.saveAdd')}}">
            {{ csrf_field() }}
            <!--  content here -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Tên playlist</label>
                        <input type="text" name="name" id="name" class="form-control" autocomplete="off"
                            value="{{ !empty(old('name')) ? old('name') : ''}}">
                        @error('name')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Thể loại</label>
                        <select name="category_id" id="category" class="form-control">
                            @foreach ($category as $item)
                            @if ($item->parent_id != 0)
                            <option {{ !empty(old('category')) && old('category') == $item->id ? 'selected' : '' }}
                                value="{{$item->id}}">{{$item->name}}</option>

                            @endif
                            @endforeach
                        </select>
                        @error('category')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Chủ đề</label>
                        <select name="topic_id" id="topics" class="form-control">
                        </select>
                        @error('topics')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Tag</label>
                        <select name="tag_id[]" multiple id="tag" class="form-control">
                        </select>
                        @error('tag_id')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Chọn bài hát</label>
                        <select name="songs[]" id="songs" multiple="multiple" class="form-control">

                        </select>
                        @error('songs')
                        <span style="color:red">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Chọn Trạng Thái</label>
                        <select name="status" class="form-control" >
                            <option selected="select" >Chọn</option>
                            <option value="1">Kích hoạt </option>
                            <option value="-1" >Vô hiệu hóa</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="image">Ảnh</label>
                        <input value="{{ !empty(old('image')) ? old('image') : '' }}" readonly min="0" type="hidden"
                            name="image" class="form-control image" id="thumbnail">
                    </div>
                    <button class="btn btn-primary choose_image" id="choose_image" type="button">Chọn ảnh</button>
                    @error('image')
                    <span style="color:red">{{$message}}</span>
                    @enderror
                    <br>
                    <div class=form-group>
                        <img style="width: auto; height: 110px; padding-top: 10px !important" id="this-img"
                            src="{{ !empty(old('image')) ? old('image') : '' }}" alt="">
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group wn-tab-ds">
                        <label class="label" for="txtAddr">Danh sách bài hát</label>
                        <div class="box_list_item_edit">
                            <div id="listSongSearch" class="list_song_in_album"
                                style="width:290px; float: left;overflow:scroll">
                                <div style="width:100%; height:37px; background:#f1f1f1">
                                    {{-- <form> --}}
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <input style="width:86.5%; margin:1.5%;" class="input input-sm form-control"
                                        type="text" id="searchInputQuickList" maxlength="300"
                                        placeholder="Nhập từ khóa">
                                    {{-- <button class="btn_search" id="searchInputQuickListBtn" type="submit" value=""
                                        title="Tìm kiếm"></button> --}}
                                    {{-- </form> --}}
                                </div>
                                <div>

                                    <ul id="idScrllSuggestionEditPlaylist" class="list_song_in_album"
                                        style="width: 280px; border: none; height: 450px; padding-left: 0px !important">
                                        <input type="hidden" id="arr_listSong" value="">
                                        <div id="idScrllSongInAlbumEditPlaylist" style="text-transform:capitalize;">

                                        </div>
                                    </ul>
                                </div>
                                <div class="clr"></div>
                            </div>
                            <span class="box-select-hero"></span>
                            <ul id="listSong" class="list_song_in_album" style="width:360px;padding-left: 0px;">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; height: 450px;">
                                    <div id="idScrllSongInAlbum" style="height: 450px; overflow: hidden; width: 360px;">

                                    </div>
                                    <div style="margin:6px 0px 10px 18px;">
                                        {{-- <a href="" class="btn-random" title="Sắp xếp ngẫu nhiên" style="color:#0689ba;">Sắp xếp ngẫu nhiên</a> --}}
                                    </div>
                                </div>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="description">Mô tả</label>
                        <textarea name="description" id="description" class="description form-control" cols="5"
                            rows="5">{{!empty(old('description')) ? old('description') : ''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class=" btn btn-success" type="submit">Lưu</button>
            </div>
        </form>


        <!-- end content here -->
    </div>
</div>



@endsection

<!----- javascript here -------->
@section('js')

<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>

<script>
    $(document).ready(function () {
        $('#category').select2();
        $('#songs').select2({
            insertTag: function (data, tag) {
                // Insert the tag at the end of the results
                data.push(tag);
            },
            ajax: {
                url: `{{route('searchSongAddPlaylist')}}`,
                data: function (params) {
                    var query = {
                        keysearch: params.term,
                    }
                    return query;
                },

                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            var artist_name = '';
                            if (item.artists.length == 0) {
                                artist_name = 'Chưa xác định';
                            } else {
                                item.artists.forEach(element => {
                                    artist_name += `${element.name}`;
                                });
                            }
                            return {
                                text: `${item.name} - ${artist_name}`,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });


        $('#topics').select2({
            ajax: {
                url: `{{route('admin.topics.dataSelect2')}}`,
                data: function (params) {
                    var query = {
                        keysearch: params.term,
                    }
                    return query;
                },

                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {

                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $('#tag').select2({
            ajax: {
                url: `{{route('admin.tags.dataSelect2_ch')}}`,
                data: function (params) {
                    var query = {
                        keysearch: params.term,
                    }
                    return query;
                },

                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {

                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });




    });

    chooseImg('choose_image', 'thumbnail', 'this-img');

</script>
<script>
    $(function () {
        $("ul.select2-selection__rendered").sortable({
            containment: 'parent'
        });
    }) 

</script>


<script>
    $(function(){
        $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    // Tìm kiếm danh sách bài hát trang thêm play list
    var e = $.Event('keydown');
    setTimeout(() => {
        $('#searchInputQuickList').trigger('click');
    }, 1000);

	$('#searchInputQuickList').bind('keyup click',function() {
		var content ='';
		var artist_name = '';
		let scope = $(this);
		var keysearch = scope.val();

		$.ajax({
			url : "{{route('searchSongAddPlaylist')}}",
			method: 'post',
			data :  { keysearch : keysearch},

		}).done(
			result => {
				$.each(result,function(i,val){
					html = $('#arr_listSong').text();

					str = html.substring(0, html.length-1);
					newStr = '['+str+']';


					if (val.artists.length == 0) {
							artist_name = 'Chưa xác định';
					} else {
						val.artists.forEach(element => {
							artist_name += `${element.name}`;
						});
					}
					var flag;


						if (newStr.indexOf(JSON.stringify(val)) !== -1) {
							flag = true;
						}else{
							flag = false;
						};


					content +=  `<li id="item_list_${val.id}" style="width:280px" class="alternate" key="${val.id}">

					<span class="nunberit">${i+1}</span>
					  <div class="item_content" style="width:190px"><a id="name_song_${val.id}"
								onclick="_gaq.push(['_trackEvent', 'Home', 'Click', 'Quick Search Edit Playlist']);"
								href="javascript:;" class="name_song">${val.name}</a> -
							<span id="name_singer_${val.id}"><a class="name_singer"
									href=""
									title="">${artist_name}</a></span>
							</div><a href="javascript:;" data-id="${val.id}" class="${flag ? `disable` : `button_select`}">${flag ? `Đã chọn` : `Chọn`}</a>
					 </li>`;
				})
				$('#idScrllSongInAlbumEditPlaylist').html(content);
        });

    });


    $('body').on('click','.button_select',function (e) {
		e.preventDefault();
		$(this).html(`Đã chọn`);
		$(this).removeClass();
		$(this).addClass('disable');
		var id = $(this).attr('data-id');
		// var artist_name = '';
		$.ajax({
		url : "{{route('searchSongById')}}",
		method: 'post',
		data : { id : id},

		}).done(
		result => {

			$('#arr_listSong').append(JSON.stringify(result)+',');

		});
    });

    $('body').on('DOMSubtreeModified','#arr_listSong',function(e) {
			var content = '';
			html = $(this).text();
			str = html.substring(0, html.length-1);

			newStr = '['+str+']';
			var artist_name ='';
			$.each(JSON.parse(newStr), function( index, value ) {
				console.log(value.artists.length);
				if (value.artists.length == 0) {
						artist_name = 'Chưa xác định';
				} else {
						artist_name ='';
						value.artists.forEach(element => {
						artist_name += `${element.name}`;
					});
				}
				content +=
				`<li style="width:350px" class="alternate" key="${value.id}">
                    <input name="arr_id[]" class="arr_id" type="hidden" value="${value.id}"></input>
                    <span class="nunberit"></span>
					 <div class="item_content" style="width:270px"><a href="javascript:;" class="name_song"
					>${value.name}</a> - <a class="name_singer"

							title="Tìm các bài hát, playlist, mv do ca sĩ Canace Wu trình bày">${artist_name}</a>
					</div>
					 <a target="_blank"
						class="button_playing" style="margin-right: 5px;" title="Nghe bài hát này"></a>
					<a href="javascript:;" data-id="${value.id}" class="button_delete" title="Xóa bài hát này"></a>
					</li>`;
					$('#idScrllSongInAlbum').html(content);
					var sizeLi = $('body').find('#idScrllSongInAlbum').find('li').length;
					// console.log(sizeLi);
					if (sizeLi == 1) {
						$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
						$('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
					} else if(sizeLi == 2){

						$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

						$('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
					}else if(sizeLi == 3){
						$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

						$('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');
					}else{
						$('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

						$('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
					}

			});

    });


    $('body').on('click','.button_upsort',function(){
		liCurrent = $(this).parent('li');
		var wrapper = $(this).closest('li');
		var current = wrapper.insertBefore(liCurrent.prev());
		var sizeLi = $('#idScrllSongInAlbum').find('li').size();
		// console.log(sizeLi);
		if (sizeLi == 1) {
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
		} else if(sizeLi == 2){

			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
		}else if(sizeLi == 3){
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');
		}else{
			$('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

			$('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
			if (liCurrent.index() == 0) {
			// console.log('đứng đầu');
			$(this).css('display','none');
			$('div#idScrllSongInAlbum').find('li:not(:first-child)').find('.button_upsort').css('display','block');
			} else {
			// console.log('ko');
			$(this).css('display','block');
			}
		}



		var content = $(this).parents('div#idScrllSongInAlbum').html();

		$('div#idScrllSongInAlbum').html(content);

	});

	$('body').on('click','.button_downsort',function(){
		liCurrent = $(this).parent('li');
		var wrapper = $(this).closest('li');
		var current = wrapper.insertAfter(liCurrent.next());

		var sizeLi = $('div#idScrllSongInAlbum > li').size();
		// console.log(sizeLi);
		if (sizeLi == 1) {
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

			$('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
		} else if(sizeLi == 2){

			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

			$('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','block');

			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');

			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
		}else if(sizeLi == 3){
			$('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','block');
			$('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');

		}else{
			$('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

			$('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
			if (liCurrent.index() == (sizeLi-1)) {
				$(this).css('display','none');
				$('div#idScrllSongInAlbum').find('li:not(:last-child)').find('.button_downsort').css('display','block');
			} else {
				$(this).css('display','block');
			}
		}


		var content = $(this).parents('div#idScrllSongInAlbum').html();
		$('div#idScrllSongInAlbum').html(content);

	});

	$('body').on('click','#checkAll',function(){
		if ($('#checkAll:checked').length == 1) {
			$('.check_data.box-checkbox > input[name=check_video]').trigger('click');
		}else{
			$('.check_data.box-checkbox > input[name=check_video]').trigger('click');
		}
    })



    jQuery('body').on('click','.button_delete',function(e) {
		var id = $(this).attr('data-id');
		var scope = $(this).parent('li').attr('key');
		$(this).parent('li').remove();
		$('#item_list_'+scope+' > a').html('Chọn');
		$('#item_list_'+scope+' > a').removeClass();
		$('#item_list_'+scope+' > a').addClass('button_select');
		$.ajax({
			url : "{{route('searchSongById')}}",
			method: 'post',
			data : { id : id},

		}).done(
		result => {
			var arr_listsong = $('#arr_listSong').text();
			res = arr_listsong.replace(JSON.stringify(result)+',','');
			$('#arr_listSong').html(res);
		});
	});







    })
</script>




<script>
    $(function () {
        $('#idScrllSongInAlbum').sortable();
    })

</script>

@endsection
