@extends('admin.layout.master')


{{-- page title --}}
@section('page_title')
Danh sách playlist
@endsection


{{-- style --}}
@section('style')
<style>
    .cke_contents {
        min-height: 100px !important;
    }

    .col-xs-12 {
        margin-top: 10px !important;
    }

    .pull-right {
        margin-left: 5px;
    }
</style>
@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning alert-dixsissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dixsiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="row">

    {{-- /////////////////// --}}
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <a href="{{route('admin.playlists.add')}}" class="btn btn-primary">Thêm playlist</a>
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                        data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>

            </div>
            <div class="box-body">
                <!--  content here -->

                <div class="col-xs-12 ">
                    <form class="form-action" id="form-action">
                        <div style="margin-right: 5px !important;" class="pull-left">
                            <select name="action" id="action" class="form-control input-sm action" field_signature="2473543864">
                                <option value="0">Tác vụ</option>
                                <option value="delete">Xoá</option>
                                <option value="agree">Duyệt</option>
                                <option value="agreeAll">Duyệt tất cả</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <button id="btn-action" class="btn btn-secondary btn-sm btn-flat btn-action">Áp
                                dụng</button>
                        </div>
                    </form>
                    <form action="" method="get">
                        <div class="search">
                            <div class="pull-right ">
                                <button class="btn btn-flat btn-primary" type="submit">Tìm</button>
                            </div>
                            <div class="pull-right ">
                                <input value="{{ !empty($_GET['search']) ? $_GET['search'] : '' }}" type="search"
                                    name="search" id="search" class="form-control search">
                            </div>
                        </div>


                    </form>
                </div>
                <br>
                <div class="col-xs-12">
                    <div class="table-responsive ">
                        <table class="table table-hover table-striped" id="table-songs">
                            <thead>
                                <tr>
                                    <th>
                                        <input id="checkAll" value="all" class="checkAll" type="checkbox" name="checkAll">
                                    </th>
                                    {{-- <th scope="col">ID</th> --}}
                                    <th scope="col">Tên playlist</th>
                                    <th scope="col">Người đăng</th>
                                    <th scope="col">Lượt nghe</th>
                                    <th scope="col">Trạng thái</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($playlists) || count($playlists) != 0)
                                @foreach ($playlists as $item)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="checkbox" value="{{  $item->id  }}" name="checkbox">
                                    </td>
                                    {{-- <td>{{ $item->id }}</td> --}}
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->customer_id == 0 ? 'admin' : getNameCustomer($item->customer_id)}}</td>
                                    <td>
                                        {{ (!empty(countListenPlaylistHelper($item->code)->listen)) ? countListenPlaylistHelper($item->code)->listen : 0 }}
                                    </td>
                                    <td>
                                    @if ($item->status==1)
                                        <span class="label label-success w-100">Kích hoạt</span>
                                    @else
                                        <span class="label label-danger w-100">Vô hiệu hoá</span>
                                    @endif
                                    </td>
                                    <td class="btn-group-xs">
                                        <a href="{{route('playlist',['name' => name_to_slug($item->name) ,'code' => $item->code])}}" target="_blank" data-toggle="modal"
                                            class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i></a>
                                        <a href="{{ route('admin.playlists.edit', $item->id) }}" class="btn btn-warning"><i
                                                class="fa fa-pencil"></i></a>
                                        <button data-remove="{{ $item->id }}" class="btn btn-danger btn-remove"><i
                                                class="fa fa-remove "></i></button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                Không có dữ liệu
                                @endif
                            </tbody>

                        </table>
                        <div class="pagination">
                            {{ $playlists->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here -->
        </div>
    </div>
</div>

<!-- end main content -->








<!----- javascript here -------->
@section('js')
<script>
    $(function () {
        $('body').on('click', '.btn-remove', function (e) {
            // alert('ok');

            var id = $(this).data('remove');
            // alert(removeUrl);
            swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn muốn xoá mục này không?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.playlists.delete')}}",
                            method: 'POST',
                            data: {
                                id: id,
                                _token: '{{csrf_token()}}'
                            }
                        }).done(result => {
                            if (result) {
                                swal("Deleted!",
                                    "Video đã bị xoá.",
                                    "success");
                            };
                            setTimeout(function () {
                                location.reload();
                            }, 1500);


                        });
                    }
                });
        });
    })

</script>
<script>
    $(function () {
        $('body').on('submit', '#form-action', function (e) {
            e.preventDefault();
            var action = $('body').find('#action').val();
            if (parseInt(action) != 0) {
                var data = [];
                $.each($("input[name='checkbox']:checked"), function () {
                    data.push($(this).val());
                });
                var formData = new FormData();
                formData.append('data', data);
                if (action == 'agreeAll') {
                    var c = confirm("Bạn có chắc chắn muốn duyệt các mục đã chọn?");
                    if (c === true) {
                        ajaxFunc("{{ route('admin.playlists.setActiveStatusAll') }}", formData);
                        swal({
                            title: "Yeee!",
                            text: "Duyệt thành công!",
                            icon: "success",
                            button: "OK",
                        }).then((value) => {
                            window.location.reload();

                        })

                    }
                    return false;
                }
                if (data.length != 0) {
                    if (action == 'delete') {
                        var c = confirm("Bạn có chắc chắn muốn xoá mục đã chọn?");
                        if (c === true) {
                            ajaxFunc("{{ route('admin.playlists.multiDel') }}", formData);
                            swal({
                                title: "Yeee!",
                                text: "Xoá thành công!",
                                icon: "success",
                                button: "OK",
                            }).then((value) => {
                                window.location.reload();
                            })

                        }
                    }
                    if (action == 'agree') {
                        var c = confirm("Bạn có chắc chắn muốn duyệt các mục đã chọn?");
                        if (c === true) {
                            ajaxFunc("{{ route('admin.playlists.setActiveStatus') }}", formData);
                            swal({
                                title: "Yeee!",
                                text: "Duyệt thành công!",
                                icon: "success",
                                button: "OK",
                            }).then((value) => {
                                window.location.reload();
                            })

                        }
                    }

                } else {
                    alert('Vui lòng chọn danh mục!')
                }

            }
        })
    })

</script>
<script>
    $('#table-songs').DataTable({
        'paging': false,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': false,
        'autoWidth': true,
        "columnDefs": [
            {
            "targets": 0,
            "orderable": false
            },
        ]
    })

</script>
@endsection
<!----- end javascript here -------->

@endsection
