@extends('admin.layout.master')

{{-- page title --}}
@section('page_title')
Home
@endsection


{{-- style --}}
@section('style')

@endsection


{{-- content --}}
@section('content')

{{-- alert --}}
@if (session('success'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif

@if (session('error'))
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible show" role="alert">
            <strong>Thông báo!</strong> {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
{{-- end alert --}}


<!-- main content -->
<div class="box box-danger">
    <div class="box-header">
        <h3 class="box-title">

        </h3>
        <div class="box-tools pull-right">
            {{-- <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                data-original-title="Collapse">
                <i class="fa fa-minus"></i></button> --}}
        </div>
    </div>
    <div class="box-body">
        <!--  content here -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>{{$total_traffic}}</h3>

                        <p>Lượt truy cập</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-universal-access"></i>
                    </div>
                    <a class="small-box-footer"><i class="fa fa-arrow-circle"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$total_download}}</h3>

                        <p>Lượt tải bài hát</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a class="small-box-footer"><i class="fa fa-arrow-circle"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                    <h3>{{$total_customer}}</h3>

                        <p>Thành viên</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                    <h3>{{$total_song}}</h3>

                        <p>Số bài hát</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-music"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$total_traffic}}</h3>

                        <p>Lượt truy cập</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-universal-access"></i>
                    </div>
                    <a class="small-box-footer"><i class="fa fa-arrow-circle"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$total_download}}</h3>

                        <p>Lượt tải bài hát</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a class="small-box-footer"><i class="fa fa-arrow-circle"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$total_customer}}</h3>

                        <p>Thành viên</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$total_song}}</h3>

                        <p>Số bài hát</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-music"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>


        <!-- end content here -->
    </div>
</div>
<!-- end main content -->


<!----- javascript here -------->
@section('js')


{{-- ckfinder --}}
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });
    console.log('a');
</script>

{{-- js --}}
@if ((old('image')))
<script>
    $('img#this-img').show();

</script>
@else
@if (!empty($product['image']))
<script>
    $('img#this-img').show();

</script>
@else
<script>
    $('img#this-img').hide();

</script>
@endif
@endif
<script>
    // function name(button id, input id, img id);
    chooseImg('choose_image', 'thumbnail', 'this-img');
</script>

@endsection
<!----- end javascript here -------->

@endsection
