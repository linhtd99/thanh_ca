<?php require_once('header.php'); ?>
<div class="home list-video-hh bxh">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_top_search" style="margin-bottom:10px;">
                    <ul class="search_control_select">
                        <li><a href="#" class="active" title="Bảng Xếp Hạng Việt Nam">Việt Nam</a></li>
                        <li><a href="#" class="" title="Bảng Xếp Hạng Âu Mỹ">Âu Mỹ</a></li>
                        <li><a href="#" class="" title="Bảng Xếp Hạng Hàn Quốc">Hàn Quốc</a></li>
                    </ul>
                </div>
                <div class="tile_box_key">
                    <h1 class="nomore" style="color: #2582be;">Bảng Xếp Hạng Bài Hát Việt Nam</h1>
                    <div class="btn_view_select">
                        <a href="#" class="active" title="Bảng Xếp Hạng Bài Hát Việt Nam">Bài Hát</a>
                        <span></span>
                        <a href="#" class="" title="Bảng Xếp Hạng Video Việt Nam">Video</a>
                    </div>
                </div>
                <div class="descriptionBXH">
                    Bảng Xếp Hạng NhacCuaTui cập nhật vào thứ Hai hàng tuần dựa trên số liệu thống kê thực tế trên desktop và mobile NhacCuaTui. Trong đó những trọng số quan trọng quyết định thứ hạng TOP 20 như sau: Nghe, Thích, Bình Luận, Chia sẻ, Tải v.v... Mỗi tương tác của người dùng đều tác động đến kết quả cuối cùng của BXH NhacCuaTui.
                </div>
                <div class="list_chart_page">
                    <div class="box_view_week">
                        <h2><strong>Tuần 30</strong> (22/07/2019 - 28/07/2019)</h2>
                        <a href="javascript:;" title="Chọn ngày" class="select_date" id="select_date"></a>
                        <a href="https://www.nhaccuatui.com/playlist/top-20-bai-hat-viet-nam-tuan-302019-va.ZcEXf3uuRWIL.html" class="active_play" title="Nghe Toàn Bộ"><span class="icon_playall"></span>Nghe Toàn Bộ</a>
                    </div>
                    <div class="box_resource_slide">
                        <ul class="list_show_chart">
                            <li>
                                <span class="chart_tw special-1">1</span>
                                <span class="chart_lw upchart">
                                    <span class="icon"></span>
                                    <p>1</p>
                                </span>
                                <div class="box_info_field">
                                    <a href="#" title="Từng Yêu - Phan Duy Anh"><img src="https://avatar-nct.nixcdn.com/song/2019/07/03/7/5/b/e/1562146897414.jpg" alt="tung yeu - phan duy anh" title="Từng Yêu - Phan Duy Anh" width="60px"></a>
                                    <h3 class="h3"><a href="#" class="name_song" title="Từng Yêu - Phan Duy Anh">Từng Yêu</a></h3>
                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Phan Duy Anh trình bày" target="_blank">Phan Duy Anh</a></h4>
                                </div>
                                <div class="box_info_more_week">
                                    <a href="javascript:;" style="cursor: default;" class="peak_position" title="Ví trí xếp hạng cao nhất"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="last_weeks_position" title="Vị trí trên BXH tuần trước"><span></span>2</a>
                                    <a href="javascript:;" style="cursor: default;" class="weeks_on_chart" title="Số tuần nằm trong BXH"><span></span>7</a>
                                </div>
                                <div class="box_song_action">
                                    <a href="#" id="btnShowBoxPlaylist_tnvcYCYt7lmv" class="button_add_playlist" title="Thêm bài hát Từng Yêu vào playlist yêu thích"></a>
                                    <a href="#" class="button_playing" title="Nghe bài hát Từng Yêu"></a>
                                </div>
                            </li>
                            <li>
                                <span class="chart_tw special-2">2</span>
                                <span class="chart_lw downchart">
                                    <span class="icon"></span>
                                    <p>1</p>
                                </span>
                                <div class="box_info_field">
                                    <a href="#" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg"><img src="https://avatar-nct.nixcdn.com/song/2019/07/03/7/5/b/e/1562137543919.jpg" alt="hay trao cho anh - son tung m-tp, snoop dogg" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg" width="60px"></a>
                                    <h3 class="h3"><a href="#" class="name_song" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg">Hãy Trao Cho Anh</a></h3>
                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Sơn Tùng M-TP trình bày" target="_blank">Sơn Tùng M-TP</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Snoop Dogg trình bày" target="_blank">Snoop Dogg</a></h4>
                                </div>
                                <div class="box_info_more_week">
                                    <a href="javascript:;" style="cursor: default;" class="peak_position" title="Ví trí xếp hạng cao nhất"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="last_weeks_position" title="Vị trí trên BXH tuần trước"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="weeks_on_chart" title="Số tuần nằm trong BXH"><span></span>4</a>
                                </div>
                                <div class="box_song_action">
                                    <a href="#" id="btnShowBoxPlaylist_vtEybe9NxLw7" class="button_add_playlist" title="Thêm bài hát Hãy Trao Cho Anh vào playlist yêu thích"></a>
                                    <a href="#" class="button_playing" title="Nghe bài hát Hãy Trao Cho Anh"></a>
                                </div>
                            </li>
                            <li>
                                <span class="chart_tw special-3">3</span>
                                <span class="chart_lw nonechart">
                                    <span class="icon"></span>
                                    <p></p>
                                </span>
                                <div class="box_info_field">
                                    <a href="#" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM"><img src="https://avatar-nct.nixcdn.com/song/2019/06/22/b/1/2/7/1561218264960.jpg" alt="cao oc 20 - b ray, dat g, masew, k-icm" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM" width="60px"></a>
                                    <h3 class="h3"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Cao Ốc 20</a></h3>
                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày" target="_blank">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày" target="_blank">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày" target="_blank">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày" target="_blank">K-ICM</a></h4>
                                </div>
                                <div class="box_info_more_week">
                                    <a href="javascript:;" style="cursor: default;" class="peak_position" title="Ví trí xếp hạng cao nhất"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="last_weeks_position" title="Vị trí trên BXH tuần trước"><span></span>3</a>
                                    <a href="javascript:;" style="cursor: default;" class="weeks_on_chart" title="Số tuần nằm trong BXH"><span></span>5</a>
                                </div>
                                <div class="box_song_action">
                                    <a href="javascript:;" id="btnShowBoxPlaylist_Wbl0lGylnp5A" class="button_add_playlist" title="Thêm bài hát Cao Ốc 20 vào playlist yêu thích"></a>
                                    <a href="#" class="button_playing" title="Nghe bài hát Cao Ốc 20"></a>
                                </div>
                            </li>
                            <li>
                                <span class="chart_tw special-4">4</span>
                                <span class="chart_lw upchart">
                                    <span class="icon"></span>
                                    <p>1</p>
                                </span>
                                <div class="box_info_field">
                                    <a href="#" title="Nhạt - Phan Mạnh Quỳnh"><img src="https://avatar-nct.nixcdn.com/song/2019/06/01/9/7/8/0/1559366762791.jpg" alt="nhat - phan manh quynh" title="Nhạt - Phan Mạnh Quỳnh" width="60px"></a>
                                    <h3 class="h3"><a href="#" class="name_song" title="Nhạt - Phan Mạnh Quỳnh">Nhạt</a></h3>
                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Phan Mạnh Quỳnh trình bày" target="_blank">Phan Mạnh Quỳnh</a></h4>
                                </div>
                                <div class="box_info_more_week">
                                    <a href="javascript:;" style="cursor: default;" class="peak_position" title="Ví trí xếp hạng cao nhất"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="last_weeks_position" title="Vị trí trên BXH tuần trước"><span></span>5</a>
                                    <a href="javascript:;" style="cursor: default;" class="weeks_on_chart" title="Số tuần nằm trong BXH"><span></span>8</a>
                                </div>
                                <div class="box_song_action">
                                    <a href="javascript:;" id="btnShowBoxPlaylist_K8zGqF0HBZmN" class="button_add_playlist" title="Thêm bài hát Nhạt vào playlist yêu thích"></a>
                                    <a href="#" class="button_playing" title="Nghe bài hát Nhạt"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
			<div class="col-md-3 col-xs-12 col-sm-12">
				<div class="sidebar">
                    <div class="box_topic_music">
                        <div class="tile_box_key">
                            <h3><a class="nomore" href="#" title="BXH Bài Hát">CHỦ ĐỀ HOT</a></h3>
                        </div>
                        <ul>
                            <li>
                                <a title="Indie" href="#"><img src="https://avatar-nct.nixcdn.com/topic/thumb/2018/12/12/e/2/f/a/1544584227345_org.jpg" alt="indie">
                                </a>
                            </li>

                            <li>
                                <a title="Song Ca" href="#"><img src="https://avatar-nct.nixcdn.com/topic/thumb/2018/11/19/9/1/5/9/1542616918934_org.jpg" alt="song ca">
                                </a>
                            </li>

                            <li>
                                <a title="Cà Phê" href="#"><img src="https://avatar-nct.nixcdn.com/topic/thumb/2018/11/19/9/1/5/9/1542607452664_org.jpg" alt="ca phe">
                                </a>
                            </li>

                            <li>
                                <a title="12 Cung Hoàng Đạo" href="#"><img src="https://avatar-nct.nixcdn.com/topic/thumb/2018/11/19/9/1/5/9/1542607392175_org.jpg" alt="12 cung hoang dao">
                                </a>
                            </li>

                            <li>
                                <a title="Nhạc Hot" href="#"><img src="https://avatar-nct.nixcdn.com/topic/thumb/2019/01/02/5/a/1/b/1546411715209_org.jpg" alt="nhac hot">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="list_singer_hot">
                        <div class="tile_box_key">
                            <h3><a title="Ca Sĩ | Nghệ Sĩ" href="#">Ca Sĩ | Nghệ Sĩ</a></h3>
                        </div><!--@end div tile_box_key-->
                        <ul>
                            <li><a href="#" class="img" title="Anna Trương"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2014/13/7E4149A9_1278_300.jpg" title="Anna Trương" alt="anna truong"></a><a href="#" class="name_singer_main" title="Anna Trương">Anna Trương</a></li>
                            <li><a href="#" class="img" title="MiA"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2017/07/24/b/e/c/2/1500881827006_300.jpg" title="MiA" alt="mia"></a><a href="#" class="name_singer_main" title="MiA">MiA</a></li>
                            <li><a href="#" class="img" title="Hạnh Sino"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2017/11/16/9/f/f/b/1510803805105_300.jpg" title="Hạnh Sino" alt="hanh sino"></a><a href="https://www.nhaccuatui.com/nghe-si-hanh-sino.html" class="name_singer_main" title="Hạnh Sino">Hạnh Sino</a></li>
                            <li><a href="#" class="img" title="Emily"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2019/06/27/5/9/5/2/1561599944824_300.jpg" title="Emily" alt="emily"></a><a href="#" class="name_singer_main" title="Emily">Emily</a></li>
                            <li><a href="#" class="img" title="ERIK"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2018/12/17/e/1/c/8/1545030250455_300.jpg"title="ERIK" alt="erik"></a><a href="#" class="name_singer_main" title="ERIK">ERIK</a></li>
                        </ul>
                    </div>
					<div class="top100_img"><img src="https://stc-id.nixcdn.com/v11/images/flower.png" class="new"></div>
					<div class="top100">
					    <a href="#" title="Top 100 bài hát">
					        <div class="top100_head">
					            <span class="logo"></span>
					            <div class="padd">
					                <div class="box_title">
					                    <p class="top100_title">Top 100 bài hát</p>
					                </div>
					            </div>
					        </div>
					    </a>
					    <div class="box_content">
					        <ul>
					            <li>
					                <a href="#">
					                    <div class="box_action">
					                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113323981.jpg" alt="" title="">
					                        <div class="bg_black"></div>
					                        <span class="icon_play"></span>
					                    </div>
					                </a>
					                <a title="Top 100 Nhạc Trữ Tình Hay Nhất" href="#"><p>Top 100 Nhạc Trữ Tình Hay Nhất</p></a>
					            </li>
					            <li>
					                <a href="#">
					                    <div class="box_action">
					                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113648430.jpg" alt="" title="">
					                        <div class="bg_black"></div>
					                        <span class="icon_play"></span>
					                    </div>
					                </a>
					                <a title="Top 100 Nhạc Country Hay Nhất" href="#"><p>Top 100 Nhạc Country Hay Nhất</p></a>
					            </li>
					            <li>
					                <a href="#">
					                    <div class="box_action">
					                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113537769.jpg" alt="" title="">
					                        <div class="bg_black"></div>
					                        <span class="icon_play"></span>
					                    </div>
					                </a>
					                <a title="Top 100 Electronica/Dance Hay Nhất" href="#"><p>Top 100 Electronica/Dance Hay Nhất</p></a>
					            </li>
					            <li>
					                <a href="#">
					                    <div class="box_action">
					                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113507770.jpg" alt="" title="">
					                        <div class="bg_black"></div>
					                        <span class="icon_play"></span>
					                    </div>
					                </a>
					                <a title="Top 100 Remix Việt Hay Nhất" href="#"><p>Top 100 Remix Việt Hay Nhất</p></a>
					            </li>
					            <li>
					                <a href="#">
					                    <div class="box_action">
					                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113690153.jpg" alt="" title="">
					                        <div class="bg_black"></div>
					                        <span class="icon_play"></span>
					                    </div>
					                </a>
					                <a title="Top 100 Blues/Jazz Hay Nhất" href="#"><p>Top 100 Blues/Jazz Hay Nhất</p></a>
					            </li>
					        </ul>
					        <div class="all">
					            <a href="#">Xem tất cả TOP 100</a>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>



