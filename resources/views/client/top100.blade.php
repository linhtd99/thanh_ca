@extends('client.layout.layout_trinh-nghe-nhac')

@section('page_title')
Top 100 bài hát {{ $category->name }}
@endsection
<style>
 .detail_menu_browsing_dashboard li {
     width: auto !important;
 }
</style>
@section('content')


<div class="home qh-top-list">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="tile_box_key">
                    <h1><a title="Top 100 {{ $parentCategory->name }}" href="javascript:;" class="nomore"
                            style="cursor: default;">TOP 100 {{ $parentCategory->name }}</a></h1>
                </div>
                <div class="descriptionBXH">
                    TOP 100 là danh sách 100 bài hát hot nhất thuộc các thể loại nhạc được nghe nhiều nhất trên
                    {{$dataGenerals->name}}. Danh sách bài hát này được hệ thống tự động đề xuất dựa trên lượt nghe, lượt share, lượt
                    comment v.v của từng bài hát trên tất cả các nền tảng (Web, Mobile web, App). Top 100 sẽ được cập
                    nhật hằng tuần dựa trên các chỉ số có được từ tuần đó. <br> <br>
                    <span>* TOP 100 vừa được cập nhật vào: <b>00:00
                            {{ date('d/m/Y', strtotime($arr_date['end'])) }}</b></span>
                </div>

                <div class="box_cata_control" style="margin-top: 10px;">
                    <ul class="detail_menu_browsing_dashboard">
                        @forelse ($childCategories as $childCate)
                        <li style="width: 130px;">
                            <a href="{{ route('top100_song', ['slug_category' => name_to_slug($childCate->name)]) }}" rel="dofollow" class="{{ Request::segment(3) == name_to_slug($childCate->name) ? 'active' : '' }}"
                                title="{{ $childCate->name }}">{{ $childCate->name }}</a>
                        </li>
                        @empty
                        @endforelse
                    </ul>

                </div>
                <div class="list_chart_page">
                    <div class="box_view_week" style="background: #e74c3c;">
                        <h2>100 ca khúc
                            <b>{{ $category->name }}</b>
                            hay nhất trên NhacCuaTui</h2>
                        <a style="display: none" href="javascript:;" id="sortBtn" title="Đảo chiều danh sách"> <span
                                class="top100_btn_sort"></span></a>
                        <a target="_blank" href="{{ route('playlist_top100_song', ['slug_category' => name_to_slug($category->name)]) }}" class="active_play" title="Nghe toàn bộ"><span
                                class="icon_playall"></span>Nghe toàn bộ</a>
                    </div>
                    <div class="showNotiWishList"></div>
                    <div class="box_resource_slide">
                        <ul class="list_show_chart">
                            @php
                                $count = 0;
                            @endphp
                            @forelse ($charts as $chart)
                            @php
                                $count++;
                            @endphp
                            <li>
                                <span class="chart_tw special-{{ $count }}">{{ $count }}</span>
                                <div class="box_info_field">
                                    <a href="{{ route('song', ['name' => name_to_slug($chart->name), 'code' => $chart->code]) }}" title="{{ $chart->name }}"><img
                                            src="{{ !empty($chart->image) ? $chart->image : 'https://i.imgur.com/QHJNedd.png' }}"
                                            alt="{{ $chart->name }}" title="{{ $chart->name }}"
                                            width="60px"></a>
                                    <h3 class="h3"><a href="{{ route('song', ['name' => name_to_slug($chart->name), 'code' => $chart->code]) }}" class="name_song" title="{{ $chart->name }}">
                                        {{ $chart->name }}
                                    </a></h3>


                                    <h4 style="display:none" class="list_name_singer" style="color: #a2a2a2"> <a  class="name_singer"
                                            title="Tìm các bài hát, playlist, mv do ca sĩ Phan Duy Anh trình bày"
                                            target="_blank">Phan Duy Anh</a></h4>
                                </div>

                                <div class="box_song_action">
                                    <a onclick="addWishList('{{$chart->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')" id="btnShowBoxPlaylist_tnvcYCYt7lmv"
                                        class="button_add_playlist"
                                        title="Thêm bài hát Từng Yêu vào playlist yêu thích"></a>
                                    <a href="{{ route('song', ['name' => name_to_slug($chart->name), 'code' => $chart->code]) }}" class="button_playing" title="Nghe bài hát Từng Yêu"></a>
                                </div>
                            </li>
                            @empty

                            @endforelse


                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="sidebar">
                    @forelse ($otherParentCategories as $otherPCate)
                    @php
                        $this_child_cate  = $otherPCate->getChildCategory();
                    @endphp
                    @if (count($this_child_cate) > 0)
                    <div class="box_prospect_singer">
                        <div class="tile_box_key">
                            <h3 class="uppercase text-uppercase"><a href="#" title="TOP 100 {{ $otherPCate->name }}">{{ $otherPCate->name }}</a></h3>
                        </div>
                        <div class="list_singer_music">
                            <ul>
                                @forelse ($this_child_cate as $childCate)
                                <li>
                                    <div class="info_data">
                                        <a href="{{ route('top100_song', ['slug_category' => name_to_slug($childCate->name)]) }}" title="Top 100  {{ $childCate->name }} Hay Nhất" class="img">
                                            <img
                                                src="https://i.imgur.com/WS5EL09.png">
                                        </a>
                                        <a href="{{ route('top100_song', ['slug_category' => name_to_slug($childCate->name)]) }}" title="Top 100  {{ $childCate->name }} Hay Nhất" class="name_song"
                                            style="margin-top: 15px;">Top 100 {{ $childCate->name }} Hay Nhất</a>
                                    </div>
                                </li>
                                @empty

                                @endforelse
                            </ul>
                        </div>

                    </div>

                    @endif
                    @empty
                    @endforelse

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('js')
<script src="https://cdn.plyr.io/3.5.6/plyr.js"></script>
<script src="{{asset('client/js/rabbit-lyrics.js')}}" type="text/javascript"></script>
<script src="https://www.youtube.com/iframe_api"></script>


@endsection
