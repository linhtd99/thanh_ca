@include('client.layout.header')

@yield('content')

@include('client.layout.footer')

@yield('js')
@if (!empty(session('alert_comment')))
    <script>
        alert(`{{session('alert_comment')}}`);
    </script>
@endif
