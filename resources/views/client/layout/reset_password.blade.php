@include('client.layout.header')
<div class="container">
    <div style="width:50%;margin:auto; padding:50px 0px 50px 0px">
        <div class="modal-content">
                    
            <div class="modal-header">
  
                <h4 class="modal-title">Nhập Mật Khẩu Mới</h4>
            </div>
            <div class="modal-body">
        
               
                <div class="row">
        
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" id="change_forgetPassword">
                            @csrf
                            <div>
                                <div class="form-group">
                                    <input type="text" name="token" class="form-control hidden" placeholder="Mã Token">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Mật khẩu Mới">
                                    
                                </div>
                                <div class="form-group">
                                    <input type="password" name="cf_password" class="form-control" placeholder="Xác nhận mật khẩu">
                                    <span style="color:red" class="err_cfpassword"></span>
                                </div> 
                                @if(session('success'))
                                <div class="alert alert-success">
                                    <h4>{{session('success')}}</h4>
                                </div>
                                @endif
                            <div class="form-group">
                                {{-- <a href="#">Quên mật khẩu?</a> --}}
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    </div>
</div>
@yield('content')

@include('client.layout.footer')

@yield('js')

