
@php
    $artistTrending  = Sidebar::artistTrending();
@endphp
<div class="singer-trending">
            <div id="sync3" class="owl-carousel owl-theme">

            @foreach ($artistTrending as $item)
                <div class="item">
                    <div class="trend_item">
                        <a id="atHref2" class="trend_link" href="#">
                            <img id="atBgImg" class="trend_thumb"
                                src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}" alt="">
                        </a>
                        <div class="trend_info">
                            <p class="trend_top_week">Top Nghệ Sĩ Trending Trong Tuần</p>
                            <a id="atHref" class="trend_name_singer" href="#">
                            <p id="atNameSinger" class="trend_name_singer_title">{{$item->name}}</p>
                            </a>
                        </div>
                    </div>

                </div>
            @endforeach
            </div>

            <div id="sync4" class="owl-carousel owl-theme">
                @foreach ($artistTrending as $key => $item)
                    <div class="item">
                    <img id="atImage{{$key}}" class="trend_img_dot"
                            src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}"
                            alt="">
                    </div>
               @endforeach
            </div>
        </div>
