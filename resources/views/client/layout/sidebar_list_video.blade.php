@php
$hotTopics = Sidebar::getTopics();
$artistRandom5 = Sidebar::randomArtists();

@endphp
<div class="col-md-3 col-xs-12 col-sm-12">
    <div class="sidebar">

        @include('client.layout.singer-trending')
        @include('client.layout.song_chart')
        @include('client.layout.singer_hot')


        @include('client.layout.w_top100')
        {{-- ****************** --}}
        {{ Sidebar::top100New() }}
    </div>
</div>
