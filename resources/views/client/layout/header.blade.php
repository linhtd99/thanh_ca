@php
$bxh = json_decode(Options::_get('choose_category_bxh'), true);
$dataGenerals = Sidebar::dataGenerals();
$dataFooter = Sidebar::dataFooter();
$top_keyword_search = Sidebar::top_keyword_search();

tracker_hit();

@endphp

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>@yield('page_title')</title>
        <link rel="stylesheet" href="{{asset('client/css/plyr.css')}}">
        {{-- <link rel="stylesheet" href="https://cdn.plyr.io/3.5.6/plyr.css" /> --}}
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=vietnamese"
            rel="stylesheet">
        <link
            href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900&display=swap&subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{asset('client/css/bootstrap.min.css')}}">

        <link rel="stylesheet" href="{{asset('client/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('client/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('client/css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('client/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('client/css/player.css')}}">
        <style>
            audio {
                display: none;
            }

        </style>
    </head>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=674553619717239&autoLogAppEvents=1">
    </script>
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>

    <body>

        <header>
            <div class="container">
                <div class="header-logo">
                    <div class="logo">
                        <a href="{{route('/')}}">
                            <img src="{{!empty($dataGenerals->logo) ? $dataGenerals->logo : '' }}" alt="">
                        </a>
                    </div>
                    <div class="search-header">
                        <form action="{{route('search')}}" method="get" autocomplete="off">
                            <input type="text" name="q" class="form-control" placeholder="Từ khóa tìm kiếm..."
                                id="search">
                            <button type="submit" class="btn"><img src="{{asset('client/img/search-btn.png')}}"
                                    alt=""></button>
                        </form>

                        <div class="w_suggestion">
                            <div class="slimScroll">
                                <div id="ScrllSuggestion">
                                    <ul class="content_search">
                                        <li id="TopKeyWordFirst">
                                            <h3 class="title_row">Top từ khóa tìm kiếm nhiều nhất</h3>
                                            <ul id="ulTopKeyWord" class="info-search">
                                                @if (!empty($top_keyword_search) && count($top_keyword_search) > 0)
                                                @foreach ($top_keyword_search as $key => $item)
                                                <li id="li{{$key}}" class="top-search" rel="{{$key}}">
                                                    <a class="tkw" href="{{route('search',['q' => $item->keyword])}}">
                                                        <span class="top-number top-number-{{$key+1}}">{{$key+1}}</span>
                                                        <span>{{$item->keyword}}</span>
                                                    </a>
                                                </li>
                                                @endforeach
                                                @endif
                                            </ul>
                                        </li>
                                        <li id="li_history">

                                            <h3 class="title_row">Lịch sử tìm kiếm của bạn
                                                <a class="icon-delete-search" href="#">Xóa lịch sử</a>
                                            </h3>
                                            <ul class="info-search">

                                                @if (!Auth::guard('customers')->check())
                                                <li class="top-search ">
                                                    <a class="tkw" href="#">
                                                        <span
                                                            style="font-style: italic; color: #a09e9e;text-transform: none;">Chưa
                                                            đăng nhập !!!</span>
                                                    </a>
                                                </li>
                                                @else
                                                @if (count(getAuth(Auth::guard('customers')->user()->id)) > 0)
                                                @foreach (getAuth(Auth::guard('customers')->user()->id) as $item1)
                                                <li class="top-search ">
                                                    <a class="tkw" href="{{route('search',['q' => $item1->keyword])}}">
                                                        <span>{{$item1->keyword}}</span>
                                                    </a>
                                                    <a id="{{$item1->id}}" class="btn-delete-search"></a>
                                                </li>
                                                @endforeach
                                                @else
                                                <li class="top-search ">
                                                    <a class="tkw" href="#">
                                                        <span
                                                            style="font-style: italic; color: #a09e9e;text-transform: none;">Chưa
                                                            có lịch sử !!!</span>
                                                    </a>
                                                </li>
                                                @endif


                                                @endif


                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end-seach -->
                    </div>
                    <div class="search-option">
                        <ul>
                            <li><a href="{{ route('indexSong') }}">Nhạc</a></li>
                            <li>|</li>
                            <li><a href="{{route('indexVideo')}}">Video</a></li>
                            <li>|</li>
                            <li><a href="{{route('posts')}}">Tin tức</a></li>
                        </ul>
                    </div>
                    <div class="user-menu-box">


                        @if (!Auth::guard('customers')->check())
                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#myModal-login">Đăng nhập</a></li>
                            <li>/</li>
                            <li><a href="#" data-toggle="modal" data-target="#myModal">Đăng kí</a></li>
                        </ul>
                        @else
                        <ul class="user-phone">
                            <li>
                                <a href="#">
                                    <img src="{{!empty(Auth::guard('customers')->user()->avatar) ? Auth::guard('customers')->user()->avatar : asset('client/img/default-avatar.png')}}"
                                        alt="">
                                    @if (Auth::guard('customers')->user()->name == null)
                                    {{Auth::guard('customers')->user()->username}}
                                    @else
                                    {{Auth::guard('customers')->user()->name}}
                                    @endif
                                    {{-- <i class="fa fa-caret-down" aria-hidden="true"></i> --}}
                                </a>

                                <ul class="submenu-user">
                                    <li><a
                                            href="{{url('user/'.Auth::guard('customers')->user()->username.'/trang-ca-nhan')}}"><i
                                                class="icons-user ic-profile"></i>Trang cá nhân</a></li>
                                    <li><a
                                            href="{{url('user/'.Auth::guard('customers')->user()->username.'/quan-ly')}}"><i
                                                class="icons-user ic-yeuthich"></i>Tài khoản</a></li>
                                    {{-- <li><a href="#"><i class="icons-user ic-mmusic"></i> Playlist của tôi</a></li> --}}
                                    <li><a
                                            href="{{url('user/'.Auth::guard('customers')->user()->username.'/lich-su')}}"><i
                                                class="icons-user ic-recent"></i>Lịch sử</a></li>
                                    <li><a onclick="return confirm('Bạn có muốn đăng xuất?');"
                                            href="{{route('logoutCustomer')}}"><i
                                                class="icons-user ic-out"></i>Thoát</a>
                                    </li>
                                </ul>
                                <script>

                                </script>
                            </li>
                        </ul>
                        @endif


                    </div>
                </div>
                <div class="menu-site">
                    <button class="btn btn-show-menu hidden-md hidden-lg"><i class="fa fa-bars"></i></button>
                    <div class="menu-box">
                        <div class="bg-menu hidden-md hidden-lg"></div>
                        <ul class="main-menu" style="display:inline-block">
                            <li class="current-menu-item"><a href="{{route('indexSong')}}">Bài hát</a></li>
                            <li><a href="{{ route('topic') }}">Chủ đề</a></li>

                            <li class="menu-item-has-children">
                                <a href="#">BXH</a>

                                <ul class="sub-menu" style="width: 480px;">
                                    @if (!empty($bxh) && count($bxh) > 0)
                                    @foreach ($bxh as $item)
                                    @php
                                    $category = get_cat_by_id($item);
                                    @endphp
                                    @if (!empty($category->name))
                                    <li class="root"><a>{{ $category->name }}</a>
                                        <ul class="sub_nav2">
                                            <li class=""><a title="Bảng xếp hạng Bài hát {{ $category->name }}"
                                                    href="{{ route('bxh_song', ['slug_category' => name_to_slug($category->name), 'week' => date('W') -1 , 'year' => date('Y')]) }}">Bài
                                                    hát</a></li>

                                            <li><a title="Bảng xếp hạng Video {{ $category->name }}"
                                                    href="{{ route('bxh_video', ['slug_category' => name_to_slug($category->name), 'week' => date('W') -1 , 'year' => date('Y')]) }}">Video</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endif

                                    @endforeach
                                    @endif


                                </ul>
                            </li>
                            <li>

                                <a href="{{route('indexPlaylist')}}">Playlist</a>
                            </li>
                            <li><a href="{{route('indexVideo')}}">Music video</a></li>
                            <li><a href="{{route('indexArtist')}}">Nghệ sĩ</a></li>
                            <li><a href="{{route('indexPlaylist')}}"><img src="{{asset('client/img/gift-2.png')}}"
                                        alt="">Playlist của bạn</a></li>
                        </ul>
                        <button class="btn btn-hide-menu hidden-md hidden-lg"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            </div>

        </header><!-- /header -->
