<style>
    .hideElm {
        display: none !important;
    }

</style>

<div id='player'>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="mainwrap">
                    <div id="nowPlay">
                        <span id="npTitle"></span>
                    </div>
                    <div id="audiowrap">
                        <div id="audio0">
                            <div class="next-track">
                                <a id="btnPrev"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                                <a id="btnNext"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
                            </div>
                            <a class="shuffle-hh"><i class="fa fa-random" aria-hidden="true"></i></a>
                            <a class="z-expand-btn "><i class="fa fa-window-restore" title="Chế độ thu nhỏ"></i>
                            </a>
                            <a style="cursor:pointer" class="z-like-btn liked this-z-like-btn" title="Thích"><i
                                    style="cursor:pointer" class="fa fa-heart-o fa-heart-current"
                                    aria-hidden="true"></i></a>
                            <a class="z-mini-media-btn"><i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>

                            <audio style="display: none !important" id="audio1" autoplay preload controls>Your browser does not
                                support HTML5 Audio! 😢
                            </audio>
                            <div class="z-playlist-wraper">
                                <a class="z-btn-playlist-expand ">
                                    <i class="fa fa-list-alt"></i>
                                    <span class="z-playlist-title">
                                        <span class="z-content" style="margin-right: 2px;">Danh sách phát</span>
                                        <span class="z-item-total">(1)</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="plwrap">
    <div class="container">
        <div class="box-list">
            <button type="button" class="btn-down"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <div class="left-list">
                        <div class="z-head">
                            <div class="z-show z-input-group z-miniplayer-search-groups">
                                <label>
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    <div class="input-wrapper">
                                        <input type="text" name="search_player" id="search_player"
                                            class="form-control search_player" placeholder="Tìm kiếm..." value="">
                                    </div>
                                </label>
                                <span class="qh-close-input">Đóng</span>
                            </div>

                            <div class="z-show zz-show">
                                <label class="z-checkbox">
                                    <input type="checkbox" class="check-all hide">
                                    <span class="z-checkmark"></span>
                                    <span class="z-checkbox-title">DANH SÁCH PHÁT (1)</span>
                                </label>
                                <div class="z-btn-features">
                                    <div class="list-buttons">
                                        <ul class="hover-view ">
                                            <li>
                                                <a class="z-delete hide " title="Xóa">
                                                    <i class="btn-z-delete fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="shuffle-hh" title="Nghe ngẫu nhiên">
                                                    <i class="fa fa-random" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="z-btn-repeat " title="Lặp lại">
                                                    <i class="fa fa-retweet" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" class="z-btn-search" title="Tìm kiếm">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="z-body-area">

                            <ul id="plList" class="nav flex-column"></ul>
                        </div>
                        <div class="z-head z-head-2">
                            <div class="z-show z-show-2">
                                <label class="z-checkbox z-checkbox-2">
                                    <span class="z-checkbox-title-2">Gợi ý</span>
                                    <a class="z-btn-random">
                                        <i class="fa fa-retweet fa-random" aria-hidden="true"></i>
                                    </a>
                                </label>
                                <label style="display: none" class="z-checkbox z-checkbox-3">
                                    <span class="z-checkbox-title-2">Tìm kiếm</span>
                                </label>
                                <div class="z-btn-features">
                                    <div class="list-buttons">
                                        <ul class="hover-view">
                                            <li>
                                                <div class="btn-switch">
                                                    <span class="text">TỰ ĐỘNG PHÁT</span>
                                                    <label class="switch">
                                                        <input checked id="switch-auto_play" name="switch-auto_play"
                                                            type="checkbox">
                                                        <span class="slider round">

                                                        </span>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="z-body-area">
                            <ul id="plListSearch" style="display: none" class="nav flex-column"></ul>
                            <ul id="plListS" class="nav flex-column"></ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="right-list">
                        <div id="mini__player">
                            <img src="{{ asset('client/img/song-default.png') }}" alt="">
                            <video id="vid-big" muted></video>
                            <i class="toggle-icon icon"></i>
                        </div>
                        <div class="false z-song-lyric-wrap">
                            <div class="z-song-name">
                                <a role="button" class="" title="Người Lạ Ơi" href="#"></a>
                            </div>
                            <div class="z-artists">
                                <a role="button" class="" title="MIN" href="#"></a>
                            </div>
                            {{-- <div class="z-album">Album:&nbsp;
<a role="button" class="" href="#"></a>
</div> --}}
                        </div>
                        <div class="z-lyric-text-wrap">
                            <div id="lyrics1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="lyrics-box">
    <div class="z-blur-bg"
        style="background-image: url('https://photo-resize-zmp3.zadn.vn/w240_r1x1_jpeg/cover/c/a/9/d/ca9d4b6cd6252d7a364c74afabf06958.jpg');">
    </div>
    <div id="lyrics">
    </div>
    <video id="vid" muted></video>
    <i class="toggle-icon icon"></i>
</div>

<!-- share Modal -->
<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shareModalLabel">Chia sẻ bài hát</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control input-share" id="input-share" value="" readonly>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>
