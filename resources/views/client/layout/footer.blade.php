
@php
    $dataGenerals = Sidebar::dataGenerals();
$dataFooter = Sidebar::dataFooter();
$top_keyword_search = Sidebar::top_keyword_search();
@endphp
<footer>
    <div class="container">
        <ul class="ul-ft">
            {{-- {{dd($dataFooter)}} --}}
            @if (!empty($dataFooter[0]))
                @foreach ($dataFooter[0] as $item)
                    <li>
                        <ul class="sub_ft">
                            <li class="top_li">{{$item->title}}</li>
                           @php
                               echo $item->content
                           @endphp
                        </ul>
                    </li>
                @endforeach
            @endif

            @if (!empty($dataFooter[1]))
                <li>
                    <ul class="sub_ft">
                    <li class="top_li">{{$dataFooter[1]->title}}</li>
                        <li class="new-sub-li"><a href="{{$dataFooter[1]->content}}" target="_blank"><img src="{{asset('client/img/icon-fb-bottom.png')}}"
                                    alt="icon fb"></a></li>
                    </ul>
                </li>
            @endif

            @if (!empty($dataFooter[2]))
                <li>
                    <ul class="sub_ft">
                        <li class="top_li">{{$dataFooter[2]->title}}</li>
                        <li>Dịch vụ nhac.vn đã có ứng dụng cho</li>
                        <li><span class="color-active">Mobile</span>, <span class="color-active">Smart TV</span></li>
                        <li class="new-sub-li-1">
                            <img src="{{asset('client/img/icon-code-bottom.png')}}" alt="icon code">
                        </li>
                        <li class="new-sub-li-1"><a href="{{$dataFooter[2]->link_down1}}"><button type="button" class="btn-down-app-bottom"><img
                                        src="{{asset('client/img/btn-down-iphone.png')}}" alt="download iphone"> Tải cho iphone</button></a>
                        </li>
                        <li class="new-sub-li-1"><a href="{{$dataFooter[2]->link_down2}}"><button type="button" class="btn-down-app-bottom"><img
                                        src="{{asset('client/img/btn-down-android.png')}}" alt="download android"> Tải cho Android</button></a>
                        </li>
                    </ul>
                </li>
            @endif
            <li>
                <ul class="sub_ft">
                    <li class="top_li">Từ khoá nổi bật</li>
                    <li>
                        @if (!empty($top_keyword_search) && count($top_keyword_search) > 0)
                        @foreach ($top_keyword_search as $key => $item)
                            <a href="{{route('search',['q' => $item->keyword])}}">{{$item->keyword}}</a>
                            @if ($key+1 < count($top_keyword_search))
                                ,
                            @endif
                        @endforeach

                        @endif
                </ul>
            </li>
        </ul>
        <div class="border"></div>
        <ul class="ft-content-bottom">
            <li>
            <a href="{{route('/')}}" class="logo_ft">
                    <img src="{{!empty($dataGenerals->logo) ? $dataGenerals->logo : '' }}" alt="Nhac.vn">
                </a>
            </li>
            <li class="term">
                @if (!empty($dataFooter[3]))
                   @php
                       echo $dataFooter[3]
                   @endphp
                @endif
            </li>
            <li class="bct-logo">
                <a href="#">
                    <img width="120" src="https://109fdaeb3.vws.vegacdn.vn/web/images/dathongbao.png" alt="dathongbao">
                </a>
            </li>
            <li class="dmca">
                <a href="#" class="dmca-badge"> <img
                        src="//images.dmca.com/Badges/dmca_protected_sml_120h.png?ID=a4f07de7-4e1d-461f-8844-a23d66fc02cb"
                        alt="DMCA.com Protection Status"></a>
            </li>
        </ul>
    </div>

</footer>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Đăng ký</h4>
            </div>
            <div class="modal-body modal-body-register">

                <h3>Đăng nhập một lần truy cập tất cả các dịch vụ của Thanhca.tv</h3>
                <p>Tạo tài khoản Thanhca.tv ID của bạn</p>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul>
                        <li><a href="{{ url('login/facebook') }}" class="fb-login"><i class="fa fa-facebook" aria-hidden="true"></i> Đăng nhập
                                    bằng Facebook</a></li>
                            <li><a href="{{ url('login/google') }}" class="gg-login"><i class="fa fa-google-plus" aria-hidden="true"></i> Đăng
                                    nhập bằng Google +</a></li>
                        </ul>
                        <p>Bằng cách nhấn vào nút đăng ký đồng nghĩa với bạn đã đồng ý với <a href="#"> điều khoản sử
                                dụng</a> của Thanhca.tv</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <form id="registerCustomer">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Tên tài khoản">
                                <span style="color:red" class="err_username"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email">
                                <span style="color:red" class="err_email"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                                <span style="color:red" class="err_password"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" name="cf_password" class="form-control"
                                    placeholder="Nhập lại mật khẩu">
                                <span style="color:red" class="err_cf_password"></span>
                            </div>
                            <div class="form-group">
                                <input placeholder="Ngày sinh" class="textbox-n form-control" name="birthday" type="text" onfocus="(this.type='date')" id="date">
                                <span style="color:red" class="err_date"></span>
                            </div>
                            <div class="form-group">
                                <select name="sex" id="sex" class="form-control">
                                    <option value="" disabled selected>Chọn giới tính</option>
                                    <option value="0">Nam</option>
                                    <option value="1">Nữ</option>
                                    <option value="-1">Khác</option>
                                </select>
                                <span style="color:red" class="err_sex"></span>
                            </div>
                            {{-- <div class="form-group">
                                <script src="https://www.google.com/recaptcha/api.js"></script>

                                <div class="g-recaptcha" data-sitekey="6LdNvQwUAAAAAJVLa1JyPsgk3j2XtY64upvzlopf">
                                    <div style="width: 304px; height: 78px;">
                                        <div>
                                            <iframe
                                                src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LdNvQwUAAAAAJVLa1JyPsgk3j2XtY64upvzlopf&amp;co=aHR0cHM6Ly92ZWdhaWQudm46NDQz&amp;hl=vi&amp;v=v1565591531251&amp;size=normal&amp;cb=c3kbsgxg6d7c"
                                                width="304" height="78" role="presentation" name="a-hb1jxusrm10w"
                                                frameborder="0" scrolling="no"
                                                sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
                                        </div><textarea id="g-recaptcha-response" name="g-recaptcha-response"
                                            class="g-recaptcha-response"
                                            style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
                                    </div>
                                </div>
                                <style>
                                    .gg-captchar-mid div.g-recaptcha div {
                                        margin: 0 auto;

                                    }

                                    @media (max-width: 479px) {
                                        .g-recaptcha {
                                            transform: scale(0.83);
                                            -webkit-transform: scale(0.83);
                                            transform-origin: 0 0;
                                            -webkit-transform-origin: 0 0;
                                        }

                                    }
                                </style>
                            </div> --}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Đăng ký</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="myModal-login" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Đăng nhập</h4>
            </div>
            <div class="modal-body">
                <h3>Đăng nhập một lần truy cập tất cả các dịch vụ của Thanhca.tv</h3>
                <p>Đăng nhập tài khoản Thanhca.tv ID của bạn</p>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul>
                            <li><a href="{{ url('login/facebook') }}" class="fb-login"><i class="fa fa-facebook" aria-hidden="true"></i> Đăng nhập
                                    bằng Facebook</a></li>
                            <li><a href="{{ url('login/google') }}" class="gg-login"><i class="fa fa-google-plus" aria-hidden="true"></i> Đăng
                                    nhập bằng Google +</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <form id="loginCustomer">
                            <span style="color:red" class="err_"></span>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control"
                                    placeholder="Tên đăng nhập/Email">
                                <span style="color:red" class="err_username"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                                <span style="color:red" class="err_password"></span>
                            </div>
                            <div class="form-group">
                                <a href="{{ route('password') }}" style="cursor: pointer" class="open_modelForget">Quên mật khẩu?</a>
                                <button type="submit" class="btn btn-primary">Đăng nhập</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="myModal-fogetPassword" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">QUÊN MẬT KHẨU</h4>
            </div>
            <div class="modal-body">
                {{-- <h3>Đăng nhập một lần truy cập tất cả các dịch vụ của Thanhca.tv</h3>
                <p>Đăng nhập tài khoản Thanhca.tv ID của bạn</p> --}}
                <div class="row">
                    {{-- <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul>
                            <li><a href="#" class="fb-login"><i class="fa fa-facebook" aria-hidden="true"></i> Đăng nhập
                                    bằng Facebook</a></li>
                            <li><a href="#" class="gg-login"><i class="fa fa-google-plus" aria-hidden="true"></i> Đăng
                                    nhập bằng Google +</a></li>
                        </ul>
                    </div> --}}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form id="forget_password">
                            <span style="color:red" class="err_"></span>

                            <div class="form-group">
                                <label>Nhập email</label>
                                <input type="type" name="email" class="form-control" placeholder="Email">
                                <span style="color:red" class="err_email"></span>
                            </div>
                            {{-- <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                                <span style="color:red" class="err_password"></span>
                            </div> --}}
                            <div class="form-group">
                                {{-- <a href="#">Quên mật khẩu?</a> --}}
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="myModal-cf_fogetPassword" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thay đổi mật khẩu</h4>
            </div>
            <div class="modal-body">
                <h5 style="color:#ff6b6b">Một email đã được gửi đến hòm thư của bạn.Vui lòng kiểm tra email !!!</h5>
                {{-- <p>Đăng nhập tài khoản Thanhca.tv ID của bạn</p>  --}}
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form id="change_forgetPassword">
                            <span style="color:red" class="err_"></span>
                            <input type="hidden" name="email" class="email">
                            <div class="form-group">
                                <input type="text" name="token" class="form-control" placeholder="Mã xác thực">
                                <span style="color:red" class="err_token"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                                <span style="color:red" class="err_password"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" name="cf_password" class="form-control" placeholder="Xác nhận mật khẩu">
                                <span style="color:red" class="err_cfpassword"></span>
                            </div>
                            <div class="form-group">
                                {{-- <a href="#">Quên mật khẩu?</a> --}}
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" src="{{asset('client/js/jquery.3.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('client/js/luong.js')}}"></script>
<script>

  var audio = document.getElementsByTagName("audio")[0];
  var att = document.createAttribute("autoplay");
  audio.setAttributeNode(att);

</script>
<script>
    $('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});
</script>
<script>
    jQuery(document).ready(function() {
        jQuery('#search').click(function(){
            jQuery('.w_suggestion').addClass('show');
            event.stopPropagation();
        });
        jQuery('.w_suggestion').hover(function () {
           $(this).attr("m", "1");
        });
        jQuery('.w_suggestion').mouseleave(function () {
           $(this).attr("m", "0");
        });
        jQuery('body').click(function() {
            if ($('.w_suggestion').attr('m') == '0') {
                jQuery('.w_suggestion').removeClass('show');

            }
        });

        });
    function more_comment(e){
        code = $(e).parent('div.more-cmt').attr('code');
        commentCurrent = $(e).parent('div.more-cmt').attr('comment-current');
        s = $(e).parent('div.more-cmt').attr('s');
        $.ajax({
            type: 'POST',
            url: '{{route("more_comment")}}',
            data: {
                code: code,
                commentCurrent: commentCurrent,
                s : s
            },
            beforeSend : function(){
            },
            success: function (data) {
                if (data.length == 0) {
                    $(e).css('display','none');
                }else{
                    $(e).parent('div.more-cmt').attr('comment-current',Number(commentCurrent)+10);
                    var content = '';
                    var auth = `{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id :  '0' }}`;
                    console.log(data);
                    data.forEach(element => {

                    content += `<li class="comment-item parent_comment" idcommentcurrent="${element.id}">
                        <p class="medium-circle-card comment-avatar"><img
                                src="${element.customer.image ? element.customer.image : `{{ asset('client/img/default-avatar.png') }}` }"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <p class="username"><span>${element.customer.username ? element.customer.username :
                                    element.customer.email}</span><span class="reply-ago-time">${element.date}</span></p>
                            <p class="content">${element.content}</p>
                            <div class="func-comment">
                                <a style="cursor:pointer" class="z-like-rate `;
                                       if (element.arr_customer_id) {
                                            var arr = element.arr_customer_id.split(',');
                                                arr.forEach(v => {
                                                    if (v == {{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id :  '0' }}) {
                                                            content += 'active';
                                                    }else{
                                                            content += '';
                                                    }
                                                });
                                            };
                                        content +=  `"
                                    onclick="likeComment(this,{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id :  '0' }},${element.id})"><i
                                        class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                        class="total_like">${element.total_like}</span></a>

                                <a class="reply-func" style="cursor:pointer">Trả lời</a>`
                                if (element.customer_id == auth) {
                                content += ` <a class="delete-comment" style="cursor:pointer;color:red"
                                    url="{{url('delete_comment/')}}${element.id}">Xóa</a>`;
                                }
                                content += `
                            </div>
                            <div class="comment-reply-list-wrapper">
                                <ul class="list-comment list-comment-reply">`;
                                    if (element.child_comment.length != 0) {
                                    element.child_comment.forEach(child_comment => {
                                    content += `<li class="comment-item">
                                        <p class="medium-circle-card comment-avatar"><img
                                                src="${child_comment.customer.image ? child_comment.customer.image : `{{ asset('client/img/default-avatar.png') }}` }"
                                                alt="Zing MP3"></p>
                                        <div class="post-comment">
                                            <p class="username"><span>${child_comment.customer.username ? child_comment.customer.username :
                                                    child_comment.customer.email}</span><span
                                                    class="reply-ago-time">${child_comment.date}</span></p>
                                            <p class="content">${child_comment.content}</p>
                                            <div class="func-comment">
                                                <a style="cursor:pointer" class="z-like-rate `;
                                                    if (child_comment.arr_customer_id) {
                                                                var arr = child_comment.arr_customer_id.split(',');
                                                        arr.forEach(v1 => {
                                                        if (v1 == {{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id :  '0' }}) {
                                                                        content += 'active';
                                                                    }else{
                                                                        content += '';
                                                                    }
                                                            });
                                                        };
                                                content += `"
                                                    onclick="likeComment(this,{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id :  '0' }},${child_comment.id})"><i
                                                        class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                        class="total_like">1</span></a>

                                                <a class="reply-func" style="cursor:pointer">Trả lời</a>`
                                                if (child_comment.customer_id == auth) {
                                                content += ` <a class="delete-comment" style="cursor:pointer;color:red"
                                                    url="{{url('delete_comment/')}}/${child_comment.id}">Xóa</a>`;
                                                };
                                                `</div>
                                        </div>
                                    </li>`;
                                    });

                                    }

                                    content += `
                                </ul>
                            </div>

                        </div>
                    </li>`;
                    });
                }

                $(e).parents('#cmt_cus').find('.comment-list-wrapper').find('.list-comment:eq(0)').find('.parent_comment:last').after(content);
            }
        });
    }

    function likeComment(e,customer_id,comment_id){
        <?php
        if (!Auth::guard('customers')->check()) {
        ?>
            alert('Đăng nhập để sử dụng chức năng này !!!');
        <?php
            } else {
        ?>
                if ($(e).hasClass('active')) {
                    $(e).removeClass('active');
                    $(e).find('.total_like').html(Number(e.text) - 1);
                    $.ajax({
                        type: 'POST',
                        url: '{{route("likeComment")}}',
                        data: {
                            customer_id: customer_id,
                            comment_id: comment_id,

                        },
                        success: function (data) {
                        }

                    });
                } else {
                    $(e).addClass('active');
                    $(e).find('.total_like').html(Number(e.text) + 1);
                    $.ajax({
                        type: 'POST',
                        url: '{{route("likeComment")}}',
                        data: {
                            customer_id: customer_id,
                            comment_id: comment_id,

                        },
                        success: function (data) {
                        }
                    });

            }
            <?php
            };
        ?>
    };


    // Đăng nhập
    $('#loginCustomer').on('submit', function (e) {
    e.preventDefault();

    var form = $(this);
    let dataForm = new FormData(form[0]);
    dataForm.set('_token', '{{csrf_token()}}');

    $.ajax({
            url: "{{route('loginCustomer')}}",
            // url: window.location.href,
            method: 'post',
            processData: false,
            contentType: false,
            data: dataForm,
            }).done(

            result => {
            var msg = result.data;

            if (result.errors) {
                if (typeof(msg) == 'object') {
                    form.find('.err_username').html(msg.username);
                    form.find('.err_password').html(msg.password);
                    form.find('.err_').html('');
                }else{
                    form.find('.err_').html(msg);
                    form.find('.err_username').html('');
                    form.find('.err_password').html('');
                }

            } else {
                window.location.reload();
            }

       });
    });

    // Đăng kí
    $('#registerCustomer').on('submit',function(e){
        e.preventDefault();
        var form = $(this);

        let dataForm = new FormData(form[0]);
        dataForm.set('_token', '{{csrf_token()}}');
        // console.log(dataForm);

        $.ajax({
            url: "{{route('registerCustomer')}}",
            // url: window.location.href,
            method: 'post',
            processData: false,
            contentType: false,
            data: dataForm,
            beforeSend :function () {
                $(this).find('button[type=submit]').html('Đang đăng ký ...');
            }
        }).done(

        result => {
            var msg = result.data;

            if (result.errors) {
                if (typeof msg.username === 'undefined') {
                    form.find('.err_username').html('');
                };
                if(typeof msg.email === 'undefined'){
                    form.find('.err_email').html('');
                };
                if(typeof msg.password === 'undefined'){
                    form.find('.err_password').html('');
                };
                if(typeof msg.cf_password === 'undefined'){
                    form.find('.err_cf_password').html('');
                };

                if(typeof msg.birthday === 'undefined'){
                form.find('.err_date').html('');
                };
                if(typeof msg.sex === 'undefined'){
                form.find('.err_sex').html('');
                };

                form.find('.err_username').html(msg.username);
                form.find('.err_email').html(msg.email);
                form.find('.err_password').html(msg.password);
                form.find('.err_cf_password').html(msg.cf_password);
                form.find('.err_date').html(msg.birthday);
                form.find('.err_sex').html(msg.sex);

            } else {

                if (typeof msg.username === 'undefined') {
                form.find('.err_username').html('');
                };
                if(typeof msg.email === 'undefined'){
                form.find('.err_email').html('');
                };
                if(typeof msg.password === 'undefined'){
                form.find('.err_password').html('');
                };
                if(typeof msg.cf_password === 'undefined'){
                form.find('.err_cf_password').html('');
                };
                if(typeof msg.birthday === 'undefined'){
                form.find('.err_date').html('');
                };
                if(typeof msg.sex === 'undefined'){
                form.find('.err_sex').html('');
                };

                $('.modal-body-register').html(`<div class="row">
                    <div class="col-sm-12">
                        <div>
                            <strong style="color:#ea8685;font-size:17px;"> Chúc mừng bạn ${msg.username} đã đăng ký thành công!
                            </strong>
                            <p>Vui lòng kiểm tra email để kích hoạt tài khoản </p>
                            <button type="button" class="btn btn-danger showModelLogin">ĐĂNG NHẬP NGAY
                            </button>
                        </div>
                    </div>
                </div>`);
            }

        });
    });
    $('body').on('click','.showModelLogin',function(){
        $('#myModal').modal("hide");
        $('.modal-body-register').html(`<h3>Đăng nhập một lần truy cập tất cả các dịch vụ của Thanhca.tv</h3>
        <p>Tạo tài khoản Thanhca.tv ID của bạn</p>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <ul>
                    <li><a href="#" class="fb-login"><i class="fa fa-facebook" aria-hidden="true"></i> Đăng nhập
                            bằng Facebook</a></li>
                    <li><a href="#" class="gg-login"><i class="fa fa-google-plus" aria-hidden="true"></i> Đăng
                            nhập bằng Google +</a></li>
                </ul>
                <p>Bằng cách nhấn vào nút đăng ký đồng nghĩa với bạn đã đồng ý với <a href="#"> điều khoản sử
                        dụng</a> của Thanhca.tv</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form id="registerCustomer">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Tên tài khoản">
                        <span style="color:red" class="err_username"></span>
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email">
                        <span style="color:red" class="err_email"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                        <span style="color:red" class="err_password"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="cf_password" class="form-control" placeholder="Nhập lại mật khẩu">
                        <span style="color:red" class="err_cf_password"></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Đăng ký</button>
                    </div>
                </form>
            </div>
        </div>`);

        $('#myModal-login').modal("show");
    });
</script>
<script>
$.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
function addWishList(code,customer_id){
        @if(Auth::guard('customers')->check())
            $.ajax({

            type:'POST',

            url:`{{route("addSongWishlist")}}`,

            data:{
                code: code,
                customer_id : customer_id
            },

            success:function(data){
                if (data.err) {
                    $('.showNotiWishList').html(`<div class="alert alert-danger" role="alert">
                        ${data.mes}
                    </div>`)
                } else {
                    $('.showNotiWishList').html(`<div class="alert alert-success" role="alert">
                        ${data.mes}
                    </div>`)
                };

                setTimeout(() => {
                    $('.showNotiWishList').html('');
                }, 1500);
            }

            });
        @else
            $('#myModal-login').modal("show");
        @endif
}

$(document).on('click','.btn-delete-search',function(){
    let id = $(this).attr('id');
    scope = $(this);
    $.ajax({
            type:'POST',

            url:`{{route("delete_history_search")}}`,

            data:{
                id : id
            },

            success:function(data){
                  scope.parent('li.top-search').remove();

            }

            });
});

$(document).on('click','.icon-delete-search',function(){
     scope = $(this);
    $.ajax({
        type:'POST',

        url:`{{route("delete_history_search")}}`,

        data:{
            customer_id : `{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : 0}}`
        },

        success:function(data){

                  scope.parents('li#li_history').find('ul.info-search').find('li.top-search').each(function(i,v){
                     v.remove();
                  })

        }

    });
});
</script>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=674553619717239&autoLogAppEvents=1">
</script>


</body>

</html>
