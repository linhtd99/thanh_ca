@include('client.layout.header')
{{-- {{dd(Request::segment(3))}} --}}
<div class="wn-content-user">
	<div class="container">
		<div class="box_left_user">
			<div class="box-img-avatar">

				<img src="{{!empty(Auth::guard('customers')->user()->avatar) ? Auth::guard('customers')->user()->avatar : asset('client/img/default-avatar.png')}}" alt="">
				<a href="">{{Auth::guard('customers')->user()->name ? Auth::guard('customers')->user()->name : ''}}</a>
			</div>
			<ul>
				<li class="{{ Request::segment(3) == 'quan-ly' ? 'active' : '' }}"><a href="#1a" data-toggle="tab"
						role="tab" class="1a" onclick="hrefQuanLy()">Quản lý tài khoản</a></li>
				<li class="{{ Request::segment(3) == 'cap-nhat-playlist' || Request::segment(3) == 'playlist'  ? 'active' : '' }}"><a href="#2a"
						 onclick="hrefPlaylist()">Playlist</a></li>
				{{-- <li><a href="#3a" data-toggle="tab" role="tab">Video</a></li> --}}
				<li class="{{ Request::segment(3) == 'lich-su' ? 'active' : '' }}"><a href="#4a" data-toggle="tab" role="tab" onclick="hrefHistory()">Lịch sử</a></li>
				<li><a href="{{route('logoutCustomer')}}">Đăng xuất</a></li>
			</ul>
		</div>
		@yield('content')
	</div>
</div>

@include('client.layout.footer')

@yield('js')

<script>

	function hrefQuanLy() {
        window.location.href = "{{url('user/'.Auth::guard('customers')->user()->username.'/quan-ly')}}";
    }
	function hrefPlaylist() {
		window.location.href = "{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}";
	}
	function hrefHistory(){
		window.location.href = "{{url('user/'.Auth::guard('customers')->user()->username.'/lich-su')}}";
	}
</script>
<script type="text/javascript">
	var idSection = "0";
	if (idSection == "0")(idSection = "");
	$("#playlist-genre option").each(function() {
		if ($(this).attr("value") == idSection) {
			$(this).attr("selected", "selected");
		}
	});
</script>
<script>

	// xem trước image
	function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#avatar_img').attr('src', e.target.result);
	}

		reader.readAsDataURL(input.files[0]);
		}
	};


	$("input[name=myfile]").change(function() {
		readURL(this);
	});

	$('.submitEdit').on('click', function(e){
		e.preventDefault();
		var Form = $(this).parents('form');
		let dataForm = new FormData(Form[0]);
		dataForm.set('_token', '{{csrf_token()}}');

		$.ajax({
			url : "{{route('postEditUser')}}",
			method: 'post',
			processData: false,
			contentType: false,
			data: dataForm,
		}).done(
			result => {
			var msg = result.data;
			if (result.errors) {
				Form.find('.err_image').html(msg.myfile[0])
			}else{
				Form.find('.err_image').html('');
				$('body').find('._err_edit').html(`
				<div class="col-sm-12">
				<div class="alert alert-success">
					<strong>Thông báo!</strong> Thay đổi thông tin thành công
				</div>
				</div>`);
				setTimeout(function(){
					window.location.reload();
				}, 1500);
			}

		});
	});

	$('.editPasswordUser').click(function(e){
		e.preventDefault();
		var Form = $(this).parents('form');
		let dataForm = new FormData(Form[0]);
		dataForm.set('_token', '{{csrf_token()}}');

		$.ajax({
			url : "{{route('postEditPwUser')}}",
			method: 'post',
			processData: false,
			contentType: false,
			data: dataForm,
		}).done(
			result => {
				// console.log(dataForm);
			var msg = result.data;
			if (result.errors) {
				if (msg.old_password) {
					Form.find('.err_old_password').html(msg.old_password);
				}else{
					Form.find('.err_old_password').html('');
				}
				if (msg.new_password) {
					Form.find('.err_new_password').html(msg.new_password);
				}else{
					Form.find('.err_new_password').html('');
				}
				if (msg.confirm_password) {
					Form.find('.err_cf_password').html(msg.confirm_password);
				}else{
					Form.find('.err_cf_password').html('');
				}
		}else{

				Form.find('.err_old_password').html('');
				Form.find('.err_new_password').html('');
				Form.find('.err_cf_password').html('');

			$('body').find('._err_edit_pw').html(`
			<div class="col-sm-12">
				<div class="alert alert-success">
					<strong>Thông báo!</strong> Thay đổi mật khẩu thành công
				</div>
			</div>`);
		setTimeout(function(){
			window.location.reload();
			}, 1500);
		}

		});
	})

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    // Tìm kiếm danh sách bài hát trang thêm play list
    var e = $.Event('keydown');
    setTimeout(() => {
        $('#searchInputQuickList').trigger('click');
    }, 1000);

	jQuery('#searchInputQuickList').bind('keyup click',function() {
		var content ='';
		var artist_name = '';
		let scope = $(this);
		var keysearch = scope.val();

		$.ajax({
			url : "{{route('searchSongAddPlaylist')}}",
			method: 'post',
			data :  { keysearch : keysearch},

		}).done(
			result => {
				jQuery.each(result,function(i,val){
					html = $('#arr_listSong').text();

					str = html.substring(0, html.length-1);
					newStr = '['+str+']';


					if (val.artists.length == 0) {
							artist_name = 'Chưa xác định';
					} else {
						val.artists.forEach(element => {
							artist_name += `${element.name}`;
						});
					}
					var flag;


						if (newStr.indexOf(JSON.stringify(val)) !== -1) {
							flag = true;
						}else{
							flag = false;
						};

					// console.log(flag);

					content +=  `<li id="item_list_${val.id}" style="width:280px" class="alternate" key="${val.id}">
					<span class="nunberit">${i+1}</span>
					  <div class="item_content" style="width:190px"><a id="name_song_${val.id}"
								onclick="_gaq.push(['_trackEvent', 'Home', 'Click', 'Quick Search Edit Playlist']);"
								href="" target="_blank" class="name_song">${val.name}</a> -
							<span id="name_singer_${val.id}"><a class="name_singer"
									href=""
									title="">${artist_name}</a></span>
							</div><a href="javascript:;" data-id="${val.id}" class="${flag ? `disable` : `button_select`}">${flag ? `Đã chọn` : `Chọn`}</a>
					 </li>`;
				})
				jQuery('#idScrllSongInAlbumEditPlaylist').html(content);
		});
	});

	jQuery('body').on('click','.button_select',function (e) {
		e.preventDefault();
		$(this).html(`Đã chọn`);
		$(this).removeClass();
		$(this).addClass('disable');
		var id = $(this).attr('data-id');
		// var artist_name = '';
		$.ajax({
		url : "{{route('searchSongById')}}",
		method: 'post',
		data : { id : id},

		}).done(
		result => {

			$('#arr_listSong').append(JSON.stringify(result)+',');

		});
	});

	jQuery('body').on('click','.button_delete',function(e) {
		var id = $(this).attr('data-id');
		var scope = $(this).parent('li').attr('key');
		$(this).parent('li').remove();
		$('#item_list_'+scope+' > a').html('Chọn');
		$('#item_list_'+scope+' > a').removeClass();
		$('#item_list_'+scope+' > a').addClass('button_select');
		$.ajax({
			url : "{{route('searchSongById')}}",
			method: 'post',
			data : { id : id},

		}).done(
		result => {
			var arr_listsong = $('#arr_listSong').text();
			res = arr_listsong.replace(JSON.stringify(result)+',','');

			$('#arr_listSong').html(res);


		});
	});

	jQuery('body').on('DOMSubtreeModified','#arr_listSong',function(e) {
			var content = '';
			html = $(this).text();
			str = html.substring(0, html.length-1);

			newStr = '['+str+']';
			var artist_name ='';
			$.each(JSON.parse(newStr), function( index, value ) {
				console.log(value.artists.length);
				if (value.artists.length == 0) {
						artist_name = 'Chưa xác định';
				} else {
						artist_name ='';
						value.artists.forEach(element => {
						artist_name += `${element.name}`;
					});
				}
				content +=
				`<li style="width:380px" class="alternate" key="${value.id}"><span class="nunberit"></span>
					 <div class="item_content" style="width:270px"><a href="javascript:;" class="name_song"
					>${value.name}</a> - <a class="name_singer"

							title="Tìm các bài hát, playlist, mv do ca sĩ Canace Wu trình bày">${artist_name}</a>
					</div>
					 <a target="_blank"
						class="button_playing" style="margin-right: 5px;" title="Nghe bài hát này"></a>
					<a href="javascript:;" data-id="${value.id}" class="button_delete" title="Xóa bài hát này"></a>
					<a href="javascript:;" class="button_upsort" title="Cho bài hày này lên trên"></a>
					<a href="javascript:;" class="button_downsort" title="Cho bài hày này xuống dưới"></a>
					</li>`;
					jQuery('#idScrllSongInAlbum').html(content);
					var sizeLi = jQuery('#idScrllSongInAlbum').find('li').size();
					// console.log(sizeLi);
					if (sizeLi == 1) {
						jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
						jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
					} else if(sizeLi == 2){

						jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

						jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
					}else if(sizeLi == 3){
						jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

						jQuery('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');
					}else{
						jQuery('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

						jQuery('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
					}

			});

	})

	jQuery('body').on('click','.button_upsort',function(){
		liCurrent = $(this).parent('li');
		var wrapper = $(this).closest('li');
		var current = wrapper.insertBefore(liCurrent.prev());
		var sizeLi = jQuery('#idScrllSongInAlbum').find('li').size();
		// console.log(sizeLi);
		if (sizeLi == 1) {
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
		} else if(sizeLi == 2){

			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
		}else if(sizeLi == 3){
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');
		}else{
			jQuery('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

			jQuery('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
			if (liCurrent.index() == 0) {
			// console.log('đứng đầu');
			$(this).css('display','none');
			$('div#idScrllSongInAlbum').find('li:not(:first-child)').find('.button_upsort').css('display','block');
			} else {
			// console.log('ko');
			$(this).css('display','block');
			}
		}



		var content = $(this).parents('div#idScrllSongInAlbum').html();

		$('div#idScrllSongInAlbum').html(content);

	});

	jQuery('body').on('click','.button_downsort',function(){
		liCurrent = $(this).parent('li');
		var wrapper = $(this).closest('li');
		var current = wrapper.insertAfter(liCurrent.next());

		var sizeLi = $('div#idScrllSongInAlbum > li').size();
		// console.log(sizeLi);
		if (sizeLi == 1) {
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','none');
		} else if(sizeLi == 2){

			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');

			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_downsort').css('display','block');

			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');

			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','none');
		}else if(sizeLi == 3){
			jQuery('#idScrllSongInAlbum').find('li:first-child').find('.button_upsort').css('display','none');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_upsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(1).find('.button_downsort').css('display','block');
			jQuery('#idScrllSongInAlbum').find('li').eq(2).find('.button_downsort').css('display','none');

		}else{
			jQuery('#idScrllSongInAlbum').find('li').eq(0).find('.button_upsort').css('display','none');

			jQuery('#idScrllSongInAlbum').find('li:last-child').find('.button_downsort').css('display','none');
			if (liCurrent.index() == (sizeLi-1)) {
				$(this).css('display','none');
				$('div#idScrllSongInAlbum').find('li:not(:last-child)').find('.button_downsort').css('display','block');
			} else {
				$(this).css('display','block');
			}
		}


		var content = $(this).parents('div#idScrllSongInAlbum').html();
		$('div#idScrllSongInAlbum').html(content);

	});

	jQuery('body').on('click','#checkAll',function(){
		if ($('#checkAll:checked').length == 1) {
			$('.check_data.box-checkbox > input[name=check_video]').trigger('click');
		}else{
			$('.check_data.box-checkbox > input[name=check_video]').trigger('click');
		}
	})







</script>
<script>
    $(function() {
        $('#idScrllSongInAlbum').sortable();
        // $( "#idScrllSongInAlbum" ).disableSelection();
    })
</script>
