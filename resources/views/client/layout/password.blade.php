@extends('client.layout.master')
@section('page_title')
Đặt lại mật khẩu
@endsection
@section('content')
<div class="container">
    <div style="width:50%;margin:auto; padding:50px 0px 50px 0px">
        <div class="login-logo">
            @if (session('error'))

            <div class="alert alert-danger" role="alert">
                <h4>{{ session('error') }}</h4>
            </div>

            @endif

            @if(session('success'))
            <div class="alert alert-success">
                <h4>{{session('success')}}</h4>
            </div>
            @endif
        </div>

        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Quên Mật Khẩu</h4>
            </div>
            <div class="modal-body">
                <h5 style="color:#ff6b6b">Nhập Email Đăng Kí</h5>
                {{-- <p>Đăng nhập tài khoản Thanhca.tv ID của bạn</p>  --}}
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" id="change_forgetPassword">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email Của Bạn">
                            </div>
                            @if(session('errors'))
                                <div class="alert alert-danger">
                                    <h6>{{session('errors')}}</h6>
                                </div>
                            @endif
                            <div class="form-group">
                                {{-- <a href="#">Quên mật khẩu?</a> --}}
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
