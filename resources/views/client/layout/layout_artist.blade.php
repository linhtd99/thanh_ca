@include('client.layout.header')
<div class="singer-top-cover">
    <div class="bg-singer-cover"
        style="background:url({{!empty($artist->banner) ? $artist->banner : ''}})"></div>
    <div class="wrap">
        <div class="container">
            <div class="singer-left-avatar">
                <div class="singer-avatar">
                    <img src="{{!empty($artist->image) ? $artist->image : asset('images/765-default-avatar.png')}}">
                </div>
                <h1 class="singer-name" style="text-overflow:ellipsis;overflow: hidden;white-space:nowrap">
                    {{!empty($artist->name) ? $artist->name : ''}}</h1>
                <p>Tên thật: {{!empty($artist->fullname) ? $artist->fullname : 'N/A'}}</p>
                <p>Sinh nhật: {{!empty($artist->birthday) ? $artist->birthday : 'N/A'}}</p>
                <p>Giới tính:
                    @if ($artist->gender == 0)
                    Nam
                    @elseif($artist->gender == 1)
                    Nữ
                    @else
                    N/A
                    @endif
                </p>
                <p>Quốc gia: {{!empty($artist->country) ? $artist->country : 'N/A'}}</p>
            </div>
            <div class="singer-right-cover"><img src="{{!empty($artist->banner) ? $artist->banner : ''}}"></div>
        </div>
    </div>
</div>
<div class="singer-top-menu">
    <div class="container">
        <ul>
            <li><a href="{{route('artistDetail',['slug' => $artist->slug])}}" class=""><span class="icon-home"></span></a></li>
            <li><a href="{{route('artistSong',['slug' => $artist->slug])}}" title="Bài Hát" class="">Bài Hát</a></li>
            <li><a href="{{route('artistVideo',['slug' => $artist->slug])}}" title="Playlist" class="active">Playlist</a></li>
            <li> <a href="{{route('artistMv',['slug' => $artist->slug])}}" title="Video" class="">Video</a></li>

            <li> <a href="#" title="Karaoke" class="">Karaoke</a></li>
            <li> <a href="#" title="Beat" class="">Beat</a></li>
        </ul>
        <div class="box_like_fb">
        </div>
    </div>
</div>
@yield('content')

@include('client.layout.footer')

@yield('js')