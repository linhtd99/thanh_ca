@php
$hotTopics = Sidebar::getTopics();
@endphp
@if (!empty($hotTopics[0]->title))
    <div class="box_topic_music  box_topic_music_thanhca">
        <div class="tile_box_key">
            <h3><a class="nomore" href="{{ route('topic') }}" title="CHỦ ĐỀ HOT">CHỦ ĐỀ HOT</a></h3>
        </div>
        <ul>
            @forelse ($hotTopics as $item)
            <li>
                <a title="{{ $item->title }}" href="{{ route('playlist',['slug' => name_to_slug($item->playlists[0]->name),'code' => $item->playlists[0]->code]) }}"><img
                        src="{{ !empty($item->banner)  ? $item->banner : '' }}"
                        alt="{{ $item->title }}"> </a>
            </li>
            @empty
            @endforelse
        </ul>
    </div>
@endif

