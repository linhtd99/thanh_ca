@include('client.layout.header')
<div class="user-profile-top-cover">
    <div class="bg-user-profile-cover"
        style="background: url(https://avatar-nct.nixcdn.com/avatar/2016/11/04/3/0/d/0/1478241342608_180.jpg">
    </div>
    <div class="container">
        <div class="row wn-user-info">
            <div class="col-md-4">
                <div class="user-avatar">
                    <div class="user-profile-avatar">
                        <img src="{{!empty($user->avatar) ? $user->avatar : asset('client/img/default-avatar.png')}}"
                            alt="">
                    </div>
                    <div class="info-profile">
                        <div class="name-profile">
                            {{!empty($user->name) ? $user->name : $user->username }}
                        </div>
                        <div class="detail-profile">
                            <p>Tên thật: {{!empty($user->name) ? $user->name : $user->username }}</p>
                            @php
                            if (!empty($user->birthday)) {
                            $birthday = date_create($user->birthday);
                            };

                            @endphp
                            <p>Sinh nhật:
                                {{!empty($user->birthday) ? date_format($birthday,'d/m/Y') : 'Chưa cập nhập' }}</p>
                            @php
                            if ($user->sex == 1) {
                            $sex = 'Nữ';
                            } elseif($user->sex == 0) {
                            $sex = 'Name';
                            }else{
                            $sex = 'Khác';
                            }

                            @endphp
                            <p>Giới tính: {{$sex}}</p>
                            <p>Địa chỉ : {{!empty($user->address) ? $user->address : 'Chưa cập nhập'}}</p>
                        </div>
                    </div>
                    <div class="box_show_status">
                        <ul>
                            <li>
                                <div class="show-status">
                                    <span>{{$user->views}}</span>
                                    <p>Lượt xem Profile</p>
                                </div>
                            </li>
                            {{-- <li> --}}
                                {{-- <div class="show-status"> --}}
                                    {{-- <span>10</span>
                                    <p>Lượt nghe Playlist</p> --}}
                                {{-- </div> --}}
                            {{-- </li> --}}
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="user-profile-right-cover" style=" background: url(https://stc-id.nixcdn.com/v11/images/bg-conver-user.png) center;">
                    @if (!empty($playlistRandom))
                        @foreach ($playlistRandom as $key => $item)
                            <a href="">
                                <div class="suggestion-fuser-item f{{$key+1}} wow zoomIn" data-wow-duration="2.5s" data-wow-delay="2s" style="background: url({{$item->image}});">
                                    <div class="icon-play">
                                    </div>
                                    <h3>{{$item->name}}</h3>
                                </div>
                            </a>
                        @endforeach
                   @endif

                </div>
            </div>
        </div>
    </div>
</div> <!-- end-user-profile-top-cover -->
<div class="singer-top-menu">
    <div class="container">
        <ul>
        <li class="{{ Request::segment(3) == 'trang-ca-nhan' ? 'active' : '' }}"><a href="{{url('user/'.$user->username.'/trang-ca-nhan')}}" ><span class="icon-home"></span></a></li>
            <li class="{{ Request::segment(3) == 'danh-sach-playlist' ? 'active' : '' }}"><a href="{{url('user/'.$user->username.'/danh-sach-playlist')}}"">Playlist</a></li>
            {{-- <li> <a href="#tvideo" data-toggle="tab" role="tab" title="Video">Video</a></li> --}}
            @if (Auth::guard('customers')->user()->username == $user->username)
            <li class="{{ Request::segment(3) == 'upload' ? 'active' : '' }}"> <a href="{{url('user/'.$user->username.'/upload')}}"  title="Upload">Upload</a></li>
            @endif
            {{-- <li> <a href="#tfriend" data-toggle="tab" role="tab" title="Bạn bè">Bạn bè</a></li> --}}
        </ul>
    </div>
</div> <!-- end-singer-top-menu -->
<div class="tab-content">
@yield('content')
</div>
@include('client.layout.footer')

@yield('js')
