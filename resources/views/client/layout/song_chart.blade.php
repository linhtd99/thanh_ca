@php
$w_song_chart = Sidebar::songChart();
$a_date = getLastWeek();
//dd($w_song_chart);
@endphp
 @if (!empty($w_song_chart['categories'][0]->name))
     <div class="box_chart_music">
    <div class="tile_box_key">
        <h3><a class="nomore" href="{{ route('playlist_bxh_song', ['slug_category' => name_to_slug($w_song_chart['categories'][0]->name), 'week' => $a_date['week'], 'year' => $a_date['year']  ]) }}" title="BXH Bài Hát">BXH Bài Hát</a></h3>
        <a id="aUrlTop20" href="{{ route('playlist_bxh_song', ['slug_category' => name_to_slug($w_song_chart['categories'][0]->name), 'week' => $a_date['week'], 'year' => $a_date['year']  ]) }}" title="BXH Bài Hát" class="play_all"></a>
    </div>
    <div class="tab-box-music">
        <ul class="nav nav-tabs">
            @php
            $count = 0;
            @endphp
            @forelse ($w_song_chart['categories'] as $wcate)
            @php
            $count++;

            @endphp

            <li data-url="{{ route('playlist_bxh_song', ['slug_category' => name_to_slug($wcate->name), 'week' => $a_date['week'], 'year' => $a_date['year']  ]) }}" class="{{ $count == 1 ? 'active' : '' }}"><a data-toggle="tab"
                    href="#{{ $wcate->slug }}">{{ $wcate->name }}</a></li>
            @empty
            @endforelse
        </ul>

        <div class="tab-content">
            @php
            $count_1 = 0;
            @endphp
            @forelse ($w_song_chart['charts'] as $wchart)
            @php
            $count_1++;
            @endphp
            <div id="{{ name_to_slug($w_song_chart['categories'][$count_1 - 1]->name) }}"
                class="tab-pane fade {{ $count_1 == 1 ? 'in active' : '' }}">
                <div class="list_chart_music" id="top20-content">
                    <ul>
                        @php
                        $count_2 = 0;
                        @endphp
                        @forelse ($wchart as $wsong)
                        @php
                        $count_2++;
                        @endphp
                        <li class="{{ $count_2 == 1 ? 'one' : '' }}">
                            @if ($count_2 != 1)
                            <span class="number special-{{ $count_2 }}">{{ $count_2 }}</span>
                            @endif
                            <div class="info_data">
                                @if ($count_2 == 1)
                                <a title="{{ $wsong->name }}" href="{{ route('song', ['name' => name_to_slug($wsong->name), 'code' =>$wsong->code]) }}" class="img"> <span
                                        class="number special-1">1</span><img
                                        src="{{ !empty($wsong->image) ? $wsong->image : asset('client/img/song-default.png') }}"></a>
                                @endif
                                <h3><a title="{{ $wsong->name }}" href="{{ route('song', ['name' => name_to_slug($wsong->name), 'code' =>$wsong->code]) }}" class="name_song">{{ $wsong->name }}</a></h3>
                                <h4><a href="" class="name_singer"
                                        title="Tìm các bài hát, playlist, mv do ca sĩ Chưa cập nhật trình bày"
                                        target="_blank">Chưa cập nhật</a></h4>
                            </div>
                        </li>
                        @empty
                        @endforelse
                    </ul>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>
 @endif

