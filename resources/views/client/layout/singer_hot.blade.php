@php
    $artistRandom5 = Sidebar::randomArtists();
@endphp
<div class="list_singer_hot">
            <div class="tile_box_key">
                <h3><a title="Ca Sĩ | Nghệ Sĩ" href="#">Ca Sĩ | Nghệ Sĩ</a></h3>
            </div>

            <ul>
                @foreach ($artistRandom5 as $item)
                    <li><a href="#" class="img" title="{{!empty($item->fullname) ? $item->fullname : $item->name}}"><img
                    src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}" title="{{!empty($item->fullname) ? $item->fullname : $item->name}}"
                                alt="{{!empty($item->fullname) ? $item->fullname : $item->name}}"></a><a href="#" class="name_singer_main" title="{{!empty($item->fullname) ? $item->fullname : $item->name}}">{{!empty($item->fullname) ? $item->fullname : $item->name}}</a></li>
                @endforeach


            </ul>
        </div>
