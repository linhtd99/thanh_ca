
@php
    $dataGenerals = Sidebar::dataGenerals();
$dataFooter = Sidebar::dataFooter();
$top_keyword_search = Sidebar::top_keyword_search();

@endphp
@include('client.layout.header')

@yield('content')

@include('client.layout.footer')

@yield('js')




