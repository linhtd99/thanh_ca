@extends('client.layout.master')
@section('page_title')
{{$categoryCurrent['name']}}
@endsection
@section('content')

<div class="home list-video-hh">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_cata_control">
                    <ul class="detail_menu_browsing_dashboard" style="margin-bottom: 30px;">
                        <li class="cate hot havelink"><h3><a href="#" rel="nofollow" title="MỚI &amp; HOT" class="{{Request::segment(1) == 'video' ? 'active' : ''}}" id="video-moi">MỚI &amp; HOT</a></h3></li>
                        @if (!empty($categories))
                        @foreach ($categories as $item)
                        <li class="line"></li>
                             <li class="cate havelink"><h3>
                                 {{-- {{dd('video-'.get_cat_by_id($item['category'])->slug.'.html')}} --}}
                             <a style="cursor: pointer;" class="{{Request::segment(1) == 'video-'.get_cat_by_id($item['category'])->slug.'.html' || Request::segment(1) == 'video-'.get_cat_by_id($item['category'])->slug  ? 'active' : ''}}" href="{{route('categoryVideo',['slug' => get_cat_by_id($item['category'])->slug])}}" rel="dofollow" title="{{get_cat_by_id($item['category'])->name}}">
                                    {{get_cat_by_id($item['category'])->name}}
                                </a>
                                </h3>
                        </li>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($item['categories'] as $category)
                            {{-- {{dd($category)}} --}}
                                @if ($i <= 7)
                                <li><a pr="list_vn" class="{{Request::segment(1) == 'video-'.get_cat_by_id($category)->slug.'.html' || Request::segment(1) == 'video-'.get_cat_by_id($category)->slug ? 'active' : ''}}" href="{{route('categoryVideo',['slug'=> get_cat_by_id($category)->slug])}}" rel="dofollow" title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                                @elseif($i == 8)
                                    <li class="view_more_list" style="z-index: 1002;">
                                        <div class="dot_more"><span class="dot_cricle"></span><span class="dot_cricle"></span><span
                                                class="dot_cricle"></span> <span id="listchild_vn">Xem thêm</span></div>
                                        <ul>
                                @elseif($i > 8 && $i <= count($item['categories']))
                                            <li><a pr="listchild_vn" href="{{route('categoryVideo',['slug'=> get_cat_by_id($category)->slug])}}" rel="dofollow" title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                                            @if ($i == count($item['categories']))
                                                </ul></li>
                                            @endif

                                @endif

                                <?php $i++ ?>
                            @endforeach
                        @endforeach
                       @endif
                    </ul>
                </div>
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2 class="title_of_box_video"><a title="{{$categoryCurrent['name']}}" href="#">{{$categoryCurrent['name']}}</a></h2>
                            <div class="btn_view_select">

                                <a href="{{route('categoryVideo',['slug'=> $categoryCurrent['slug']])}}" class="{{ Request::segment(1) == 'video-'.$categoryCurrent['slug'].'.html' && Request::segment(2) == null ? 'active' : ''}}" title="Hot nhất">Hot nhất</a>
                                <a href="{{route('videoOrderNew',['slug'=> $categoryCurrent['slug']])}}" class="{{ Request::segment(1) == 'video-'.$categoryCurrent['slug'] && Request::segment(2) == 'moi-nhat.html'  ? 'active' : ''}}" title="Mới nhất">Mới nhất</a>

                            </div>
                        </div>
                        <div class="home-mv">
                            <div class="row">

                                {{-- {{dd($videos)}} --}}
                                @if (count($videos) > 0)
                                @foreach ($videos as $video)

											<div class="col-md-3 col-xs-6 col-sm-3">
											<div class="videosmall">
												<div class="box_absolute">
													<span class="view_mv"><span class="icon_view"></span><span>{{ (!empty(countListenHelper($video['code'])->listen)) ? countListenHelper($video['code'])->listen : 0 }}</span></span>
													<span class="tab_lable_"></span>
													<a href="{{route('detailVideo',['name' => name_to_slug($video['name']),'code' => $video['code']])}}" title="{{$video['name']}}" class="img">
														<span class="icon_play"></span>

														<img src="{{!empty($video['image']) ? $video['image'] : getVideoYoutubeApi($video['link'])['thumbnail']['mqDefault'] }}"
															alt="{{$video['name']}}"
															title="{{$video['name']}}">
													</a>
												<span class="icon_time_video">{{getVideoYoutubeApi($video['link'])['duration_sec']}}</span>
												</div>
											</div>
										</div>

									@endforeach
							   @else
								   <div class="alert alert-primary" role="alert" style="background: #69bbe6;color: white;font-weight: bold;">
										Chưa có video !!!
									</div>
							   @endif



                            </div>
                            <div>{{$videos->links()}}</div>
                        </div>
                    </div>



            </div>
			@include('client.layout.sidebar_list_video')
		</div>
	</div>
</div>

@endsection



