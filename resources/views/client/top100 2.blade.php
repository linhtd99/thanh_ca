<?php require_once('header.php'); ?>
	<div class="home qh-top-list">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="tile_box_key">
	                    <h1><a title="Top 100 VIỆT NAM" href="javascript:;" class="nomore" style="cursor: default;">TOP 100 VIỆT NAM</a></h1>
	                </div>
	                <div class="descriptionBXH">
	                    TOP 100 là danh sách 100 bài hát hot nhất thuộc các thể loại nhạc được nghe nhiều nhất trên NhacCuaTui. Danh sách bài hát này được hệ thống tự động đề xuất dựa trên lượt nghe, lượt share, lượt comment v.v của từng bài hát trên tất cả các nền tảng (Web, Mobile web, App). Top 100 sẽ được cập nhật hằng tuần dựa trên các chỉ số có được từ tuần đó. <br> <br>
	                    <span>* TOP 100 vừa được cập nhật vào: <b>06:00 01/08/2019</b></span>
	                </div>
	                <div class="box_cata_control" style="margin-top: 10px;">                         
	                    <ul class="detail_menu_browsing_dashboard">    
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="active" title="Nhạc Trẻ">Nhạc Trẻ</a>
	                        </li>
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="" title="Trữ Tình">Trữ Tình</a>
	                        </li>
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="" title="Nhạc Trịnh">Nhạc Trịnh</a>
	                        </li>
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="" title="Tiền Chiến">Tiền Chiến</a>
	                        </li>
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="" title="Rap Việt">Rap Việt</a>
	                        </li>
	                        
	                        <li style="width: 130px;">
	                            <a href="#" rel="dofollow" class="" title="Remix Việt">Remix Việt</a>
	                        </li>
	                        
	                    </ul>
	                    
	                </div>
	                <div class="list_chart_page">
	                	<div class="box_view_week" style="background: #e74c3c;"> 
	                        <h2>100 ca khúc 
	                            <b>Nhạc Trẻ</b> 
	                            hay nhất trên NhacCuaTui</h2>
	                        <a href="javascript:;" id="sortBtn" title="Đảo chiều danh sách"> <span class="top100_btn_sort"></span></a>
	                        <a target="_blank" href="#" class="active_play" title="Nghe toàn bộ"><span class="icon_playall"></span>Nghe toàn bộ</a>
	                    </div>
	                    <div class="box_resource_slide">
	                    	<ul class="list_show_chart">
	                    	    <li>
	                                <span class="chart_tw special-1">1</span>                                
	                                <div class="box_info_field">
	                                    <a href="#" title="Từng Yêu - Phan Duy Anh"><img src="https://avatar-nct.nixcdn.com/song/2019/07/03/7/5/b/e/1562146897414.jpg" alt="tung yeu - phan duy anh" title="Từng Yêu - Phan Duy Anh" width="60px"></a>
	                                    <h3 class="h3"><a href="#" class="name_song" title="Từng Yêu - Phan Duy Anh">Từng Yêu</a></h3>
	                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Phan Duy Anh trình bày" target="_blank">Phan Duy Anh</a></h4>
	                                </div>
	                               
	                                <div class="box_song_action">
	                                    <a href="javascript:;" id="btnShowBoxPlaylist_tnvcYCYt7lmv" class="button_add_playlist" title="Thêm bài hát Từng Yêu vào playlist yêu thích"></a>
	                                    <a href="#" class="button_playing" title="Nghe bài hát Từng Yêu"></a>
	                                </div>
	                            </li>
	                            <li>
	                                <span class="chart_tw special-2">2</span>                                
	                                <div class="box_info_field">
	                                    <a href="#" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg"><img src="https://avatar-nct.nixcdn.com/song/2019/07/03/7/5/b/e/1562137543919.jpg" alt="hay trao cho anh - son tung m-tp, snoop dogg" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg" width="60px"></a>
	                                    <h3 class="h3"><a href="#" class="name_song" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg">Hãy Trao Cho Anh</a></h3>
	                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Sơn Tùng M-TP trình bày" target="_blank">Sơn Tùng M-TP</a>, <a href="#" title="Tìm các bài hát, playlist, mv do ca sĩ Snoop Dogg trình bày" target="_blank" class="name_singer">Snoop Dogg</a></h4>
	                                </div>
	                               
	                                <div class="box_song_action">
	                                    <a href="javascript:;" id="btnShowBoxPlaylist_vtEybe9NxLw7" class="button_add_playlist" title="Thêm bài hát Hãy Trao Cho Anh vào playlist yêu thích"></a>
	                                    <a href="#" class="button_playing" title="Nghe bài hát Hãy Trao Cho Anh"></a>
	                                </div>
	                            </li>
	                            <li>
	                                <span class="chart_tw special-3">3</span>                                
	                                <div class="box_info_field">
	                                    <a href="#" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM"><img src="https://avatar-nct.nixcdn.com/song/2019/06/22/b/1/2/7/1561218264960.jpg" alt="cao oc 20 - b ray, dat g, masew, k-icm" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM" width="60px"></a>
	                                    <h3 class="h3"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Cao Ốc 20</a></h3>
	                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày" target="_blank">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày" target="_blank">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày" target="_blank">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày" target="_blank">K-ICM</a></h4>
	                                </div>
	                               
	                                <div class="box_song_action">
	                                    <a href="javascript:;" id="btnShowBoxPlaylist_Wbl0lGylnp5A" class="button_add_playlist" title="Thêm bài hát Cao Ốc 20 vào playlist yêu thích"></a>
	                                    <a href="#" class="button_playing" title="Nghe bài hát Cao Ốc 20"></a>
	                                </div>
	                            </li>
	                            <li>
	                                <span class="chart_tw special-4">4</span>                                
	                                <div class="box_info_field">
	                                    <a href="#" title="Sóng Gió Cover - Nguyễn Hương Ly"><img src="https://avatar-nct.nixcdn.com/singer/avatar/2019/06/10/1/0/9/0/1560150109760.jpg" alt="song gio cover - nguyen huong ly" title="Sóng Gió Cover - Nguyễn Hương Ly" width="60px"></a>
	                                    <h3 class="h3"><a href="#" class="name_song" title="Sóng Gió Cover - Nguyễn Hương Ly">Sóng Gió Cover</a></h3>
	                                    <h4 class="list_name_singer" style="color: #a2a2a2"> <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Nguyễn Hương Ly trình bày" target="_blank">Nguyễn Hương Ly</a></h4>
	                                </div>
	                               
	                                <div class="box_song_action">
	                                    <a href="javascript:;" id="btnShowBoxPlaylist_x9AYdWNx4kRr" class="button_add_playlist" title="Thêm bài hát Sóng Gió Cover vào playlist yêu thích"></a>
	                                    <a href="#" class="button_playing" title="Nghe bài hát Sóng Gió Cover"></a>
	                                </div>
	                            </li>
	                    	</ul>
	                    </div>
	                </div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="sidebar">
						<div class="box_prospect_singer">
		                    <div class="tile_box_key">
		                        <h3><a href="#" title="TOP 100 KHÔNG LỜI">TOP 100 KHÔNG LỜI</a></h3>
		                    </div>
		                    <div class="list_singer_music">
		                        <ul>
		                            
		                            <li>
		                                <div class="info_data">                           
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="img"> 
		                                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113243778.jpg">
		                                    </a>
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="name_song" style="margin-top: 15px;">Top 100 Nhạc Không lời Hay Nhất</a>                                    
		                                </div>
		                            </li>
		                        </ul>
		                    </div>

		                </div>
		                <div class="box_prospect_singer">
		                    <div class="tile_box_key">
		                        <h3><a href="#" title="TOP 100 KHÔNG LỜI">TOP 100 ÂU MỸ</a></h3>
		                    </div>
		                    <div class="list_singer_music">
		                        <ul>
		                            
		                            <li>
		                                <div class="info_data">                           
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="img"> 
		                                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113243778.jpg">
		                                    </a>
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="name_song" style="margin-top: 15px;">Top 100 Nhạc Không lời Hay Nhất</a>                                    
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                    
		                </div>
		                <div class="box_prospect_singer">
		                    <div class="tile_box_key">
		                        <h3><a href="#" title="TOP 100 KHÔNG LỜI">TOP 100 CHÂU Á</a></h3>
		                    </div>
		                    <div class="list_singer_music">
		                        <ul>
		                            
		                            <li>
		                                <div class="info_data">                           
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="img"> 
		                                        <img src="https://avatar-nct.nixcdn.com/playlist/2019/07/26/2/3/9/1/1564113243778.jpg">
		                                    </a>
		                                    <a href="#" title="Top 100 Nhạc Không lời Hay Nhất" class="name_song" style="margin-top: 15px;">Top 100 Nhạc Không lời Hay Nhất</a>                                    
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                    
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php require_once('footer.php'); ?>
