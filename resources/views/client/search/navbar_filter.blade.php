<div class="sn_box_top_search">
    <ul class="sn_search_control_select">
        <li><a href="{{route('search',['q' => !empty($_GET['q']) ? $_GET['q'] : ''])}}" title="Tất Cả"
                class="{{Request::segment(1) == 'tim-kiem' && Request::segment(2) == null ? 'active' : ''}}">Tất cả</a>
            <span class="sn_border"></span></li>
        <li><a title="Bài Hát" href="{{route('searchSong',['q' => !empty($_GET['q']) ? $_GET['q'] : ''])}}"
                class="{{Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'bai-hat' ? 'active' : ''}}">Bài
                hát</a>
            <span class="sn_border"></span></li>
        <li><a title="Playlist" href="{{route('searchPlaylist',['q' => !empty($_GET['q']) ? $_GET['q'] : ''])}}"
                class="{{Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'playlist' ? 'active' : ''}}">Playlist</a>
            <span class="sn_border"></span></li>
        <li><a title="Video" href="{{route('searchVideo',['q' => !empty($_GET['q']) ? $_GET['q'] : ''])}}"
                class="{{Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'video' ? 'active' : ''}}">Video</a>
            <span class="sn_border"></span></li>
        <li><a href="{{route('searchVideoKaraoke',['q' => !empty($_GET['q']) ? $_GET['q'] : ''])}}" title="Karaoke" class="{{Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'karaoke' ? 'active' : ''}}">Karaoke</a> <span class="sn_border"></span></li>
        <li class="sn_box_filter">
            <a href="javascript:;" onclick="$('#listSearchFilterBox').toggle();">Lọc</a>
            <span class="sn_border"></span>
            <span onclick="$('#listSearchFilterBox').toggle();" class="ic_filter"></span>
        </li>
    </ul>

    @php
         
        if (Request::segment(1) == 'tim-kiem' && Request::segment(2) == null) {
            $type = 'search';
        }elseif(Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'bai-hat'){
            $type = 'searchSong';
        }elseif(Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'playlist'){
            $type = 'searchPlaylist';
        }elseif(Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'video'){
            $type = 'searchVideo';
        }elseif(Request::segment(1) == 'tim-kiem' && Request::segment(2) == 'karaoke'){
            $type = 'searchVideoKaraoke';
        };
        
    @endphp

    <div id="listSearchFilterBox" class="sn_list_search_follow " style="display: none;">
        <span class="dot_show_menu"></span>
        <div class="sn_box_list_filter">
            <p class="sn_top_list_filter">Tìm kiếm theo</p>
            <ul class="sn_list_filter" id="search_follow" rel="keyword">
                <li><a id="lk_search_keyword" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => '',
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{ empty($_GET['k']) ? 'active' : ''}}">Từ
                        khóa</a><span class="ic_tick"></span>
                </li>
                <li><a id="lk_search_singer" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => 'singer',
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{!empty($_GET['k']) && $_GET['k'] == 'singer' ? 'active' : ''}} "  >Tên ca sĩ</a><span class="ic_tick"></span>
                </li>
                <li><a id="lk_search_title" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => 'title',
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{!empty($_GET['k']) && $_GET['k'] == 'title' ? 'active' : ''}} ">Tiêu đề</a><span class="ic_tick"></span>
                </li>
                {{-- <li><a id="lk_search_lyric" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => 'lyric',
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{!empty($_GET['k']) && $_GET['k'] == 'lyric' ? 'active' : ''}}">Lyric</a><span class="ic_tick"></span></li> --}}
                
            </ul>
        </div>
        <div class="sn_box_list_filter">
            <p class="sn_top_list_filter">Khu vực</p>
            <ul class="sn_list_filter" id="search_location" rel="tat-ca">
                <li><a id="lk_location_tat-ca"
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => '',
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{ empty($_GET['l'])  ? 'active' : ''}}"
                       >Tất cả</a><span class="ic_tick"></span>
                 </li>
                @if (count(get_cat_parent()) > 0 && !empty(get_cat_parent()))
                    @foreach (get_cat_parent() as $item)
                        <li><a 
                        href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => $item->slug,
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                        class="content_filter {{!empty($_GET['l']) && $_GET['l'] == $item->slug ? 'active' : ''}} ">{{$item->name}}</a><span class="ic_tick"></span></li>
                    @endforeach
                @endif
                
            </ul>
        </div>
        <div class="sn_box_list_filter">
            <p class="sn_top_list_filter">Tiêu chí</p>
            <ul class="sn_list_filter" id="search_target" rel="">
                <li><a id="lk_sort_default" class="content_filter {{ empty($_GET['c']) ? 'active' : ''}}" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => '',
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '')
                                    ])}}"
                >Mặc định</a><span class="ic_tick"></span>
                </li>

                <li><a id="lk_target_lyric" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => 'lyric',
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{!empty($_GET['c']) && $_GET['c'] == 'lyric' ? 'active' : ''}}">Có lời</a><span class="ic_tick"></span></li>
                <li><a id="lk_target_karaoke" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => 'karaoke',
                                     's' => (!empty($_GET['s']) ? $_GET['s'] : '') ])}}"
                class="content_filter {{!empty($_GET['c']) && $_GET['c'] == 'karaoke' ? 'active' : ''}}">Có lời Karaoke</a><span
                        class="ic_tick"></span></li>
            </ul>
        </div>
        <div class="sn_box_list_filter">
            <p class="sn_top_list_filter">Sắp xếp theo</p>
            <ul class="sn_list_filter" id="search_sort" rel="default">
                <li><a id="lk_sort_default" class="content_filter {{ empty($_GET['s']) ? 'active' : ''}}" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => '' ])}}"
                >Mặc định</a><span class="ic_tick"></span>
                </li>
                <li><a id="lk_sort_hot" class="content_filter {{!empty($_GET['s']) && $_GET['s'] == 'hot' ? 'active' : ''}}" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => 'hot' ])}}"
                >Nghe nhiều nhất</a><span class="ic_tick"></span>
                </li>
                <li><a id="lk_sort_new" class="content_filter {{!empty($_GET['s']) && $_GET['s'] == 'new' ? 'active' : ''}}" 
                href="{{route($type,['q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                     'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                     'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                     'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                     's' => 'new' ])}}"
                >Mới nhất</a><span class="ic_tick"></span></li>

                <li></li>
            </ul>
        </div>
    </div>
</div>