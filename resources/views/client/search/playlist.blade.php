@extends('client.layout.master')
@section('page_title')
Tìm kiếm : {{!empty($_GET['q']) ? $_GET['q'] : ''}}
@endsection
@section('content')
<div class="home">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="sn_box_title_search">
                    <h3 class="title_search pad"><a style="padding-top: 0px;" title="Tìm kiếm">Tìm kiếm</a></h3>
                </div>
                @include('client.search.navbar_filter')
                {{-- <p class="sn_search_suggestions">Bạn muốn tìm <a href="#" title="Bài Hát">Bài Hát</a>, <a href="#"
                        title="Album">Playlist</a> hay <a href="#" title="Video">Video</a> cho <a href="#"
                        class="keyword"
                        title="{{!empty($_GET['q']) ? $_GET['q'] : ''}}">{{!empty($_GET['q']) ? $_GET['q'] : ''}}</a>?
                </p> --}}

                <div class="home-list-item">
                    <div class="sn_box_title_search">
                        <h2 class="title_search"><a href="#">PlAYLIST </a></h2>
                    </div>
                    @if (count($playlists) > 0)
                        <ul>
                            @foreach ($playlists as $item)
                                <li>
                                    <div class="box-left-album">
                                        <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="box_absolute" title="{{$item->name}}">
                                            <div class="bg_action_info">
                                                <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335"
                                            wgct="1">{{!empty(countListenPlaylistHelper($item->code)->listen) ? countListenPlaylistHelper($item->code)->listen : '0' }}</span></span>
                                                <span class="icon_play"></span>
                                
                                            </div>
                                        <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}" title="{{$item->name}}"></span>
                                        </a>
                                    </div>
                                    <div class="info_album">
                                        <h3 class="h3seo"><a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_song"
                                                title="{{$item->name}}">{{$item->name}}</a>
                                        </h3>
                                
                                    </div>
                                </li>
                            @endforeach
                           
                        </ul>
                    @endif
                    
                    
                
                </div>
        </div>
        @include('client.layout.sidebar_list_video')
    </div>
</div>
</div>
@endsection