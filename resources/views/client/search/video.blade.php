@extends('client.layout.master')
@section('page_title')
Tìm kiếm : {{!empty($_GET['q']) ? $_GET['q'] : ''}}
@endsection
@section('content')
<div class="home">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="sn_box_title_search">
                    <h3 class="title_search pad"><a style="padding-top: 0px;" title="Tìm kiếm">Tìm kiếm</a></h3>
                </div>
                @include('client.search.navbar_filter')
                
              
                <div class="sn_search_returns_frame">
                    
                    
                    <ul class="sn_search_returns_list_song">
                        <div class="sn_box_title_search">
                            <h2 class="title_search"><a href="#">VIDEO </a></h2>
                        </div>
                        @if (count($videos) > 0)
                        <div class="home-list-item">
                            <div class="home-mv">
                                <div class="row">
                                    @foreach ($videos as $key => $video)    
                                        <div class="col-md-3 col-xs-6 col-sm-3">
                                            <div class="videosmall">
                                                <div class="box_absolute">
                                                    <span class="view_mv"><span
                                                            class="icon_view"></span><span>{{!empty(countListenHelper($video->code)->listen) ? countListenHelper($video->code)->listen : '0'}}</span></span>
                                                    <span class="tab_lable_"></span>
                                                    <a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}"
                                                        title="{{$video->name}}" class="img">
                                                        <span class="icon_play"></span>
                                                        <img src="{{!empty($video->image) ? $video->image : getVideoYoutubeApi($video->link)['thumbnail']['mqDefault']}}"
                                                            title="{{$video->name}}">
                                                    </a>
                                                    <span
                                                        class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
                                                </div>
                                            </div>
                                        </div>
                                @endforeach

                                </div>
                            {{$videos->appends([
                                'q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                's' => (!empty($_GET['s']) ? $_GET['s'] : ''),
                                ])->links()}}
                            </div>
                        </div>
                    @else
                        <p>Không có video nào cho "{{!empty($_GET['q']) ? $_GET['q'] : '' }}"</p>
                    @endif
                </ul>

            </div>
        </div>
        @include('client.layout.sidebar_list_video')
    </div>
</div>
</div>
@endsection