@extends('client.layout.master')
@section('page_title')
Tìm kiếm : {{!empty($_GET['q']) ? $_GET['q'] : ''}}
@endsection
@section('content')
<div class="home">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="sn_box_title_search">
                    <h3 class="title_search pad"><a style="padding-top: 0px;" title="Tìm kiếm">Tìm kiếm</a></h3>
                </div>
               @include('client.search.navbar_filter')
               
                {{-- <p class="sn_search_suggestions">Bạn muốn tìm <a href="#" title="Bài Hát">Bài Hát</a>, <a href="#"
                        title="Album">Playlist</a> hay <a href="#" title="Video">Video</a> cho <a href="#"
                        class="keyword"
                        title="{{!empty($_GET['q']) ? $_GET['q'] : ''}}">{{!empty($_GET['q']) ? $_GET['q'] : ''}}</a>?
                </p> --}}

                <div class="sn_search_returns_frame">
                    <ul class="sn_search_returns_list_song">
                        <div class="sn_box_title_search">
                            <h2 class="title_search"><a href="#">Bài hát </a></h2>
                        </div>
                        @if (!empty($songs) && count($songs) > 0)
                            @foreach ($songs as $item)
                            @php
                            $arr_nameArtist = explode(',',$item->artists_name);
                            $arr_slugArtist = explode(',',$item->artists_slug);
                            @endphp
                            <li class="sn_search_single_song">
                                <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                    title="{{$item->name}}" key="hD2QLdXZWkKn"><img class="thumb"
                                        src="{{!empty($item->image) ? $item->image : asset('client/img/song-default.png')}}"></a>
                                <div class="box_info">
                                    <h3 class="title_song"><a
                                            href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                            title="{{$item->name}}">{{$item->name}}</a></h3>
                                    <h4 class="singer_song">
                                        @if (count($arr_nameArtist) > 0 && $arr_nameArtist[0] != null)
                                        @foreach ($arr_nameArtist as $key => $artist)
                                        <a href="{{route('artistDetail',['slug' => $arr_slugArtist[$key]])}}"
                                            class="name_singer" target="_blank">{{$artist}}</a>
                                        @if ($key+1 < count($arr_nameArtist)) , @endif @endforeach @else <a
                                            class="name_singer" target="_blank">Chưa cập nhập</a>
                                            @endif

                                    </h4>
                                </div>
                                <div class="box_action">
                                    <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                        class="button_new_window" target="_blank"
                                        title="Nghe bài hát {{$item->name}} ở cửa sổ mới"></a>
                                    <span class="button_add_playlist" id="btnShowBoxPlaylist_hD2QLdXZWkKn"
                                        title="Thêm bài hát {{$item->name}} vào playlist yêu thích"></span>

                                    <p class="count_listen"><span></span><span
                                            id="NCTCounter_sg_6039119">{{!empty(countListenSongHelper($item->code)->listen) ? countListenSongHelper($item->code)->listen : '0'}}</span>
                                    </p>
                                    <p class="user_upload"><span></span><span ad="true">admin</span></p>
                                    {{-- <span class="icon_tag_official" title="Bản Chính Thức">Official</span> --}}
                                    {{-- <span class="icon_tag_lossless" title="Super Quality (Lossless)">SQ</span> --}}

                                </div>
                            </li>
                            @endforeach
                            
                                {{$songs->appends([
                                'q' => (!empty($_GET['q']) ? $_GET['q'] : ''),
                                'k' => (!empty($_GET['k']) ? $_GET['k'] : ''),
                                'l' => (!empty($_GET['l']) ? $_GET['l'] : ''),
                                'c' => (!empty($_GET['c']) ? $_GET['c'] : ''),
                                's' => (!empty($_GET['s']) ? $_GET['s'] : ''),
                                ])->links()}}

                        @else
                        <p>Không có bài hát nào cho "{{!empty($_GET['q']) ? $_GET['q'] : ''}}"</p>
                        @endif

                    </ul>
                   
               
            </div>
        </div>
        @include('client.layout.sidebar_list_video')
    </div>
</div>
</div>
@endsection