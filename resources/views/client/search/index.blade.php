@extends('client.layout.master')
@section('page_title')
Tìm kiếm : {{!empty($_GET['q']) ? $_GET['q'] : ''}}
@endsection
@section('content')
<div class="home">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 col-sm-12">
                <div class="sn_box_title_search">
                    <h3 class="title_search pad"><a style="padding-top: 0px;" title="Tìm kiếm">Tìm kiếm</a></h3>
                </div>
                @include('client.search.navbar_filter')
                {{-- <div class="sn_title_info_key">
                    <h1 style="display: inline-block;">
                        <a href="#" title="Ba Chúng Ta (Three Of Us)">Ba Chúng Ta (Three Of Us)</a>
                    </h1> có 493,983 kết quả
                </div> --}}
                <p class="sn_search_suggestions">Bạn muốn tìm <a href="#" title="Bài Hát">Bài Hát</a>, <a href="#" title="Album">Playlist</a> hay <a href="#" title="Video">Video</a> cho <a href="#" class="keyword" title="{{!empty($_GET['q']) ? $_GET['q'] : ''}}">{{!empty($_GET['q']) ? $_GET['q'] : ''}}</a>?</p>
                
                <div class="sn_search_returns_frame">
                    <ul class="sn_search_returns_list_song">
                        @if (count($songs) > 0)
                            <div class="sn_box_title_search">
                                <h2 class="title_search"><a href="#">Bài hát </a></h2>
                            </div>
                            @foreach ($songs as $item)
                            @php
                                $arr_nameArtist = explode(',',$item->artists_name);
                                $arr_slugArtist = explode(',',$item->artists_slug);
                            @endphp
                                <li class="sn_search_single_song">
                                    <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" title="{{$item->name}}" key="hD2QLdXZWkKn"><img class="thumb"
                                    src="{{!empty($item->image) ? $item->image : asset('client/img/song-default.png')}}"></a>
                                    <div class="box_info">
                                    <h3 class="title_song"><a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" title="{{$item->name}}">{{$item->name}}</a></h3>
                                        <h4 class="singer_song">
											@if (count($arr_nameArtist) > 0 && $arr_nameArtist[0] != null)
												@foreach ($arr_nameArtist as $key => $artist)
												<a href="{{route('artistDetail',['slug' => $arr_slugArtist[$key]])}}" class="name_singer"
													target="_blank">{{$artist}}</a>
													@if ($key+1 < count($arr_nameArtist))
														,
													@endif
												@endforeach
											@else
												<a class="name_singer"
													target="_blank">Chưa cập nhập</a>
											@endif
                                            
                                        </h4>
                                    </div>
                                    <div class="box_action">
                                        <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="button_new_window" target="_blank"
                                            title="Nghe bài hát {{$item->name}} ở cửa sổ mới"></a>
                                        <span class="button_add_playlist" id="btnShowBoxPlaylist_hD2QLdXZWkKn"
                                            title="Thêm bài hát {{$item->name}} vào playlist yêu thích"></span>
                                
                                        <p class="count_listen"><span></span><span id="NCTCounter_sg_6039119">{{!empty(countListenSongHelper($item->code)->listen) ? countListenSongHelper($item->code)->listen : '0'}}</span></p>
                                        <p class="user_upload"><span></span><span ad="true">admin</span></p>
                                        {{-- <span class="icon_tag_official" title="Bản Chính Thức">Official</span> --}} 
                                        {{-- <span class="icon_tag_lossless" title="Super Quality (Lossless)">SQ</span> --}}
                                
                                    </div>
                                </li>
                            @endforeach
                        @endif
                        
					</ul>
					@if (count($playlists) > 0)
						<ul class="sn_search_returns_list_song">
							<div class="sn_box_title_search">
								<h2 class="title_search"><a href="#">PLAYLIST </a></h2>
							</div>
							<div class="owl-carousel owl-theme show_slider">
								@foreach ($playlists as $playlist)
								<li class="item search_item_album">
									<div class="thumb">
										<a href="{{route('playlist',['name' => $playlist->name,'code' => $playlist->code])}}"
											title="{{$playlist->name}}">
											<img src="{{!empty($playlist->image) ? $playlist->image : ''}}">
											<span class="overlay"></span>
											<p class="count_listen"><span></span><span
													id="NCTCounter_pl_63711335">{{!empty(countListenPlaylistHelper($playlist->code)->listen) ? countListenPlaylistHelper($playlist->code)->listen : '0'}}</span>
											</p>
										</a>
									</div>
									
									<div class="box_info">
										<h3 class="title_song"><a
												href="{{route('playlist',['name' => name_to_slug($playlist->name),'code' => $playlist->code])}}"
												title="{{$playlist->name}}">{{$playlist->name}}</a></h3>
									</div>
								</li>
								@endforeach
						
						
							</div>
						</ul>
					@endif
						
					@if (count($videos) > 0)
					
						<ul class="sn_search_returns_list_song">
							<div class="sn_box_title_search">
								<h2 class="title_search"><a href="#">VIDEO </a></h2>
							</div>
							<div class="home-list-item">
								<div class="home-mv">
									<div class="row">
										@foreach ($videos as $key => $video)
										@if ($key < 2)
											<div class="col-md-6 col-sm-6 col-xs-12 videolarge">
												<div class="videosmall">
													<div class="box_absolute">
														<span class="view_mv"><span class="icon_view"></span><span>{{!empty(countListenHelper($video->code)->listen) ? countListenHelper($video->code)->listen : '0'}}</span></span>
														<span class="tab_lable_"></span>
														<a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}" title="{{$video->name}}" class="img">
															<span class="icon_play"></span>
															<img src="{{!empty($video->image) ? $video->image : getVideoYoutubeApi($video->link)['thumbnail']['default']}}"
																alt="{{$video->name}}"
																title="{{$video->name}}">
													</a>
													<span class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
													<div class="name_video_large">
														<h3><a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}" title="{{$video->name}}"
																class="name_song">{{$video->name}}</a></h3>
														<h4>
														@php
															$arr_nameArtist = explode(',',$video->artists_name);
															$arr_slugArtist = explode(',',$video->artists_slug);
														@endphp
														
														@if (count($arr_nameArtist) > 0 && $arr_nameArtist[0] != null)
															@foreach ($arr_nameArtist as $key => $artist)
																<a href="{{route('artistDetail',['slug' => $arr_slugArtist[$key]])}}" class="name_singer"
																	target="_blank">{{$artist}}</a>
																@if ($key+1 < count($arr_nameArtist)) , @endif 
															@endforeach
														@else 
														<a class="name_singer" target="_blank">Chưa cập nhật</a>
														@endif
															
														</h4>
													</div>
													<a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}" title="{{$video->name}}"><span
															class="item-mask-bg"></span></a>
												</div>
											</div>
									</div>
									@elseif($key >= 2)
									<div class="col-md-3 col-xs-6 col-sm-3">
										<div class="videosmall">
											<div class="box_absolute">
												<span class="view_mv"><span class="icon_view"></span><span>{{!empty(countListenHelper($video->code)->listen) ? countListenHelper($video->code)->listen : '0'}}</span></span>
												<span class="tab_lable_"></span>
												<a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}" title="{{$video->name}}" class="img">
													<span class="icon_play"></span>
													<img src="{{!empty($video->image) ? $video->image : getVideoYoutubeApi($video->link)['thumbnail']['mqDefault']}}"
														title="{{$video->name}}">
												</a>
												<span class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
											</div>
										</div>
									</div>
									@endif
									@endforeach
								</div>
								</div>
							</div>
							
						</ul>
					@endif
                   
                </div>
            </div>
			@include('client.layout.sidebar_list_video')
		</div>
	</div>
</div>
@endsection

