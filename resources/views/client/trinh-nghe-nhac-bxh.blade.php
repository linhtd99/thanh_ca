@extends('client.layout.layout_trinh-nghe-nhac')
@php
    $dataGenerals = json_decode(Options::_get('generals'));
@endphp
@section('page_title')
Bảng xếp hạng
@endsection
<style>
    [role=button] {
        color: white;
        cursor: pointer;
    }

</style>
@section('content')
<div class="container" style="margin-bottom:150px">
    <div class="row" style="margin-top:20px">
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="container-head">
                <div class="container-song">
                    <div class="song-box">

                        <div class="blur-container"
                            style="background: url('{{ !empty($playlist->image) ? $playlist->image : 'https://i.imgur.com/qPsUxNg.jpg' }}') center center / cover no-repeat rgb(157, 147, 125); height: 220px;">
                        </div>
                        <div class="song-info">
                            <div class="row">
                                <div class="col-md-8 col-xs-12 col-sm-7 hh-box-left">
                                    <div class="song-img">
                                        <a href="#"><img src="{{ !empty($playlist->image) ? $playlist->image : 'https://i.imgur.com/qPsUxNg.jpg' }}" alt="Bảng xếp hạng {{ $category->name }} hang {{ !empty($month) ? $month : '' }}</strong> ({{ date('d/m/Y', strtotime($arr_date['start'])) }} - {{ date('d/m/Y', strtotime($arr_date['end'])) }})"></a>
                                    </div>
                                    <div class="left-info">
                                        <div class="ranking"><a role="button"></a></div>
                                        <h3>Bảng xếp hạng {{ $category->name }} Tháng {{ !empty($month) ? $month : '' }}</strong> ({{ date('d/m/Y', strtotime($arr_date['start'])) }} - {{ date('d/m/Y', strtotime($arr_date['end'])) }})</h3>
                                        <div class="artist-name"><a role="button" class=""></a></div>
                                        {{-- <div class="subtext album">Bảng xếp hạng: <a role="button" class="ml-5"
                                                title="{{ !empty($playlist->name) ? $playlist->name : '' }} (Single)" href="#">{{ !empty($playlist->name) ? $playlist->name : '' }}</a>
                                        </div> --}}
                                        <div class="subtext authors">Upload bởi:
                                            @if (empty($playlist->customer_id))
                                            <a role="buttllon">Admin</a>
                                            @else
                                            @if ($playlist->customer_id->name == null)
                                            <a role="button"
                                                href="{{route('trangCaNhan',['username' => $playlist->customer_id->username])}}"
                                                title="{{$playlist->customer_id->username}}">{{$playlist->customer_id->username}}</a>
                                            @else
                                            <a role="button"
                                                href="{{route('trangCaNhan',['username' => $playlist->customer_id->username])}}"
                                                title="{{$playlist->customer_id->name}}">{{$playlist->customer_id->name}}</a>
                                            @endif
                                            @endif</div>
                                        <div class="subtext category">Thể loại:
                                            <a role="button" class="mr-2" title="{{ $category->name }}" href="#">{{ $category->name }}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-12">
                                    <div class="social-share-block">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank"><img
                                                        src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/facebook.svg" alt=""></a></li>
                                            <li><a href="#" class="zalo-share-button" data-customize=true data-oaid="579745863508352884"><img
                                                        src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/zalo.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                    <div class="log-stats">
                                        {{-- <div class="viewed"><i class="fa fa-play" aria-hidden="true"></i> {{$playlist->views}}
                                    </div> --}}
                                    {{-- <a class="action liked"><i class="fa fa-heart" aria-hidden="true"></i> {{Request::segment(1) == 'album' ? !empty(countListenAlbumHelper($playlist->code)) ? countListenAlbumHelper($playlist->code)->listen : '0' : !empty(countListenPlaylistHelper($playlist->code)) ? countListenPlaylistHelper($playlist->code)['listen'] : '0' }}</a>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-btn">
                <ul>
                    <li><a href="#" class="btn-play-pause"><i class="fa fa-play" aria-hidden="true"></i> Nghe bài
                            hát</a>
                    </li>
                    {{-- <li><a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Thêm vào danh sách phát</a></li> --}}
                    {{-- <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> Thích</a></li> --}}
                    {{-- <li><a href="#"><i class="fa fa-download" aria-hidden="true"></i> Tải xuống</a></li> --}}
                    {{-- <li><a href="#" class="more">...</a></li> --}}
                </ul>
            </div>
            <div class="comment tab-box-music">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo {{$dataGenerals->name}}</a></li>
                    <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
                </ul>
                <div class="tab-content">
                    <div id="cmt_cus" class="tab-pane fade in active">
                        <div class="comment-header">
                            <div class="comment-total">{{!empty($totalComment) ? $totalComment->total_comment : '0' }} Bình luận
                            </div>
                            <div class="sorting-block-wrapper clearfix">
                                <div class="sorting-by-wrapper">
                                    <a>
                                        <div class="active-sorting">Bình luận mới nhất <i class="fa fa-angle-down"
                                                aria-hidden="true"></i></div>
                                    </a>
                                    <div class="dropdown-menu-list sort-comment z-hide">
                                        <ul>

                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{ Request::url() }}?s=highlight">Bình
                                                        luận nổi bật</a></div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{ Request::url() }}">Bình
                                                        luận mới nhất</a></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list-wrapper">
                            <ul class="list-comment">

                                @if (count($comments) > 0)
                                @foreach ($comments as $comment)
                                <li class="comment-item parent_comment" idCommentCurrent="{{$comment->id}}">
                                    <p class="medium-circle-card comment-avatar"><img
                                            src="{{!empty($comment->customer->avatar) ? $comment->customer->avatar : asset('client/img/default-avatar.png')}}"
                                            alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <p class="username">
                                            <span>{{!empty($comment->customer->name) ? $comment->customer->name : $comment->customer->username }}</span><span
                                                class="reply-ago-time">{{ $comment->date }}</span></p>
                                        <p class="content">{{$comment->content}}</p>
                                        <div class="func-comment">
                                            <a style="cursor:pointer" class="z-like-rate
                                                    <?php
                                                        if (Auth::guard('customers')->check()) {
                                                            if (!empty($comment->arr_customer_id)) {
                                                                $arr_customer_id = explode(',',$comment->arr_customer_id);
                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                    if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                        echo "active";
                                                                    } else {
                                                                        echo "";
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            echo "";
                                                        }
                                                    ?>
                                                    "
                                                onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$comment->id}})"><i
                                                    class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                    class="total_like">{{!empty($comment->total_like) ? $comment->total_like : '0'}}</span></a>

                                            <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                            @if (Auth::guard('customers')->check())
                                            @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                            <a class="delete-comment" style="cursor:pointer;color:red"
                                                url="{{url('delete_comment/'.$comment->id)}}">Xóa</a>
                                            @endif
                                            @endif
                                        </div>
                                        @php
                                        $comments_reply = $comment->getChildComment();
                                        @endphp
                                        <div class="comment-reply-list-wrapper">
                                            <ul class="list-comment list-comment-reply">
                                                @foreach ($comments_reply as $commentChild)
                                                <li class="comment-item">
                                                    <p class="medium-circle-card comment-avatar"><img
                                                            src="{{!empty($commentChild->customer->avatar) ? $commentChild->customer->avatar : asset('client/img/default-avatar.png')}}"
                                                            alt="Zing MP3"></p>
                                                    <div class="post-comment">
                                                        <p class="username">
                                                            <span>{{!empty($commentChild->customer->name) ? $commentChild->customer->name : $commentChild->customer->username }}</span><span
                                                                class="reply-ago-time">{{ $commentChild->date }}</span></p>
                                                        <p class="content">{{$commentChild->content}}</p>
                                                        <div class="func-comment">
                                                            <a style="cursor:pointer" class="z-like-rate
                                                                    <?php
                                                                        if (Auth::guard('customers')->check()) {
                                                                            if (!empty($commentChild->arr_customer_id)) {
                                                                                $arr_customer_id = explode(',',$commentChild->arr_customer_id);
                                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                                if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                                    echo "active";
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                }
                                                                            }

                                                                        } else {
                                                                            echo "";
                                                                        }

                                                                    ?>
                                                                "
                                                                onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$commentChild->id}})"><i
                                                                    class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                                    class="total_like">{{!empty($commentChild->total_like) ? $commentChild->total_like : '0'}}</span></a>

                                                            <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                            @if (Auth::guard('customers')->check())
                                                            @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                            <a class="delete-comment" style="cursor:pointer;color:red"
                                                                url="{{url('delete_comment/'.$commentChild->id)}}">Xóa</a>
                                                            @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif


                                @if (!Auth::guard('customers')->check())
                                <div  style="text-align: center"  class="btn-login">
                                    <button  style="text-align: center"  class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal-login">Đăng nhâp để bình luận</button>
                                </div>
                                @else

                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img
                                            src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                            alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <form method="POST" action="{{route('saveComment')}}">
                                            @csrf
                                            <input type="hidden" name="status" value="-1">
                                            <input type="hidden" name="code" value="{{Request::segment(3)}}">
                                            <input type="hidden" name="customer_id"
                                                value="{{ Auth::guard('customers')->user()->id }}">
                                            <input type="hidden" name="parent_id" value="0">
                                            <textarea class="form-control" placeholder="" name="content" id="content"></textarea>
                                            <button type="submit" class="btn btn-success green"> Lưu</button>
                                        </form>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="more-cmt" code="{{Request::segment(3)}}" comment-current="10"
                            s="{{!empty($_GET['s']) ? $_GET['s'] : 'normal'}}">
                            <a onclick="more_comment(this)">Xem thêm</a>
                        </div>
                    </div>
                    <div id="cmt_fb" class="tab-pane fade">
                        <div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_menu_player new" style="border-top: solid 1px #ececec;">
            <div class="user_upload">

            </div>

        </div>

        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
            </div>
            <ul>
                @if (!empty($playList4) && count($playList4) > 0)
                @foreach ($playList4 as $item)
                <li>
                    <div class="box-left-album">
                        <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                            class="box_absolute" title="{{$item->name}}">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335"
                                        wgct="1">{{ !empty(countListenPlaylistHelper($item->code)->listen) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                    title="{{$item->name}}"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a
                                href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                class="name_song" title="{{$item->name}}">{{$item->name}}</a>
                        </h3>
                        {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4> --}}
                    </div>
                </li>
                @endforeach
                @else
                <div class="alert alert-primary" role="alert"
                    style="background: #69bbe6;color: white;font-weight: bold;">
                    Chưa có playlist !!!
                </div>
                @endif
            </ul>
        </div>
        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
            </div>
            <div class="home-mv">
                <div class="row">
                    {{-- {{ dd($mv) }} --}}
                    <?php $i = 1 ?>
                    @foreach ($mv as $video1)

                    <div class="col-md-3 col-xs-6 col-sm-3">
                        <div class="videosmall">
                            <div class="box_absolute">
                                <span class="view_mv"><span
                                        class="icon_view"></span><span>{{!empty(countListenHelper($video1['code'])['listen']) ? countListenHelper($video1['code'])['listen'] : '0' }}</span></span>
                                <span class="tab_lable_"></span>
                                <a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code']])}}"
                                    title="{{$video1['name']}}" class="img">
                                    <span class="icon_play"></span>
                                    <img src="{{!empty($video1['image']) ? $video1['image'] : getVideoYoutubeApi($video1['link'])['thumbnail']['mqDefault']}}"
                                        alt="{{$video1['name']}}" title="{{$video1['name']}}">
                                </a>
                                <span
                                    class="icon_time_video">{{getVideoYoutubeApi($video1['link'])['duration_sec']}}</span>
                            </div>
                            <h3><a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code'] ])}}"
                                    title="{{$video1['name']}}" class="name_song_index">{{$video1['name']}}</a></h3>
                            <h4>
                                {{-- {{dd($video1->artists)}} --}}
                                @if (!empty($video1['artists']) && count($video1['artists']) > 0)
                                @foreach ($video1['artists'] as $key => $item)
                                <a href="#" class="name_singer" title="{{$item['name']}}"
                                    target="_blank">{{$item['name']}}</a>
                                @if (($key+1) < count($video1['artists'])) ,
                                    @elseif(($key+1)==count($video1['artists']))@endif @endforeach @else <a>Chưa cập
                                    nhật</a>
                                    @endif
                            </h4>
                        </div>
                    </div>

                    <?php
                                           if ($i == 4) {
                                                  break;
                                            };
                                            $i++;
                                    ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="sidebar">
            {{ Sidebar::top100New() }}
        </div>
    </div>
</div>
</div>


@include('client.layout.fe_player')

@php

@endphp
@endsection
@section('js')
<script src="{{asset('client/js/plyr.js')}}" type="text/javascript"></script>
<script src="{{asset('client/js/rabbit-lyrics.js')}}" type="text/javascript"></script>
<script src="{{asset('client/js/player.js')}}" type="text/javascript"></script>
<script>



    var json_listSong = '<?= json_encode($json_listSong) ?>';
    var json_listSongRandom = '<?= json_encode($listSongRandom5) ?>';
    var route_listen_song = `{{ route("songListen") }}`;
    var route_random_song = `{{ route("route_random_song") }}`;
    var add_to_wishlist = `{{ route("add_to_wishlist") }}`;
    var player_search = `{{ route("player_search") }}`;

    jQuery(function () {
        $('footer').css('display', 'none');
    });

    tc_player(JSON.parse(json_listSong), JSON.parse(json_listSongRandom), route_listen_song, route_random_song, add_to_wishlist, player_search);

$(document).on('click','.delete-comment',function(e){
        e.preventDefault();
        r = confirm("Xóa comment!!!");
        if (r == true) {
            window.location.href = $(this).attr('url');
        } else {
            txt = "You pressed Cancel!";
        }
    })

$(document).on('click', '.reply-func', function () {
        <?php
            if (Auth::guard('customers')->check()){
        ?>
            if($(this).hasClass('have_write_comment')){
                $(this).removeClass('have_write_comment');
                $(this).parents('.comment-item').find('.comment-remove').remove();
            }else{
                $(this).addClass('have_write_comment');
                if ($(this).parents('.comment-item').has('li.comment-remove')) {
                    $(this).parents('.comment-item').find('.comment-remove').remove();
                    parent_id = $(this).parents('.parent_comment').attr('idcommentcurrent');
                    username = $(this).parent('.func-comment').parent('.post-comment').find('.username > span:eq(0)').text();
                    // console.log(username);
                    $(this).parents('.comment-item').find('.comment-reply-list-wrapper').append(
                    `<li class="comment-item comment-remove">
                        <p class="medium-circle-card comment-avatar"><img
                                src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <form method="POST" action="{{route('saveComment')}}">
                                @csrf
                                <input type="hidden" name="status" value="-1">
                                <input type="hidden" name="code" value="{{Request::segment(3)}}">
                                <input type="hidden" name="customer_id" value="{{ !empty(Auth::guard('customers')->check()) ? Auth::guard('customers')->user()->id : '' }}">
                                <input type="hidden" name="parent_id" value="${parent_id}" >
                                <textarea class="form-control" placeholder="" name="content" id="content">@${username}:</textarea>
                                <button type="submit" class="btn btn-success green"> Lưu</button>
                            </form>
                            <div class="comment-reply-list-wrapper"></div>
                        </div>
                    </li>`);
                };

            }
        <?php
            }else{
        ?>
            alert('Đăng nhập để sử dụng chức năng này !!!');
        <?php
            }
        ?>

});

@if (!empty($chart[0]))


   setTimeout(function(){

        $.ajax({

           type:'POST',

           url:'{{route("videoListen")}}',

           data:{
			   code:`{{!empty($_GET['c']) && !empty($_GET['st']) ? $_GET['c'] : $chart[0]->code}}`
			},

           success:function(data){

            console.log('+view')

           }

        });



    }, 210000);
    @endif
</script>





@endsection
