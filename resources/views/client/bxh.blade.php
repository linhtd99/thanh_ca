@extends('client.layout.layout_trinh-nghe-nhac')
@php
    $dataGenerals = Sidebar::dataGenerals();
@endphp
@section('page_title')
Bảng xếp hạng
@endsection
<style>

</style>
@section('content')
<div class="home list-video-hh bxh">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_top_search" style="margin-bottom:10px;">
                    @php
                    $bxh = json_decode(Options::_get('choose_category_bxh'), true);
                    @endphp
                    <ul class="search_control_select">
                        @forelse ($bxh as $b)
                        @php
                        $cate = get_cat_by_id($b);
                        @endphp
                        <li><a href="{{ route('bxh_song', ['slug_category' => name_to_slug($cate->name), 'week' => date('t') -1 , 'year' => date('Y')]) }}" class="{{ name_to_slug($cate->name) == name_to_slug($category->name) ? 'active' : '' }}" title="Bảng Xếp Hạng {{ $cate->name }}">{{ $cate->name }}</a></li>
                        @empty
                        @endforelse

                    </ul>
                </div>
                <div class="tile_box_key">
                    <h1 class="nomore" style="color: #2582be;">Bảng Xếp Hạng Bài Hát {{ $category->name }}</h1>
                    <div class="btn_view_select">

                        <a href="{{ route('bxh_song', ['slug_category' => name_to_slug($category->name), 'week' => $month , 'year' => $year]) }}" class="{{ Request::segment(1) == 'bai-hat' ? 'active' : '' }}" title="Bảng Xếp Hạng Bài Hát {{ $category->name }}">Bài Hát</a>
                        <span></span>
                        <a href="{{ route('bxh_video', ['slug_category' => name_to_slug($category->name), 'week' => $month , 'year' => $year]) }}" class="{{ Request::segment(1) == 'video' ? 'active' : '' }}" class="{{ Request::segment(1) == 'video' ? 'active' : '' }}" title="Bảng Xếp Hạng Video {{ $category->name }}">Video</a>
                    </div>
                </div>
                <div class="descriptionBXH">
                    Bảng Xếp Hạng {{$dataGenerals->name}} cập nhật vào hàng tháng dựa trên số liệu thống kê thực tế trên
                    desktop và mobile NhacCuaTui. Trong đó những trọng số quan trọng quyết định thứ hạng TOP 20 như sau:
                    Nghe, Thích, Bình Luận, Chia sẻ, Tải v.v... Mỗi tương tác của người dùng đều tác động đến kết quả
                    cuối cùng của BXH {{$dataGenerals->name}}.
                </div>
                <div class="list_chart_page">
                    <div class="box_view_week">

                        <a id="prev_foo_video_more" class="prev" href="{{ route('bxh_song', ['slug_category' => name_to_slug($category->name), 'week' => $lastMonth  , 'year' => $lastYear]) }}" style="display: block;" title="Bảng xếp hạng tháng trước"></a>
                        <h2><strong>Tháng {{ $month }}</strong> ({{ date('d/m/Y', strtotime($arr_date['start'])) }} - {{ date('d/m/Y', strtotime($arr_date['end'])) }})</h2>
                        <a href="javascript:;" title="Chọn ngày" class="select_date" id="select_date"></a>
                        <a href="{{ route('playlist_bxh_song', ['slug_category' => name_to_slug($category->name), 'week' => $month , 'year' => $year]) }}"
                            class="active_play" title="Nghe Toàn Bộ"><span class="icon_playall"></span>Nghe Toàn Bộ</a>
                    </div>
                    <div class="showNotiWishList"></div>
                    <div class="box_resource_slide">
                        <ul class="list_show_chart">
                            @php
                            $count = 0;
                            @endphp
                            @forelse ($chart as $item)
                            @php
                                $count++;
                            @endphp
                            <li>
                                <span class="chart_tw special-{{ $count }}">{{ $count }}</span>
                                @php
                                $old_rank = getNumberOnChart($item->code, $category->id, $lastMonth, $lastYear );
                                // dd($old_rank);
                                $class = '';
                                if($old_rank == '') {
                                    $class = 'newchart';
                                    $old_rank = 'Mới';
                                } else {
                                    if((int)$old_rank == (int) $count) {
                                        $class = 'nonechart';
                                    } elseif((int)$old_rank > (int) $count) {
                                        $class = 'upchart';
                                    } else {
                                        $class = 'downchart';
                                    }
                                }

                                @endphp
                                <span class="chart_lw {{ $class }}">
                                    <span class="icon"></span>
                                    <p>{{ $old_rank }}</p>
                                </span>
                                <div class="box_info_field">
                                    <a href="{{ route('song', ['name' => name_to_slug($item->name), 'code' => $item->code]) }}"
                                        title="{{ $item->name }}"><img
                                            src="{{ !empty($item->image) ? $item->image : 'https://i.imgur.com/QHJNedd.png' }}"
                                            alt="{{ $item->name }}" title="{{ $item->name }}" width="60px"></a>
                                    <h3 class="h3"><a
                                            href="{{ route('song', ['name' => name_to_slug($item->name), 'code' => $item->code]) }}"
                                            class="name_song" title="{{ $item->name }}">{{ $item->name }}</a></h3>
                                    <h4 class="list_name_singer" style="color: #a2a2a2">
                                        @php
                                        $artists = $item->artists();
                                        $str = '';
                                        if(!empty($artists->name))
                                        {
                                        foreach ($artists as $artist) {
                                        $str = '<a href="#" class="name_singer"
                                            title="Tìm các bài hát, playlist, mv do ca sĩ '.$artist->name.' trình bày"
                                            target="_blank">' . $artist->name . '</a>, ';
                                        }
                                        }

                                        @endphp
                                        {{ $str }}


                                    </h4>
                                </div>
                                <div style="display: none" style="display:none" class="box_info_more_week">
                                    <a href="javascript:;" style="cursor: default;" class="peak_position"
                                        title="Ví trí xếp hạng cao nhất"><span></span>1</a>
                                    <a href="javascript:;" style="cursor: default;" class="last_weeks_position"
                                        title="Vị trí trên BXH tháng trước"><span></span>2</a>
                                    <a href="javascript:;" style="cursor: default;" class="weeks_on_chart"
                                        title="Số tháng nằm trong BXH"><span></span>7</a>
                                </div>
                                <div class="box_song_action">
                                    <a onclick="addWishList('{{$item->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')" id="" class="button_add_playlist" title=""></a>
                                    <a href="{{ route('song', ['name' => name_to_slug($item->name), 'code' => $item->code]) }}" class="button_playing" title="{{ $item->name }}"></a>
                                </div>
                            </li>
                            @empty
                            Không có dự liệu
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="sidebar">
                    @include('client.layout.singer_hot')
                    {{ Sidebar::top100New() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdn.plyr.io/3.5.6/plyr.js"></script>
<script src="{{asset('client/js/rabbit-lyrics.js')}}" type="text/javascript"></script>
<script src="https://www.youtube.com/iframe_api"></script>


@endsection
