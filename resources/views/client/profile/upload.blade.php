@extends('client.layout.layout_profile')
@section('page_title')
Upload
@endsection
<link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
<style>
.lds-facebook {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-facebook div {
  display: inline-block;
  position: absolute;
  left: 6px;
  width: 13px;
  background: #eb4d4b;
  animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
}
.lds-facebook div:nth-child(1) {
  left: 6px;
  animation-delay: -0.24s;
}
.lds-facebook div:nth-child(2) {
  left: 26px;
  animation-delay: -0.12s;
}
.lds-facebook div:nth-child(3) {
  left: 45px;
  animation-delay: 0;
}
input {
    cursor: pointer;
}

@keyframes lds-facebook {
  0% {
    top: 6px;
    height: 51px;
  }
  50%, 100% {
    top: 19px;
    height: 26px;
  }
}


.box_info_upload {
    width: 90%;
    font-size: 1.25rem;
    background-color: #fff;
    position: relative;
    padding: 0;
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border: 1px solid #e1e1e1;
    height: 325px;
    display: block;
    display: none;
    margin-bottom:40px;
}

.box_info_upload .file_box {

    height: 75px;
    background-color: #f5f5f5;
    padding: 10px 40px;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    border-bottom: 1px solid #e1e1e1;
}
.form_edit_upload {
width: calc(100% - 80px);
padding-left: 40px;
padding-top: 10px;
}

.input-group {
margin-top: 2px;
margin-bottom: 8px;
}
.form_edit_upload .label {
width: 100%;
font-size: 14px;
color: #a6a6a6;
}

.form_edit_upload .edit-user-content {
width: 100%;
font-size: 14px;
/* color: #a6a6a6; */
padding-bottom: 7px;
}

.edit-user-content input[type="text"] {
    padding: 8px 0;
    font-size: 14px;
    float: left;
}
.clr {
    clear: both;
}
.select2-selection.select2-selection--multiple{
    border:none !important;
    border-bottom: 1px solid #c7c7c7 !important;
}
ul.list_item_music li {
    width: 840px;
    float: left;
    height: 56px;
    line-height: 56px;
    display: block;
    border-top: #ececec solid 1px;
}
ul.list_item_music li .item_content {
    font-size: 12px;
    float: left;
    width: 460px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    color: #a2a2a2;
    margin-right: 20px;
}
ul.list_item_music li span.tag {
    float: left;
    width: 140px;
    text-align: center;
    padding: 0;
    height: 30px;
    line-height: 30px;
    margin: 13px 10px 0 0px;
    display: block;
    background: #fe8f01;
    color: #FFFFFF;
    border-radius: 15px;
}
a.name_song:hover {
    color: #2daaed !important;
}
ul.list_item_music li span.icon_listen {
    float: left;
    color: #999999;
    right: 0;
    font-size: 12px;
     background: url(https://stc-id.nixcdn.com/v11/images/icon-repeat.png) left -194px no-repeat;
    padding-left: 15px;
    line-height: 24px;
    display: block;
    margin: 15px 0 0 0;
}
.box_info_upload_video{
    width: 90%;
    font-size: 1.25rem;
    background-color: #fff;
    position: relative;
    padding: 0;
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border: 1px solid #e1e1e1;
    height: 450px;
    display: block;
    display: none;
    margin-bottom:40px;
}
.box_info_upload_video .file_box {

height: 75px;
background-color: #f5f5f5;
padding: 10px 40px;
border-top-left-radius: 8px;
border-top-right-radius: 8px;
border-bottom: 1px solid #e1e1e1;
}
</style>
@section('content')
<div id="tup" class="box-content tab-pane active in">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="home-list-item listAlbumName">
                    <div class="tile_box_key">
                        <h2>
                            <a title="Playlist" href="#" class="nomore">UPLOAD</a>
                        </h2>

                        <div class="btn_view_select-edit">
                            <ul class="btn_view_select">
                                <li>
                                    <a title="Song" href="#upsong" data-toggle="tab" role="tab">Upload nhạc</a>
                                </li>
                                <li>
                                        <a title="Video" href="#upvideo" data-toggle="tab" role="tab">Upload Video</a>
                                    </li>
                                <li class="active"><a title="Video" href="#upvideo" data-toggle="tab" role="tab">Đã tải</a></li>
                                <span></span>

                            </ul>
                        </div>

                    </div>
                    <div class="show-success"></div>
                    <div class="user_cp_profile_playlist ">
                        <div class="tab-content">

                            <div class="tab-pane song" id="upsong" >
                                <div class="box_info_upload" style="display: none;">
                                    <div class="file_box">
                                        <div class="avatar_small_upload" ><img width="50" height="50" id="avatar_small_upload" style="display: none;">
                                        </div>
                                        <div class="title_small_upload" style="font-size:1.25rem"></div>
                                        <div class="info_small_upload"></div>
                                    </div>
                                    <div class="form_edit_upload">

                                    <form enctype="multipart/form-data">
                                        <input type="file" id="file-select" name="fileUpload" style="display:none">
                                        <div class="input-group">
                                            <label class="label" for="txtSongname">Tên bài hát:</label>
                                            <div class="edit-user-content">
                                                <input class="input" type="text" id="txtSongName" name="name_song" value="" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
                                            </div>
                                            <div class="err_name"></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="input-group">
                                            <label class="label" for="artist_id">Nghệ sĩ thể hiện:</label>
                                            <div class="edit-user-content">
                                                <select class="form-control" multiple name="artist_id[]" id="artist_id"
                                                    style="width:500px;border: none !important;border-bottom: 1px solid #c7c7c7 !important;">

                                                </select>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="input-group">
                                            <label class="label" for="txtSongname">Thể loại:</label>
                                            <div class="edit-user-content">
                                            <select id="category_id" name="category_id" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
												<option value="0" title="Chọn thể loại">Chọn thể loại</option>
                                                    @foreach ($categories as $item)

                                                    <option rel="playlist" value="{{$item->id}}"
                                                        style="{{$item->step != 0 ? 'font-size:13px;font-weight:nomal' : 'font-weight:bold'}}">
                                                        @for ($i = 0; $i < $item->step; $i++)
                                                            &nbsp
                                                            @endfor
                                                            {{$item->name}}
                                                    </option>
                                                    @endforeach
											</select>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <div style="height:70px;">
                                            <button type="button" class="btn btn-secondary" id="close_upload" style="float: left;margin-top: 10px;">HỦY</button>
                                            <button type="button" class="btn btn-primary" id="save_upload" style="margin-top: 10px;float: left;margin-left: 10px;">TẢI BÀI, CHỜ DUYỆT</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                                <div class="box_notice_content">
                                    <p class="err_file" style="color:red"> </p>
                                    <div class="icon_notice"><span class="icon_song"></span></div>

                                    <p class="title_name_notice">Bạn chưa upload bài hát nào</p>
                                    <p class="p_content_notice"></p>
                                    <p class="p_content_notice">Hãy upload và chia sẻ niềm vui đến cộng đồng yêu nhạc
                                        khắp mọi nơi!</p>
                                        <div class="btn btn_upload_page">
                                        <form enctype="multipart/form-data" class="form-song">
                                            <input  accept=".mp3,audio/*" type="file" title="Upload Song" name="file" id="uploadsong">Upload
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="upvideo" >
                                    <div class="box_info_upload_video" style="display: none;">
                                        <div class="file_box">
                                            <div class="avatar_small_upload" ><img width="50" height="50" id="avatar_small_upload" style="display: none;">
                                            </div>
                                            <div class="title_small_upload" style="font-size:1.25rem"></div>
                                            <div class="info_small_upload"></div>
                                        </div>
                                        <div class="form_edit_upload">

                                        <form enctype="multipart/form-data">
                                            <input type="file" id="file-select" name="fileUploadvideo" style="display:none">
                                            <div class="input-group">
                                                <label class="label" for="txtvideoname">Tên bài hát, video:</label>
                                                <div class="edit-user-content">
                                                    <input class="input" type="text" id="txtvideoName" name="name_video" value="" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
                                                </div>
                                                <div class="err_name_video"></div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="input-group">
                                                <label class="label" for="artist_id">Nghệ sĩ thể hiện:</label>
                                                <div class="edit-user-content">
                                                    <select class="form-control" multiple name="artist_id_video[]" id="artist_id_video"
                                                        style="width:500px;border: none !important;border-bottom: 1px solid #c7c7c7 !important;">

                                                    </select>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="input-group">
                                                <label class="label" for="txtvideoname">Beat:</label>
                                                <div class="edit-user-content">
                                                <select id="" name="beat" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
                                                    <option value="0">Không</option>
                                                    <option value="1">Có</option>
                                                </select>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="input-group">
                                                    <label class="label" for="txtvideoname">Karaoke:</label>
                                                    <div class="edit-user-content">
                                                    <select id="" name="karaoke" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
                                                        <option value="0">Không</option>
                                                        <option value="1">Có</option>
                                                    </select>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                            <div class="input-group">
                                                <label class="label" for="txtvideoname">Thể loại:</label>
                                                <div class="edit-user-content">
                                                <select id="category_id_video" name="category_id_video" style="width:500px;border: none;border-bottom: 1px solid #c7c7c7;">
                                                    <option value="0" title="Chọn thể loại">Chọn thể loại</option>
                                                        @foreach ($categories as $item)
                                                        <option rel="playlist" value="{{$item->id}}"
                                                            style="{{$item->step != 0 ? 'font-size:13px;font-weight:nomal' : 'font-weight:bold'}}">
                                                            @for ($i = 0; $i < $item->step; $i++)
                                                                &nbsp
                                                                @endfor
                                                                {{$item->name}}
                                                        </option>
                                                        @endforeach
                                                </select>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                            <div style="height:70px;">
                                                <button type="button" class="btn btn-secondary" id="close_upload_video" style="float: left;margin-top: 10px;">HỦY</button>
                                                <button type="button" class="btn btn-primary" id="save_upload_video" style="margin-top: 10px;float: left;margin-left: 10px;">TẢI BÀI, CHỜ DUYỆT</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                    <div class="box_notice_content">
                                        <p class="err_file" style="color:red"> </p>
                                        <div class="icon_notice"><span class="icon_song"></span></div>

                                        <p class="title_name_notice">Bạn chưa Upload video nào</p>
                                        <p class="p_content_notice">Website Thanhca là nơi bạn có thể thoải mái upload và
                                            chia sẻ giai điệu/video yêu thích đến bạn bè, cũng như lưu trữ những ký ức du
                                            dương cho riêng bạn.</p>
                                        <p class="p_content_notice">Hãy upload và chia sẻ niềm vui đến cộng đồng yêu nhạc
                                            khắp mọi nơi!</p>
                                            <div class="btn btn_upload_page">
                                            <form enctype="multipart/form-data" class="form-video">
                                                <input type="file" title="Upload Video" name="file" id="uploadvideo">Upload
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <div class="tab-pane active" id="upvideo">
                                <div class="showNotiWishList"></div>
                                <div class="box_notice_content">
                                    <ul class="list_item_music">
                                        @foreach ($songs as $item)
                                            <li>
                                                <div class="item_content"><a
                                                        href=""
                                                         class="name_song">{{$item->name}}</a> -
                                                         @if (count($item->artists) > 0)
                                                             @foreach ($item->artists as $artist)
                                                                <a href="" target="_blank" class="name_singer">{{$artist->name}}</a>
                                                            @endforeach
                                                         @else
                                                           <a class="name_singer">Chưa cập nhật</a>
                                                         @endif
                                                         </div>

                                                         @if ($item->status == 1)
                                                             <span class="tag pending" style="background-color:#2ed573">Kích hoạt</span>
                                                            @elseif($item->status == 0)
                                                                <span class="tag pending" style="background-color:#ff4d4d">Vô hiệu hóa</span>
                                                            @else
                                                                <span class="tag pending" style="background-color:#fe8f01">Chưa duyệt</span>
                                                        @endif
                                                        <span id="NCTCounter_sg_4603090" class="icon_listen" wgct="1">{{$item->listen}}</span>
                                                        <a href="h{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="button_new_window"
                                                            title="Nghe bài hát {{$item->name}} ở cửa sổ mới"></a>
                                                        <div class="fright_button_add_playlist"><a style="cursor:pointer"
                                                                onclick="addWishList('{{$item->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')"
                                                                id="btnShowBoxPlaylist_ipcniQ31onj1" class="button_add_playlist"
                                                                title="Thêm bài hát {{$item->name}} vào playlist yêu thích"></a> </div>
                                                        <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="button_playing"
                                                            title="Nghe bài hát {{$item->name}} này"></a>
                                            </li>
                                        </ul>
                                        @endforeach
                                        <ul class="list_item_music">

                                        @foreach ($video as $item)

                                            <li>
                                                <div class="item_content"><a
                                                        href=""
                                                         class="name_video">{{$item->name}}</a> -
                                                         @if (count($item->artists) > 0)
                                                             @foreach ($item->artists as $artist)
                                                                <a href="" target="_blank" class="name_singer">{{$artist->name}}</a>
                                                            @endforeach
                                                         @else
                                                           <a class="name_singer">Chưa cập nhật</a>
                                                         @endif
                                                         </div>

                                                         @if ($item->status == 1)
                                                             <span class="tag pending" style="background-color:#2ed573">Kích hoạt</span>
                                                            @elseif($item->status == 0)
                                                                <span class="tag pending" style="background-color:#ff4d4d">Vô hiệu hóa</span>
                                                            @else
                                                                <span class="tag pending" style="background-color:#fe8f01">Chưa duyệt Video</span>
                                                         @endif
                                                        <span id="NCTCounter_sg_4603090" class="icon_listen" wgct="1">{{$item->listen}}</span>
                                                        <a href="{{route('detailVideo',['name' => $item->name,'code' => $item->code])}}" class="button_new_window"
                                                            title="Xem Video {{$item->name}} ở cửa sổ mới"></a>
                                                        <div class="fright_button_add_playlist"><a style="cursor:pointer"
                                                                onclick="addWishList('{{$item->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')"
                                                                id="btnShowBoxPlaylist_ipcniQ31onj1" class="button_add_playlist"
                                                                title="Thêm Video {{$item->name}} vào playlist yêu thích"></a> </div>
                                                        <a href="{{route('detailVideo',['name' => $item->name,'code' => $item->code])}}" class="button_playing"
                                                            title="Xem Video {{$item->name}} này"></a>
                                                </li>
                                            @endforeach
                                        </ul>

                                    {{$songs->links()}}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('client/js/wow.min.js')}}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    new WOW().init();
</script>
<script>
 $("document").ready(function(){
    $("#uploadsong").change(function() {
        types = /(\.|\/)(mp3|m4a)$/i;
        file = this.files[0];

        if (types.test(file.type) || types.test(file.name)) {
            $('.box_notice_content').css('display','none');
            var name = this.files[0].name;
            var newName = name.split('.');
            var resultName = name.replace(newName[newName.length-1],'');
            resultName = resultName.replace(/\.$/, "");
            $('.box_info_upload .title_small_upload').html(resultName);
            $('.box_info_upload .info_small_upload').html(formatBytes(this.files[0].size)+' '+this.files[0].type);
            $('.box_info_upload input[name=name_song]').val(resultName);
            $('.box_info_upload').css('display','block');
        }else{
            $('.err_file').html('Chỉ chấp nhận các file có định dạng mp3 và m4a !!!');
        };
    });
    $('#close_upload').click(function(){
        $('.err_file').html('');
        $("#uploadsong").val('');
        $('#category_id').prop('selectedIndex',0);
        $('.box_info_upload').css('display','none');
        $('.box_notice_content').css('display','block');
    });

    $('#save_upload').on('click',function(e){

        if ($('input[name=name_song]').val().length == 0) {
            $('.err_name').html('<span style="red">Tên không được để trống !!!</span>');
        } else {
            $('.err_name').html('');
            var form = $(this).parents('form');
            let dataForm = new FormData(form[0]);
            dataForm.set('fileUpload',file,$('#uploadsong').prop('files')[0].name);
            dataForm.set('author_id',`{{Auth::guard('customers')->user()->id}}`);
            dataForm.set('_token', '{{ csrf_token() }}');
            $.ajax({
                url: "{{route('saveUploadSong',['username' => Auth::guard('customers')->user()->username])}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
                beforeSend: function() {
                $("input[name=name_song]").attr( "disabled", "disabled" );
                $("#artist_id_video").attr( "disabled", "disabled" );
                $("#category_id").attr( "disabled", "disabled" );
                $('#save_upload').after(`<div class="show_loading"><div class="lds-facebook">
                    <div></div>
                    <div></div>
                    <div></div>
                </div><span style="position: absolute;bottom: 6%;color:red">Đang tải dữ liệu.Vui lòng đợi...</span><div>`);}
            }).done(
                result => {
                    $('.show_loading').css('display','none');
                    $('.show-success').html(`<div class="col-sm-12"><div style="background-color: #1abc9c;color:white;font-weight: bold;" class="alert alert-secondary" role="alert" >
                        Upload file thành công !
                    </div></div>`);
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
            });
        }

    });
});

$("document").ready(function(){
    $("#uploadvideo").change(function() {
        types = /(\.|\/)(mp4)$/i;
        file = this.files[0];

        if (types.test(file.type) || types.test(file.name)) {
            $('.box_notice_content').css('display','none');
            var name = this.files[0].name;
            var newName = name.split('.');
            var resultName = name.replace(newName[newName.length-1],'');
            resultName = resultName.replace(/\.$/, "");
            $('.box_info_upload_video .title_small_upload').html(resultName);
            $('.box_info_upload_video .info_small_upload').html(formatBytes(this.files[0].size)+' '+this.files[0].type);
            $('.box_info_upload_video input[name=name_video]').val(resultName);
            $('.box_info_upload_video').css('display','block');
        }else{
            $('.err_file').html('Chỉ chấp nhận các file có định dạng mp4 !!!');
        };
    });
    $('#close_upload_video').click(function(){
        $('.err_file').html('');
        $("#uploadvideo").val('');
        $('#category_id_video').prop('selectedIndex',0);
        $('.box_info_upload_video').css('display','none');
        $('.box_notice_content').css('display','block');
    });

    $('#save_upload_video').on('click',function(e){

        if ($('input[name=name_video]').val().length == 0) {
            $('.err_name_video').html('<span style="red">Tên không được để trống !!!</span>');
        } else {
            $('.err_name_video').html('');
            var form = $(this).parents('form');
            let dataForm = new FormData(form[0]);
            dataForm.set('fileUploadvideo',file,$('#uploadvideo').prop('files')[0].name);
            dataForm.set('author_id',`{{Auth::guard('customers')->user()->id}}`);
            dataForm.set('_token', '{{ csrf_token() }}');
            $.ajax({
                url: "{{route('saveUploadvideo',['username' => Auth::guard('customers')->user()->username])}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
                beforeSend: function() {
                $("input[name=name_video]").attr( "disabled", "disabled" );
                $("#artist_id").attr( "disabled", "disabled" );
                $("#category_id_video").attr( "disabled", "disabled" );
                $('#save_upload_video').after(`<div class="show_loading"><div class="lds-facebook">
                    <div></div>
                    <div></div>
                    <div></div>
                </div><span style="position: absolute;bottom: 6%;color:red">Đang tải dữ liệu.Vui lòng đợi...</span><div>`);}
            }).done(
                result => {
                    $('.show_loading').css('display','none');
                    $('.show-success').html(`<div class="col-sm-12"><div style="background-color: #1abc9c;color:white;font-weight: bold;" class="alert alert-secondary" role="alert" >
                        Upload file thành công !
                    </div></div>`);
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
            });
        }

    });
});
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
var artists_select2 = $("#artist_id").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
});
var artists_select2 = $("#artist_id_video").select2({
            ajax: {
                url: "{{ route('admin.artists.data_select2') }}",
                type: "post",
                dataType: 'json',
                data: function (params) {
                    //console.log(params);
                    return {
                        q: $.trim(params.term),
                        _token: "{{csrf_token()}}"
                    };
                },
                processResults: function (data) {
                    //console.log(data);
                    var empt = [{
                        id: 0,
                        name: "--- Unknown ---"
                    }];
                    data = empt.concat(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                                value: item.id
                            }
                        })
                    };
                },
                cache: true
            }
});
</script>
@endsection
