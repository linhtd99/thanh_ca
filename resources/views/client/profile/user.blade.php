@extends('client.layout.layout_profile')
@section('page_title')
Trang cá nhân
@endsection
@section('content')


	<div id="thome" class="box-content active tab-pane fade in">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12 col-sm-12">
					<div class="home-list-item listAlbumName">
						<div class="tile_box_key">
							<h2>
								<a title="Playlist" href="#" class="nomore">PLAYLIST | ALBUM</a>

							</h2>
							<div class="btn_view_select-edit">
								@if (Auth::guard('customers')->user()->username == $user->username)
									<div class="edit_page_action">
									<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/cap-nhat-playlist')}}" class="btn_edit_item active">Tạo playlist</a>
									</div>
									<div class="edit_page_action">
										<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}" class="btn_edit_item">Chỉnh sửa</a>
									</div>
								@endif

							</div>
						</div>
						<ul style="width: 100%;">

						@if (count($playlists) > 0 )
						@foreach ($playlists as $playlist)
								<li>
									<div class="box-left-album">
										<a href="{{route('playlist',['name' => name_to_slug($playlist->name),'code' => $playlist->code])}}" class="box_absolute" title="{{$playlist->name}}">
											<div class="bg_action_info">
												<span class="view_listen"><span class="icon_listen"></span><span
														wgct="1">{{!empty(countListenPlaylistHelper($playlist->code)->listen) ? countListenPlaylistHelper($playlist->code)->listen : '0'}}</span></span>
												<span class="icon_play"></span>
											</div>
											<span class="avatar"><img src="{{$playlist->image}}"
													alt="{{$playlist->name}}"
													title="{{$playlist->name}}"></span>
										</a>
									</div>
									<div class="info_album">
									<h3 class="h3seo"><a href="{{route('playlist',['name' => name_to_slug($playlist->name),'code' => $playlist->code])}}" class="name_song" title="{{$playlist->name}}">{{$playlist->name}}</a>
										</h3>

									</div>
								</li>
							@endforeach
								</ul>
								<div class="row">
									<div class="col-sm-12">
									{{ $playlists->links() }}
									</div>
								</div>
						@else
						<div class="row">
							<div class="col-sm-12">
								<div class="alert alert-primary" role="alert" style="background: #41cc76;color: white;font-weight: bold;">
									Hiện tại chưa có playlist nào
								</div>
							</div>
						</div>
						@endif




					</div>
				</div>
				<div class="col-md-3 col-xs-12 col-sm-12">
					<div class="sidebar-singer">
						<div class="singer_playlist_new" >

							<div class="tile_box_key">
								<h3><a title="Playlist mới" class="nomore">Playlist mới nghe</a></h3>
							</div>
							<?php $username = Auth::guard('customers')->user()->username; $keyCookie = str_replace('.','_',$username)?>
							@if (!empty($_COOKIE[$keyCookie]))

                            @php
                                $recentPlaylist = json_decode($_COOKIE[$keyCookie]);
                            @endphp
                            {{-- {{ dd($recentPlaylist) }} --}}
							<div class="box_name_album_info" style="display:inline-block">
								<div class="info_album">
									<div class="avatar_album">
										<div class="rotate_album"><img src="{{!empty($recentPlaylist->image) ? $recentPlaylist->image : ''}}" class="rotate" width="164"></div>
									<span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_12580535" wgct="1">{{!empty(countListenPlaylistHelper($recentPlaylist->code)->listen) ? countListenPlaylistHelper($recentPlaylist->code)->listen : '0'}}</span></span>
										<a href="{{route('playlist',['name' => name_to_slug($recentPlaylist->name),'code' => $recentPlaylist->code])}}" id="mainAlbum" class="box_font_album box_absolute" style="left:0;">
											<span class="icon_play_single_playlist"></span>
										</a>
										<div class="box_bg_album">
											<a href="{{route('playlist',['name' => name_to_slug($recentPlaylist->name),'code' => $recentPlaylist->code])}}" class="img_avatar"><img src="{{!empty($recentPlaylist->image) ? $recentPlaylist->image : ''}}" class="img_album" width="180"></a>
										</div>
									</div>
									<h3 style="font-size: 16px; margin-top: 10px; margin-bottom: 20px;"><a href="{{route('playlist',['name' => name_to_slug($recentPlaylist->name),'code' => $recentPlaylist->code])}}">{{$recentPlaylist->name}}</a></h3>
								</div>
							</div>
							<div class="list_chart_music" style="border-top: 1px solid #f3f3f3; padding-top: 10px;">
								<ul id="ulSingle">
									@foreach ($recentPlaylist->songs as $item)
										<li id="itemSong_86c24b7ee708b7974640d52a300ad028">
										<div class="info_data"><a title="{{$item->name}}" href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_song">{{$item->name}}</a>
												<p class="list_name_singer">
													<?php
													$artist_name = '';
													// dd($item->artists);
													foreach ($item->artists as $value) {
														$artist_name .= '<a href="'.route('artistDetail',['slug' => $value->slug]).'" class="name_singer" title="'.$value->name.'" target="_blank">'.$value->name.'</a>,';
													}
													// dd($artist_name);
													?>
													<?php echo rtrim($artist_name,',')?>

												</p>
											</div>

										<span id="NCTCounter_sg_5978903" class="icon_listen" wgct="1">{{!empty(countListenSongHelper($item->code)->listen) ? countListenSongHelper($item->code)->listen : '0'}}</span>
											<div class="bg_line_status_loading" style="margin-top: 10px;">
											</div>
										</li>
									@endforeach

								</ul>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>




@endsection

@section('js')
<script src="{{asset('client/js/wow.min.js')}}"></script>
<script>
	new WOW().init();
</script>
@endsection

