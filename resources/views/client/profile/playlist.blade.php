@extends('client.layout.layout_profile')
@section('page_title')
Playlist
@endsection
@section('content')


<div id="tlistsong" class="box-content tab-pane active in">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="home-list-item listAlbumName">
                    <div class="tile_box_key">
                        <h2>
                            <a title="Playlist" href="#" class="nomore">PLAYLIST</a>
                        </h2>
                        <div class="btn_view_select-edit">
                            @if (Auth::guard('customers')->user()->username == $user->username)
                            <div class="edit_page_action">
                                <a href="{{url('user/'.Auth::guard('customers')->user()->username.'/cap-nhat-playlist')}}"
                                    class="btn_edit_item active">Tạo playlist</a>
                            </div>
                            <div class="edit_page_action">
                                <a href="{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}"
                                    class="btn_edit_item">Chỉnh
                                    sửa</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="user_cp_profile_playlist">
                        <ul class="show_cp_profile_playlist" style="list-style: none;">
                            <li>
                                <div class="box_left ">
                                @php
                                    // $user = explode('-',Request::segment(2));

                                @endphp
                                <a href="{{route('wishList',['username' => Request::segment(2) ])}}" class="avatar "><img
                                            src="https://avatar-nct.nixcdn.com/playlist/2016/09/09/4/c/6/e/1473409265946.jpg "
                                            alt="Bài hát yêu thích " title="Bài hát yêu thích "></a>
                                </div>
                                <div class="box_right ">
                                    <h3><a href="{{route('wishList',['username' => Request::segment(2) ])}}"
                                            class="name_album_search ">Bài hát yêu thích</a></h3>
                                    <div class="box_list_singer "><span></span>Đang Cập Nhật</div>
                                    <div class="box_info_upload_export "><span id="NCTWG_D_21019885 " at="_blank " ac="user_upload "></span><span
                                            class="export_listen " id="NCTCounter_pl_26210388 " wgct="1 "></span></div>
                                </div>
                            </li>
                            @if (count($playlists) > 0)
                                @foreach ($playlists as $item)
                                <li>
                                    <div class="box_left ">
                                        <a href="" class="avatar"><img
                                         src="{{$item->image}}"
                                                alt="{{$item->name}} " title="{{$item->name}} "></a>
                                    </div>
                                    <div class="box_right ">
                                    <h3><a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                                class="name_album_search ">{{$item->name}}</a></h3>

                                        <div class="box_info_upload_export "><span id="NCTWG_D_21019885 " at="_blank " ac="user_upload "></span><span class="export_listen "
                                                id="NCTCounter_pl_26210388 " wgct="1 ">{{!empty(countListenPlaylistHelper($item->code)->listen) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></div>
                                    </div>
                                </li>
                                @endforeach
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ $playlists->links() }}
                                </div>
                            </div>
                        @else

                        @endif
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="sidebar-singer">
                    <div class="singer_playlist_new">
                        @if (count($songRandom) > 0 )
                        <div class="tile_box_key">
                            <h3><a title="Playlist mới" class="nomore">Có thể bạn thích</a></h3>
                        </div>

                        <div class="list_chart_music" style="border-top: 1px solid #f3f3f3; padding-top: 10px; ">
                            <ul id="ulSingle" style="display: inline-grid;">
                                @foreach ($songRandom as $item)
                                <li id="itemSong_2cb82719960dd04763d11fa5bdb8ab94">
                                    <div class="info_data">
                                    <a title="{{$item->name}}" href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                    class="name_song icon-music" style="padding-left: 35px;">{{$item->name}}</a>
                                        <p class="list_name_singer"
                                            style="padding-left: 35px; margin-top: -18px;    display: table-caption;">
                                            @foreach ($item->artists as $key => $artist)
                                                <a
                                                 href="{{route('artistDetail',['slug' => $artist->slug])}}" class="name_singer"
                                                    title="Tìm các bài hát, playlist, mv do ca sĩ {{$artist->name}} trình bày"
                                                    target="_blank">{{$artist->name}}
                                                 </a>
                                                 @if ($key+1 < count($item->artists))
                                                     ,
                                                 @endif
                                            @endforeach

                                            </p>
                                    </div>
                                <span id="NCTCounter_sg_5355218" class="icon_listen" wgct="1">{{!empty(countListenSongHelper($item->code)) ? countListenSongHelper($item->code)->listen : '0'}}</span>
                                    <div class="bg_line_status_loading" style="margin-top: 10px;">
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('client/js/wow.min.js')}}"></script>
<script>
    new WOW().init();
</script>
@endsection
