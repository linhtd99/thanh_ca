@extends('client.layout.layout_user')
@section('page_title')
Chỉnh sửa danh sách yêu thích
@endsection
@section('content')

<div class="box_right_user">
    <div class="tab-content">

        <div id="2a" class="active tab-pane fade in">
            <div class="form-playlist2" style="display:block">
                <div class="tile_box_key">
                    <h3><a class="nomore" title="Quản lý tài khoản" href="">Chỉnh sửa danh sách yêu thích</a></h3>
                </div>

                <div class="box-edit-user-profile">
                    
                    <form>
                        <div class="form-edit">
                            
                            <div class="input-group wn-tab-ds">
                                <label class="label" for="txtAddr">Danh sách bài hát</label>
                                <div class="box_list_item_edit">
                                    <div id="listSongSearch" class="list_song_in_album"
                                        style="width:290px; float: left;overflow:scroll">
                                        <div style="width:100%; height:37px; background:#f1f1f1">
                                            {{-- <form> --}}
                                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                                            <input style="width:86.5%; margin:1.5%;" class="input" type="text"
                                                id="searchInputQuickList" maxlength="300"
                                                placeholder="Nhập từ khóa hoặc link bài hát">
                                            <a class="btn_search" value="" title="Tìm kiếm"></a>
                                            {{-- </form> --}}
                                        </div>
                                        <div>
                                            <ul id="idScrllSuggestionEditPlaylist" class="list_song_in_album"
                                                style="width: 280px; border: none; height: 450px;">
                                                <input type="hidden" id="arr_listSong">
                                                <div id="idScrllSongInAlbumEditPlaylist"
                                                    style="text-transform:capitalize;">

                                                </div>
                                            </ul>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <span class="box-select-hero"></span>
                                    <ul id="listSong" class="list_song_in_album" style="width:360px">
                                        <div class="slimScrollDiv"
                                            style="position: relative; overflow: hidden; height: 450px;">
                                            <div id="idScrllSongInAlbum"
                                                style="height: 450px; overflow: hidden; width: 360px;">

                                            </div>
                                            <div style="margin:6px 0px 10px 18px;">
                                                {{-- <a href="" class="btn-random" title="Sắp xếp ngẫu nhiên" style="color:#0689ba;">Sắp xếp ngẫu nhiên</a> --}}
                                            </div>
                                        </div>

                                    </ul>
                                </div>
                            </div>

                            <div id="error-box1" class="error-box1"></div>
                            <div id="error-box2" class="error-box2"></div>
                            <div class="input-group">
                                <label class="label" for="txtFullname"></label>
                                <div class="edit-user-content">
                                    <input type="submit" style="margin-right:10px;" class="btn save_wishlist btn-btn"
                                        value="Lưu">
                                    {{-- <a href="{{url('user/deletePlaylist/'.$playlist->id)}}"
                                    style="margin-right:10px;" id="btnDeletePlaylist" class="btn btn-btn">Xóa</a> --}}

                                    <a href="{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}"
                                        class="btn btn-btn" title="Trở lại">Trở lại</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div><!-- end-form-playlist2 -->
        </div><!-- end-tab -->
    </div>
</div> <!-- end-box_right_user -->

@endsection
@section('js')

@if (!empty($songs))

<script>
    $(document).ready(function(){
        
            var arr_listSong = `{{json_encode($songs)}}`;
            // console.log(`{{json_encode($songs)}}`);
            if (arr_listSong.length == 2) {
                $('#arr_listSong').html('');        
            }else{
                arr_listSong = arr_listSong.replace(/^.{1}/g, '');
                arr_listSong = arr_listSong.replace(/.$/,",");
                $('#arr_listSong').html(arr_listSong);
            }
		});
</script>
@endif

<script>
   $('.save_wishlist').on('click', function(e){
		e.preventDefault();
		arr_listSong = new Array();
		$(this).parents('form').find('#idScrllSongInAlbum').find('li').each(function(i,v) {
			arr_listSong[i] = $(this).attr('key'); 
		});
		var Form = $(this).parents('form');
		let dataForm = new FormData(Form[0]);
		dataForm.set('_token', '{{csrf_token()}}');
		dataForm.set('arr_listSong', arr_listSong);
		dataForm.set('customer_id', `<?= Auth::guard('customers')->user()->id ?>`);
		// console.log(dataForm);
		$.ajax({
			url : "{{route('saveEditWishList',['username' => Auth::guard('customers')->user()->username ])}}",
			method: 'post',
			processData: false,
			contentType: false,
			data: dataForm,
		}).done(
			result => {
			
				Form.find('#error-box1').html('');
				Form.find('#error-box2').html('');
				Form.find('#error-box1').html(
				`<div class="col-md-12">
					<div class="alert alert-success" role="alert">
						Thêm playlist thành công!
					</div>
					</div`);
				
				setTimeout(function(){
					window.location.reload();
				}, 1500);
			
            
		});
	});
</script>
@endsection