@extends('client.layout.layout_artist')
@section('page_title')
Nghệ sĩ {{!empty($artist->name) ? $artist->name : ''}}
@endsection
@section('content')

<div class="box-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="home-list-item listAlbumName">
                    <div class="tile_box_key">
						{{-- {{dd(Request::segment(2))}} --}}
					<h2><a title="Playlist" href="#" class="nomore">Album {{$artist->name}}</a></h2>
                        <div class="btn_view_select">
						<a href="{{route('artistVideoHot',['slug' => $artist->slug])}}" title="Hot Nhất" class="{{Request::segment(2) == 'album-hot-nhat' ? 'active' : ''}}">Hot Nhất</a>
						<a href="{{route('artistVideo',['slug' => $artist->slug])}}" title="Mới nhất" class="{{Request::segment(2) == 'album' ? 'active' : ''}}">Mới Nhất</a>
                        </div>
                    </div>
					@if (count($albums) > 0)
                    <ul>
                        @foreach ($albums as $item)
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="{{$item->name}}">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span
                                    id="NCTCounter_pl_63601335" wgct="1">{{!empty(countListenAlbumHelper($item->code)->listen) ? countListenAlbumHelper($item->code)->listen : '0'}}</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img
                                    src="{{!empty($item->image) ? $item->image : ''}}"
                                            alt="{{$item->name}}"
                                            title="{{$item->name}}"></span>
                                </a>
                            </div>
                            <div class="info_album">
                            <h3 class="h3seo"><a href="{{route('album',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_song"
                                title="{{$item->name}} - {{$artist->name}}">{{$item->name}}</a>
                                </h3>
                                <h4><a href="{{route('artistDetail',['slug' => $item->slug])}}}" class="name_singer"
                                        title="Tìm các bài hát, playlist, mv do ca sĩ {{$artist->name}} trình bày">{{$artist->name}}</a>
                                </h4>
                            </div>
                        </li>
                        @endforeach
                    </ul>
					{{$albums->links()}}
					@else
					<div class="alert alert-primary" role="alert"
					style="color:white;font-weight:bold;background-color:#3498db">
					Chưa có album !!!
				</div>
				@endif
			</div>
		</div>
                {{-- <div class="box_pageview">
		                <a href="javascript:;" class="number active">1</a>
		                <a href="#" class="number " rel="next" title="">2</a>
		                <a href="#" class="number " rel="Trang cuối" title="">Trang cuối</a>
		            </div> --}}
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="sidebar-singer">
                    {{-- <div class="singer_playlist_new">
						<div class="tile_box_key">
	                        <h3><a title="Playlist mới" class="nomore">Playlist mới</a></h3>
	                    </div>
	                    <div class="box_name_album_info">
	                       	<div class="info_album">
	                            <div class="avatar_album">
	                                <div class="rotate_album"><img src="https://avatar-nct.nixcdn.com/playlist/2018/10/01/3/9/7/8/1538377959138.jpg" class="rotate" width="164"></div>
	                                <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_12580535" wgct="1">427.091</span></span>
	                                <a href="javascript:;" id="mainAlbum" class="box_font_album box_absolute">
	                                    <span class="icon_play_single_playlist"></span>
	                                </a>
	                                <div class="box_bg_album">
	                                    <a href="#" class="img_avatar"><img src="https://avatar-nct.nixcdn.com/playlist/2018/10/01/3/9/7/8/1538377959138.jpg" class="img_album" width="180"></a>
	                                </div>
	                            </div>
	                            <h3 style="font-size: 16px; margin-top: 10px; margin-bottom: 20px;"><a href="#">Những Bài Hát Hay Nhất Của MIN</a></h3>
	                        </div>
	                    </div>
	                    <div class="list_chart_music" style="border-top: 1px solid #f3f3f3; padding-top: 10px;">
	                        <ul id="ulSingle">
	                            <li id="itemSong_86c24b7ee708b7974640d52a300ad028">
	                                <div class="info_data"><a title="Bài Này Chill Phết - Đen, MIN" href="javascript:;"class="name_song">Bài Này Chill Phết</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đen trình bày" target="_blank">Đen</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>

	                                <span id="NCTCounter_sg_5978903" class="icon_listen" wgct="1">5.958.752</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>

	                            <li id="itemSong_4502abfe81cb039491d8443a9597be09">
	                                <div class="info_data"><a title="Em Mới Là Người Yêu Anh - MIN" href="javascript:;" class="name_song">Em Mới Là Người Yêu Anh</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5489449" class="icon_listen" wgct="1">18.202.422</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>

	                            <li id="itemSong_2cb82719960dd04763d11fa5bdb8ab94">
	                                <div class="info_data"><a title="Đừng Xin Lỗi Nữa (Don't Say Sorry) - ERIK, MIN" href="javascript:;" class="name_song">Đừng Xin Lỗi Nữa (Don't Say Sorry)</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ ERIK trình bày" target="_blank">ERIK</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5355218" class="icon_listen" wgct="1">8.488.846</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>

	                            <li id="itemSong_2604aee22868a86d0c9f9944190a445f">
	                                <div class="info_data"><a title="Take Me Away - MIN" href="javascript:;" class="name_song">Take Me Away</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_4536349" class="icon_listen" wgct="1">2.100.608</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>

	                            <li id="itemSong_3cd4fb938ab7d528b80371e51af9106e">
	                                <div class="info_data"><a title="Y.Ê.U (EDM Version) - MIN" href="javascript:;" class="name_song">Y.Ê.U (EDM Version)</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>

	                                <span id="NCTCounter_sg_3827847" class="icon_listen" wgct="1">3.823.511</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>

	                            <li id="itemSong_31d6632aebd8f161987cc49622e56747">
	                                <div class="info_data"><a title="STEPS2FAME - MIN" href="javascript:;" class="name_song">STEPS2FAME</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5333644" class="icon_listen" wgct="1">1.379.352</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                        </ul>
	                    </div>
					</div> --}}

                    @include('client.layout.singer-trending')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
