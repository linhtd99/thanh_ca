@extends('client.layout.master')
@section('page_title')
Bài hát
@endsection
@section('content')

<div class="home list-video-hh">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_cata_control">
                    <ul class="detail_menu_browsing_dashboard" style="margin-bottom: 30px;">
                        <li class="cate hot havelink">
                            <h3><a href="{{route('indexSong')}}" rel="nofollow" title="MỚI &amp; HOT" class="active" id="video-moi">MỚI &amp;
                                    HOT</a></h3>
                        </li>
                        @if (1==1)
                        @foreach ($categories as $item)
                        <li class="line"></li>
                        <li class="cate havelink">
                            <h3>
                                <a style="cursor: pointer;"
                                    href="{{route('listSongByCategory',['slug' => get_cat_by_id($item['category'])->slug])}}"
                                    rel="dofollow" title="{{get_cat_by_id($item['category'])->name}}">
                                    {{get_cat_by_id($item['category'])->name}}
                                </a>
                            </h3>
                        </li>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($item['categories'] as $category)
                        {{-- {{dd($category)}} --}}
                        @if ($i <= 7) <li><a pr="list_vn"
                                href="{{route('listSongByCategory',['slug'=> get_cat_by_id($category)->slug])}}"
                                rel="dofollow"
                                title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                            @elseif($i == 8)
                            <li class="view_more_list" style="z-index: 1002;">
                                <div class="dot_more"><span class="dot_cricle"></span><span
                                        class="dot_cricle"></span><span class="dot_cricle"></span> <span
                                        id="listchild_vn">Xem thêm</span></div>
                                <ul>
                                    @elseif($i > 8 && $i <= count($item['categories'])) <li><a pr="listchild_vn"
                                            href="{{route('listSongByCategory',['slug'=> get_cat_by_id($category)->slug])}}"
                                            rel="dofollow"
                                            title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a>
                            </li>
                            @if ($i == count($item['categories']))
                    </ul>
                    </li>
                    @endif

                    @endif

                    <?php $i++ ?>
                    @endforeach
                    @endforeach
                    @endif
                    </ul>
                </div>
                 <div class="showNotiWishList"></div>
                <div class="home-list-item">
                    <div class="tile_box_key">
                    <h2 class="title_of_box_video"><a title="" href="{{route('indexSong')}}">Mới & Hot</a></h2>
                        <div class="btn_view_select">

                            <a href="{{route('indexSong')}}" class="{{Request::segment(1) == 'bai-hat.html' && Request::segment(2) == null ? 'active' : ''}}" title="Hot nhất">Hot nhất</a>
                            <a href="{{route('indexSongNew')}}" class="{{Request::segment(1) == 'bai-hat' && Request::segment(2) == 'moi-nhat.html' ? 'active' : ''}}" title="Mới nhất">Mới nhất</a>

                        </div>
                    </div>
                  <div class="list_music_full">
                    <div class="tile_box_key">
                        {{-- <h2><a title="Beat" href="javascript:;" class="nomore">Beat Hà anh tuấn</a></h2> --}}
                    </div>
                    @if (count($songs) > 0 && !empty($songs))
                        <ul class="list_item_music">
                            @foreach ($songs as $song)
                            <li>
                                <div class="item_content">
                                    <h3 style="display: inline;"><a
                                            href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                                            class="name_song">{{!empty($song->name) ? $song->name : ''}}</a></h3> - <span
                                        style="color: #9a9a9a;">
                                        @foreach ($song->artists as $key => $item)
                                        <a href="{{route('artistDetail',['slug' => $item->slug])}}" class="name_singer"
                                            target="_blank">{{$item->name}}</a>
                                        @if ($key+1 < count($song->artists))
                                            ,
                                            @endif
                                            @endforeach


                                    </span>
                                </div>
                                <div class="list_mark">
                                    <span class="icon-tag-official" title="Bản Chính Thức">Official</span>

                                    <span class="icon-tag-hd" title="High Quality (Chất Lượng Cao)">HQ</span>
                                </div>

                                <span id="NCTCounter_sg_4603090" class="icon_listen" wgct="1">{{$song->listen}}</span>
                                <a href="h{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                                    class="button_new_window" title="Nghe bài hát {{$song->name}} ở cửa sổ mới"></a>
                                <div class="fright_button_add_playlist"><a
                                        style="cursor:pointer" onclick="addWishList('{{$song->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')"
                                        id="btnShowBoxPlaylist_ipcniQ31onj1"  class="button_add_playlist"
                                        title="Thêm bài hát {{$song->name}} vào playlist yêu thích"></a> </div>
                                <a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}" class="button_playing"
                                    title="Nghe bài hát {{$song->name}} này"></a>
                            </li>
                            @endforeach
                        </ul>
                        {{$songs->links()}}
                    @else
                        <div class="alert alert-primary" role="alert" style="color:white;font-weight:bold;background-color:#3498db">
                            Chưa có bài hát !!!
                        </div>
                    @endif



                </div>
                </div>

            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
    <div class="sidebar">

        @include('client.layout.singer-trending')
        @include('client.layout.song_chart')
        @include('client.layout.singer_hot')


        @include('client.layout.w_top100')
        {{-- ****************** --}}
        {{ Sidebar::top100New() }}
    </div>
</div>
        </div>
    </div>
</div>

@endsection
