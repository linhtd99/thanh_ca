@extends('client.layout.layout_trinh-nghe-nhac')

@section('page_title')
Top 100  bài hát {{ $category->name }}
@endsection
<style>
    [role=button] {
        color: white;
        cursor: pointer;
    }

</style>
@section('content')
<div class="container" style="margin-bottom:150px">
    <div class="row" style="margin-top:20px">
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="container-head">
                <div class="container-song">
                    <div class="song-box">
                        <div class="blur-container"
                            style="background: url('{{ !empty($playlist->image) ? $playlist->image : 'https://i.imgur.com/qPsUxNg.jpg' }}') center center / cover no-repeat rgb(157, 147, 125); height: 220px;">
                        </div>
                        <div class="song-info">
                            <div class="row">
                                <div class="col-md-8 col-xs-12 col-sm-7 hh-box-left">
                                    <div class="song-img">
                                        <a href="#"><img src="{{ !empty($playlist->image) ? $playlist->image : 'https://i.imgur.com/qPsUxNg.jpg' }}" alt="Top 100 bài hát  {{ $category->name }}"></a>
                                    </div>
                                    <div class="left-info">
                                        <div class="ranking"><a role="button"></a></div>
                                        <h3>Top 100 bài hát  {{ $category->name }}</h3>
                                        <div class="artist-name"><a role="button" class=""></a></div>
                                        <div class="subtext album">Top 100 bài hát : <a role="button" class="ml-5"
                                                title="{{ !empty($playlist->name) ? $playlist->name : '' }} (Single)" href="#">{{ !empty($playlist->name) ? $playlist->name : '' }}</a>
                                        </div>
                                        <div class="subtext authors">Upload bởi:
                                            @if (empty($playlist->customer_id))
                                            <a role="buttllon">Admin</a>
                                            @else
                                            @if ($playlist->customer_id->name == null)
                                            <a role="button"
                                                href="{{route('trangCaNhan',['username' => $playlist->customer_id->username])}}"
                                                title="{{$playlist->customer_id->username}}">{{$playlist->customer_id->username}}</a>
                                            @else
                                            <a role="button"
                                                href="{{route('trangCaNhan',['username' => $playlist->customer_id->username])}}"
                                                title="{{$playlist->customer_id->name}}">{{$playlist->customer_id->name}}</a>
                                            @endif
                                            @endif</div>
                                        <div class="subtext category">Thể loại:
                                            <a role="button" class="mr-2" title="Việt Nam" href="#">Việt Nam</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-12">
                                    <div class="social-share-block">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}"
                                                    target="_blank"><img src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/facebook.svg" alt=""></a></li>
                                            <li><a href="#" class="zalo-share-button" data-customize=true data-oaid="579745863508352884"><img
                                                        src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/zalo.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                    <div class="log-stats">
                                        {{-- <div class="viewed"><i class="fa fa-play" aria-hidden="true"></i> {{$playlist->views}}
                                    </div> --}}
                                    {{-- <a class="action liked"><i class="fa fa-heart" aria-hidden="true"></i> {{Request::segment(1) == 'album' ? !empty(countListenAlbumHelper($playlist->code)) ? countListenAlbumHelper($playlist->code)->listen : '0' : !empty(countListenPlaylistHelper($playlist->code)) ? countListenPlaylistHelper($playlist->code)['listen'] : '0' }}</a>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-btn">
                <ul>
                    <li><a href="#" class="btn-play-pause"><i class="fa fa-play" aria-hidden="true"></i> Nghe bài
                            hát</a>
                    </li>
                    {{-- <li><a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Thêm vào danh sách phát</a></li> --}}
                    {{-- <li style="display: none"><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> Thích</a></li> --}}
                    {{-- <li style="display: none"><a href="#"><i class="fa fa-download" aria-hidden="true"></i> Tải xuống</a></li> --}}
                    {{-- <li style="display: none"><a href="#" class="more">...</a></li> --}}
                </ul>
            </div>
            <div class="comment tab-box-music">
                <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo {{$dataGenerals->name}}</a></li>
                    <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
                </ul>
                <div class="tab-content">
                    <div id="cmt_cus" class="tab-pane fade in active">
                        <div class="comment-header">
                            <div class="comment-total">{{!empty($totalComment) ? $totalComment->total_comment : '0' }} Bình luận
                            </div>
                            <div class="sorting-block-wrapper clearfix">
                                <div class="sorting-by-wrapper">
                                    <a>
                                        <div class="active-sorting">Bình luận mới nhất <i class="fa fa-angle-down"
                                                aria-hidden="true"></i></div>
                                    </a>
                                    <div class="dropdown-menu-list sort-comment z-hide">
                                        <ul>

                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{Request::url() }}?s=highlight">Bình
                                                        luận nổi bật</a></div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{Request::url() }}">Bình
                                                        luận mới nhất</a></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list-wrapper">
                            <ul class="list-comment">

                                @if (count($comments) > 0)
                                @foreach ($comments as $comment)
                                <li class="comment-item parent_comment" idCommentCurrent="{{$comment->id}}">
                                    <p class="medium-circle-card comment-avatar"><img
                                            src="{{!empty($comment->customer->avatar) ? $comment->customer->avatar : asset('client/img/default-avatar.png')}}"
                                            alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <p class="username">
                                            <span>{{!empty($comment->customer->name) ? $comment->customer->name : $comment->customer->username }}</span><span
                                                class="reply-ago-time">{{ $comment->date }}</span></p>
                                        <p class="content">{{$comment->content}}</p>
                                        <div class="func-comment">
                                            <a style="cursor:pointer" class="z-like-rate
                                                <?php
                                                    if (Auth::guard('customers')->check()) {
                                                        if (!empty($comment->arr_customer_id)) {
                                                            $arr_customer_id = explode(',',$comment->arr_customer_id);
                                                            foreach ($arr_customer_id as $key => $customer_id) {
                                                                if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                    echo "active";
                                                                } else {
                                                                    echo "";
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>
                                                "
                                                onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$comment->id}})"><i
                                                    class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                    class="total_like">{{!empty($comment->total_like) ? $comment->total_like : '0'}}</span></a>

                                            <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                            @if (Auth::guard('customers')->check())
                                            @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                            <a class="delete-comment" style="cursor:pointer;color:red"
                                                url="{{url('delete_comment/'.$comment->id)}}">Xóa</a>
                                            @endif
                                            @endif
                                        </div>
                                        @php
                                        $comments_reply = $comment->getChildComment();
                                        @endphp
                                        <div class="comment-reply-list-wrapper">
                                            <ul class="list-comment list-comment-reply">
                                                @foreach ($comments_reply as $commentChild)
                                                <li class="comment-item">
                                                    <p class="medium-circle-card comment-avatar"><img
                                                            src="{{!empty($commentChild->customer->avatar) ? $commentChild->customer->avatar : asset('client/img/default-avatar.png')}}"
                                                            alt="Zing MP3"></p>
                                                    <div class="post-comment">
                                                        <p class="username">
                                                            <span>{{!empty($commentChild->customer->name) ? $commentChild->customer->name : $commentChild->customer->username }}</span><span
                                                                class="reply-ago-time">{{ $commentChild->date }}</span></p>
                                                        <p class="content">{{$commentChild->content}}</p>
                                                        <div class="func-comment">
                                                            <a style="cursor:pointer" class="z-like-rate
                                                                <?php
                                                                    if (Auth::guard('customers')->check()) {
                                                                        if (!empty($commentChild->arr_customer_id)) {
                                                                            $arr_customer_id = explode(',',$commentChild->arr_customer_id);
                                                                            foreach ($arr_customer_id as $key => $customer_id) {
                                                                            if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                                echo "active";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            }
                                                                        }

                                                                    } else {
                                                                        echo "";
                                                                    }

                                                                ?>
                                                                "
                                                                onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$commentChild->id}})"><i
                                                                    class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                                    class="total_like">{{!empty($commentChild->total_like) ? $commentChild->total_like : '0'}}</span></a>

                                                            <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                            @if (Auth::guard('customers')->check())
                                                            @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                            <a class="delete-comment" style="cursor:pointer;color:red"
                                                                url="{{url('delete_comment/'.$commentChild->id)}}">Xóa</a>
                                                            @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif


                                @if (!Auth::guard('customers')->check())
                                <div  style="text-align: center"  class="btn-login">
                                    <button  style="text-align: center"  class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal-login">Đăng nhâp để bình luận</button>
                                </div>
                                @else

                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img
                                            src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                            alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <form method="POST" action="{{route('saveComment')}}">
                                            @csrf
                                            <input type="hidden" name="status" value="-1">
                                            <input type="hidden" name="code" value="{{'top100.'.Request::segment(3)}}">
                                            <input type="hidden" name="customer_id"
                                                value="{{ Auth::guard('customers')->user()->id }}">
                                            <input type="hidden" name="parent_id" value="0">
                                            <textarea class="form-control" placeholder="" name="content" id="content"></textarea>
                                            <button type="submit" class="btn btn-success green"> Lưu</button>
                                        </form>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="more-cmt" code="{{Request::segment(3)}}" comment-current="10"
                            s="{{!empty($_GET['s']) ? $_GET['s'] : 'normal'}}">
                            <a onclick="more_comment(this)">Xem thêm</a>
                        </div>
                    </div>
                    <div id="cmt_fb" class="tab-pane fade">
                        <div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
            {{-- <div class="lyric" id="_divLyricHtml">
                <div class="pd_name_lyric">
                    <h2 class="name_lyric"><b>Lời bài hát:
                            @if (!empty($_GET['c']) && !empty($_GET['st']))
                            {{getInfoVideo($_GET['c'])->name}}
                            @else
                            {{$charts[0]->name}}
                            @endif
                        </b></h2>
                    <p class="name_post">Lời đăng bởi: <a href="#" title="">
                            admin
                        </a></p>
                </div>
                <p id="divLyric" class="pd_lyric trans" style="height:auto;max-height:255px;overflow:hidden;">
                    @if (!empty($_GET['c']) && !empty($_GET['st']))
                    @php echo !empty(getInfoVideo($_GET['c'])->description) ? getInfoVideo($_GET['c'])->description : 'Chưa có lời
                    bài hát !!!' @endphp
                    @else
                    @php echo !empty($charts[0]->description) ? $charts[0]->description : 'Chưa có lời bài hát !!!' @endphp
                    @endif

                </p>

                <div class="more_add" id="divMoreAddLyric">
                    <a href="#" id="seeMoreLyric" title="Xem toàn bộ" class="btn_view_more">Xem toàn bộ<span
                            class="down"></span></a>
                    <a href="#" id="hideMoreLyric" title="Thu gọn" class="btn_view_hide hide-hh">Thu gọn<span class="up"></span></a>

                </div>
            </div> --}}

        </div>
        <div class="box_menu_player new" style="border-top: solid 1px #ececec;">
            <div class="user_upload">

            </div>

        </div>

        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
            </div>
            <ul>
                @if (!empty($playList4) && count($playList4) > 0)
                @foreach ($playList4 as $item)
                <li>
                    <div class="box-left-album">
                        <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                            class="box_absolute" title="{{$item->name}}">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335"
                                        wgct="1">{{ !empty(countListenPlaylistHelper($item->code)->listen) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                    title="{{$item->name}}"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a
                                href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                class="name_song" title="{{$item->name}}">{{$item->name}}</a>
                        </h3>
                        {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4> --}}
                    </div>
                </li>
                @endforeach
                @else
                <div class="alert alert-primary" role="alert"
                    style="background: #69bbe6;color: white;font-weight: bold;">
                    Chưa có playlist !!!
                </div>
                @endif
            </ul>
        </div>
        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
            </div>
            <div class="home-mv">
                <div class="row">
                    {{-- {{ dd($mv) }} --}}
                    <?php $i = 1 ?>
                    @foreach ($mv as $video1)

                    <div class="col-md-3 col-xs-6 col-sm-3">
                        <div class="videosmall">
                            <div class="box_absolute">
                                <span class="view_mv"><span
                                        class="icon_view"></span><span>{{!empty(countListenHelper($video1['code'])['listen']) ? countListenHelper($video1['code'])['listen'] : '0' }}</span></span>
                                <span class="tab_lable_"></span>
                                <a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code']])}}"
                                    title="{{$video1['name']}}" class="img">
                                    <span class="icon_play"></span>
                                    <img src="{{!empty($video1['image']) ? $video1['image'] : getVideoYoutubeApi($video1['link'])['thumbnail']['mqDefault']}}"
                                        alt="{{$video1['name']}}" title="{{$video1['name']}}">
                                </a>
                                <span
                                    class="icon_time_video">{{getVideoYoutubeApi($video1['link'])['duration_sec']}}</span>
                            </div>
                            <h3><a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code'] ])}}"
                                    title="{{$video1['name']}}" class="name_song_index">{{$video1['name']}}</a></h3>
                            <h4>
                                {{-- {{dd($video1->artists)}} --}}
                                @if (!empty($video1['artists']) && count($video1['artists']) > 0)
                                @foreach ($video1['artists'] as $key => $item)
                                <a href="#" class="name_singer" title="{{$item['name']}}"
                                    target="_blank">{{$item['name']}}</a>
                                @if (($key+1) < count($video1['artists'])) ,
                                    @elseif(($key+1)==count($video1['artists']))@endif @endforeach @else <a>Chưa cập
                                    nhật</a>
                                    @endif
                            </h4>
                        </div>
                    </div>

                    <?php
                                           if ($i == 4) {
                                                  break;
                                            };
                                            $i++;
                                    ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="sidebar">
            {{ Sidebar::top100New() }}

        </div>
    </div>
</div>
</div>


<div id='player'>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="mainwrap">
                    <div id="nowPlay">
                        <span id="npTitle"></span>
                    </div>
                    <div id="audiowrap">
                        <div id="audio0">
                            <div class="next-track">
                                <a id="btnPrev"><i class="fa fa-step-backward" aria-hidden="true"></i></a>
                                <a id="btnNext"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
                            </div>
                            <a class="shuffle-hh"><i class="fa fa-random" aria-hidden="true"></i></a>
                            <a class="z-expand-btn "><i class="fa fa-window-restore" title="Chế độ thu nhỏ"></i></a>
                            <a class="z-mini-media-btn"><i class="fa fa-window-maximize" aria-hidden="true"></i></a>
                            <audio id="audio1" preload controls>Your browser does not support HTML5 Audio! 😢</audio>
                            <div class="z-playlist-wraper">
                                <a class="z-btn-playlist-expand ">
                                    <i class="fa fa-list-alt"></i>
                                    <span class="z-playlist-title">
                                        <span class="z-content" style="margin-right: 2px;">Danh sách phát</span>
                                        <span class="z-item-total">(1)</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="plwrap">
    <div class="container">
        <div class="box-list">
            <button type="button" class="btn-down"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <div class="left-list">
                        <div class="z-head">
                            <div class="z-input-group z-miniplayer-search-group pull-left">
                                <label>
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    <div class="input-wrapper">
                                        <input type="text" class="form-control" placeholder="Tìm kiếm..." value="">
                                    </div>
                                </label>
                            </div>
                            <div class="z-show">
                                <label class="z-checkbox">
                                    <span class="z-checkbox-title">DANH SÁCH PHÁT (1)</span>
                                </label>
                                <div class="z-btn-features">
                                    <div class="list-buttons">
                                        <ul class="hover-view ">
                                            <li>
                                                <a class="shuffle-hh" title="Nghe ngẫu nhiên">
                                                    <i class="fa fa-random" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="z-btn-repeat " title="Lặp lại">
                                                    <i class="fa fa-retweet" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" class="z-btn-search" title="Tìm kiếm">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="z-body-area">
                            <ul id="plList" class="nav flex-column"></ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="right-list">
                        <div id="mini__player">
                            <img src="{{ asset('images/song-default.png') }}" alt="">
                            <div class="player">
                                <div id="vid-big"></div>
                            </div>
                            <i class="toggle-icon icon"></i>
                        </div>
                        <div class="false z-song-lyric-wrap">
                            <div class="z-song-name">
                                <a role="button" class="" title="Người Lạ Ơi" href="#"></a>
                            </div>
                            <div class="z-artists">
                                <a role="button" class="" title="" href="#"></a>
                            </div>
                            <div class="z-album">Album:&nbsp;
                                <a role="button" class="" href="#"></a>
                            </div>
                        </div>
                        <div class="z-lyric-text-wrap">
                            <div id="lyrics1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<div class="lyrics-box">
    <div class="z-blur-bg"
        style="background-image: url('https://photo-resize-zmp3.zadn.vn/w240_r1x1_jpeg/cover/c/a/9/d/ca9d4b6cd6252d7a364c74afabf06958.jpg');">
    </div>
    <div id="lyrics">
    </div>
    <div class="player1">
        <div id="vid"></div>
    </div>
    <i class="toggle-icon icon"></i>
</div>
@php

@endphp
@endsection
@section('js')
<script src="https://cdn.plyr.io/3.5.6/plyr.js"></script>
<script src="{{asset('client/js/rabbit-lyrics.js')}}" type="text/javascript"></script>
<script src="https://www.youtube.com/iframe_api"></script>

<script>



    var json_listSong = '<?= json_encode($json_listSong) ?>';
    var player;

    listSong = JSON.parse(json_listSong);

    if (listSong[0].video != null) {
        function onYouTubeIframeAPIReady() {
            var url = listSong[0].video;
            var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
            player = new YT.Player('vid-big', {
                height: '390',
                width: '640',
                videoId: videoid[1],
                playerVars: {
                    controls: 0,
                    disablekb: 1,
                    mute: 1
                },
            });
            load_iframe1('vid', videoid[1]);
            $('#lyrics').css('display', 'none');
        };
        // setTimeout(function(){
        //         var duration = formatTime(player.getDuration());
        //         // jQuery('#audio0').find('.plyr__time--duration').text(duration);
        // }, 3000);

    } else {
        $('#lyrics').css('display', 'block');
    }

    $('body').on('click', '.plyr__controls > button:eq(1)', function () {
        player.seekTo(0);
        player1.seekTo(0);
    });

    $('body').on('mouseup touchend', '.plyr__controls .plyr__progress input ', function (e) {

        var newTime = player.getDuration() * (e.target.value / 100);

        // Skip video to new time.
        player.seekTo(newTime);
        player1.seekTo(newTime);
    });

    // This function is called by initialize()
    function updateProgressBar() {
        // Update the value of our progress bar accordingly.
        $('.plyr__controls .plyr__progress input').val((player.getCurrentTime() / player.getDuration()) * 100);
        $('.plyr__controls .plyr__progress input').val((player1.getCurrentTime() / player1.getDuration()) * 100);
    }

    function load_iframe(selectedId, vid) {
        player = new YT.Player(selectedId, {
            height: '390',
            width: '640',
            videoId: vid,
            playerVars: {
                controls: 0,
                disablekb: 1,
                mute: 1
            },
        });
    }

    function load_iframe1(selectedId, vid) {
        player1 = new YT.Player(selectedId, {
            height: '100%',
            width: '100%',
            videoId: vid,
            playerVars: {
                controls: 0,
                disablekb: 1,
                mute: 1
            },
        });
    }


    function onPlayerReady(event) {
        event.target.playVideo();
    }

    var done = false;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }

    function formatTime(time) {
        time = Math.round(time);

        var minutes = Math.floor(time / 60),
            seconds = time - minutes * 60;

        seconds = seconds < 10 ? '0' + seconds : seconds;
        return minutes + ":" + seconds;
    }

    jQuery(function () {
        $('footer').css('display', 'none');
    });
    jQuery(function ($) {
        'use strict';
        var supportsAudio = !!document.createElement('audio').canPlayType;
        if (supportsAudio) {
            // initialize plyr
            var playersong = new Plyr('#audio1', {
                controls: ['play-large', // The large play button in the center

                    //'rewind', // Rewind by the seek time (default 10 seconds)
                    'play', // Play/pause playback
                    //'fast-forward', // Fast forward by the seek time (default 10 seconds)
                    'restart', // Restart playback
                    'progress', // The progress bar and scrubber for playback and buffering
                    'current-time', // The current time of playback
                    'duration', // The full duration of the media
                    'mute', // Toggle mute
                    'volume', // Volume control
                    'captions', // Toggle captions
                    //'settings', // Settings menu
                    'pip', // Picture-in-picture (currently Safari only)
                    'airplay', // Airplay (currently Safari only)
                    'fullscreen', // Toggle fullscreen
                ]
            });
            // initialize playlist and controls
            var index = 0,
                playing = false,
                playrand = false,
                checkvideo = false,
                mediaPath = 'audio/',
                extension = '',
                tracks = JSON.parse(json_listSong),
                buildPlaylist = $(tracks).each(function (key, value) {
                    var trackNumber = value.track,
                        trackName = value.name,
                        trackDuration = value.duration;
                    if (trackNumber.toString().length === 1) {
                        trackNumber = '0' + trackNumber;
                    }
                    $('#plList').append(`<li class="nav-item">
                            <div class="plItem">
                            <span class="plNum">${trackNumber}</span>
                            <span class="plTitle">${trackName}</span>
                        </li>`);
                }),
                trackCount = tracks.length,
                npAction = $('#npAction'),
                npTitle = $('#npTitle'),
                audio = $('#audio1').on('play', function () {
                    if (tracks[index].video != null) {
                        player.playVideo();
                        player1.playVideo();
                    }

                    jQuery('.tab-btn>ul>li:first-child>a').children('i').removeClass('fa-play');
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').addClass('fa-pause');
                    jQuery('.tab-btn>ul>li:first-child>a').addClass('btn-pause-tab');
                    jQuery('.tab-btn>ul>li:first-child>a').removeClass('btn-play-pause');
                    var y = jQuery('.tab-btn>ul>li:first-child>a').html();
                    y = y.replace('Nghe bài hát', 'Tạm dừng');
                    jQuery('.tab-btn>ul>li:first-child>a').html(y);
                    // jQuery('#vid, #vid-big').trigger('play');
                    jQuery('.lyrics-box').children('.icon').removeClass('show-icon');


                    playing = true;
                    npAction.text('Now Playing...');
                }).on('pause', function () {

                    if (tracks[index].video != null) {
                        player.pauseVideo();
                        player1.pauseVideo();
                    }
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').addClass('fa-play');
                    jQuery('.tab-btn>ul>li:first-child>a').children('i').removeClass('fa-pause');
                    jQuery('.tab-btn>ul>li:first-child>a').removeClass('btn-pause-tab');
                    jQuery('.tab-btn>ul>li:first-child>a').addClass('btn-play-pause');
                    var y = jQuery('.tab-btn>ul>li:first-child>a').html();
                    y = y.replace('Tạm dừng', 'Nghe bài hát');
                    jQuery('.tab-btn>ul>li:first-child>a').html(y);
                    // jQuery('#vid,#vid-big').trigger('pause');
                    jQuery('.lyrics-box').children('.icon').addClass('show-icon');
                    playing = false;
                    npAction.text('Paused...');
                }).on('ended', function () {
                    npAction.text('Paused...');

                    if (playrand) {
                        var min = 0;
                        var max = 4;
                        var random = Math.floor(Math.random() * 10 % (max - min + 1)) + min;
                        index = random;
                        loadTrack(index);
                        audio.play();
                    } else {
                        if ((index + 1) < trackCount) {
                            index++;
                            loadTrack(index);
                            audio.play();
                        } else {
                            audio.pause();
                            index = 0;
                            loadTrack(index);
                        }
                    }

                }).get(0),
                rand = $('.shuffle-hh').on('click', function () {
                    playrand = !playrand;
                }),

                btnPrev = $('#btnPrev').on('click', function () {
                    if ((index - 1) > -1) {
                        index--;
                        let title = tracks[index].name;
                        $('.z-song-name a').text(title)
                        jQuery('#audio1').trigger('play');
                        if (tracks[index].video != null) {
                            // console.log(tracks[index].video);
                            var url = tracks[index].video;
                            var videoid = url.match(
                                /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
                                );
                            jQuery('.player').html('<div id="vid-big"></div>');
                            jQuery('.player1').html('<div id="vid"></div>');
                            load_iframe('vid-big', videoid[1]);
                            load_iframe1('vid', videoid[1]);
                            $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`)

                        } else {
                            jQuery('.player').html('<div id="vid-big"></div>');
                            jQuery('.player1').html('<div id="vid"></div>');
                            let image = tracks[index].image;

                            if (tracks[index].image != 'null') {
                                // console.log('có ảnh');
                                $('#mini__player > img').attr('src', image)
                            } else {
                                // console.log('ko');
                                $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`)
                            }
                        }

                        let singer = tracks[index].singer;
                        $('.z-artists a').text(singer)

                        let album = tracks[index].album;
                        $('.z-album a').text(album)

                        loadTrack(index);
                        if (playing) {
                            audio.play();
                        }
                    } else {
                        audio.pause();
                        index = 0;
                        loadTrack(index);
                    }
                }),
                btnNext = $('#btnNext').on('click', function () {
                    if ((index + 1) < trackCount) {
                        index++;
                        loadTrack(index);
                        if (playing) {
                            audio.play();
                        }
                    } else {
                        audio.pause();
                        index = 0;
                        loadTrack(index);
                    }
                    let title = tracks[index].name;
                    $('.z-song-name a').text(title)


                    if (tracks[index].video != null) {
                        jQuery('.player').html('<div id="vid-big"></div>');
                        jQuery('.player1').html('<div id="vid"></div>');
                        var url = tracks[index].video;
                        var videoid = url.match(
                            /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
                            );
                        load_iframe('vid-big', videoid[1]);
                        load_iframe1('vid', videoid[1]);
                        $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`);

                    } else {
                        jQuery('.player').html('<div id="vid-big"></div>');
                        jQuery('.player1').html('<div id="vid"></div>');
                        let image = tracks[index].image;

                        if (tracks[index].image != 'null') {
                            // console.log('có ảnh');
                            $('#mini__player > img').attr('src', image)
                        } else {
                            // console.log('ko');
                            $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`)
                        }
                    }

                    let singer = tracks[index].singer;
                    $('.z-artists a').text(singer)

                    let album = tracks[index].album;
                    $('.z-album a').text(album)
                    jQuery('#audio1').trigger('play');
                }),
                li = $('#plList li').on('click', function () {
                    var id = parseInt($(this).index());

                    if (id !== index) {
                        let title = tracks[id].name;
                        $('.z-song-name a').text(title);

                        if (tracks[id].video != null) {
                            jQuery('.player').html('<div id="vid-big"></div>');
                            jQuery('.player1').html('<div id="vid"></div>');
                            var url = tracks[id].video;
                            var videoid = url.match(
                                /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
                                );
                            load_iframe('vid-big', videoid[1]);
                            load_iframe1('vid', videoid[1]);
                            $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`);
                            let singer = tracks[id].singer;
                            $('.z-artists a').text(singer)

                            let album = tracks[id].album;
                            $('.z-album a').text(album)

                            playTrack(id);
                            $('#mini__player').addClass('active');
                            player.playVideo();
                            player1.playVideo();

                        } else {
                            jQuery('.player').html('<div id="vid-big"></div>');
                            jQuery('.player1').html('<div id="vid"></div>');
                            let image = tracks[id].image;

                            if (tracks[id].image != 'null') {
                                // console.log('có ảnh');
                                $('#mini__player > img').attr('src', image)
                            } else {
                                // console.log('ko');
                                $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`);
                            }
                            let singer = tracks[id].singer;
                            $('.z-artists a').text(singer)

                            let album = tracks[id].album;
                            $('.z-album a').text(album)
                            jQuery('#audio1').trigger('play');
                            playTrack(id);
                            $('#mini__player').addClass('active');

                            audio.play();
                        }


                    }
                }),
                loadTrack = function (id) {
                    // console.log(tracks[id]);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    setTimeout(function () {

                        $.ajax({

                            type: 'POST',

                            url: '{{route("songListen")}}',

                            data: {
                                code: tracks[id].code
                            },

                            success: function (data) {
                                console.log('+view song')

                            }

                        });



                    }, 150000);
                    jQuery('#audio1').attr('src', tracks[id].filename);
                    $('.plSel').removeClass('plSel');
                    $('#plList li:eq(' + id + ')').addClass('plSel');
                    npTitle.text(tracks[id].name);
                    index = id;

                    /**
                     * Lyrics
                     */
                    let text = '';
                    // file_lyrics = tracks[id].lyrics;
                    // console.log(tracks);
                    if (tracks[id].lyrics.length != 0) {
                        // let Lyrics = jQuery(mediaPath + file_lyrics, function(data){
                        //     console.log(data);
                        tracks[id].lyrics.forEach((val, key) => {
                            // console.log(val)
                            text += `[${val.time}] ${val.text} \n`;
                        });
                        // console.log(text);
                        $('#lyrics').text(text);
                        $('#lyrics1').text(text);
                        // }).then(()=>{
                        new RabbitLyrics({
                            audioElement: $('#audio1'),
                            lyricsElement: $('#lyrics1')
                        });
                        new RabbitLyrics({
                            audioElement: $('#audio1'),
                            lyricsElement: $('#lyrics')
                        });
                        // });
                    } else {
                        $('#lyrics').text('Khong Co Sub');
                    }


                    if (tracks[id].video != null) {
                        var url = tracks[id].video;
                        var videoid = url.match(
                            /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
                            );
                        $('.player').html('<div id="vid-big"><div>');
                        $('.player1').html('<div id="vid"><div>');
                        load_iframe('vid-big', videoid[1]);
                        load_iframe1('vid', videoid[1]);
                        // setTimeout(function(){
                        // var duration =formatTime(player.getDuration());
                        // console.log(duration);
                        // jQuery('#audio0').find('.plyr__time--duration').text(duration);
                        // }, 1700);
                        setTimeout(function () {
                            jQuery('#audio1').trigger('play');
                        }, 2000);





                        checkvideo = false;
                        $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`)
                        jQuery('.lyrics-box').children('#lyrics').hide();
                        jQuery('#mini__player img').hide();
                        jQuery('#vid-big').show();
                        jQuery('#audio1').attr('src', tracks[id].filename);
                    } else {

                        jQuery('.player').html('<div id="vid-big"></div>');
                        jQuery('.player1').html('<div id="vid"></div>');
                        let image = tracks[id].image;

                        if (tracks[id].image != 'null') {
                            // console.log('có ảnh');
                            $('#mini__player > img').attr('src', image)
                        } else {

                            $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`)
                        }
                        jQuery('#vid').html('');
                        // jQuery('#vid-big').removeAttr('src');
                        // $('.player').html('<div id="vid-big"><div>');
                        checkvideo = false;
                        jQuery('.lyrics-box').children('#lyrics').show();
                        jQuery('#mini__player img').show();
                        // jQuery('#vid').trigger('pause');
                        // jQuery('#vid-big').trigger('pause');
                        jQuery('#vid-big').hide();

                    }

                    // audio.src = mediaPath + tracks[id].file + extension;
                    audio.src = tracks[id].filename;

                },
                playTrack = function (id) {

                    loadTrack(id);
                    if (checkvideo == false) audio.play();
                };
            extension = audio.canPlayType('audio/mpeg') ? '.mp3' : '';
            loadTrack(index);


        } else {
            // boo hoo

            $('.column').addClass('hidden');
            var noSupport = $('#audio1').text();
            $('.container').append('<p class="no-support">' + noSupport + '</p>');
        }
        jQuery(document).ready(function () {

            let title = tracks[index].name;
            $('.z-song-name a').text(title)


            let image = tracks[index].image;
            if (image != 'null') {

                $('#mini__player > img').attr('src', image);
            } else {

                $('#mini__player > img').attr('src', `{{ asset('images/song-default.png') }}`);
            }

            let singer = tracks[index].singer;
            $('.z-artists a').text(singer)

            let album = tracks[index].album;
            $('.z-album a').text(album)

            var number_song = jQuery('.z-item-total').html();
            var numbersg = jQuery('.z-checkbox-title').html();

            numbersg = numbersg.replace('1', trackCount);
            number_song = number_song.replace('1', trackCount);

            jQuery('.z-checkbox-title').html(numbersg);
            jQuery('.z-item-total').html(number_song);


        });

    });

    $(document).on('click','.delete-comment',function(e){
        e.preventDefault();
        r = confirm("Xóa comment!!!");
        if (r == true) {
            window.location.href = $(this).attr('url');
        } else {
            txt = "You pressed Cancel!";
        }
    })

$(document).on('click', '.reply-func', function () {
        <?php
            if (Auth::guard('customers')->check()){
        ?>
            if($(this).hasClass('have_write_comment')){
                $(this).removeClass('have_write_comment');
                $(this).parents('.comment-item').find('.comment-remove').remove();
            }else{
                $(this).addClass('have_write_comment');
                if ($(this).parents('.comment-item').has('li.comment-remove')) {
                    $(this).parents('.comment-item').find('.comment-remove').remove();
                    parent_id = $(this).parents('.parent_comment').attr('idcommentcurrent');
                    username = $(this).parent('.func-comment').parent('.post-comment').find('.username > span:eq(0)').text();
                    // console.log(username);
                    $(this).parents('.comment-item').find('.comment-reply-list-wrapper').append(
                    `<li class="comment-item comment-remove">
                        <p class="medium-circle-card comment-avatar"><img
                                src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <form method="POST" action="{{route('saveComment')}}">
                                @csrf
                                <input type="hidden" name="status" value="-1">
                                <input type="hidden" name="code" value="{{'top100.'.Request::segment(3)}}">
                                <input type="hidden" name="customer_id" value="{{ !empty(Auth::guard('customers')->check()) ? Auth::guard('customers')->user()->id : '' }}">
                                <input type="hidden" name="parent_id" value="${parent_id}" >
                                <textarea class="form-control" placeholder="" name="content" id="content">@${username}:</textarea>
                                <button type="submit" class="btn btn-success green"> Lưu</button>
                            </form>
                            <div class="comment-reply-list-wrapper"></div>
                        </div>
                    </li>`);
                };

            }
        <?php
            }else{
        ?>
            alert('Đăng nhập để sử dụng chức năng này !!!');
        <?php
            }
        ?>

});


</script>





@endsection
