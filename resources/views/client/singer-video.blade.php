@extends('client.layout.layout_artist')
@section('page_title')
Nghệ sĩ {{!empty($artist->name) ? $artist->name : ''}}
@endsection
@section('content')

<div class="box-content singer-detail-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">   
                <div class="home-list-item">
                    <div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">MV {{!empty($artist->name) ? $artist->name : ''}}</a>
                        </h2>
                    </div>
                    <div class="home-mv">
                        @if (count($mvs) != 0)
                        <div class="row">
                            @foreach ($mvs as $mv)
                            
                            <div class="col-md-3 col-xs-6 col-sm-3">
                                <div class="videosmall">
                                    <div class="box_absolute">
                                        <span class="view_mv"><span
                                                class="icon_view"></span><span>{{!empty(countListenHelper($mv->code)->listen) ? countListenHelper($mv->code)->listen : '0'}}</span></span>
                                        <span class="tab_lable_"></span>
                                        <a href="{{route('detailVideo',['name' => name_to_slug($mv->name),'code' => $mv->code])}}"
                                            title="{{!empty($mv->name) ? $mv->name : ''}}" class="img">
                                            <span class="icon_play"></span>
                                            <img src="{{!empty($mv->image) ? $mv->image : getVideoYoutubeApi($mv->link)['thumbnail']['mqDefault']}}"
                                                alt="{{!empty($mv->name) ? $mv->name : ''}}"
                                                title="{{!empty($mv->name) ? $mv->name : ''}}">
                                        </a>
                                        <span
                                            class="icon_time_video">{{getVideoYoutubeApi($mv->link)['duration_sec']}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{$mvs->links()}}
                        @else
                        <div class="alert alert-primary" role="alert"
                            style="color:white;font-weight:bold;background-color:#3498db">
                            Chưa có video !!!
                        </div>
                        @endif
                    </div>
                </div>
               
            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="sidebar-singer">
                    {{-- <div class="singer_playlist_new">
						<div class="tile_box_key">
	                        <h3><a title="Playlist mới" class="nomore">Playlist mới</a></h3>  
	                    </div>
	                    <div class="box_name_album_info">
	                       	<div class="info_album">
	                            <div class="avatar_album">
	                                <div class="rotate_album"><img src="https://avatar-nct.nixcdn.com/playlist/2018/10/01/3/9/7/8/1538377959138.jpg" class="rotate" width="164"></div>
	                                <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_12580535" wgct="1">427.091</span></span>
	                                <a href="javascript:;" id="mainAlbum" class="box_font_album box_absolute">
	                                    <span class="icon_play_single_playlist"></span>
	                                </a>
	                                <div class="box_bg_album">
	                                    <a href="#" class="img_avatar"><img src="https://avatar-nct.nixcdn.com/playlist/2018/10/01/3/9/7/8/1538377959138.jpg" class="img_album" width="180"></a>
	                                </div>                	   
	                            </div>  
	                            <h3 style="font-size: 16px; margin-top: 10px; margin-bottom: 20px;"><a href="#">Những Bài Hát Hay Nhất Của MIN</a></h3>                  	                               
	                        </div> 
	                    </div>
	                    <div class="list_chart_music" style="border-top: 1px solid #f3f3f3; padding-top: 10px;">
	                        <ul id="ulSingle">
	                            <li id="itemSong_86c24b7ee708b7974640d52a300ad028">
	                                <div class="info_data"><a title="Bài Này Chill Phết - Đen, MIN" href="javascript:;"class="name_song">Bài Này Chill Phết</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đen trình bày" target="_blank">Đen</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                
	                                <span id="NCTCounter_sg_5978903" class="icon_listen" wgct="1">5.958.752</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                            
	                            <li id="itemSong_4502abfe81cb039491d8443a9597be09">
	                                <div class="info_data"><a title="Em Mới Là Người Yêu Anh - MIN" href="javascript:;" class="name_song">Em Mới Là Người Yêu Anh</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5489449" class="icon_listen" wgct="1">18.202.422</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                            
	                            <li id="itemSong_2cb82719960dd04763d11fa5bdb8ab94">
	                                <div class="info_data"><a title="Đừng Xin Lỗi Nữa (Don't Say Sorry) - ERIK, MIN" href="javascript:;" class="name_song">Đừng Xin Lỗi Nữa (Don't Say Sorry)</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ ERIK trình bày" target="_blank">ERIK</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5355218" class="icon_listen" wgct="1">8.488.846</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                            
	                            <li id="itemSong_2604aee22868a86d0c9f9944190a445f">
	                                <div class="info_data"><a title="Take Me Away - MIN" href="javascript:;" class="name_song">Take Me Away</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_4536349" class="icon_listen" wgct="1">2.100.608</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                            
	                            <li id="itemSong_3cd4fb938ab7d528b80371e51af9106e">
	                                <div class="info_data"><a title="Y.Ê.U (EDM Version) - MIN" href="javascript:;" class="name_song">Y.Ê.U (EDM Version)</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                
	                                <span id="NCTCounter_sg_3827847" class="icon_listen" wgct="1">3.823.511</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                            
	                            <li id="itemSong_31d6632aebd8f161987cc49622e56747">
	                                <div class="info_data"><a title="STEPS2FAME - MIN" href="javascript:;" class="name_song">STEPS2FAME</a>
	                                    <p class="list_name_singer"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ MIN trình bày" target="_blank">MIN</a></p>
	                                </div>
	                                <span id="NCTCounter_sg_5333644" class="icon_listen" wgct="1">1.379.352</span>
	                                <div class="bg_line_status_loading" style="margin-top: 10px;">
	                                </div>
	                            </li>
	                        </ul>
	                    </div>
					</div> --}}

                    <div class="list_singer_hot">
                        <div class="tile_box_key">
                            <h3><a title="Ca Sĩ | Nghệ Sĩ" href="{{route('indexArtist')}}">Ca Sĩ | Nghệ Sĩ</a></h3>
                        </div>
                        <ul>
                            @foreach ($artistsRandom as $item)
                            <li><a href="{{route('artistDetail',['slug' => $item->slug])}}" class="img"
                                    title="{{!empty($item->name) ? $item->name : ''}}"><img
                                        src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}"
                                        title="{{!empty($item->name) ? $item->name : ''}}"
                                        alt="{{!empty($item->name) ? $item->name : ''}}"></a><a
                                    href="{{route('artistDetail',['slug' => $item->slug])}}" class="name_singer_main"
                                    title="{{!empty($item->name) ? $item->name : ''}}">{{!empty($item->name) ? $item->name : ''}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection