@extends('client.layout.master')
@section('page_title')
Video
@endsection
@section('content')

<div class="home list-video-hh">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_cata_control">
                    <ul class="detail_menu_browsing_dashboard" style="margin-bottom: 30px;">
                        <li class="cate hot havelink"><h3><a href="#" rel="nofollow" title="MỚI &amp; HOT" class="active" id="video-moi">MỚI &amp; HOT</a></h3></li>
                        @if (!empty($categories))
                        @foreach ($categories as $item)
                        <li class="line"></li>
                             <li class="cate havelink"><h3>
                                <a style="cursor: pointer;" href="{{route('categoryVideo',['slug' => get_cat_by_id($item['category'])->slug])}}" rel="dofollow" title="{{get_cat_by_id($item['category'])->name}}">
                                    {{get_cat_by_id($item['category'])->name}}
                                </a>
                                </h3>
                        </li>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($item['categories'] as $category)
                            {{-- {{dd($category)}} --}}
                                @if ($i <= 7)
                                <li><a pr="list_vn" href="{{route('categoryVideo',['slug'=> get_cat_by_id($category)->slug])}}" rel="dofollow" title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                                @elseif($i == 8)
                                    <li class="view_more_list" style="z-index: 1002;">
                                        <div class="dot_more"><span class="dot_cricle"></span><span class="dot_cricle"></span><span
                                                class="dot_cricle"></span> <span id="listchild_vn">Xem thêm</span></div>
                                        <ul>
                                @elseif($i > 8 && $i <= count($item['categories']))
                                            <li><a pr="listchild_vn" href="{{route('categoryVideo',['slug'=> get_cat_by_id($category)->slug])}}" rel="dofollow" title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                                            @if ($i == count($item['categories']))
                                                </ul></li>
                                            @endif

                                @endif

                                <?php $i++ ?>
                            @endforeach
                        @endforeach
                       @endif
                    </ul>
                </div>
                @if (!empty($categories))
                @foreach ($categories as $item)
                    {{-- {{dd($categories)}} --}}
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2 class="title_of_box_video"><a title="{{get_cat_by_id($item['category'])->name}}" href="#">{{get_cat_by_id($item['category'])->name}}</a></h2>
                            <div class="btn_view_select">
                                @if (!empty($item['categories']))
                                <?php $i = 0 ?>
                                @foreach ($item['categories'] as $category_id)
                                    <a href="{{route('categoryVideo',['slug'=> get_cat_by_id($category_id)->slug])}}" title="{{get_cat_by_id($category_id)->name}}">{{get_cat_by_id($category_id)->name}}</a>
                                    <?php $i++;
                                        if ($i == 4) {
                                            break;
                                        }
                                    ?>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="home-mv">
                            <div class="row">
                                <?php $i = 1 ?>
								@if (count(get_videos_by_id_cat($item['category'])) > 0)
											@foreach (get_videos_by_id_cat($item['category']) as $video)
											<div class="col-md-3 col-xs-6 col-sm-3">
											<div class="videosmall">
												<div class="box_absolute">
													<span class="view_mv"><span class="icon_view"></span><span>{{ (!empty($video->listens->listen)) ? $video->listens->listen : 0 }}</span></span>
													<span class="tab_lable_"></span>
													<a href="{{route('detailVideo',['name' => name_to_slug($video['name']),'code' => $video['code']])}}" title="{{$video['name']}}" class="img">
														<span class="icon_play"></span>

														<img src="{{!empty($video['image']) ? $video['image'] : getVideoYoutubeApi($video->link)['thumbnail']['mqDefault'] }}"
															alt="{{$video['name']}}"
															title="{{$video['name']}}">
													</a>
												<span class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
												</div>
											</div>
										</div>
										<?php $i++;
											if ($i == 9) {
												break;
											}
										?>
									@endforeach
							   @else
								   <div class="alert alert-primary" role="alert" style="background: #69bbe6;color: white;font-weight: bold;">
										Chưa có video !!!
									</div>
							   @endif



                            </div>
                        </div>
                    </div>
                @endforeach
              @endif

            </div>
			@include('client.layout.sidebar_list_video')
		</div>
	</div>
</div>

@endsection



