@extends('client.layout.master')
@section('page_title')
Chủ đề
@endsection
@section('content')
<div class="list_topic_full">
    <div class="container">
        <div class="tile_box_key">
            <h1><a title="Chủ đề nổi bật" href="#">Chủ đề nổi bật</a></h1>
        </div>
        <div class="topic_more">
            <div class="fram_select">
                <ul>

                    @php
                    $count = 0;
                    @endphp
                    @forelse ($topics as $topic)

                    @php
                    $playlists = $topic->playlists;
                    @endphp
                    @if (count($playlists) > 0)
                    @php
                    $count++;
                    @endphp
                    <li>
                        <div class="box_info_show {{ $count % 3 == 0 ? 'poleft' : '' }}">
                            <span class="dot_show_menu"></span>
                            <div class="box-info-detail">
                                <h3>{{ $count }}{{ $topic->title }}</h3>
                                <p>{{ $topic->description }}</p>
                            </div>
                        </div>
                        <a class="box_absolute" href="{{ route('playlist',['slug' => name_to_slug($playlists[0]->name),'code' => $playlists[0]->code]) }}" target="_top" title="$playlists[0]->name">
                            <span class="icon_play"></span>
                            <img src="{{ (!empty($topic->banner)) ? $topic->banner : 'https://i.imgur.com/Ytuey9k.jpg' }}"
                                alt="title_nu">
                        </a>

                        <ul>
                            @forelse ($playlists as $playlist)
                            <li>
                                <h3><a href="{{ route('playlist',['slug' => name_to_slug($playlist->name),'code' => $playlist->code]) }}"
                                        class="name_song">{{ $playlist->name }}</a></h3>
                            </li>
                            @empty

                            @endforelse



                        </ul>
                    </li>
                    @endif


                    @empty

                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection
