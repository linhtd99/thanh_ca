@extends('client.layout.layout_user')
@section('page_title')
Playlist
@endsection
@section('content')
<div class="wn-content-user">
	<div class="container">
		<div class="box_right_user">
			<div class="tab-content">
				{{-- alert --}}
				@if (session('success'))
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success alert-dismissible show" role="alert">
							<strong>Thông báo!</strong> {{ session('success') }}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				@endif

				@if (session('error'))
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-warning alert-dismissible show" role="alert">
							<strong>Thông báo!</strong> {{ session('error') }}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				@endif
				{{-- end alert --}}

				<div id="2a" class="tab-pane active">
					<div class="form-playlist1">
						<div class="success_alert"></div>
						<div class="tile_box_key">
							<h3><a class="nomore" title="Quản lý tài khoản" href="">PLAYLIST</a></h3>
						</div>
						<div class="check_field_delete box-checkbox">
							<input id="checkAll" type="checkbox" name="check_video">
							<label for="checkbox1">&nbsp;</label>
                            <a id="btnRemoveAll" href="javascript:;" class="btn_delete_checkbox">Xóa</a>

							<a id="btnCreatePlayListHero" href="{{url('user/'.Auth::guard('customers')->user()->username.'/cap-nhat-playlist')}}" class="btn_delete_checkbox active" style="margin: 0 0 0 10px;">Tạo playlist
							</a>
						</div>
						<div class="user_cp_profile_playlist">
							<ul class="show_cp_profile_playlist">
								<li>
									<div class="box_action_edit">
									<a href="{{route('editwishList',['username' => Auth::guard('customers')->user()->username])}}" class="btn_btn2">Chỉnh sửa</a>
											{{-- <a href="{{url('user/'.Auth::guard('customers')->user()->username.'/deletePlaylist/'.$playlist->id)}}" class="btn_delete_item">Xóa</a> --}}
									</div>
									<span class="check_data box-checkbox ">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<div class="box_left ">
									<a href="{{route('wishList',['username' => Request::segment(2)])}}" class="avatar "><img src="https://avatar-nct.nixcdn.com/playlist/2016/09/09/4/c/6/e/1473409265946.jpg " alt="Bài hát yêu thích " title="Bài hát yêu thích "></a>
									</div>
									<div class="box_right ">
										<h3><a href="{{route('wishList',['username' => Request::segment(2)])}}" class="name_album_search ">Bài hát yêu thích</a></h3>
										<div class="box_list_singer "><span></span>Đang Cập Nhật</div>
										<div class="box_info_upload_export "><span id="NCTWG_D_21019885 " at="_blank " ac="user_upload "></span><span class="export_listen " id="NCTCounter_pl_26210388 " wgct="1 "></span></div>
									</div>
								</li>
								@foreach ($playlists as $playlist)
									<li>
										<div class="box_action_edit">
											<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/cap-nhat-playlist/'.$playlist->slug)}}" class="btn_btn2">Chỉnh sửa</a>
											<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/deletePlaylist/'.$playlist->id)}}" class="btn_delete_item">Xóa</a>
										</div>
										<span class="check_data box-checkbox">
											<input type="checkbox" name="check_video">
											<label for="checkbox1">&nbsp;</label>
										</span>
										<div class="box_left ">
											<a href="" class="avatar "><img
												src="{{$playlist->image}}"
													alt="{{$playlist->name}}" title="{{$playlist->name}}"></a>
										</div>
										<div class="box_right ">
										<h3><a href="{{ route('playlist', ['name' => name_to_slug($playlist->name), 'code' => $playlist->code]) }}"
													class="name_album_search ">{{ $playlist->name }}</a></h3>

											<div class="box_info_upload_export "><span id="NCTWG_D_21019885 " at="_blank " ac="user_upload "></span><span
													class="export_listen " id="NCTCounter_pl_26210388 " wgct="1 ">0</span></div>
										</div>
									</li>
								@endforeach

							</ul>

							{{ $playlists->links() }}
						</div>
					</div>
					<div class="form-playlist2">
						<div class="tile_box_key">
							<h3><a class="nomore" title="Quản lý tài khoản" href="">CẬP NHẬT PLAYLIST</a></h3>
						</div>
						<div class="box-edit-user-profile">

							<form enctype="multipart/form-data">
								<div class="form-edit">
									<div class="input-group">
										<label class="label" for="txtAddr">Tên Playlist</label>
										<div class="edit-user-content">
											<input style="width:512px;" class="input" type="text" id="playlist-name" name="playlist-name" >
										</div>
									</div>
									<div class="input-group" style="height:36px !important;">
										<label class="label" for="txtAddr">Ca sĩ</label>
										<div class="edit-user-content">
											<input style="width: 512px;" class="input" type="text" id="playlist-singer" name="playlist-singer" value="" autocomplete="off">
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="txtAddr">Thể loại</label>
										<div class="edit-user-content">
											<select style="width:512px;" id="playlist-genre" name="playlist-genre">
												<option value="" title="Chọn thể loại" selected="selected">Chọn thể loại</option>
												<optgroup label="Việt Nam">
													<option rel="playlist" value="5118">Nhạc Trẻ</option>
													<option rel="playlist" value="5120">Trữ Tình</option>
													<option rel="playlist" value="5263">Remix Việt</option>
													<option rel="playlist" value="5109">Rap Việt</option>
													<option rel="playlist" value="5107">Tiền Chiến</option>
													<option rel="playlist" value="5106">Nhạc Trịnh</option>
													<option rel="playlist" value="5119">Rock Việt</option>
													<option rel="playlist" value="5104">Cách Mạng</option>
												</optgroup>
												<optgroup label="Âu Mỹ">
													<option rel="playlist" value="5264">Pop</option>
													<option rel="playlist" value="5265">Rock</option>
													<option rel="playlist" value="5266">Electronica/Dance</option>
													<option rel="playlist" value="5267">R&amp;B/HipHop/Rap</option>
													<option rel="playlist" value="5268">Blues/Jazz</option>
													<option rel="playlist" value="5269">Country</option>
													<option rel="playlist" value="5270">Latin</option>
													<option rel="playlist" value="5271">Indie</option>
													<option rel="playlist" value="5272">Âu Mỹ Khác</option>
												</optgroup>
												<optgroup label="Châu Á">
													<option rel="playlist" value="5113">Nhạc Hàn</option>
													<option rel="playlist" value="5112">Nhạc Hoa</option>
													<option rel="playlist" value="5114">Nhạc Nhật</option>
													<option rel="playlist" value="5261">Nhạc Thái</option>
												</optgroup>
												<optgroup label="Khác">
													<option rel="playlist" value="5108">Thiếu Nhi</option>
													<option rel="playlist" value="5105">Không Lời</option>
													<option rel="playlist" value="5110">Nhạc Phim</option>
													<option rel="playlist" value="5115">Thể Loại Khác</option>
												</optgroup>
											</select>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="avatar">Hình ảnh playlist</label>
										<div style="width: 170px; font-size: 11px; position: absolute; margin-top: 30px; text-align: right;">(Hình tối thiểu <span style="color: red">500 x 500 pixels</span>. Nếu nhỏ hơn sẽ bị mất hình và lấy hình mặc định của NhacCuaTui)</div>
										<div class="edit-user-content">
											<div class="image-wrapper">
												<img id="avatar_img" src="https://stc-id.nixcdn.com/v11/images/img-plist-full.jpg" width="80" height="80">
											</div>
											<input type="text" style="width:273px; margin-top: 23px; margin-left: 10px;" class="upload-new-avatar" id="txtNewPhotoName" value="" readonly="true">
											<div class="upload-btn-wrapper upload-new-avatar">
												<button class="btn">Chọn file</button>
												<input type="file" name="myfile">
											</div>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="txtAddr">Mô tả</label>
										<div class="edit-user-content">
											<textarea style="width:500px" class="txtinput" id="txtDesc"></textarea>
										</div>
									</div>
									<div class="input-group wn-tab-ds">
										<label class="label" for="txtAddr">Danh sách bài hát</label>
										<div class="box_list_item_edit">
											<div id="listSongSearch" class="list_song_in_album" style="width:280px; float: left;">
												<div style="width:100%; height:37px; background:#f1f1f1">
													<input style="width:86.5%; margin:1.5%;" class="input" type="text" id="searchInputQuickList" name="searchInputQuickList" maxlength="300" placeholder="Nhập từ khóa hoặc link bài hát">
													<button class="btn_search" id="searchInputQuickListBtn" type="submit" value="" title="Tìm kiếm"></button>
												</div>
												<div>
													<ul id="idScrllSuggestionEditPlaylist" class="list_song_in_album" style="width: 280px; border: none; height: 450px;">
														<div id="idScrllSongInAlbumEditPlaylist" style="text-transform:capitalize;">

														</div>
													</ul>
												</div>
												<div class="clr"></div>
											</div>
											<span class="box-select-hero"></span>
											<ul id="listSong" class="list_song_in_album" style="width:360px">
												<div class="slimScrollDiv" style="position: relative; overflow: hidden; height: 450px;">
													<div id="idScrllSongInAlbum" style="height: 450px; overflow: hidden; width: 360px;">
														<li  class=" item_end"  draggable="true">
															<span class="nunberit">1</span>
															<div class="item_content" style="width:270px">
																<a href="" class="name_song" draggable="false">Túy Âm (Rap Version)</a> -
																<a href="" target="_blank" >Xesi</a>,
																<a href="" class="name_singer" target="_blank" draggable="false">Masew</a>,
																<a href="" class="name_singer"  target="_blank" draggable="false">Nhật Nguyễn</a>,
																<a href="" class="name_singer" target="_blank" draggable="false">Fantom</a></div>
																<a target="_blank" href="" style="margin-right: 5px;" title="Nghe bài hát này" draggable="false" class="button_playing"></a>
																<a href="" class="button_delete" title="Xóa bài hát này" draggable="false"></a>
																<a href="" class="button_downsort" title="Cho bài hày này xuống dưới" draggable="false"></a>
															</div>
														</li>
													</div>
													<div style="margin:6px 0px 10px 18px;">
														<a href="" class="btn-random" title="Sắp xếp ngẫu nhiên" style="color:#0689ba;">Sắp xếp ngẫu nhiên</a>
													</div>
												</div>

											</ul>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="txtAddr"></label>
										<div class="edit-user-content">
											<span class="check_data">
												<input type="checkbox" name="txtToSelected" id="txtToSelected" style="float: left;margin-top: 10px;">
												<label for="txtToSelected" style="text-align: left !important; margin-left: 5px;">Đăng ký chọn lọc?</label>
											</span>
											<span class="check_data" style="display: none;">
												<input disabled="disabled" type="checkbox" name="txtToPlaylistHero" id="txtToPlaylistHero" style="float: left;margin-top: 10px;">
												<label for="txtToPlaylistHero" style="cursor: auto;color: #bdbdb8;text-align: left !important; width: 300px; margin-left: 5px;">Đăng ký tham gia Playlist Hero tuần này (<a style="color: #bdbdb8;" href="">?</a>)</label>
											</span>
										</div>
									</div>
									<div id="error-box" class="error-box">

									</div>
									<div class="input-group">
										<label class="label" for="txtFullname"></label>
										<div class="edit-user-content">
											<a href="" id="btnUpdatePlaylist" style="margin-right:10px;" class="btn btn_accept_item">Lưu</a>
											<a href="" style="margin-right:10px;" id="btnDeletePlaylist" class="btn btn_accept_item">Xóa</a>
											<a href="" class="btn btn_accept_item" title="Trở lại">Trở lại</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end-form-playlist2 -->
				</div><!-- end-tab -->
			</div>
		</div> <!-- end-box_right_user -->
	</div>
</div>
@endsection
@section('js')
<script>
jQuery('body').on('click','#btnRemoveAll',function(){
		var arr_send = [];
		$('.check_data.box-checkbox > input:checkbox:checked').parents('li').each(function(i,v){
				var url = $(this).find('.btn_delete_item').attr('href');
				url_arr = url.split('/');
				arr = url_arr[url_arr.length - 1];
				if (arr != 'undefined') {
					arr_send.push(arr);
				}
		});
		$.ajax({
			url:"{{ route('deleteMultiPlaylist',['username' => Auth::guard('customers')->user()->username ] )}}",
			method:"POST",
			data:{
				arr_id : JSON.stringify(arr_send),
				},
			beforeSend: function() {
				$('.success_alert').html(`<div class="col-md-12">
					<div class="alert alert-primary" role="alert" style="background:#e74c3c;color:white">
						Đang xóa playlist. Vui lòng đợi ...
					</div>
				</div>`);
			},
			success:function(data){
				$('.success_alert').html(`<div class="col-md-12"><div class="alert alert-primary" role="alert" style="background:#3498db;color:white">
					Xóa playlist thành công
				</div></div>`);
				setTimeout(function(){
					window.location.reload();
					}, 1300);
				}


		});
});
</script>
@endsection

