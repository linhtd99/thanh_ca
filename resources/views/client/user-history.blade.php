@extends('client.layout.layout_user')
@section('page_title')
Tài khoản
@endsection
@section('content')
<div class="wn-content-user">
	<div class="container">
		<div class="box_right_user">
			<div class="tab-content">
				<div id="4a" class="tab-pane active">
                    <div class="success_alert"></div>
					<div class="tile_box_key">
						<h3><a class="nomore" title="Quản lý tài khoản" href="">LỊCH SỬ NGHE NHẠC</a></h3>
					</div>
					<div class="check_field_delete box-checkbox">
						<input id="checkAll" type="checkbox" name="check_video">
						<label for="checkbox1">&nbsp;</label>
						<a id="btnRemoveAll" href="javascript:;" class="btn_delete_checkbox">Xóa</a>
						<ul class="btn_view_select">
							<li class="active" ><a title="Bài hát" href="#1b" data-toggle="tab" role="tab">Bài hát</a></li>
							<span></span>
							<li><a title="Playlist" href="#2b" data-toggle="tab" role="tab">Playlist</a></li>
							<span></span>
							<li><a title="Video" href="#3b" data-toggle="tab" role="tab" >Video</a></li>
						</ul>
					</div>
					<div class="user_cp_profile_playlist ">
						<div class="tab-content" > 
							<ul class="show_cp_history_playlist avideo tab-pane fade active in" id="1b">
                                @if (!empty($songs) && count($songs) > 0)
                                    @foreach ($songs as $item)
                                        <li>
                                           
                                            <div class="box_action_edit">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" id="btnShare_video_6057528" target="blank" key="" class="btn_share_item">Chia sẻ</a>
                                            <a href="{{route('deleteHistory',['username' => Auth::guard('customers')->user()->username , 'id' => $item->history_id])}}" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
                                            </div>
                                            <span class="check_data box-checkbox">
                                            <input type="checkbox" name="check_video">
                                                <label for="checkbox1">&nbsp;</label>
                                            </span>
                                            <div class="box_left">
                                            <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="avatar"><img src="{{!empty($item->image) ? $item->image : asset('client/img/song-default.png')}}"
                                                        title="{{$item->name}}"></a>
                                            </div>
                                            <div class="box_right">
                                                <h3><a id="url_6057528" href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_album_search">{{$item->name}}</a></h3>
                                                <div class="box_list_singer"><span></span>
                                                @php
                                                    $artist_name = '';
                                                    if (count($item->artists) > 0) {
                                                        foreach ($item->artists as $key => $artist) {
                                                           $artist_name .= '<a href="'.route('artistDetail',['slug' => $artist->slug]).'" class="name_singer" target="_blank">'.$artist->name.'</a>';
                                                           if ($key+1 < count($item->artists)) {
                                                                $artist_name .= ',';
                                                            }
                                                        }
                                                        
                                                    } else {
                                                        $artist_name .= '<a class="name_singer" target="_blank">Chưa cập nhật</a>';
                                                    }
                                                @endphp
                                                   <?= $artist_name ?>
                                                </div>
                                            </div>
                                            @php
                                                if (!empty($item->created_at)) {
                                                    $date = date_create($item->created_at);
                                                    $dateFormat = date_format($date,"H:i - d/m/Y");
                                                } else {
                                                    $dateFormat = '';
                                                }
                                                
                                            @endphp
                                            <span class="export_dateplay">{{$dateFormat}}</span>
                                        </li>
                                    @endforeach
                                @endif
							</ul>
							<ul class="show_cp_history_playlist avideo tab-pane" id="2b">
                                @if (!empty($playlists) && count($playlists) > 0)
                                    @foreach ($playlists as $item)
                                        <li>
                                            <div class="box_action_edit">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" target="blank" id="btnShare_video_6057528" key="6057528" class="btn_share_item">Chia sẻ</a>
                                                <a href="{{route('deleteHistory',['username' => Auth::guard('customers')->user()->username , 'id' => $item->history_id])}}" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
                                            </div>
                                            <span class="check_data box-checkbox">
                                                <input  type="checkbox" name="check_video">
                                                <label for="checkbox1">&nbsp;</label>
                                            </span>
                                            <div class="box_left">
                                            <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="avatar"><img src="{{$item->image}}"
                                                        title="{{$item->name}}"></a>
                                            </div>
                                            <div class="box_right">
                                                <h3><a id="url_6057528" href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_album_search">{{$item->name}}</a></h3>
                                                {{-- <div class="box_list_singer"><span></span><a href="" class="name_singer" target="_blank">Vĩnh Thuyên Kim</a>
                                                </div> --}}
                                            </div>
                                            @php
                                                if (!empty($item->created_at)) {
                                                    $date = date_create($item->created_at);
                                                    $dateFormat = date_format($date,"H:i - d/m/Y");
                                                } else {
                                                    $dateFormat = '';
                                                }
                                                
                                            @endphp
                                            <span class="export_dateplay">{{$dateFormat}}</span>
                                        </li>
                                    @endforeach
                                @endif
								
							</ul>
							<ul class="show_cp_history_playlist avideo  tab-pane " id="3b">
                                @if (!empty($videos) && count($videos) > 0)
                                    @foreach ($videos as $item)
                                        <li>
                                            <div class="box_action_edit">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{route('detailVideo',['name' => name_to_slug($item->name),'code' => $item->code])}}" target="blank" id="btnShare_video_6057528" key="6057528" class="btn_share_item">Chia sẻ</a>
                                                <a href="{{route('deleteHistory',['username' => Auth::guard('customers')->user()->username , 'id' => $item->history_id])}}" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
                                            </div>
                                            <span class="check_data box-checkbox">
                                                <input  type="checkbox" name="check_video">
                                                <label for="checkbox1">&nbsp;</label>
                                            </span>
                                            <div class="box_left">
                                            <a href="{{route('detailVideo',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="avatar"><img src="{{!empty($item->image) ? $item->image : getVideoYoutubeApi($item->link)['thumbnail']['mqDefault']}}" title="{{$item->name}}"></a>
                                            </div>
                                            <div class="box_right">
                                                <h3><a id="url_6057528" href="{{route('detailVideo',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_album_search">{{$item->name}}</a></h3>
                                                <div class="box_list_singer"><span></span>
                                                    @php
                                                    $artist_name = '';
                                                    if (count($item->artists) > 0) {
                                                        foreach ($item->artists as $key => $artist) {
                                                           $artist_name .= '<a href="'.route('artistDetail',['slug' => $artist->slug]).'" class="name_singer" target="_blank">'.$artist->name.'</a>';
                                                           if ($key+1 < count($item->artists)) {
                                                                $artist_name .= ',';
                                                            }
                                                        }
                                                        
                                                    } else {
                                                        $artist_name .= '<a class="name_singer" target="_blank">Chưa cập nhật</a>';
                                                    }
                                                @endphp
                                                   <?= $artist_name ?>
                                                </div>
                                            </div>
                                             @php
                                                if (!empty($item->created_at)) {
                                                    $date = date_create($item->created_at);
                                                    $dateFormat = date_format($date,"H:i - d/m/Y");
                                                } else {
                                                    $dateFormat = '';
                                                }
                                                
                                            @endphp
                                            <span class="export_dateplay">{{$dateFormat}}</span>
                                        </li>
                                    @endforeach
                                @endif
							</ul>
						</div> 
					</div>
				</div><!-- end-tab -->
				
			</div>
		</div> <!-- end-box_right_user -->
	</div>
</div>
@endsection
@section('js')
<script>
    
    $(document).on('click','#btnRemoveAll',function(){
            var arr_send = [];
            $('.check_data.box-checkbox > input:checkbox:checked').parents('ul.active').find('li').each(function(i,v){
                    var url = $(this).find('.btn_delete_item').attr('href');
                    url_arr = url.split('/');
                    arr = url_arr[url_arr.length - 1];
                    if (arr != 'undefined') {
                        arr_send.push(arr);
                }
			});
            console.log(arr_send);
            $.ajax({
			url:"{{ route('deleteAllHistory',['username' => Auth::guard('customers')->user()->username ] )}}", 
			method:"POST", 
			data:{
				arr_id : JSON.stringify(arr_send),
				},
			beforeSend: function() {
				$('.success_alert').html(`<div class="col-md-12">
					<div class="alert alert-primary" role="alert" style="background:#e74c3c;color:white">
						Đang xóa lịch sử nghe. Vui lòng đợi ...
					</div>
				</div>`);
			},
			success:function(data){ 
				$('.success_alert').html(`<div class="col-md-12"><div class="alert alert-primary" role="alert" style="background:#3498db;color:white">
					Xóa lịch sử nghe thành công
				</div></div>`);
				setTimeout(function(){
					window.location.reload();
					}, 1300);
			
            	
            }
			
		});
        
    })
</script>
@endsection

