@extends('client.layout.master')
@section('page_title')
{{$page->name}}
@endsection
@section('content')
<div class="home">
    <div class="container">
        <div class="row">
        <h1>{{$page->name}}</h1>
        @php
            $date = date_create($page->created_at);
            $date_format = date_format($date,"H:i d/m/Y");
        @endphp
        <p>Ngày đăng : {{$date_format}}</p>
        @php echo $page->content @endphp
        </div>
    </div>
</div>

@endsection