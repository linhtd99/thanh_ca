@extends('client.layout.layout_user')
@section('page_title')
Tài khoản
@endsection
@section('content')
<div class="wn-content-user">
	<div class="container">
		<div class="box_right_user">
			<div class="tab-content">
				<div id="1a" class="active tab-pane fade in">
					<div class="form-edit1">
						<div class="tile_box_key">
							<h3><a class="nomore" title="Quản lý tài khoản" href="">Quản lý tài khoản</a></h3>
						</div>
						<div class="box_group_left">
							<div class="box_detail_info">
								<span class="txt_sub_group">Giới thiệu 
									<div class="box_user_action">
										<a href="" id="up-btnEdit" class="btn_edit_item">Chỉnh sửa</a>
									</div>
								</span>
								<p>Họ và tên: {{!empty($user->name) ? $user->name : ''}}</p>
								@php
									$date = date_create($user->birthday);
								@endphp
								<p>Ngày sinh: {{!empty($user->birthday) ? date_format($date,"d/m/Y") : ''}}</p>
								<p>Giới tính: 
									@if ($user->sex == 0)
										{{ 'Nam' }}
									@elseif($user->sex == 1)
										{{ 'Nữ' }}
									@else
										{{ 'Khác' }}
									@endif
								</p>
								<p>Điện thoại: {{!empty($user->phone) ? $user->phone : ''}} </p>
								<p>Địa chỉ: {{!empty($user->address) ? $user->address : ''}}</p>
								
								<p>Giới thiệu: {{!empty($user->introduction) ? $user->introduction : ''}}</p>
								
							</div>
							<div id="divUserInfo" class="box_detail_info">
								<span class="txt_sub_group">Tài khoản</span>
								<p>Tên tài khoản: {{!empty($user->username) ? $user->username : ''}}</p>                            
								<p>Mật khẩu: <a href="" class="wn-reset-pas"> Đổi mật khẩu</a></p>
								<p>Email: {{!empty($user->email) ? $user->email : ''}}</p> 
								{{-- <form target="ifSendMail" id="_formSendMail" >
									<p>Ngày hết hạn: Bạn chưa có <a href="">Tài khoản VIP</a></p>
								</form> --}}
							</div>
						</div>
						<div class="box_group_right">
							<div class="box_list_item_video_album">
								<span class="txt_sub_group">Playlist</span>
								<ul class="usercp_list_playlist">
									@foreach ($playlists as $playlist)
										<li>
											<a title="{{$playlist->name}}" href="">
												<img src="{{$playlist->image}}">
											</a>
										</li>
									@endforeach
									
									
									<li>
									<a href="{{url('user/'.$user->username.'/playlist')}}" class="sum_cout_pl">
										<span class="count-number">{{$countPlaylist}}</span> playlist
										</a>
									</li>
								</ul>
							</div>
							<div class="box_list_item_video_album">
								<span class="txt_sub_group">Video</span>
								<ul class="usercp_list_video">
									<li>
										<a href="" class="sum_cout_pl">
											<span class="count-number">0</span> video
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div><!-- end-form-edit -->
					<div class="form-edit2">
						<div class="tile_box_key">
							<h3><a class="nomore" title="CẬP NHẬT TÀI KHOẢN" href="">CẬP NHẬT TÀI KHOẢN</a></h3>
						</div>
						<form id="formEditUser" enctype="multipart/form-data">
							<div class="_err_edit"></div>
							<input type="hidden" name="id" value="{{$user->id}}">
							<div id="formEditUserId" class="box-edit-user-profile">
								<div class="input-group">
									<label class="label" for="avatar">Ảnh đại diện</label>
									<div class="edit-user-content">
										<div class="image-wrapper">
											<a class="avatar"><img id="avatar_img" src="{{$user->avatar ? $user->avatar : asset('client/img/default-avatar.png')}}" width="80" height="80"></a>
										</div>
										<div style="margin-top:58px">
											<div class="edit-user-content">
												<div class="upload-btn-wrapper">
													<button class="btn">Thay avatar	</button>
													<input type="file" name="myfile" />
												</div>
											</div>
										</div>
									</div>
									<span class="err_image" style="color:red"></span>
								</div>
								<div class="input-group">
									<label class="label" for="txtUserName">Tên đăng nhập</label>
									<div class="edit-user-content">
										<input class="input" type="text" name="username" id="txtUserName" disabled="disabled" value="{{$user->username ? $user->username : ''}}" style="width:400px;">
									</div>
								</div>
								<div class="input-group">
									<label class="label" for="txtFullname">Tên hiển thị</label>
									<div class="edit-user-content">
										<input class="input" type="text" name="name" id="txtFullname" value="{{$user->name ? $user->name : ''}}" style="width:400px">
									</div>
								</div>
								@php
								if (!empty($user->birthday)) {
									$birthday_arr = explode('-',$user->birthday);
								}
								@endphp
								<div class="input-group">
									<label class="label" for="txtFullname">Ngày sinh</label>
									<div class="edit-user-content">
										<select name="day" id="selDOB_Day" style="width:60px;">
											@for ($i = 1; $i <= 31; $i++)
												<option {{ !empty($birthday_arr) ? $i == $birthday_arr[2] ? 'selected' : '' : ''}} value="{{$i}}">{{$i}}</option>
											@endfor
											
										</select>
										<select name="month" id="selDOB_Month" style="width:60px;">
											@for ($i = 1; $i <= 12; $i++)
												<option {{ !empty($birthday_arr) ? $i == $birthday_arr[1] ? 'selected' : '' : ''}} value="{{$i}}">{{$i}}</option>
											@endfor
										</select>
										@php
											$year = date("Y");
										@endphp
										<select name="year" id="selDOB_Year" style="width:100px;">
											@for ($i = 1912; $i <= $year; $i++)
											<option {{ !empty($birthday_arr) ? $i == $birthday_arr[0] ? 'selected' : '' : ''}} value="{{$i}}">{{$i}}</option>
											@endfor
										</select>
									</div>
								</div>
								<div class="input-group">
									<label class="label" for="selGender">Giới tính</label>
									<div class="edit-user-content">
										<select name="sex" id="selGender" style="width:80px;">
											<option {{$user->sex == '-1' ? 'selected' : ''}} value="-1" selected="">Khác</option>
											<option {{$user->sex == '0' ? 'selected' : ''}} value="0">Nam</option>
											<option {{$user->sex == '1' ? 'selected' : ''}} value="1">Nữ</option>
										</select>
									</div>
								</div>
								<div class="input-group">
									<label class="label" for="txtAddr">Địa chỉ</label>
									<div class="edit-user-content">
									<input class="input" type="text" id="txtAddr" name="address" value="{{!empty($user->address) ? $user->address : ''}}" style="width:400px">
									</div>
								</div>
						
								<div class="input-group">
									<label class="label" for="txtPhone">Số điên thoại</label>
									<div class="edit-user-content">
										<input class="input" type="text" name="phone" id="txtPhone" value="{{!empty($user->phone) ? $user->phone : ''}}" style="width:138px">
									</div>
								</div>
								{{-- <div class="input-group">
									<label class="label" for="txtCMND">Số CMND</label>
									<div class="edit-user-content">
										<input class="input" type="text" id="txtCMND" value="" style="width:138px">
									</div>
								</div> --}}
								<div class="input-group">
									<label class="label" for="txtFullname">Giới thiệu</label>
									<div class="edit-user-content">
										<textarea class="txtinput" name="introduction" id="txtAbout" style="width:400px">{{!empty($user->introduction) ? $user->introduction : ''}}</textarea>
									</div>
								</div>
								<div class="input-group">
									<label class="label" for="txtFullname"></label>
									<div class="edit-user-content">
										<input class="btn btn_accept_item submitEdit" style="margin-right:10px;" value="Lưu">
										<a id="btnCloseEdit" href="" class="btn btn_accept_item" title="Trở lại">Trở lại</a>
									</div>
								</div>
							</div>
						</form>
					</div> <!-- end-form-edit -->
					<div class="form-edit3">
						<div class="tile_box_key">
							<h3><a class="nomore" title="CẬP NHẬT TÀI KHOẢN" href="">ĐỔI MẬT KHẨU</a></h3>
							<div class="box_user_action"><a href="" class="wn-back-user btn_back_item">Xem tài khoản</a></div>
							<div class="_err_edit_pw"></div>
						</div>
						<form>
						<input type="hidden" name="id" value="{{$user->id}}">
							<div class="box-edit-user-profile">
								<div class="input-group">
									<label class="label" for="txtFullname">Tên tài khoản</label>
									<div class="edit-user-content">
									<input class="input" type="text" value="{{$user->username}}" disabled="" style="width:400px">
									</div>
								</div>
								<div class="input-group">
									<label class="label" for="txtFullname">Mật khẩu hiện tại</label>
									<div class="edit-user-content">
										<input class="input" type="password" name="old_password" id="password" style="width:400px">
										
									</div>
									<span class="err_old_password" style="color:red;margin-left:21%"></span>
								</div>
								<div class="input-group">
									<label class="label" for="txtFullname">Mật khẩu mới</label>
									<div class="edit-user-content">
										<input class="input" type="password" name="new_password" id="new-password"  style="width:400px" aria-autocomplete="list">
									</div>
									<span class="err_new_password" style="color:red;margin-left:21%"></span>
								</div>
								<div class="input-group">
									<label class="label" for="txtFullname">Nhập lại mật khẩu mới</label>
									<div class="edit-user-content">
										<input class="input" type="password" name="confirm_password" id="confirm-password" style="width:400px">
									</div>
									<span class="err_cf_password" style="color:red;margin-left:21%"></span>
								</div>
								{{-- <div class="input-group" style="padding: 0px 0;border-top: none;margin-top: -15px;">
									<label class="label" for="txtFullname"></label>
									<div class="edit-user-content">
										<p style="color: red;font-size: 13px;">Lưu ý: Tất cả đăng nhập trước đó trên các thiết bị và ứng dụng sẽ được tự động đăng xuất.</p>
									</div>
								</div> --}}
								<div class="input-group">
									<label class="label" for="txtFullname"></label>
									<div class="edit-user-content">
										<button class="btn btn_accept_item editPasswordUser" id="up-btnSave" title="Lưu">Lưu</button>
									</div>
								</div>
							</div>
						</form>
					</div><!-- end-form-edit -->
				</div><!-- end-tab -->
				<div id="4a" class="tab-pane ">
					<div class="tile_box_key">
						<h3><a class="nomore" title="Quản lý tài khoản" href="">LỊCH SỬ NGHE NHẠC</a></h3>
					</div>
					<div class="check_field_delete box-checkbox">
						<input id="checkAll" type="checkbox" name="check_video">
						<label for="checkbox1">&nbsp;</label>
						<a id="btnRemoveAll" href="javascript:;" class="btn_delete_checkbox">Xóa</a>
						<ul class="btn_view_select">
							<li><a title="Bài hát" href="#1b" data-toggle="tab" role="tab">Bài hát</a></li>
							<span></span>
							<li><a title="Playlist" href="#2b" data-toggle="tab" role="tab">Playlist</a></li>
							<span></span>
							<li class="active"><a title="Video" href="#3b" data-toggle="tab" role="tab" >Video</a></li>
						</ul>
					</div>
					<div class="user_cp_profile_playlist ">
						<div class="tab-content" > 
							<ul class="show_cp_history_playlist avideo tab-pane fade active in" id="3b">
								<?php for ($i = 0; $i <4 ; $i++): ?>
									<li>
										<div class="box_action_edit">
											<a href="javascript:;" id="btnShare_video_6057528" key="6057528" class="btn_share_item">Chia sẻ</a>
											<a href="" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
										</div>
										<span class="check_data box-checkbox">
											<input id="check_6057528" type="checkbox" name="check_video">
											<label for="checkbox1">&nbsp;</label>
										</span>
										<div class="box_left">
											<a href="" class="avatar"><img src="https://avatar-nct.nixcdn.com/mv/2019/08/23/7/2/2/8/1566555698955_268.jpg" title="Tận Cùng Chỉ Là Lời Hứa"></a>
										</div>
										<div class="box_right">
											<h3><a id="url_6057528" href="" class="name_album_search">Tận Cùng Chỉ Là Lời Hứa</a></h3>
											<div class="box_list_singer"><span></span><a href="" class="name_singer" target="_blank">Vĩnh Thuyên Kim</a></div>
										</div>
										<span class="export_dateplay">01:16 - 04/09/2019</span>
									</li>
								<?php endfor;?>
							</ul>
							<ul class="show_cp_history_playlist avideo tab-pane" id="2b">
								<li>
									<div class="box_action_edit">
										<a href="javascript:;" id="btnShare_video_6057528" key="6057528" class="btn_share_item">Chia sẻ</a>
										<a href="" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
									</div>
									<span class="check_data box-checkbox">
										<input id="check_6057528" type="checkbox" name="check_video">
										<label for="checkbox1">&nbsp;</label>
									</span>
									<div class="box_left">
										<a href="" class="avatar"><img src="https://avatar-nct.nixcdn.com/mv/2019/08/23/7/2/2/8/1566555698955_268.jpg" title="Tận Cùng Chỉ Là Lời Hứa"></a>
									</div>
									<div class="box_right">
										<h3><a id="url_6057528" href="" class="name_album_search">Tận Cùng Chỉ Là Lời Hứa</a></h3>
										<div class="box_list_singer"><span></span><a href="" class="name_singer" target="_blank">Vĩnh Thuyên Kim</a></div>
									</div>
									<span class="export_dateplay">01:16 - 04/09/2019</span>
								</li>
							</ul>
							<ul class="show_cp_history_playlist avideo  tab-pane " id="1b">
								<li>
									<div class="box_action_edit">
										<a href="javascript:;" id="btnShare_video_6057528" key="6057528" class="btn_share_item">Chia sẻ</a>
										<a href="" id="btnRemove_video_6057528" class="btn_delete_item">Xóa</a>
									</div>
									<span class="check_data box-checkbox">
										<input id="check_6057528" type="checkbox" name="check_video">
										<label for="checkbox1">&nbsp;</label>
									</span>
									<div class="box_left">
										<a href="" class="avatar"><img src="https://avatar-nct.nixcdn.com/mv/2019/08/23/7/2/2/8/1566555698955_268.jpg" title="Tận Cùng Chỉ Là Lời Hứa"></a>
									</div>
									<div class="box_right">
										<h3><a id="url_6057528" href="" class="name_album_search">Tận Cùng Chỉ Là Lời Hứa</a></h3>
										<div class="box_list_singer"><span></span><a href="" class="name_singer" target="_blank">Vĩnh Thuyên Kim</a></div>
									</div>
									<span class="export_dateplay">01:16 - 04/09/2019</span>
								</li>
							</ul>
						</div> 
					</div>
				</div>
			
					
				
				
			</div>
		</div> <!-- end-box_right_user -->
	</div>
</div>
@endsection
@section('js')

@endsection

