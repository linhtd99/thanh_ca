@extends('client.layout.master')
@section('page_title')
Nghệ sĩ
@endsection
@section('content')
<div class="container">
    <div class="trending_artist_content">
        <div class="trending_artist_thumb">
            <a href="#">
                {{-- {{dd($artistTrending[0]->image)}} --}}
                <img class="trending_artist_img"
                    src="{{!empty($artistTrending[0]->image) ? $artistTrending[0]->image : asset('images/765-default-avatar.png')}}" alt="">
            <p class="trending_name_top">{{!empty($artistTrending[0]->name) ? $artistTrending[0]->name : ''}}</p>
            </a>
        </div>
        @php
        $firstDayOfTheWeek = date('Y/m/d', strtotime("this week"));
        $lastDayOfTheWeek = date('Y/m/d', strtotime($firstDayOfTheWeek . ' + 6 days'));
        $firstDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfTheWeek. " - 1 week"));
        $lastDayOfPreviousWeek = date('Y/m/d', strtotime($firstDayOfPreviousWeek . " + 6 days"));
        $date = date_create($lastDayOfPreviousWeek);
        $dateFormat = date_format($date,"d/m/Y")
        @endphp
        <div class="trending_artist_info">
            <h3 class="trending_top_text">TOP Nghệ Sĩ Trending Trong Tuần</h3>
            <span class="trending_update_date">[Cập nhật ngày {{ $dateFormat }}]</span>
            <a href="#">
                <span class="trending_share">
                    <img src="https://stc-id.nixcdn.com/v11/images/img-artist-trending/btn-share.png" alt="">
                </span>
            </a>
            <span class="trending_test_version">Phiên bản thử nghiệm</span>
            <div class="trending_charts">
            @if (count($artistTrending) < 5)
                <div class="trending_charts_content">
                    <ul>
                        @for ($i = 0; $i < count($artistTrending); $i++) <li class="trending_charts_singer">
                            <p class="trending_stt">{{$i+1}}</p>
                            {{-- <img class="trending_icon_up"
                                    src="https://stc-id.nixcdn.com/v11/images/img-artist-trending/icon-up.png" alt=""> --}}
                            {{-- <span class="trending_text_green">5</span> --}}
                        <a href="{{route('artistDetail',['slug' => $artistTrending[$i]->slug])}}">
                                <img class="trending_singer_img"
                                    src="{{!empty($artistTrending[$i]->image) ? $artistTrending[$i]->image : asset('images/765-default-avatar.png')}}"
                                    alt="">
                                <p class="trending_name_singer">{{$artistTrending[$i]->name}}</p>
                            </a>
                            </li>
                            @endfor
                    </ul>
            </div>
            @else
                <div class="trending_charts_content">
                    <ul>
                        @for ($i = 0; $i < 5; $i++) <li class="trending_charts_singer">
                            <p class="trending_stt">{{$i+1}}</p>
                            {{-- <img class="trending_icon_up"
                                                                                src="https://stc-id.nixcdn.com/v11/images/img-artist-trending/icon-up.png" alt=""> --}}
                            {{-- <span class="trending_text_green">5</span> --}}
                            <a href="{{route('artistDetail',['slug' => $artistTrending[$i]->slug])}}">
                                <img class="trending_singer_img"
                                    src="{{!empty($artistTrending[$i]->image) ? $artistTrending[$i]->image : asset('images/765-default-avatar.png')}}"
                                    alt="">
                                <p class="trending_name_singer">{{$artistTrending[$i]->name}}</p>
                            </a>
                            </li>
                            @endfor
                    </ul>
                </div>
            @endif


            @if (count($artistTrending) > 5)
                <div class="trending_charts_content">
                    <ul>
                        @for ($i = 5; $i < count($artistTrending); $i++)
                        <li class="trending_charts_singer">
                            <p class="trending_stt">{{$i+1}}</p>

                            {{-- <img class="trending_icon_up"
                                                            src="https://stc-id.nixcdn.com/v11/images/img-artist-trending/icon-up.png" alt="">
                                                        <span class="trending_text_green">11</span> --}}
                            <a href="{{route('artistDetail',['slug' => $artistTrending[$i]->slug])}}">
                                <img class="trending_singer_img"
                                    src="{{!empty($artistTrending[$i]->image) ? $artistTrending[$i]->image : asset('images/765-default-avatar.png')}}"
                                    alt="">
                                <p class="trending_name_singer">{{$artistTrending[$i]->name}}</p>
                            </a>
                            </li>
                            @endfor
                    </ul>
                </div>
            @endif

        </div>
    </div>
</div>
<div class="tile_box_key">
    <h3><a title="Nghệ Sĩ" href="{{route('indexArtist')}}">Nghệ Sĩ</a></h3>
<div class="sing-select-abc"><a href="{{route('indexArtist')}}" title="" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == '' ? 'active' : ''}}">HOT</a>
        <a href="{{route('indexArtist',['keyword' => 'a'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'a' ? 'active' : ''}}" title="A">A</a>
        <a href="{{route('indexArtist',['keyword' => 'b'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'b' ? 'active' : ''}}" title="B">B</a>
        <a href="{{route('indexArtist',['keyword' => 'c'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'c' ? 'active' : ''}}" title="C">C</a>
        <a href="{{route('indexArtist',['keyword' => 'd'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'd' ? 'active' : ''}}" title="D">D</a>
        <a href="{{route('indexArtist',['keyword' => 'e'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'e' ? 'active' : ''}}" title="E">E</a>
        <a href="{{route('indexArtist',['keyword' => 'f'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'f' ? 'active' : ''}}" title="F">F</a>
        <a href="{{route('indexArtist',['keyword' => 'g'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'g' ? 'active' : ''}}" title="G">G</a>
        <a href="{{route('indexArtist',['keyword' => 'h'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'h' ? 'active' : ''}}" title="H">H</a>
        <a href="{{route('indexArtist',['keyword' => 'i'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'i' ? 'active' : ''}}" title="I">I</a>
        <a href="{{route('indexArtist',['keyword' => 'j'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'j' ? 'active' : ''}}" title="J">J</a>
        <a href="{{route('indexArtist',['keyword' => 'k'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'k' ? 'active' : ''}}" title="K">K</a>
        <a href="{{route('indexArtist',['keyword' => 'l'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'l' ? 'active' : ''}}" title="L">L</a>
        <a href="{{route('indexArtist',['keyword' => 'm'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'm' ? 'active' : ''}}" title="M">M</a>
        <a href="{{route('indexArtist',['keyword' => 'n'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'n' ? 'active' : ''}}" title="N">N</a>
        <a href="{{route('indexArtist',['keyword' => 'o'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'o' ? 'active' : ''}}" title="O">O</a>
        <a href="{{route('indexArtist',['keyword' => 'p'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'p' ? 'active' : ''}}" title="P">P</a>
        <a href="{{route('indexArtist',['keyword' => 'q'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'q' ? 'active' : ''}}" title="Q">Q</a>
        <a href="{{route('indexArtist',['keyword' => 'r'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'r' ? 'active' : ''}}" title="R">R</a>
        <a href="{{route('indexArtist',['keyword' => 's'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 's' ? 'active' : ''}}" title="S">S</a>
        <a href="{{route('indexArtist',['keyword' => 't'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 't' ? 'active' : ''}}" title="T">T</a>
        <a href="{{route('indexArtist',['keyword' => 'u'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'u' ? 'active' : ''}}" title="U">U</a>
        <a href="{{route('indexArtist',['keyword' => 'v'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'v' ? 'active' : ''}}" title="V">V</a>
        <a href="{{route('indexArtist',['keyword' => 'w'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'w' ? 'active' : ''}}" title="W">W</a>
        <a href="{{route('indexArtist',['keyword' => 'x'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'x' ? 'active' : ''}}" title="X">X</a>
        <a href="{{route('indexArtist',['keyword' => 'y'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'y' ? 'active' : ''}}" title="Y">Y</a>
        <a href="{{route('indexArtist',['keyword' => 'z'])}}" class="{{Request::segment(1) == 'nghe-si' && Request::segment(2) == 'z' ? 'active' : ''}}" title="Z">Z</a></div>
</div>
<ul class="list-singer-item">

@if (count($artists) < 10)
    @for ($i = 0; $i < count($artists); $i++)

                <li>
                    <a title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" href="{{route('artistDetail',['slug' => $artists[$i]['slug']])}}">
                        <img src="{{!empty($artists[$i]->image) ? $artists[$i]->image : asset('images/765-default-avatar.png')}}"
                            title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" alt="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}">
                    </a>
                <h3><a style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;" title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" href="{{route('artistDetail',['slug' => $artists[$i]['slug']])}}">{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}</a></h3>
            </li>
    @endfor
@else
    @for ($i = 0; $i < 10; $i++) <li>
            <a title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" href="{{route('artistDetail',['slug' => $artists[$i]['slug']])}}">
                <img src="{{!empty($artists[$i]->image) ? $artists[$i]->image : asset('images/765-default-avatar.png')}}"
                    title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" alt="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}">
            </a>
            <h3><a style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;" title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}"
                    href="{{route('artistDetail',['slug' => $artists[$i]['slug']])}}">{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}</a></h3>
            </li>
    @endfor
@endif
</ul>
@if (count($artists) > 10)
    <ul class="list-singer-item-more">
        @for ($i = 10; $i < count($artists); $i++)
            <li>
                <h3><a title="{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}" href="{{route('artistDetail',['slug' => $artists[$i]->slug])}}">{{!empty($artists[$i]->name) ? $artists[$i]->name : ''}}</a></h3>
            </li>
        @endfor
    </ul>
@endif
@if ($keyword != null)
    {{$artists->links()}}
@endif
</div>
<div class="list-singer-top-week">
    <div class="container">
        <div class="singer-top-week-content">
            <div class="tile_box_key">
                <h3>Nghệ sĩ nổi bật</h3>
            </div>
            <ul class="item-top">
                @foreach ($artistsHighlight as $item)
                    <li>
                        <a href="{{route('artistDetail',['slug' => $item->slug])}}">
                            <img src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}"
                                title="{{$item->name}}" alt="{{$item->name}}"></a>
                        <h3><a href="{{route('artistDetail',['slug' => $item->slug])}}">{{$item->name}}</a></h3>
                        <ul>
                            @foreach ($item->songs as $song)
                                <li>
                                <h3><a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}">{{$song->name}}</a></h3>
                                </li>
                            @endforeach

                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endsection
