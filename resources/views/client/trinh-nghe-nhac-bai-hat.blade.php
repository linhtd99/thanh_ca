@extends('client.layout.layout_trinh-nghe-nhac')

@section('page_title')
Trình nghe nhạc
@endsection
<style>
    [role=button] {
        color: white;
        cursor: pointer;
    }

    .auto-box>div:first-of-type .active:after {
        content: "";
        display: block;
        width: 13px;
        height: 13px;
        border-radius: 8px;
        cursor: pointer;
        position: absolute;
        top: 2px;
        z-index: 1;
        left: 20px;
        background-color: #FFFFFD;
        -webkit-transition: left .2s ease-in-out;
        transition: left .2s ease-in-out;
    }

</style>

@section('content')

<div class="container" style="margin-bottom:150px">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="box_playing">
                {{-- layout bai hat --}}
                <div class="content" style="padding-top:30px">
                    <div class="container-head" style="padding:0">

                        <div class="container-song">
                            <div class="song-box">
                                <div class="blur-container"
                                    style="background: url('{{!empty($song->image) ? $song->image : ''}}') center center / cover no-repeat rgb(157, 147, 125); height: 220px;">
                                </div>
                                <div class="song-info">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-12 col-sm-7 hh-box-left">
                                            <div class="song-img">
                                                <a href="#"><img
                                                        src="{{!empty($song->image) ? $song->image : asset('client/img/song-default.png')}}"
                                                        alt="{{!empty($song->name) ? $song->name : ''}}"></a>
                                            </div>
                                            <div class="left-info">
                                                <div class="ranking"><a role="button"></a></div>
                                                <h3>{{!empty($song->name) ? $song->name : ''}}</h3>
                                                <div class="artist-name"><a role="button" class=""></a></div>
                                                {{-- <div class="subtext album">Playlist: <a role="button" class="ml-5"
                                                        title="{{!empty($song->name) ? $song->name : ''}} (Single)"
                                                href="#">{{!empty($song->name) ? $song->name : ''}}</a>
                                            </div> --}}
                                            <div class="subtext authors">Upload bởi:
                                                @if ($song->author_id == 0)
                                                <a role="button">Admin</a>
                                                @else

                                                <a role="button"
                                                    href="{{!empty($song->author_id) ? route('trangCaNhan',['username' => getNameCustomer($song->author_id)]) : ''}}"
                                                    title="{{getNameCustomer($song->author_id)}}">{{getNameCustomer($song->author_id)}}</a>

                                                @endif</div>
                                            <div class="subtext category">Thể loại:
                                                <a role="button" class="mr-2"
                                                    title="{{!empty(get_cat_by_id($song->category_id)->name) ? get_cat_by_id($song->category_id)->name : '' }}">{{!empty(get_cat_by_id($song->category_id)->name) ? get_cat_by_id($song->category_id)->name : ''}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <div class="social-share-block">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                </li>
                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{route('song',['song' => name_to_slug($song->name),'code' => $song->code])}}"
                                                        target="_blank"><img
                                                            src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/facebook.svg"
                                                            alt=""></a></li>
                                                <li><a href="#" class="zalo-share-button" data-customize=true
                                                        data-oaid="579745863508352884"><img
                                                            src="https://static-zmp3.zadn.vn/skins/zmp3-v5.2/images/zalo.svg"
                                                            alt=""></a></li>
                                            </ul>
                                        </div>
                                        <div class="log-stats">
                                            <div class="viewed"><i class="fa fa-play" aria-hidden="true"></i>
                                                {{!empty(countListenSongHelper($song->code)->listen) ? countListenSongHelper($song->code)->listen : '0'}}
                                            </div>
                                            {{-- <a class="action liked"><i class="fa fa-heart" aria-hidden="true"></i>
                                                        </a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="showNotiWishList"></div>
            {{-- end layout bai hat --}}
            <div class="box_menu_player new">
                <div class="tab-btn">
                    <ul>
                        <li><a href="#" class="btn-play-pause"><i class="fa fa-play" aria-hidden="true"></i>
                                Nghe bài hát</a>
                        </li>
                        <li><a onclick="addWishList('{{$song->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')"
                                style="cursor: pointer;"><i class="fa fa-plus" aria-hidden="true"></i> Thêm vào
                                danh sách
                                phát</a></li>
                        {{-- <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> Thích</a></li> --}}
                        <li><a href="{{  route('download',['file' => $song->filename,'code' => $song->code]) }}"><i
                                    class="fa fa-download" aria-hidden="true"></i> Tải xuống</a></li>
                        {{-- <li><a href="#" class="more">...</a></li> --}}
                        <li style="display: inline-block;"><a href="javascript:;" id="btnReportError" title="Báo lỗi"
                                class=""><span class="icon_report"></span></a></li>
                        <li style="display: inline-block;" class="box-share-like-new">
                            @php
                            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" :
                            "http") .
                            "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            @endphp
                            <div class="fb-like" data-href="{{ $actual_link }}" data-width="" data-layout="button_count"
                                data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                        </li>
                    </ul>
                </div>
                <ul style="float: right">

                </ul>

            </div>
        </div>

        <div class="report_luong" style="display: none">
            <div class="show_noti"></div>
            <div style="padding:0 20 20 20">
                <h3>Bạn gặp vấn đề gì ?</h3>
                <form method="POST">
                    <p>
                        <input type="radio" id="test1" value="Vi phạm bản quyền" name="radio_group" checked>
                        <label for="test1">Vi phạm bản quyền</label>
                    </p>
                    <p>
                        <input type="radio" id="test2" value="Không play được" name="radio_group">
                        <label for="test2">Không play được</label>
                    </p>
                    <p>
                        <input type="radio" id="test3" value="Không tải được" name="radio_group">
                        <label for="test3">Không tải được</label>
                    </p>
                    <p>
                        <input type="radio" id="test4" value="Chất lượng kém" name="radio_group">
                        <label for="test4">Chất lượng kém</label>
                    </p>
                    <p>
                        <input type="radio" id="test5" value="Thông tin chưa chính xác" name="radio_group">
                        <label for="test5">Thông tin chưa chính xác</label>
                    </p>
                    <p>
                        <input type="radio" id="test6" value="Lời bài hát chưa chính xác" name="radio_group">
                        <label for="test6">Lời bài hát chưa chính xác</label>
                    </p>
                    <p>
                        <input type="radio" id="test7" value="Karaoke chưa chính xác" name="radio_group">
                        <label for="test7">Karaoke chưa chính xác</label>
                    </p>
                    <p>
                        <input type="radio" id="test8" value="0" name="radio_group">
                        <label for="test8">Lỗi khác</label>
                    </p>
                    <div class="text_" style="display:none">
                        <h5>Nội dung</h5>
                        <textarea name="full" cols="30" rows="4" name="content" class="form-control"></textarea>
                    </div>
                    <button class="btn btn-primary send_report" style="margin-top:10px">Gửi báo lỗi</button>
                </form>
            </div>
        </div>
        <div class="lyric" id="_divLyricHtml">
            <div class="pd_name_lyric">
                <h2 class="name_lyric"><b>Lời bài hát: {{$song->name}}</b></h2>
                <p class="name_post">Lời đăng bởi: <a href="#"
                        title="{{getNameCustomer($song->author_id)}}">{{getNameCustomer($song->author_id)}}</a></p>
            </div>
            <p id="divLyric" class="pd_lyric trans" style="height: auto; max-height: 255px; overflow: hidden;">

                @if (!empty($song->lyric->content))
                @php
                echo preg_replace("/[\n\r]/", '<br>', trim($song->lyric->content, ''));
                @endphp
                @else
                {{ 'Chưa cập nhập lời bài hát' }}
                @endif

            </p>

            <div class="more_add" id="divMoreAddLyric">
                <a href="#" id="seeMoreLyric" title="Xem toàn bộ" class="btn_view_more" style="display: inline;">Xem
                    toàn
                    bộ<span class="down"></span></a>
                <a href="#" id="hideMoreLyric" title="Thu gọn" class="btn_view_hide hide-hh" style="display: none;">Thu
                    gọn<span class="up"></span></a>

            </div>
        </div>
        <div class="box-content-player new">
            <div class="album_info">
                <div class="detail_info_playing_now">
                    <span class="logo-official docquyen"></span>
                    <p>{{!empty($song->description) ? $song->description : 'Chưa cập nhập lời giới thiệu !!'}}</p>
                </div>
            </div>
        </div>

        <div class="comment tab-box-music">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo ThanhCa.tv</a></li>
                <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
            </ul>
            <div class="tab-content">
                <div id="cmt_cus" class="tab-pane fade in active">
                    @error('content')
                    <div class="alert alert-danger" role="alert">
                        <span style="color:red">{{$message}}</span>
                    </div>
                    @enderror
                    <div class="comment-header">
                        <div class="comment-total">{{$totalComment}} Bình luận</div>
                        <div class="sorting-block-wrapper clearfix">
                            <div class="sorting-by-wrapper">
                                <a>
                                    <div class="active-sorting">Bình luận
                                        {{empty($_GET['s']) ? 'mới nhất' : 'nổi bật'}} <i class="fa fa-angle-down"
                                            aria-hidden="true"></i></div>
                                </a>
                                <div class="dropdown-menu-list sort-comment z-hide">
                                    <ul>
                                        <li>
                                            <div class="dropdown-item"><a
                                                    href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}?s=highlight">Bình
                                                    luận nổi bật</a></div>
                                        </li>
                                        <li>
                                            <div class="dropdown-item"><a
                                                    href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}">Bình
                                                    luận mới nhất</a></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comment-list-wrapper">
                        <ul class="list-comment">

                            @if (count($comments) > 0)
                            @foreach ($comments as $comment)
                            <li class="comment-item parent_comment" idCommentCurrent="{{$comment->id}}">
                                <p class="medium-circle-card comment-avatar"><img
                                        src="{{!empty($comment->customer->avatar) ? $comment->customer->avatar : asset('client/img/default-avatar.png')}}"
                                        alt="Zing MP3"></p>
                                <div class="post-comment">
                                    <p class="username">
                                        <span>{{!empty($comment->customer->name) ? $comment->customer->name : $comment->customer->username }}</span><span
                                            class="reply-ago-time">{{ $comment->date }}</span></p>
                                    <p class="content">{{$comment->content}}</p>
                                    <div class="func-comment">
                                        <a style="cursor:pointer" class="z-like-rate
                                                    <?php
                                                        if (Auth::guard('customers')->check()) {
                                                            if (!empty($comment->arr_customer_id)) {
                                                                $arr_customer_id = explode(',',$comment->arr_customer_id);
                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                   if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                      echo "active";
                                                                   } else {
                                                                      echo "";
                                                                   }
                                                                }
                                                            }
                                                        } else {
                                                            echo "";
                                                        }
                                                    ?>
                                                    "
                                            onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$comment->id}})"><i
                                                class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                class="total_like">{{!empty($comment->total_like) ? $comment->total_like : '0'}}</span></a>

                                        <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                        @if (Auth::guard('customers')->check())
                                        @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                        <a class="delete-comment" style="cursor:pointer;color:red"
                                            url="{{url('delete_comment/'.$comment->id)}}">Xóa</a>
                                        @endif
                                        @endif
                                    </div>
                                    @php
                                    $comments_reply = $comment->getChildComment();
                                    @endphp
                                    <div class="comment-reply-list-wrapper">
                                        <ul class="list-comment list-comment-reply">
                                            @foreach ($comments_reply as $commentChild)
                                            <li class="comment-item">
                                                <p class="medium-circle-card comment-avatar"><img
                                                        src="{{!empty($commentChild->customer->avatar) ? $commentChild->customer->avatar : asset('client/img/default-avatar.png')}}"
                                                        alt="Zing MP3"></p>
                                                <div class="post-comment">
                                                    <p class="username">
                                                        <span>{{!empty($commentChild->customer->name) ? $commentChild->customer->name : $commentChild->customer->username }}</span><span
                                                            class="reply-ago-time">{{ $commentChild->date }}</span>
                                                    </p>
                                                    <p class="content">{{$commentChild->content}}</p>
                                                    <div class="func-comment">
                                                        <a style="cursor:pointer" class="z-like-rate
                                                                                <?php
                                                                                    if (Auth::guard('customers')->check()) {
                                                                                        if (!empty($commentChild->arr_customer_id)) {
                                                                                            $arr_customer_id = explode(',',$commentChild->arr_customer_id);
                                                                                            foreach ($arr_customer_id as $key => $customer_id) {
                                                                                            if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                                                echo "active";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            }
                                                                                        }

                                                                                    } else {
                                                                                        echo "";
                                                                                    }

                                                                                ?>
                                                                                "
                                                            onclick="likeComment(this,{{ Auth::guard('customers')->check() }} ? Auth::guard('customers')->user()->id : '0'}},{{$commentChild->id}})"><i
                                                                class="fa fa-thumbs-o-up" aria-hidden="true"></i><span
                                                                class="total_like">{{!empty($commentChild->total_like) ? $commentChild->total_like : '0'}}</span></a>

                                                        <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                        @if (Auth::guard('customers')->check())
                                                        @if (Auth::guard('customers')->user()->id ==
                                                        $comment->customer_id)
                                                        <a class="delete-comment" style="cursor:pointer;color:red"
                                                            url="{{url('delete_comment/'.$commentChild->id)}}">Xóa</a>
                                                        @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                            @endif


                            @if (!Auth::guard('customers')->check())
                            <div style="text-align: center" class="btn-login">
                                <button style="text-align: center" class="btn btn-sm btn-primary" data-toggle="modal"
                                    data-target="#myModal-login">Đăng nhâp để bình luận</button>
                            </div>
                            @else
                            <li class="comment-item">
                                <p class="medium-circle-card comment-avatar"><img
                                        src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                        alt="Zing MP3"></p>
                                <div class="post-comment">
                                    <form method="POST" action="{{route('saveComment')}}">
                                        @csrf
                                        <input type="hidden" name="status" value="-1">
                                        <input type="hidden" name="code" value="{{$song->code}}">
                                        <input type="hidden" name="customer_id"
                                            value="{{ Auth::guard('customers')->user()->id }}">
                                        <input type="hidden" name="parent_id" value="0">
                                        <textarea class="form-control" placeholder="" name="content"
                                            id="content"></textarea>
                                        <button type="submit" class="btn btn-success green"> Lưu</button>
                                    </form>
                                    <div class="comment-reply-list-wrapper"></div>
                                </div>
                            </li>

                            @endif

                        </ul>
                    </div>
                    <div class="more-cmt" code="{{$song->code}}" comment-current="10"
                        s="{{!empty($_GET['s']) ? $_GET['s'] : 'normal'}}">
                        <a onclick="more_comment(this)">Xem thêm</a>
                    </div>
                </div>
                <div id="cmt_fb" class="tab-pane fade">
                    <div class="fb-comments"
                        data-href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                        data-width="100%" data-numposts="5"></div>
                </div>
            </div>
        </div>
        @if (!empty($playList4) && count($playList4) > 0)
        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
            </div>
            <ul>
                @foreach ($playList4 as $item)
                <li>
                    <div class="box-left-album">
                        <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                            class="box_absolute" title="{{$item->name}}">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335"
                                        wgct="1">{{ !empty(countListenPlaylistHelper($item->code)->listen) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                    title="{{$item->name}}"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a
                                href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                class="name_song" title="{{$item->name}}">{{$item->name}}</a>
                        </h3>
                        {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4> --}}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (count($mv) > 0 && !empty($mv))
        <div class="home-list-item">
            <div class="tile_box_key">
                <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
            </div>
            <div class="home-mv">
                <div class="row">
                    {{-- {{ dd($mv) }} --}}
                    <?php $i = 1 ?>
                    @foreach ($mv as $video1)

                    <div class="col-md-3 col-xs-6 col-sm-3">
                        <div class="videosmall">
                            <div class="box_absolute">
                                <span class="view_mv"><span
                                        class="icon_view"></span><span>{{!empty(countListenHelper($video1['code'])['listen']) ? countListenHelper($video1['code'])['listen'] : '0' }}</span></span>
                                <span class="tab_lable_"></span>
                                <a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code']])}}"
                                    title="{{$video1['name']}}" class="img">
                                    <span class="icon_play"></span>
                                    <img src="{{!empty($video1['image']) ? $video1['image'] : getVideoYoutubeApi($video1['link'])['thumbnail']['mqDefault']}}"
                                        alt="{{$video1['name']}}" title="{{$video1['name']}}">
                                </a>
                                <span
                                    class="icon_time_video">{{getVideoYoutubeApi($video1['link'])['duration_sec']}}</span>
                            </div>
                            <h3><a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code'] ])}}"
                                    title="{{$video1['name']}}" class="name_song_index">{{$video1['name']}}</a></h3>
                            <h4>
                                {{-- {{dd($video1->artists)}} --}}
                                @if (!empty($video1['artists']) && count($video1['artists']) > 0)
                                @foreach ($video1['artists'] as $key => $item)
                                <a href="#" class="name_singer" title="{{$item['name']}}"
                                    target="_blank">{{$item['name']}}</a>
                                @if (($key+1) < count($video1['artists'])) ,
                                    @elseif(($key+1)==count($video1['artists']))@endif @endforeach @else <a>Chưa cập
                                    nhật</a>
                                    @endif
                            </h4>
                        </div>
                    </div>

                    <?php
                                    if ($i == 4) {
                                            break;
                                    };
                                    $i++;
                            ?>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="sidebar">
            <div class="box_video_recommended">
                <div class="tile_box_key" id="autoBox">
                    <h3><a class="nomore fontsmall">Xem tiếp</a></h3>
                    <div class="auto-box">
                        Autoplay
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"
                                checked>
                            <label class="onoffswitch-label active" for="myonoffswitch"
                                style="background: rgb(93, 196, 222);"></label>
                        </div>
                        <span class="icon-info-auto">i</span>
                        <div class="show-info-auto hideShowCase">Khi bật tính năng Autoplay, video được đề xuất sẽ
                            tự động phát tiếp.</div>
                    </div>
                </div>
                <div class="list_item_music">
                    <ul id="recommendZone" style="overflow: hidden;height: 400px;">
                        @if (!empty($songRandom5) && count($songRandom5) > 0)
                        @foreach ($songRandom5 as $item)

                        <li class="li_recommend_item">
                            <div class="box_absolute_video">

                                <a href="{{route('song',['name' =>  name_to_slug($item->name),'code' => $item->code])}}"
                                    title="{{$item->name}}" class="thum-video">
                                    {{-- <span class="icon_time_video">04:46</span> --}}
                                    <span class="icon_play"></span>
                                    <img src="{{!empty($item->image) ? $item->image : asset('images/default.png')}}"
                                        alt="{{$item->name}}" title="{{$item->name}}">
                                </a>
                            </div>
                            <div class="info_data">

                                <h3><a href="{{route('song',['name' =>  name_to_slug($item->name),'code' => $item->code])}}"
                                        title="{{$item->name}}" class="name_song listAutonext">{{$item->name}}</a>
                                </h3>
                                <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap">
                                    @if (!empty($item->artists) && count($item->artists) > 0)
                                    @foreach ($item->artists as $key => $artist)
                                    <a href="{{route('artistDetail',['slug' => $artist->slug])}}" class="name_singer"
                                        title="{{$artist->name}}" target="_blank">{{$artist->name}}</a>
                                    @if (($key+1) < count($item->artists))
                                        ,
                                        @elseif(($key+1) == count($item->artists))

                                        @endif
                                        @endforeach
                                        @else
                                        <a class="name_singer">Chưa cập nhật</a>
                                        @endif
                                </h4>
                                <span class="icon_view" id="NCTCounter_sg_6026072"
                                    wgct="1">{{!empty(countListenSongHelper($item->code)->listen) ? countListenSongHelper($item->code)->listen : '0'}}</span>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                    <p class="btn_more_item">
                        {{-- <a id="showMoreRecommend" href="javascript:;" class="btn_viewall">xem thêm</a>
                            <a id="hideMoreRecommend" href="javascript:;" class="btn_viewall hide">rút gọn</a> --}}
                    </p>
                </div>
            </div>
            {{ Sidebar::top100New() }}
        </div>
    </div>
</div>
</div>

@include('client.layout.fe_player')

@endsection
@section('js')
{{-- <script src="https://cdn.plyr.io/3.5.6/plyr.js"></script> --}}
<script src="{{asset('client/js/plyr.js')}}" type="text/javascript"></script>
<script src="{{asset('client/js/rabbit-lyrics.js')}}" type="text/javascript"></script>
<script src="{{asset('client/js/player.js')}}" type="text/javascript"></script>

<script>
    $('.send_report').on('click', function (e) {
        e.preventDefault();
        var Form = $(this).parents('form');
        let dataForm = new FormData(Form[0]);
        dataForm.set('_token', '{{csrf_token()}}');
        dataForm.set('user_id',
            `{{ Auth::guard('customers')->check() }} ? Auth::guard('customers')->user()->id : 0 }}`);
        dataForm.set('provider', 'song');
        dataForm.set('code', '{{$song->code}}')
        var checkedValue = $('input[name=radio_group]:checked').val();
        if (checkedValue == '0') {
            if ($('textarea[name=full]').val().trim().length == 0) {
                $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                    Nội dung không được bỏ trống !
                </div>`);
            } else {
                $.ajax({
                    url: "{{route('saveReport')}}",
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: dataForm,
                }).done(
                    result => {
                        if (result.status) {
                            $('.show_noti').html(`<div class="alert alert-success" role="alert">
                            Cám ơn ý kiến đóng góp của bạn !
                        </div>`);

                        } else {
                            $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                            Có lỗi xảy ra !
                        </div>`);
                        }
                        setTimeout(() => {
                            $('.show_noti').html('');
                            $('#btnReportError').trigger('click');
                        }, 2500);

                    });
            }
        } else {
            $.ajax({
                url: "{{route('saveReport')}}",
                method: 'post',
                processData: false,
                contentType: false,
                data: dataForm,
            }).done(
                result => {

                    if (result.status) {
                        $('.show_noti').html(`<div class="alert alert-success" role="alert">
                            Cám ơn ý kiến đóng góp của bạn !
                        </div>`);
                    } else {
                        $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                            Có lỗi xảy ra !
                        </div>`);
                    }
                    setTimeout(() => {
                        $('.show_noti').html('');
                        $('#btnReportError').trigger('click');
                    }, 2500);
                });
        }

    });
    $('#btnReportError').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.report_luong').css('display', 'none');
        } else {
            $(this).addClass('active');
            $('.report_luong').css('display', 'block');
        }
    });

    $('input[name=radio_group]').on('click', function () {
        if ($('#test8').is(':checked')) {
            $('.text_').css('display', 'block');
        } else {
            $('.text_').css('display', 'none');
        }
    })

</script>
<script>
    $(document).on('click', '.delete-comment', function (e) {
        e.preventDefault();
        r = confirm("Xóa comment!!!");
        if (r == true) {
            window.location.href = $(this).attr('url');
        } else {
            txt = "You pressed Cancel!";
        }
    })

</script>



@if (Auth::guard('customers')->check())
<script>
    $(document).on('click', '.reply-func', function () {
        if ($(this).hasClass('have_write_comment')) {
            $(this).removeClass('have_write_comment');
            $(this).parents('.comment-item').find('.comment-remove').remove();
        } else {
            $(this).addClass('have_write_comment');
            if ($(this).parents('.comment-item').has('li.comment-remove')) {
                $(this).parents('.comment-item').find('.comment-remove').remove();
                parent_id = $(this).parents('.parent_comment').attr('idcommentcurrent');
                username = $(this).parent('.func-comment').parent('.post-comment').find(
                    '.username > span:eq(0)').text();
                // console.log(username);
                $(this).parents('.comment-item').find('.comment-reply-list-wrapper').append(
                    `<li class="comment-item comment-remove">
                        <p class="medium-circle-card comment-avatar"><img
                                src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <form method="POST" action="{{route('saveComment')}}">
                                @csrf
                                <input type="hidden" name="status" value="-1">
                                <input type="hidden" name="code" value="{{$song->code}}">
                                <input type="hidden" name="customer_id" value="{{ !empty(Auth::guard('customers')->check()) ? Auth::guard('customers')->user()->id : '' }}">
                                <input type="hidden" name="parent_id" value="${parent_id}" >
                                <textarea class="form-control" placeholder="" name="content" id="content">@${username}:</textarea>
                                <button type="submit" class="btn btn-success green"> Lưu</button>
                            </form>
                            <div class="comment-reply-list-wrapper"></div>
                        </div>
                    </li>`);
            };

        }
    })

</script>
@else
<script>
    jQuery(function () {
        $('footer').css('display', 'none');
        $(document).on('click', '.reply-func', function () {
            alert('Đăng nhập để sử dụng chức năng này !!!');
        })
    });

</script>
@endif

<script>
    var json_listSong = '<?= json_encode($json_listSong) ?>';
    var json_listSongRandom = '<?= json_encode($listSongRandom5) ?>';
    var route_listen_song = `{{ route("songListen") }}`;
    var route_random_song = `{{ route("route_random_song") }}`;
    var add_to_wishlist = `{{ route("add_to_wishlist") }}`;
    var player_search = `{{ route("player_search") }}`;

    // listSong = JSON.parse(json_listSong);
    // json_listSongRandom = JSON.parse(json_listSongRandom);
    // console.log(listSong);
    function formatTime(time) {
        time = Math.round(time);

        var minutes = Math.floor(time / 60),
            seconds = time - minutes * 60;

        seconds = seconds < 10 ? '0' + seconds : seconds;
        return minutes + ":" + seconds;
    }



    tc_player(JSON.parse(json_listSong), JSON.parse(json_listSongRandom), route_listen_song, route_random_song, add_to_wishlist, player_search);

    $('#myonoffswitch').click(function () {
        if ($('#myonoffswitch:checked').length == 1) {
            $('.onoffswitch').find('label').css('background', '#5DC4DE');
            $('.onoffswitch').find('label').addClass('active');
            localStorage.setItem("nextSong", true);
        } else {
            $('.onoffswitch').find('label').css('background', '#c0c0c0');
            $('.onoffswitch').find('label').removeClass('active');
            localStorage.setItem("nextSong", false);
        }
    });

</script>


{{-- thêm bài hát vào lịch sử nghe --}}
@if (Auth::guard('customers')->check())
<script>
    $.ajax({
        type: 'POST',
        url: '{{route("addToHistory")}}',
        data: {
            code: '{{$song->code}}',
            customer_id: `{{ Auth::guard('customers')->user()->id }}`,
            provider: 'song',
        },
        success: function (data) {
            console.log('+ add history');
        }
    });

</script>
@endif
{{-- END--thêm bài hát vào lịch sử nghe --}}

@endsection
