@extends('client.layout.master')
@section('page_title')
Trang chủ
@endsection
@php
    $dataGenerals = json_decode(Options::_get('generals'));
@endphp
@section('content')

<div class="home">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div id="sync1" class="owl-carousel owl-theme">
                    {{-- {{dd($dataGenerals)}} --}}
                    @if (!empty($dataGenerals->slideshow))
                    @foreach ($dataGenerals->slideshow as $item)
                    <div class="item">
                        <div class="bn-home-item">
                            <a href="{{$item->link}}">
                                <img src="{{$item->slide}}" alt="">
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>

                <div id="sync2" class="owl-carousel owl-theme">
                    @if (!empty($dataGenerals->slideshow))
                    @foreach ($dataGenerals->slideshow as $item)
                    <div class="item">
                        <div class="bn-home-item">
                            <a href="{{$item->link}}">
                                <img src="{{$item->slide}}" alt="">
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                @php

                @endphp
                @if (Auth::guard('customers')->check() && count(get_song_history_user(Auth::guard('customers')->user()->id)) > 0)
                     <div class="home-list-item">
                        <div class="tile_box_key" style="padding-bottom:20px">
                            <h2><a title="BÀI HÁT BẠN ĐÃ NGHE" href="{{route('history_song_player',['id' => Auth::guard('customers')->user()->id ])}}" style="float:left">BÀI HÁT BẠN ĐÃ NGHE</a><a id="aUrlTop20" href="{{route('history_song_player',['id' => Auth::guard('customers')->user()->id ])}}" title="BXH Bài Hát"
                                class="play_all" style="padding: 0px"></a></h2>
                        </div>
                        <div class="owl-carousel owl-theme history_listen">
                            @foreach (get_song_history_user(Auth::guard('customers')->user()->id) as $song)
                                <div class="item">
                                    <div class="box-left-album">
                                        <a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code ])}}" class="box_absolute"
                                            title="{{ !empty($song->name) ? $song->name : '' }}">
                                            <div class="bg_action_info">
                                                {{-- <span class="view_listen"><span class="icon_listen"></span><span id=""
                                                        wgct="1">{{!empty($song->listen) ? $song->listen : '0'}}</span></span> --}}
                                                <span class="icon_play"></span>
                                            </div>
                                            <span class="avatar"><img src="{{!empty($song->image) ? $song->image : asset('client/img/song-default.png') }}"
                                                    alt="{{ !empty($song->name) ? $song->name : '' }}" title="{{ !empty($song->name) ? $song->name : '' }}"></span>
                                        </a>
                                    </div>
                                    <div class="info_album">
                                        <h3 class="h3seo"><a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code ])}}" class="name_song"
                                                title="{{ !empty($song->name) ? $song->name : '' }}">{{ !empty($song->name) ? $song->name : '' }}</a>
                                        </h3>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endif


                @if (!empty($dataGenerals->topic))
                <div class="home-list">
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">Nghe gì hôm nay</a></h2>
                        </div>
                        <ul>
                            @forelse ($playlist_topic as $pt)
                            <li>
                                <div class="box-left-album">
                                    <a href="{{route('playlist',['name' => name_to_slug($pt->name),'code' => $pt->code ])}}"
                                        class="box_absolute" title="{{ !empty($pt->name) ? $pt->name : '' }}">
                                        <div class="bg_action_info">
                                            <span class="view_listen"><span class="icon_listen"></span><span id=""
                                                    wgct="1">{{!empty($pt->listen) ? $pt->listen : '0'}}</span></span>
                                            <span class="icon_play"></span>
                                        </div>
                                        <span class="avatar"><img
                                                src="{{!empty($pt->image) ? $pt->image : asset('images/default.png') }}"
                                                alt="{{ !empty($pt->name) ? $pt->name : '' }}"
                                                title="{{ !empty($pt->name) ? $pt->name : '' }}"></span>
                                    </a>
                                </div>
                                <div class="info_album">
                                    <h3 class="h3seo"><a href="#" class="name_song"
                                            title="{{ !empty($pt->name) ? $pt->name : '' }}">{{ !empty($pt->name) ? $pt->name : '' }}</a>
                                    </h3>
                                </div>
                            </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                @endif

                    {{-- @if (count($playlistsNew) > 0 && !empty($playlistsNew))
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Bài hát bạn đã nghe" href="#">BÀI HÁT BẠN ĐÃ NGHE</a></h2>
                        </div>
                        <div class="history_song">
                            @foreach ($playlistsNew as $item)

                            <div class="item">
                                <div class="box-left-album">
                                    <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                        class="box_absolute" title="{{$item->name}}">
                                        <div class="bg_action_info">
                                            <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335"
                                                    wgct="1">{{!empty(countListenPlaylistHelper($item->code)) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></span>
                                            <span class="icon_play"></span>

                                        </div>

                                        <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                                title="{{$item->name}}"></span>
                                    </a>
                                </div>
                                <div class="info_album">
                                    <h3 class="h3seo"><a
                                            href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                            class="name_song" title="{{$item->name}}">{{$item->name}}</a>
                                    </h3>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif --}}

                    @if (count($playlistsNew) > 0 && !empty($playlistsNew))
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">Mới phát hành</a></h2>
                        </div>
                        <ul>
                            @foreach ($playlistsNew as $item)

                            <li>
                                <div class="box-left-album">
                                    <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                        class="box_absolute" title="{{$item->name}}">
                                        <div class="bg_action_info">
                                            <span class="view_listen"><span class="icon_listen"></span><span
                                                    id="NCTCounter_pl_63601335"
                                                    wgct="1">{{!empty(countListenPlaylistHelper($item->code)) ? countListenPlaylistHelper($item->code)->listen : '0'}}</span></span>
                                            <span class="icon_play"></span>

                                        </div>

                                        <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                                title="{{$item->name}}"></span>
                                    </a>
                                </div>
                                <div class="info_album">
                                    <h3 class="h3seo"><a
                                            href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                            class="name_song" title="{{$item->name}}">{{$item->name}}</a>
                                    </h3>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (count($videosHot) > 0 && !empty($videosHot))
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">MV HOT</a></h2>
                        </div>
                        <div class="home-list-item">
                            <div class="home-mv">
                                <div class="row">
                                    @foreach ($videosHot as $key => $video)
                                    @if ($key < 2) <div class="col-md-6 col-sm-6 col-xs-12 videolarge">
                                        <div class="videosmall">

                                            <div class="box_absolute">
                                                <span class="view_mv"><span
                                                        class="icon_view"></span><span>{{!empty(countListenHelper($video->code)->listen) ? countListenHelper($video->code)->listen : '0'}}</span></span>
                                                <span class="tab_lable_"></span>
                                                <a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}"
                                                    title="{{$video->name}}" class="img">
                                                    <span class="icon_play"></span>
                                                    <img src="{{!empty($video->image) ? $video->image : getVideoYoutubeApi($video->link)['thumbnail']['default']}}"
                                                        alt="{{$video->name}}" title="{{$video->name}}">
                                                </a>
                                                <span
                                                    class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
                                                <div class="name_video_large">
                                                    <h3><a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}"
                                                            title="{{$video->name}}"
                                                            class="name_song">{{$video->name}}</a></h3>
                                                    <h4>
                                                        @if (!empty($video->artists) && count($video->artists) > 0)
                                                        @foreach ($video->artists as $key => $item)
                                                        <a class="name_singer"
                                                            href="{{route('artistDetail',['slug' => $item->slug])}}">{{$item->name}}</a>
                                                        @if (($key+1) < count($video->artists))
                                                            ,
                                                            @elseif(($key+1) == count($video->artists))

                                                            @endif

                                                            @endforeach
                                                            @else
                                                            <a class="name_singer"> Chưa cập nhật</a>
                                                            @endif
                                                    </h4>
                                                </div>
                                                <a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}"
                                                    title="{{$video->name}}"><span class="item-mask-bg"></span></a>
                                            </div>
                                        </div>
                                </div>
                                @elseif($key >= 2)
                                <div class="col-md-3 col-xs-6 col-sm-3">
                                    <div class="videosmall">
                                        <div class="box_absolute">
                                            <span class="view_mv"><span
                                                    class="icon_view"></span><span>{{!empty(countListenHelper($video->code)->listen) ? countListenHelper($video->code)->listen : '0'}}</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code ])}}"
                                                title="{{$video->name}}" class="img">
                                                <span class="icon_play"></span>
                                                <img src="{{!empty($video->image) ? $video->image : getVideoYoutubeApi($video->link)['thumbnail']['mqDefault']}}"
                                                    title="{{$video->name}}">
                                            </a>
                                            <span
                                                class="icon_time_video">{{getVideoYoutubeApi($video->link)['duration_sec']}}</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (count($songs) > 0 && !empty($songs))
                <div class="home-list-item">
                    <div class="showNotiWishList"></div>
                    <div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">Bài hát</a></h2>
                    </div>
                    <div class="row">
                        @foreach ($songs as $song)
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="box-content-music-list">
                                <div class="info_song" style="color: #999999;">
                                    <a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                                        class="avatar_song" title="{{$song->name}}">
                                        <img src="{{!empty($song->image) ? $song->image : asset('client/img/song-default.png')}}"
                                            alt="{{$song->name}}" title="{{$song->name}}">
                                    </a>
                                    <h3><a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                                            class="name_song" title="{{$song->name}}">{{$song->name}}</a></h3>
                                    <div class="name_sing_under">
                                        @if (!empty($song->artists) && count($song->artists) > 0)
                                        @foreach ($song->artists as $key => $item)
                                        <a class="name_singer"
                                            href="{{route('artistDetail',['slug' => $item->slug])}}">{{$item->name}}</a>
                                        @if (($key+1) < count($song->artists))
                                            ,
                                            @elseif(($key+1) == count($song->artists))

                                            @endif

                                            @endforeach
                                            @else
                                            <a>Chưa cập nhật</a>
                                            @endif

                                    </div>
                                </div>

                                <span class="icon_listen">{{$song->listen}}</span>
                                <div class="box_song_action">
                                    <a onclick="addWishList('{{$song->code}}','{{ Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : ''}}')"
                                        style="cursor: pointer" id="btnShowBoxPlaylist_Z4vh3ifcU1vN"
                                        class="button_add_playlist"
                                        title="Thêm bài hát {{$song->name}} vào playlist yêu thích"></a>
                                    <a href="{{route('song',['name' => name_to_slug($song->name),'code' => $song->code])}}"
                                        class="button_playing" title="Nghe bài hát {{$song->name}}"></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                @if (count($karaoke) > 0 && !empty($karaoke))
                <div class="home-list-item">
                    <div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">Karaoke</a></h2>
                    </div>
                    <div class="home-mv">
                        <div class="row">
                            @foreach ($karaoke as $ka)
                            <div class="col-md-3 col-xs-6 col-sm-3">
                                <div class="videosmall">
                                    <div class="box_absolute">
                                        <span class="view_mv"><span
                                                class="icon_view"></span><span>{{!empty(countListenHelper($ka->code)->listen) ? countListenHelper($ka->code)->listen : '0'}}</span></span>
                                        <span class="tab_lable_"></span>
                                        <a href="{{route('detailVideo',['name' => name_to_slug($ka->name),'code' => $ka->code ])}}"
                                            title="{{$ka->name}}" title="{{$ka->name}}" class="img">
                                            <span class="icon_play"></span>
                                            <img src="{{!empty($ka->image) ? $ka->image : getVideoYoutubeApi($ka->link)['thumbnail']['default']}}"
                                                alt="{{$ka->name}}" title="{{$ka->name}}">
                                        </a>
                                        <span class="icon_time_video"></span>
                                    </div>
                                    <h3><a href="{{route('detailVideo',['name' => name_to_slug($ka->name),'code' => $ka->code ])}}"
                                            title="{{$ka->name}}" title="{{$ka->name}}"
                                            class="name_song_index">{{$ka->name}}</a></h3>
                                    <h4>
                                        @if (!empty($ka->artists) && count($ka->artists) > 0)
                                        @foreach ($ka->artists as $key => $item)
                                        <a class="name_singer"
                                            href="{{route('artistDetail',['slug' => $item->slug])}}">{{$item->name}}</a>
                                        @if (($key+1) < count($ka->artists))
                                            ,
                                            @elseif(($key+1) == count($ka->artists))

                                            @endif

                                            @endforeach
                                            @else
                                            <a class="name_singer">Chưa cập nhật</a>
                                            @endif

                                    </h4>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="sidebar">
                    @include('client.layout.singer-trending')
                    {{-- ****************** --}}
                    @include('client.layout.song_chart')
                    {{-- ****************** --}}
                    @include('client.layout.video_chart')
                    {{-- ****************** --}}
                    @include('client.layout.w_top100')
                    {{-- ****************** --}}
                    {{ Sidebar::top100New() }}
                </div>
            </div>

        </div>

    </div>
</div>
</div>

@endsection
@section('js')
    @if (!empty(session('login_fail')))
    <script>
       alert('{{session('login_fail')}}');
    </script>
    @endif
    @if (!empty(session('active_success')))
    <script>
        alert('{{session('active_success')}}');
    </script>
    @endif
@endsection
