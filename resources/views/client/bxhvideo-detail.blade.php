@extends('client.layout.master')
@section('page_title')
Danh sách bảng xếp hạng Video
@endsection
@section('content')
@php
    $dataGenerals = json_decode(Options::_get('generals'));

@endphp
<div class="video home video-bxh">
	<div class="container">
        <div class="box_playing">
            <div class="playing_video">
                <div class="box-view-player" style="height:466px">
                    <div class="player0" style="height:100%">
                        <div id="playerVideo"></div>
                    </div>
                </div>
                <div id="listAlbum" class="box-list-playing">
                    <div class="header-opa"></div>
                    <div class="title-show-name">
                        <h1>
                            @if (!empty($_GET['c']) && !empty($_GET['st']))
                                {{getInfoVideo($_GET['c'])->name}}
                            @else
                                {{ !empty($chart[0]->name) ? $chart[0]->name : '' }}
                            @endif
                        </h1>
                        <span>
                            @if (!empty($_GET['c']) && !empty($_GET['st']))
                                {{$_GET['st']}}
                            @else
                                1
                            @endif
                            / {{count($chart)}} video</span>
                    </div>
                    <ul class="box-detail-item-playlist">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 411px;">
                            <div id="idScrllVideoInAlbum" style="overflow: auto; width: auto; height: 411px;">
                            @php
                                $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") ."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                $arr_link = explode('?',$actual_link);

                            @endphp
                            @foreach ($chart as $key => $item)
                                @php
                                    !empty($_GET['st']) ? $_GET['st'] : $_GET['st'] = 1;
                                @endphp
                                <li vid="{{getIdVideo($item->link)}}" class="{{!empty($_GET['st']) && $_GET['st'] == ($key+1)? 'active' : ''}}">
                                <a title="{{$item->name}}" class="click_active" href="{{$arr_link[0].'?c='.$item->code.'&st='.($key+1)}}" style="cursor:pointer">
                                <span>{{$key+1}}</span>
                                        <img alt="{{$item->name}}" title="{{$item->name}}h"
                                        src="{{!empty($item->image) ? $item->image : getVideoYoutubeApi($item->link)['thumbnail']['mqDefault']}}">
                                        <h3 class="name-title click_active" href="{{$arr_link[0].'?c='.$item->code.'&st='.($key+1)}}" style="cursor:pointer">{{$item->name}}h</h3>
                                       @if (!empty($item->artists) && count($item->artists) > 0)
                                         <p class="name-singer">
                                            @php
                                                $name = '';
                                            @endphp
                                            @foreach ($item->artists as $key1 => $item1)

                                                @php
                                                    $name .= $item1->name.',';
                                                @endphp

                                            @endforeach
                                            {{rtrim($name,',')}}
                                            </p>
                                        @else
                                            <p class="name-singer">Chưa cập nhật</p>
                                        @endif

                                    </a>
                                </li>
                            @endforeach



                            </div>
                        </div>
                    </ul>
                </div>

            </div>
            <div id="infoVideo" class="info_name_songmv">

                <div class="topbreadCrumb">
                    <span><a href="#">Nghe nhạc</a></span>
                    {{-- › <span><a href="#">Video Việt Nam</a>, <a href="#">Video Nhạc Trẻ</a></span> --}}
                    › <span>
                        @if (!empty($_GET['c']) &&  !empty($_GET['st']))

                            @if (!empty(getInfoVideo($_GET['c'])->artists) && count(getInfoVideo($_GET['c'])->artists) > 0)
                                    @foreach (getInfoVideo($_GET['c'])->artists as $key => $item)
                                         <a href="{{route('artistDetail',['slug' => $item->name])}}">{{$item->name}}</a>
                                            @if (($key+1) < count(getInfoVideo($_GET['c'])->artists))
                                                ,
                                            @elseif(($key+1) == count(getInfoVideo($_GET['c'])->artist))

                                            @endif

                                    @endforeach
                            @else
                                        <a>Chưa cập nhật</a>
                            @endif
                        @else
                            @if (!empty($chart[0]->artists) && count($chart[0]->artists) > 0)
                                    @foreach ($chart[0]->artists as $key => $item)
                                         <a href="{{route('artistDetail',['slug' => $item->name])}}">{{$item->name}}</a>
                                            @if (($key+1) < count($chart[0]->artists))
                                                ,
                                            @elseif(($key+1) == count($chart[0]->artist))

                                            @endif

                                    @endforeach
                            @else
                                        <a>Chưa cập nhật</a>
                            @endif
                        @endif

                    </span>
                </div>
                <div class="name_title">
                    <h1>
                        @if (!empty($_GET['c']) && !empty($_GET['st']))
                            {{getInfoVideo($_GET['c'])->name}}
                        @else
                            {{ !empty($chart[0]->name) ? $chart[0]->name : '' }}
                        @endif
                    </h1>
                    <span style="font-size: 22px;"> - </span>
                    <h2 class="name-singer" style="color: #000;">
                        @if (!empty($_GET['c']) &&  !empty($_GET['st']))

                            @if (!empty(getInfoVideo($_GET['c'])->artists) && count(getInfoVideo($_GET['c'])->artists) > 0)
                                    @foreach (getInfoVideo($_GET['c'])->artists as $key => $item)
                                         <a href="{{route('artistDetail',['slug' => $item->name])}}">{{$item->name}}</a>
                                            @if (($key+1) < count(getInfoVideo($_GET['c'])->artists))
                                                ,
                                            @elseif(($key+1) == count(getInfoVideo($_GET['c'])->artist))

                                            @endif

                                    @endforeach
                            @else
                                    <a>Chưa cập nhật</a>
                            @endif
                        @else
                            @if (!empty($chart[0]->artists) && count($chart[0]->artists) > 0)
                                    @foreach ($chart[0]->artists as $key => $item)
                                         <a href="{{route('artistDetail',['slug' => $item->name])}}">{{$item->name}}</a>
                                            @if (($key+1) < count($chart[0]->artists))
                                                ,
                                            @elseif(($key+1) == count($chart[0]->artist))

                                            @endif

                                    @endforeach
                            @else
                                        <a>Chưa cập nhật</a>
                            @endif
                        @endif
                    </h2>
                    <div class="box-link-songmv"><a href="#" title="Nghe phiên bản Audio của Video này" class="btn-link-songmv btn-to-music"></a></div></div>
                <div class="show_listen">
                    <span id="NCTCounter_sg_6010701" wgct="1">
                        @if (!empty($_GET['c']) && !empty($_GET['st']))
                            {{!empty(countListenHelper($_GET['c'])->listen) ? countListenHelper($_GET['c'])->listen : '0' }}
                        @else
                            {{!empty(countListenHelper(!empty($chart[0]->name) ? $chart[0]->name : '')->listen) ? countListenHelper(!empty($chart[0]->name) ? $chart[0]->name : '')->listen : '0' }}
                        @endif
                    </span>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="box_menu_player new" style="border-top: solid 1px #ececec;">
                    <div class="user_upload">
                        {{-- <span ic="userImageNowplaying"><a href="#" class=""><img src="https://avatar-nct.nixcdn.com/avatar/2015/08/26/6/7/6/7/1440574626064.jpg" title="nct_official" class="userImageNowplaying"></a></span> --}}
                        <i>Upload bởi:</i><br>
                        <div style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;"><span><a href="#" rel="nofollow" target="_top" title="nct_official" class="">admin</a></span></div>
                    </div>

                </div>

                <div class="lyric" id="_divLyricHtml">
                    <div class="pd_name_lyric">
                        <h2 class="name_lyric"><b>Lời bài hát:
                            @if (!empty($_GET['c']) && !empty($_GET['st']))
                                {{getInfoVideo($_GET['c'])->name}}
                            @else
                                {{ !empty($chart[0]->name) ? $chart[0]->name : '' }}
                            @endif
                        </b></h2>
                        <p class="name_post">Lời đăng bởi: <a href="#" title="">
                            admin
                        </a></p>
                    </div>
                    <p id="divLyric" class="pd_lyric trans" style="height:auto;max-height:255px;overflow:hidden;">
                        @if (!empty($_GET['c']) && !empty($_GET['st']))
                                @php echo !empty(getInfoVideo($_GET['c'])->description) ? getInfoVideo($_GET['c'])->description : 'Chưa có lời bài hát !!!' @endphp
                        @else
                                @php echo !empty($chart[0]->description) ? $chart[0]->description : 'Chưa có lời bài hát !!!'  @endphp
                        @endif

                    </p>

                    <div class="more_add" id="divMoreAddLyric">
                        <a href="#" id="seeMoreLyric" title="Xem toàn bộ" class="btn_view_more">Xem toàn bộ<span class="down"></span></a>
                        <a href="#" id="hideMoreLyric" title="Thu gọn" class="btn_view_hide hide-hh">Thu gọn<span class="up"></span></a>

                    </div>
                </div>
                <div class="comment tab-box-music">
                    <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo {{$dataGenerals->name}}</a></li>
                    <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="cmt_cus" class="tab-pane fade in active">
                        <div class="comment-header">
                            <div class="comment-total">{{!empty($totalComment) ? $totalComment->total_comment : '0' }} Bình luận</div>
                            <div class="sorting-block-wrapper clearfix">
                                <div class="sorting-by-wrapper">
                                    <a><div class="active-sorting">Bình luận mới nhất <i class="fa fa-angle-down" aria-hidden="true"></i></div></a>
                                    <div class="dropdown-menu-list sort-comment z-hide">
                                        <ul>

                                            <li>
                                                <div class="dropdown-item"><a
                                                href="{{Request::url().'?c='.(!empty($_GET['c']) ? $_GET['c'] : '').'&st='.(!empty($_GET['st']) ? $_GET['st'] : '')}}?s=highlight">Bình
                                                        luận nổi bật</a></div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item"><a
                                                    href="{{Request::url().'?c='.(!empty($_GET['c']) ? $_GET['c'] : '').'&st='.(!empty($_GET['st']) ? $_GET['st'] : '')}}">Bình
                                                        luận mới nhất</a></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list-wrapper">
                            <ul class="list-comment">

                                @if (count($comments) > 0)
                                    @foreach ($comments as $comment)
                                        <li class="comment-item parent_comment" idCommentCurrent="{{$comment->id}}">
                                            <p class="medium-circle-card comment-avatar"><img
                                                    src="{{!empty($comment->customer->avatar) ? $comment->customer->avatar : asset('client/img/default-avatar.png')}}" alt="Zing MP3"></p>
                                            <div class="post-comment">
                                            <p class="username"><span>{{!empty($comment->customer->name) ? $comment->customer->name : $comment->customer->username }}</span><span class="reply-ago-time">{{ $comment->date }}</span></p>
                                                <p class="content">{{$comment->content}}</p>
                                                <div class="func-comment">
                                                    <a style="cursor:pointer" class="z-like-rate
                                                    <?php
                                                        if (Auth::guard('customers')->check()) {
                                                            if (!empty($comment->arr_customer_id)) {
                                                                $arr_customer_id = explode(',',$comment->arr_customer_id);
                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                   if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                      echo "active";
                                                                   } else {
                                                                      echo "";
                                                                   }
                                                                }
                                                            }
                                                        } else {
                                                            echo "";
                                                        }
                                                    ?>
                                                    " onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$comment->id}})"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span class="total_like">{{!empty($comment->total_like) ? $comment->total_like : '0'}}</span></a>

                                                    <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                    @if (Auth::guard('customers')->check())
                                                        @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                            <a class="delete-comment" style="cursor:pointer;color:red" url="{{url('delete_comment/'.$comment->id)}}">Xóa</a>
                                                        @endif
                                                    @endif
                                                </div>
                                                @php
                                                    $comments_reply = $comment->getChildComment();
                                                @endphp
                                                <div class="comment-reply-list-wrapper">
                                                    <ul class="list-comment list-comment-reply">
                                                        @foreach ($comments_reply as $commentChild)
                                                        <li class="comment-item">
                                                            <p class="medium-circle-card comment-avatar"><img
                                                                    src="{{!empty($commentChild->customer->avatar) ? $commentChild->customer->avatar : asset('client/img/default-avatar.png')}}"
                                                                    alt="Zing MP3"></p>
                                                            <div class="post-comment">
                                                                <p class="username"><span>{{!empty($commentChild->customer->name) ? $commentChild->customer->name : $commentChild->customer->username }}</span><span class="reply-ago-time">{{ $commentChild->date }}</span></p>
                                                                <p class="content">{{$commentChild->content}}</p>
                                                                <div class="func-comment">
                                                                    <a style="cursor:pointer" class="z-like-rate
                                                                    <?php
                                                                        if (Auth::guard('customers')->check()) {
                                                                            if (!empty($commentChild->arr_customer_id)) {
                                                                                $arr_customer_id = explode(',',$commentChild->arr_customer_id);
                                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                                if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                                    echo "active";
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                }
                                                                            }

                                                                        } else {
                                                                            echo "";
                                                                        }

                                                                    ?>
                                                                    " onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$commentChild->id}})"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span class="total_like">{{!empty($commentChild->total_like) ? $commentChild->total_like : '0'}}</span></a>

                                                                    <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                                    @if (Auth::guard('customers')->check())
                                                                        @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                                            <a class="delete-comment" style="cursor:pointer;color:red" url="{{url('delete_comment/'.$commentChild->id)}}">Xóa</a>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif


                                @if (!Auth::guard('customers')->check())
                                <div class="btn-login">
                                    <button  style="text-align: center"  class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal-login">Đăng nhâp để bình luận</button>
                                </div>
                                @else

                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img
                                    src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}" alt="Zing MP3"></p>
                                    <div class="post-comment">
                                            <form method="POST" action="{{route('saveComment')}}">
                                                @csrf
                                            <input type="hidden" name="code" value="{{Request::segment(3)}}">
                                                <input type="hidden" name="customer_id" value="{{ Auth::guard('customers')->user()->id }}">
                                                <input type="hidden" name="parent_id" value="0">
                                                <input type="hidden" name='status' value="-1">
                                                <textarea class="form-control" placeholder="" name="content" id="content"></textarea>
                                                <button type="submit" class="btn btn-success green"> Lưu</button>
                                            </form>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="more-cmt" code="{{Request::segment(3)}}" comment-current="10" s="{{!empty($_GET['s']) ? $_GET['s'] : 'normal'}}">
                            <a onclick="more_comment(this)">Xem thêm</a>
                        </div>
                    </div>
                    <div id="cmt_fb" class="tab-pane fade">
                        <div class="fb-comments"
                            data-href="{{Request::url()}}" data-width="100%"
                            data-numposts="5"></div>
                    </div>
                  </div>
                </div>
                @if (!empty($playList4) && count($playList4) > 0)
                <div class="home-list-item">
                    <div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
                    </div>
                    <ul>
                        @foreach ($playList4 as $item)
                        <li>
                            <div class="box-left-album">
                                <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                    class="box_absolute" title="{{$item->name}}">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335"
                                                wgct="1">{{!empty(countListenHelper($item->code)->listen) ? countListenHelper($item->code)->listen : '0'}}</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}"
                                            title="{{$item->name}}"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a
                                        href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}"
                                        class="name_song" title="{{$item->name}}">{{$item->name}}</a></h3>
                                {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4> --}}
                            </div>
                        </li>
                        @endforeach


                    </ul>
                </div>
                @endif
                @if (count($videoRandom4) > 0 && !empty($videoRandom4))
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
                        </div>
                        <div class="home-mv">
                            <div class="row">
                                <?php $i = 1 ?>
                                @foreach ($videoRandom4 as $video1)

                                <div class="col-md-3 col-xs-6 col-sm-3">
                                    <div class="videosmall">
                                        <div class="box_absolute">
                                            <span class="view_mv"><span
                                                    class="icon_view"></span><span>{{!empty(countListenHelper($video1['code'])['listen']) ? countListenHelper($video1['code'])['listen'] : '0' }}</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code']])}}"
                                                title="{{$video1['name']}}" class="img">
                                                <span class="icon_play"></span>
                                                <img src="{{!empty($video1['image']) ? $video1['image'] : getVideoYoutubeApi($video1['link'])['thumbnail']['mqDefault']}}"
                                                    alt="{{$video1['name']}}" title="{{$video1['name']}}">
                                            </a>
                                            <span class="icon_time_video">{{getVideoYoutubeApi($video1['link'])['duration_sec']}}</span>
                                        </div>
                                        <h3><a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code'] ])}}"
                                                title="{{$video1['name']}}" class="name_song_index">{{$video1['name']}}</a></h3>
                                        <h4>
                                            {{-- {{dd($video1->artists)}} --}}
                                            @if (!empty($video1['artists']) && count($video1['artists']) > 0)
                                            @foreach ($video1['artists'] as $key => $item)
                                            <a href="{{route('artistDetail',['slug' => $item['slug']])}}" class="name_singer"
                                                title="{{$item['name']}}" target="_blank">{{$item['name']}}</a>
                                            @if (($key+1) < count($video1['artists'])) , @elseif(($key+1)==count($video1['artists']))@endif
                                                @endforeach @else <a>Chưa cập nhật</a>
                                                @endif
                                        </h4>
                                    </div>
                                </div>

                                <?php
                                    if ($i == 4) {
                                        break;
                                    };
                                    $i++;
                            ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                </div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="sidebar">

                    @include('client.layout.w_top100')
                    {{-- ****************** --}}
					{{ Sidebar::top100New() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('js')
<script src="http://www.youtube.com/player_api"></script>
<script>
@if(!empty($_GET['st']) && !empty($_GET['c']) )

    function onYouTubePlayerAPIReady() {
            player = new YT.Player('playerVideo', {
              width: '100%',
              height: '100%',
              videoId: `{{ getIdVideo(getInfoVideo($_GET['c'])->link) }}`,
              events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
              },
              playerVars: {
                        autoplay : 1,
                        rel: 0,
                        showinfo: 0,
                        ecver: 2

                },
        });
    }
@else{
    function onYouTubePlayerAPIReady() {
            player = new YT.Player('playerVideo', {
              width: '100%',
              height: '100%',
              videoId: `{{getIdVideo(!empty($chart[0]->link)? $chart[0]->link : '')}}`,
              events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
              },
              playerVars: {
                        autoplay : 1,
                        rel: 0,
                        showinfo: 0,
                        ecver: 2

                },
        });
    }
}
@endif

function onPlayerReady(event) {
            event.target.playVideo();
    }

        // when video ends
function onPlayerStateChange(event) {
    if (event.data === 0) {
                $('.player0').html('<div id="playerVideo"></div>');


                liCurrent  = $('#idScrllVideoInAlbum').find('li.active');
                liNext = liCurrent.next();
                href = liNext.find('a:eq(0)').attr('href');
                window.location.href = href;
            }
}

$(document).on('click','.delete-comment',function(e){
        e.preventDefault();
        r = confirm("Xóa comment!!!");
        if (r == true) {
            window.location.href = $(this).attr('url');
        } else {
            txt = "You pressed Cancel!";
        }
    })

$(document).on('click', '.reply-func', function () {
        <?php
            if (Auth::guard('customers')->check()){
        ?>
            if($(this).hasClass('have_write_comment')){
                $(this).removeClass('have_write_comment');
                $(this).parents('.comment-item').find('.comment-remove').remove();
            }else{
                $(this).addClass('have_write_comment');
                if ($(this).parents('.comment-item').has('li.comment-remove')) {
                    $(this).parents('.comment-item').find('.comment-remove').remove();
                    parent_id = $(this).parents('.parent_comment').attr('idcommentcurrent');
                    username = $(this).parent('.func-comment').parent('.post-comment').find('.username > span:eq(0)').text();
                    // console.log(username);
                    $(this).parents('.comment-item').find('.comment-reply-list-wrapper').append(
                    `<li class="comment-item comment-remove">
                        <p class="medium-circle-card comment-avatar"><img
                                src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <form method="POST" action="{{route('saveComment')}}">
                                @csrf
                                <input type="hidden" name="status" value="-1">
                                <input type="hidden" name="code" value="{{Request::segment(3)}}">
                                <input type="hidden" name="customer_id" value="{{ !empty(Auth::guard('customers')->check()) ? Auth::guard('customers')->user()->id : '' }}">
                                <input type="hidden" name="parent_id" value="${parent_id}" >
                                <textarea class="form-control" placeholder="" name="content" id="content">@${username}:</textarea>
                                <button type="submit" class="btn btn-success green"> Lưu</button>
                            </form>
                            <div class="comment-reply-list-wrapper"></div>
                        </div>
                    </li>`);
                };

            }
        <?php
            }else{
        ?>
            alert('Đăng nhập để sử dụng chức năng này !!!');
        <?php
            }
        ?>

});


   setTimeout(function(){

        $.ajax({

           type:'POST',

           url:'{{route("videoListen")}}',

           data:{
			   code:`{{!empty($_GET['c']) && !empty($_GET['st']) ? $_GET['c'] : !empty($chart[0]->code) ? $chart[0]->code : ''}}`
			},

           success:function(data){

            console.log('+view')

           }

        });



	}, 210000);


</script>
@endsection
