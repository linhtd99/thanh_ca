@extends('client.layout.master')
@section('page_title')
Bài hát
@endsection
@section('content')
@php

@endphp
<div class="tag-song">
    <div class="container">
        <div class="tile_box_key">
            <h3>PHÂN LOẠI TUYỂN TẬP THEO TAGS</h3>
        </div>
        @php
            $c = 0;

        @endphp
        @forelse ($pa as $tagPa)
        @php
            $c++;



        @endphp
        @if ($tagPa->getTagChildren->count() > 0)
        <div class="box_menu_tag">
            <h3>{{ $tagPa->title }}</h3>
            <ul class="detail_menu_browsing_dashboard">
                @php
                $count = 0;
                $str = '';

                @endphp
                @forelse ($tagPa->getTagChildren as $tagCh)
                @php
                $count++;
                @endphp
                @if ($count < 10)
                @php
                $specialparam = $params;

                    $specialparam['p'. $c] = $tagCh->slug;

                @endphp
                <li><a class="{{ in_array($tagCh->slug, $params) ? 'active' : '' }}" href="{{ route('tags', $specialparam) }}" rel="dofollow"
                        title="{{ $tagCh->title }}">{{ $tagCh->title }}</a></li>
                    @else
                    @php
                    $str .= "<li><a class='' href='#' rel='dofollow' title='$tagCh->title'>$tagCh->title</a>
                    </li>";
                    @endphp
                    @endif

                    @empty @endforelse
                    @if ($tagPa->getTagChildren->count() > 9)

                    @endif
                    @if ($count > 9)
                    <li id="menu_tag_id_0" rel="0" class="view_more_list " style="z-index: 1001;">
                        <div class="dot_more"><span>Xem Thêm</span><span class="arrow_down"></span></div>
                        <ul id="menu_tag_box_1" style="margin-top: 2px; width: 276px; right: auto; left: 0px;">
                            @php
                            echo $str;
                            @endphp
                        </ul>
                    </li>
                    @endif
            </ul>
        </div>
        @endif

        @empty

        @endforelse

        <div class="clr"></div>
        <div class="tile_box_key tag">
            <h3>TAGS:</h3>

            @forelse ($params as $key => $param)
            @if ($param != null)
            <div class="tag_item">
                <span>{{ $param }}</span>
                <a href="#" class="close_tag"></a>
            </div>
            @endif
            @empty @endforelse


        </div>
        <div class="list_album home-list-item">
            <ul>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/16/3/4/d/0/1563273457681_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Mùa Mưa Ngâu Nằm Cạnh</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/14/8/9/1/f/1563070583490_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Tiếng Mưa Lòng Tôi</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/16/3/4/d/0/1563273457681_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Mùa Mưa Ngâu Nằm Cạnh</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
                <li>
                    <div class="box-left-album">
                        <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                            <div class="bg_action_info">
                                <span class="view_listen"><span class="icon_listen"></span><span
                                        id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                <span class="icon_play"></span>

                            </div>
                            <span class="avatar"><img
                                    src="https://avatar-nct.nixcdn.com/playlist/2019/07/16/3/4/d/0/1563273457681_300.jpg"
                                    alt="nguoi toi yeu da trot thuong ai roi - v.a"
                                    title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                        </a>
                    </div>
                    <div class="info_album">
                        <h3 class="h3seo"><a href="#" class="name_song"
                                title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Mùa Mưa Ngâu Nằm Cạnh</a></h3>
                        <h4><span class="name_user_tag">17/7/2019</span></h4>
                    </div>
                </li>
            </ul>
        </div>
        <div class="box_pageview">
            <a href="javascript:;" class="number active">1</a>
            <a href="#" class="number " rel="next" title="">2</a>
            <a href="#" class="number " rel="next" title="">3</a>
            <a href="#" class="number " rel="next" title="">4</a>
            <a href="#" class="number " rel="next" title="">5</a>
            <a href="#" class="number " rel="next" title="">6</a>
            <a href="#" class="number " rel="next" title="">7</a>
            <a href="#" class="number " rel="Trang cuối" title="">Trang cuối</a>
        </div>
    </div>
</div>

@endsection
