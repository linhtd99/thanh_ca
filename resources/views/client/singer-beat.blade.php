@extends('client.layout.layout_artist')
@section('page_title')
Nghệ sĩ {{!empty($artist->name) ? $artist->name : ''}}
@endsection
@section('content')

<div class="box-content">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 col-sm-12">
                <div class="list_music_full">
                    <div class="tile_box_key">
                        <h2><a title="Beat" href="javascript:;" class="nomore">Beat {{!empty($artist->name) ? $artist->name : ''}}</a></h2>                     
                    </div>
                    @if (count($songs) > 0)
                        <ul class="list_item_music">
                            @foreach ($songs as $item)
                            <li>
                                <div class="item_content">
                                    <h3 style="display: inline;"><a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_song">{{$item->name}}</a></h3> - <span
                                        style="color: #9a9a9a;">
                                        <?php $arr = explode(',',$item->artist_name);$arr_slug = explode(',',$item->artist_slug) ?>
                                        @foreach ($arr as $key => $item1)
                                        <a href="{{$arr_slug[$key]}}" class="name_singer"
                                            title="Tìm các bài hát, playlist, mv do ca sĩ {{$item1}} trình bày" target="_blank">{{$item1}}</a>
                                            @if ($key+1 < count($arr))
                                                ,
                                            @endif
                                        @endforeach
                                        {{-- <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ JustaTee trình bày"
                                                                    target="_blank">JustaTee</a> --}}
                                    </span>
                                </div>
                                <div class="list_mark">
                                    <span class="icon-tag-official" title="Bản Chính Thức">Official</span>
                        
                                    <span class="icon-tag-hd" title="High Quality (Chất Lượng Cao)">HQ</span>
                                </div>
                        
                                <span id="NCTCounter_sg_4603090" class="icon_listen" wgct="1">{{!empty(countListenSongHelper($item->code)->listen) ? countListenSongHelper($item->code)->listen : '0' }}</span>
                                <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="button_new_window"
                                    title="Nghe bài hát {{$item->name}} - {{$item->artist_name}} ở cửa sổ mới"></a>
                                <div class="fright_button_add_playlist"><a href="#" id="btnShowBoxPlaylist_ipcniQ31onj1"
                                        class="button_add_playlist"
                                        title="Thêm bài hát {{$item->name}} - {{$item->artist_name}} vào playlist yêu thích"></a> </div>
                                <a href="{{route('song',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="button_playing"
                                    title="Nghe bài hát {{$item->name}} - {{$item->artist_name}} này"></a>
                            </li>
                            @endforeach
                        </ul>
                        {{$songs->links()}}
                    @else
                        <div class="alert alert-primary" role="alert" style="color:white;font-weight:bold;background-color:#3498db">
                            Chưa có bài hát !!!
                        </div>
                    @endif
                   
                </div>
				
			</div>
			<div class="col-md-3 col-xs-12 col-sm-12">
				<div class="sidebar-singer">
                    <div class="list_singer_hot">
	                    <div class="tile_box_key">
                        <h3><a title="Ca Sĩ | Nghệ Sĩ" href="{{route('indexArtist')}}">Ca Sĩ | Nghệ Sĩ</a></h3>                  
	                    </div><!--@end div tile_box_key-->
	                    <ul>
	                        @foreach ($artistsRandom as $item)
                            <li><a href="{{route('artistDetail',['slug' => $item->slug])}}" class="img"
                                    title="{{!empty($item->name) ? $item->name : ''}}"><img
                                        src="{{!empty($item->image) ? $item->image : asset('images/765-default-avatar.png')}}"
                                        title="{{!empty($item->name) ? $item->name : ''}}" alt="{{!empty($item->name) ? $item->name : ''}}"></a><a
                                    href="{{route('artistDetail',['slug' => $item->slug])}}" class="name_singer_main"
                                    title="{{!empty($item->name) ? $item->name : ''}}">{{!empty($item->name) ? $item->name : ''}}</a></li>
                            @endforeach
	                    </ul>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection