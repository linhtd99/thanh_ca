@extends('client.layout.layout_user')
@section('page_title')
Cập nhật playlist
@endsection
@section('content')

       <div class="box_right_user">
			<div class="tab-content">

				<div id="2a" class="active tab-pane fade in">
					<div class="form-playlist2" style="display:block">
						<div class="tile_box_key">
							<h3><a class="nomore" title="Quản lý tài khoản" href="">CẬP NHẬT PLAYLIST</a></h3>
                        </div>

						<div class="box-edit-user-profile">
						@if (!empty($playlist))

							<form>
							<input type="hidden" name="id" value="{{ $playlist }}->id}}">
								<div class="form-edit">
									<div class="input-group">
										<label class="label" for="txtAddr">Tên Playlist</label>
										<div class="edit-user-content">
										<input style="width:512px;" class="input" type="text" value="{{!empty($playlist->name) ? $playlist->name : ''}}" id="playlist-name" name="name">
										</div>
									</div>

									<div class="input-group">
										<label class="label" for="txtAddr">Thể loại</label>
										<div class="edit-user-content">
											<select style="width:512px;" id="category_id" name="category_id">
												<option value="" title="Chọn thể loại">Chọn thể loại</option>
												@foreach ($categories as $item)

												<option rel="playlist" {{ $playlist }}->category_id == $item->id ? 'selected' : ''}} value="{{$item->id}}"
													style="{{$item->step != 0 ? 'font-size:13px;font-weight:nomal' : 'font-weight:bold'}}">
													@for ($i = 0; $i < $item->step; $i++)
														&nbsp
														@endfor
														{{$item->name}}
												</option>
												@endforeach


											</select>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="avatar">Hình ảnh playlist</label>
										<div style="width: 170px; font-size: 11px; position: absolute; margin-top: 30px; text-align: right;"></div>
										<div class="edit-user-content">
											<div class="image-wrapper">
											<img id="img_preview" src="{{!empty($playlist->image) ? $playlist->image : ''}}" width="80"
													height="80">
											</div>
											<input type="text" style="width:273px; margin-top: 23px; margin-left: 10px;" class="upload-new-avatar"
												id="txtNewPhotoName" value="" readonly="true">
											<div class="upload-btn-wrapper upload-new-avatar">
												<button class="btn">Chọn file</button>
												<input type="file" name="playlist_img">
											</div>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="txtAddr">Mô tả</label>
										<div class="edit-user-content">
											<textarea style="width:500px" class="txtinput" name="description" id="txtDesc">{{!empty($playlist->description) ? $playlist->description : ''}}</textarea>
										</div>
									</div>
									<div class="input-group wn-tab-ds">
										<label class="label" for="txtAddr">Danh sách bài hát</label>
										<div class="box_list_item_edit">
											<div id="listSongSearch" class="list_song_in_album" style="width:290px; float: left;overflow:scroll">
												<div style="width:100%; height:37px; background:#f1f1f1">
													{{-- <form> --}}
													<input type="hidden" name="_token" value="{{ Session::token() }}">
													<input style="width:86.5%; margin:1.5%;" class="input" type="text" id="searchInputQuickList"
														maxlength="300" placeholder="Nhập từ khóa hoặc link bài hát">
													<button class="btn_search" id="searchInputQuickListBtn" type="submit" value=""
														title="Tìm kiếm"></button>
													{{-- </form> --}}
												</div>
												<div>

													<ul id="idScrllSuggestionEditPlaylist" class="list_song_in_album"
														style="width: 280px; border: none; height: 450px;">
														<input type="hidden" id="arr_listSong" value="">
														<div id="idScrllSongInAlbumEditPlaylist" style="text-transform:capitalize;">

														</div>
													</ul>
												</div>
												<div class="clr"></div>
											</div>
											<span class="box-select-hero"></span>
											<ul id="listSong" class="list_song_in_album" style="width:360px">
												<div class="slimScrollDiv" style="position: relative; overflow: hidden; height: 450px;">
													<div id="idScrllSongInAlbum" style="height: 450px; overflow: hidden; width: 360px;">

													</div>
													<div style="margin:6px 0px 10px 18px;">
														{{-- <a href="" class="btn-random" title="Sắp xếp ngẫu nhiên" style="color:#0689ba;">Sắp xếp ngẫu nhiên</a> --}}
													</div>
												</div>

											</ul>
										</div>
									</div>

									<div id="error-box1" class="error-box1"></div>
									<div id="error-box2" class="error-box2"></div>
									<div class="input-group">
										<label class="label" for="txtFullname"></label>
										<div class="edit-user-content">
											<input type="submit" style="margin-right:10px;" class="btn edit_playlist btn-btn" value="Lưu">
											{{-- <a href="{{url('user/deletePlaylist/'.$playlist->id)}}" style="margin-right:10px;" id="btnDeletePlaylist" class="btn btn-btn">Xóa</a> --}}
											<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}" class="btn btn-btn" title="Trở lại">Trở lại</a>
										</div>
									</div>
								</div>
							</form>
						@else
							<form>
								<div class="form-edit">
									<div class="input-group">
										<label class="label" for="txtAddr">Tên Playlist</label>
										<div class="edit-user-content">
											<input style="width:512px;" class="input" type="text" id="playlist-name" name="name">
										</div>
									</div>

									<div class="input-group">
										<label class="label" for="txtAddr">Thể loại</label>
										<div class="edit-user-content">
											<select style="width:512px;" id="category_id" name="category_id">
												<option value="" title="Chọn thể loại" selected="selected">Chọn thể loại</option>
												@foreach ($categories as $item)

												<option rel="playlist" value="{{$item->id}}"
													style="{{$item->step != 0 ? 'font-size:13px;font-weight:nomal' : 'font-weight:bold'}}">
													@for ($i = 0; $i < $item->step; $i++)
														&nbsp
														@endfor
														{{$item->name}}
												</option>
												@endforeach


											</select>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="avatar">Hình ảnh playlist</label>
										<div style="width: 170px; font-size: 11px; position: absolute; margin-top: 30px; text-align: right;">Ảnh mặc
											định</div>
										<div class="edit-user-content">
											<div class="image-wrapper">
												<img id="img_preview" src="https://stc-id.nixcdn.com/v11/images/img-plist-full.jpg" width="80"
													height="80">
											</div>
											<input type="text" style="width:273px; margin-top: 23px; margin-left: 10px;" class="upload-new-avatar"
												id="txtNewPhotoName" value="" readonly="true">
											<div class="upload-btn-wrapper upload-new-avatar">
												<button class="btn">Chọn file</button>
												<input type="file" name="playlist_img">
											</div>
										</div>
									</div>
									<div class="input-group">
										<label class="label" for="txtAddr">Mô tả</label>
										<div class="edit-user-content">
											<textarea style="width:500px" class="txtinput" name="description" id="txtDesc"></textarea>
										</div>
									</div>
									<div class="input-group wn-tab-ds">
										<label class="label" for="txtAddr">Danh sách bài hát</label>
										<div class="box_list_item_edit">
											<div id="listSongSearch" class="list_song_in_album" style="width:290px; float: left;overflow:scroll">
												<div style="width:100%; height:37px; background:#f1f1f1">
													{{-- <form> --}}
													<input type="hidden" name="_token" value="{{ Session::token() }}">
													<input style="width:86.5%; margin:1.5%;" class="input" type="text" id="searchInputQuickList"
														maxlength="300" placeholder="Nhập từ khóa">
													<a class="btn_search" value=""
														title="Tìm kiếm"></a>
													{{-- </form> --}}
												</div>
												<div>
													<ul id="idScrllSuggestionEditPlaylist" class="list_song_in_album"
														style="width: 280px; border: none; height: 450px;">
														<input type="hidden" id="arr_listSong">
														<div id="idScrllSongInAlbumEditPlaylist" style="text-transform:capitalize;">

														</div>
													</ul>
												</div>
												<div class="clr"></div>
											</div>
											<span class="box-select-hero"></span>
											<ul id="listSong" class="list_song_in_album" style="width:360px">
												<div class="slimScrollDiv" style="position: relative; overflow: hidden; height: 450px;">
													<div id="idScrllSongInAlbum" style="height: 450px; overflow: hidden; width: 360px;">

													</div>
													<div style="margin:6px 0px 10px 18px;">
														{{-- <a href="" class="btn-random" title="Sắp xếp ngẫu nhiên" style="color:#0689ba;">Sắp xếp ngẫu nhiên</a> --}}
													</div>
												</div>

											</ul>
										</div>
									</div>

									<div id="error-box1" class="error-box1"></div>
									<div id="error-box2" class="error-box2"></div>
									<div class="input-group">
										<label class="label" for="txtFullname"></label>
										<div class="edit-user-content">
											<input type="submit" style="margin-right:10px;" class="btn create_playlist btn-btn" value="Lưu">
											{{-- <a href="{{url('user/deletePlaylist/'.$playlist->id)}}" style="margin-right:10px;" id="btnDeletePlaylist" class="btn btn-btn">Xóa</a> --}}

											<a href="{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}" class="btn btn-btn" title="Trở lại">Trở lại</a>
										</div>
									</div>
								</div>
							</form>
						@endif

						</div>
					</div><!-- end-form-playlist2 -->
				</div><!-- end-tab -->
			</div>
		</div> <!-- end-box_right_user -->

@endsection
@section('js')

@if (!empty($playlist))

	<script>
		$(document).ready(function(){
			var arr_listSong = `{{json_encode($playlist->songs)}}`;
			arr_listSong = arr_listSong.replace(/^.{1}/g, '');
			arr_listSong = arr_listSong.replace(/.$/,",");
			$('#arr_listSong').html(arr_listSong);
		});
	</script>
@endif
<script>

$("input[name=playlist_img]").change(function() {
	var image_name = $(this).val();

	$('#txtNewPhotoName').val(image_name);
	});

$('.create_playlist').on('click', function(e){
		e.preventDefault();
		arr_listSong = new Array();
		$(this).parents('form').find('#idScrllSongInAlbum').find('li').each(function(i,v) {
			arr_listSong[i] = $(this).attr('key');
		});
		var Form = $(this).parents('form');
		let dataForm = new FormData(Form[0]);
		dataForm.set('_token', '{{csrf_token()}}');
		dataForm.set('arr_listSong', arr_listSong);
		dataForm.set('customer_id', `<?= Auth::guard('customers')->user()->id ?>`);
		// console.log(dataForm);
		$.ajax({
			url : "{{route('savePlaylist')}}",
			method: 'post',
			processData: false,
			contentType: false,
			data: dataForm,
		}).done(
			result => {
			var msg = result.data;
			// console.log(msg);
			var content = '';
			if (result.errors) {
				if (typeof(msg.name) != 'undefined') {
					Form.find('#error-box1').html(
					`<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							${msg.name}
						</div>
					</div`);
				}else{
					Form.find('#error-box1').html('')
				};

				if (typeof(msg.arr_listSong) != 'undefined') {
					Form.find('#error-box2').html(
					`<div class="col-md-12"><div class="alert alert-danger" role="alert">
					${msg.arr_listSong}
					</div></div>`);
				}else{
					Form.find('#error-box2').html('')
				}



			}else{
				Form.find('#error-box1').html('');
				Form.find('#error-box2').html('');
				Form.find('#error-box1').html(
				`<div class="col-md-12">
					<div class="alert alert-success" role="alert">
						Thêm playlist thành công!
					</div>
					</div`);

				setTimeout(function(){
					window.location.href = `{{url('user/'.Auth::guard('customers')->user()->username.'/playlist')}}`;
				}, 1500);
			}

		});
	});
</script>

<script>
$('.edit_playlist').on('click', function(e){
		e.preventDefault();
		arr_listSong = new Array();
		$(this).parents('form').find('#idScrllSongInAlbum').find('li').each(function(i,v) {
			arr_listSong[i] = $(this).attr('key');
		});
		var Form = $(this).parents('form');
		let dataForm = new FormData(Form[0]);
		dataForm.set('_token', '{{csrf_token()}}');
		dataForm.set('arr_listSong', arr_listSong);
		dataForm.set('customer_id', `<?= Auth::guard('customers')->user()->id ?>`);
		// console.log(dataForm);
		$.ajax({
			url : "{{route('saveEditPlaylist')}}",
			method: 'post',
			processData: false,
			contentType: false,
			data: dataForm,
		}).done(
			result => {
			var msg = result.data;
			// console.log(msg);
			var content = '';
			if (result.errors) {
				if (typeof(msg.name) != 'undefined') {
					Form.find('#error-box1').html(
					`<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							${msg.name}
						</div>
					</div`);
				}else{
					Form.find('#error-box1').html('')
				};

				if (typeof(msg.arr_listSong) != 'undefined') {
					Form.find('#error-box2').html(
					`<div class="col-md-12"><div class="alert alert-danger" role="alert">
					${msg.arr_listSong}
					</div></div>`);
				}else{
					Form.find('#error-box2').html('')
				}
			}else{
				Form.find('#error-box1').html('');
				Form.find('#error-box2').html('');
				Form.find('#error-box1').html(
				`<div class="col-md-12">
					<div class="alert alert-success" role="alert">
						Sửa playlist thành công!
					</div>
					</div`);

				setTimeout(function(){
					window.location.reload();
				}, 1500);
			}

		});
	});
</script>



@endsection
