@extends('client.layout.master')
@section('page_title')
	Tin tức
@endsection
@section('content')
<div style="background: white" class="page_blog">
	<div class="w_section-inner ">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					@if (count($posts) > 0)
						<div class="w_section-inner">

							@foreach ($posts as $item)
							<div class="item_block w_item_rows" style="height:190px">
								<div class="thumb_images" style="height:100%">
									<a href="{{route('postDetail',['slug' => $item->slug])}}">
										<img src="{{!empty($item->image) ? $item->image : asset('client/img/post-default.jpg')}}" alt="" style="height:100%">
									</a>
								</div>

								<div class="post-five-content">
									<div class="content-inner">
										<h2>
											<a href="{{route('postDetail',['slug' => $item->slug])}}">{{$item->name}}</a>
										</h2>
										<div class="dateCreated">
											@php
											$date = date_create($item->created_at);
											$date_format = date_format($date,"H:i d/m/Y");
											@endphp

											<a><i class="fa fa-calendar-o" aria-hidden="true"></i>{{$date_format}}</a>
										</div>
										<p class="mkd-post-excerpt"><?php echo strip_tags(substr($item->content,0,200)) ?>...</p>

									</div>
									<div class="info-section clearfix">
										<div class="bt-blog-share">
										<a href="https://www.facebook.com/sharer/sharer.php?u={{route('postDetail',['slug' => $item->slug])}}"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
											<div class="social-share-dropdown ">
												<ul>

													<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
												</ul>
											</div>
										</div>

									</div><!-- end-info-section clearfix -->
								</div>
							</div>
							@endforeach


						</div> <!-- end-w_section-inner -->
					<div>{{$posts->links()}}</div>
					@else
						<div class="alert alert-primary" role="alert" style="color: #004085;
							background-color: #cce5ff;
							border-color: #b8daff;">
							Chưa có bài đăng nào !!!
						</div>
					@endif


				</div>
				@include('client.layout.sidebar_list_video')
			</div>
		</div>
	</div><!-- end-w_section-inner -->




</div>

@endsection
