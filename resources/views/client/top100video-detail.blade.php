<?php require_once('header.php'); ?>
<div class="video home video-bxh">
	<div class="container">
        <div class="box_playing">
            <div class="playing_video">
                <div class="box-view-player">
                    <video width="100%" controls>
                      <source src="audio/cautraloi.mp4" type="video/mp4">
                    </video>
                </div>
                <div id="listAlbum" class="box-list-playing">
                    <div class="header-opa"></div>
                    <div class="title-show-name">
                        <h1>Hãy Trao Cho Anh</h1>
                        <span>1/20 video</span>
                    </div>
                    <ul class="box-detail-item-playlist">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 411px;">
                            <div id="idScrllVideoInAlbum" style="overflow: auto; width: auto; height: 411px;">

                                <li class="active">
                                    <a href="#" title="Hãy Trao Cho Anh - Sơn Tùng M-TP, Snoop Dogg, Madison Beer">
                                        <span>1</span>
                                        <img alt="hay trao cho anh - son tung m-tp, snoop dogg, madison beer" title="Hãy Trao Cho Anh" src="https://avatar-nct.nixcdn.com/mv/2019/07/02/9/a/1/7/1562039387631_268.jpg">
                                        <h3 class="name-title">Hãy Trao Cho Anh</h3>
                                        <p class="name-singer">Sơn Tùng M-TP, Snoop Dogg, Madison Beer</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Độ Ta Không Độ Nàng - Khánh Phương">
                                        <span>2</span>
                                        <img alt="do ta khong do nang - khanh phuong" title="Độ Ta Không Độ Nàng" src="https://avatar-nct.nixcdn.com/mv/2019/06/09/e/6/a/4/1560087814971_268.jpg">
                                        <h3 class="name-title">Độ Ta Không Độ Nàng</h3>
                                        <p class="name-singer">Khánh Phương</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Anh Đổ Rồi Đấy - Trọng Hiếu">
                                        <span>3</span>
                                        <img alt="anh do roi day - trong hieu" title="Anh Đổ Rồi Đấy" src="https://avatar-nct.nixcdn.com/mv/2019/06/30/1/9/4/4/1561897931364_268.jpg">
                                        <h3 class="name-title">Anh Đổ Rồi Đấy</h3>
                                        <p class="name-singer">Trọng Hiếu</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Từng Yêu - Phan Duy Anh">
                                        <span>4</span>
                                        <img alt="tung yeu - phan duy anh" title="Từng Yêu" src="https://avatar-nct.nixcdn.com/mv/2019/06/12/0/9/8/a/1560337098745_268.jpg">
                                        <h3 class="name-title">Từng Yêu</h3>
                                        <p class="name-singer">Phan Duy Anh</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Chạm Làn Môi Em (Lyric Video) - Andree, Hoàng Tôn, Tinle">
                                        <span>5</span>
                                        <img alt="cham lan moi em (lyric video) - andree, hoang ton, tinle" title="Chạm Làn Môi Em (Lyric Video)" src="https://avatar-nct.nixcdn.com/mv/2019/07/16/9/3/9/6/1563252846018_268.jpg">
                                        <h3 class="name-title">Chạm Làn Môi Em (Lyric Video)</h3>
                                        <p class="name-singer">Andree, Hoàng Tôn, Tinle</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Joker - Zero 9">
                                        <span>6</span>
                                        <img alt="joker - zero 9" title="Joker" src="https://avatar-nct.nixcdn.com/mv/2019/07/12/4/a/7/4/1562927914251_268.jpg">
                                        <h3 class="name-title">Joker</h3>
                                        <p class="name-singer">Zero 9</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Joker - Zero 9">
                                        <span>7</span>
                                        <img alt="joker - zero 9" title="Joker" src="https://avatar-nct.nixcdn.com/mv/2019/07/12/4/a/7/4/1562927914251_268.jpg">
                                        <h3 class="name-title">Joker</h3>
                                        <p class="name-singer">Zero 9</p>
                                    </a>
                                </li>

                            </div>
                        </div>
                    </ul>
                </div>

            </div>
            <div id="infoVideo" class="info_name_songmv">

                <div class="topbreadCrumb">
                    <span><a href="#">Nghe nhạc</a></span>
                    › <span><a href="#">Video Việt Nam</a>, <a href="#">Video Nhạc Trẻ</a></span>
                    › <span><a href="#">Sơn Tùng M-TP</a>, <a href="#">Snoop Dogg</a>, <a href="#">Madison Beer</a></span>
                </div>
                <div class="name_title">
                    <h1>Hãy Trao Cho Anh</h1>
                    <span style="font-size: 22px;"> - </span>
                    <h2 class="name-singer" style="color: #000;"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Sơn Tùng M-TP trình bày" target="_blank">Sơn Tùng M-TP</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Snoop Dogg trình bày" target="_blank">Snoop Dogg</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Madison Beer trình bày" target="_blank">Madison Beer</a></h2>
                    <div class="box-link-songmv"><a href="#" title="Nghe phiên bản Audio của Video này" class="btn-link-songmv btn-to-music"></a></div></div>
                <div class="show_listen"><span id="NCTCounter_sg_6010701" wgct="1">196.330</span></div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="box_menu_player new" style="border-top: solid 1px #ececec;">
                    <div class="user_upload">
                        {{-- <span ic="userImageNowplaying"><a href="#" class=""><img src="https://avatar-nct.nixcdn.com/avatar/2015/08/26/6/7/6/7/1440574626064.jpg" title="nct_official" class="userImageNowplaying"></a></span> --}}
                        <i>Upload bởi:</i><br>
                        <div style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;"><span><a href="#" rel="nofollow" target="_top" title="nct_official" class="">nct_official</a></span></div>
                    </div>

                </div>
                <div class="box-content-player new">
                    <div class="album_info">
                        <div class="detail_info_playing_now">
                           <span class="logo-official docquyen"></span>
                           Video <b>Hãy Trao Cho Anh</b> do ca sĩ <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Sơn Tùng M-TP trình bày" target="_blank">Sơn Tùng M-TP</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Snoop Dogg trình bày" target="_blank">Snoop Dogg</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Madison Beer trình bày" target="_blank">Madison Beer</a> thể hiện, thuộc thể loại <a title="Video Việt Nam" alt="viet nam" target="_blank" href="#">Video Việt Nam</a>, <a href="#">Video Nhạc Trẻ</a><span id="introduceMV"></span>. Các bạn có thể nghe, download MV/Video <i>Hãy Trao Cho Anh</i> miễn phí tại NhacCuaTui.com.
                        </div>
                    </div>
                </div>
                <div class="lyric" id="_divLyricHtml">
                    <div class="pd_name_lyric">
                        <h2 class="name_lyric"><b>Lời bài hát: Hãy Trao Cho Anh</b></h2>
                        <p class="name_post">Lời đăng bởi: <a href="#" title="quangcool">quangcool</a></p>
                    </div>
                    <p id="divLyric" class="pd_lyric trans" style="height:auto;max-height:255px;overflow:hidden;">
                        HÃY TRAO CHO ANH
						<br>Sơn Tùng M-TP
						<br>
						<br>Bóng ai đó nhẹ nhàng vụt qua nơi đây
						<br>Quyến rũ ngây ngất loạn nhịp làm tim mê say
						<br>Cuốn lấy áng mây theo cơn sóng xô dập dìu
						<br>Nụ cười ngọt ngào cho ta tan vào phút giây miên man quên hết con đường về eh
						<br>Chẳng thể tìm thấy lối về ehhhhh
						<br>Điệu nhạc hòa quyện trong ánh mắt đôi môi
						<br>Dẫn lối những bối rối rung động khẽ lên ngôi
						<br>
						<br>Chạm nhau mang vô vàn
						<br>Đắm đuối vấn vương dâng tràn
						<br>Lấp kín chốn nhân gian
						<br>Làn gió hoá sắc hương mơ màng
						<br>Một giây ngang qua đời
						<br>Cất tiếng nói không nên lời
						<br>Ấm áp đến trao tay ngàn sao trời lòng càng thêm chơi vơi
						<br>Dịu êm không gian bừng sáng đánh thức muôn hoa mừng
						<br>Quấn quít hát ngân nga từng chút níu bước chân em dừng
						<br>Bao ý thơ tương tư ngẩn ngơ
						<br>Lưu dấu nơi mê cung đẹp thẫn thờ
						<br>
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh thứ anh đang mong chờ
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy mau làm điều ta muốn vào khoảnh khắc này đê
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao anh trao cho anh đi những yêu thương nồng cháy
						<br>Trao anh ái ân nguyên vẹn đong đầy
						<br>
						<br>Looking at my Gucci is about that time
						<br>We can smoke a blunt and pop a bottle of wine
						<br>Now get yourself together and be ready by nine
						<br>Cuz we gon’ do some things that will shatter your spine
						<br>Come one, undone, Snoop Dogg, Son Tung
						<br>Long Beach is the city that I come from
						<br>So if you want some, get some
						<br>Better enough take some, take some
						<br>
						<br>
						<br>Chạm nhau mang vô vàn
						<br>Đắm đuối vấn vương dâng tràn
						<br>Lấp kín chốn nhân gian làn
						<br>Gió hóa sắc hương mơ màng
						<br>Một giây ngang qua đời
						<br>Cất tiếng nói không nên lời
						<br>Ấm áp đến trao tay ngàn sao trời lòng càng thêm chơi vơi
						<br>Dịu êm không gian bừng sáng đánh thức muôn hoa mừng
						<br>Quấn quít hát ngân nga từng chút níu bước chân em dừng
						<br>Bao ý thơ tương tư ngẩn ngơ
						<br>Lưu dấu nơi mê cung đẹp thẫn thờ
						<br>
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh thứ anh đang mong chờ
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy mau làm điều ta muốn vào khoảnh khắc này đê
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao anh trao cho anh đi những yêu thương nồng cháy
						<br>Trao anh ái ân nguyên vẹn đong đầy
						<br>
						<br>
						<br>Em cho ta ngắm thiên đàng vội vàng qua chốc lát
						<br>Như thanh âm chứa bao lời gọi mời trong khúc hát
						<br>Liêu xiêu ta xuyến xao rạo rực khát khao trông mong
						<br>Dịu dàng lại gần nhau hơn dang tay ôm em vào lòng
						<br>Trao đi trao hết đi đừng ngập ngừng che dấu nữa
						<br>Quên đi quên hết đi ngại ngùng lại gần thêm chút nữa
						<br>Chìm đắm giữa khung trời riêng hai ta như dần hòa quyện mắt nhắm mắt tay đan tay hồn lạc về miền trăng sao
						<br>
						<br>Em cho ta ngắm thiên đàng vội vàng qua chốc lát
						<br>Như thanh âm chứa bao lời gọi mời trong khúc hát
						<br>Liêu xiêu ta xuyến xao rạo rực khát khao trông mong
						<br>Dịu dàng lại gần nhau hơn dang tay ôm em vào lòng
						<br>Trao đi trao hết đi đừng ngập ngừng che dấu nữa
						<br>Quên đi quên hết đi ngại ngùng lại gần thêm chút nữa
						<br>Chìm đắm giữa khung trời riêng hai ta như dần hòa quyện mắt nhắm mắt tay đan tay hồn lạc về miền trăng sao
						<br>
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh
						<br>Hãy trao cho anh thứ anh đang mong chờ

                    </p>

                    <div class="more_add" id="divMoreAddLyric">
                        <a href="#" id="seeMoreLyric" title="Xem toàn bộ" class="btn_view_more">Xem toàn bộ<span class="down"></span></a>
                        <a href="#" id="hideMoreLyric" title="Thu gọn" class="btn_view_hide hide-hh">Thu gọn<span class="up"></span></a>

                    </div>
                </div>
                <div class="comment tab-box-music">
                    <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo ThanhCa.tv</a></li>
                    <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="cmt_cus" class="tab-pane fade in active">
                        <div class="comment-header">
                            <div class="comment-total">110 Bình luận</div>
                            <div class="sorting-block-wrapper clearfix">
                                <div class="sorting-by-wrapper">
                                    <a><div class="active-sorting">Bình luận mới nhất <i class="fa fa-angle-down" aria-hidden="true"></i></div></a>
                                    <div class="dropdown-menu-list sort-comment z-hide">
                                        <ul>
                                            <li><div class="dropdown-item"><a>Bình luận nổi bật</a></div></li>
                                            <li><div class="dropdown-item"><a>Bình luận mới nhất</a></div></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list-wrapper">
                            <ul class="list-comment">
                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img src="https://s120-ava-talk.zadn.vn/a/a/b/d/0/120/131c0d786e9987e5ce34a281a8ea8964.jpg" alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <p class="username">Đặng Nguyên Minh Thư<span class="reply-ago-time">một ngày trước</span></p>
                                        <p class="content">có ai đang tôi ở trong thành phố ko</p>
                                        <div class="func-comment">
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>0</a>
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>0</a>
                                            <a class="reply-func">Trả lời</a>
                                        </div>
                                        <div class="comment-reply-list-wrapper">
                                            <ul class="list-comment list-comment-reply">
                                                <li class="comment-item">
                                                    <p class="medium-circle-card comment-avatar"><img src="https://s120-ava-talk.zadn.vn/9/f/2/4/0/120/19dfb5e4ad19848d19dac58b5d377002.jpg" alt="Zing MP3"></p>
                                                    <div class="post-comment">
                                                        <p class="username">nhatvu<span class="reply-ago-time">7 ngày trước</span></p>
                                                        <p class="content">Trong khong phải chong 🙂</p>
                                                        <div class="func-comment">
                                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>0</a>
                                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>0</a>
                                                            <a class="reply-func">Trả lời</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img src="https://s120-ava-talk.zadn.vn/a/a/b/d/0/120/131c0d786e9987e5ce34a281a8ea8964.jpg" alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <p class="username">Đặng Nguyên Minh Thư<span class="reply-ago-time">một ngày trước</span></p>
                                        <p class="content">có ai đang tôi ở trong thành phố ko</p>
                                        <div class="func-comment">
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>0</a>
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>0</a>
                                            <a class="reply-func">Trả lời</a>
                                        </div>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>
                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img src="https://s120-ava-talk.zadn.vn/a/a/b/d/0/120/131c0d786e9987e5ce34a281a8ea8964.jpg" alt="Zing MP3"></p>
                                    <div class="post-comment">
                                        <p class="username">Đặng Nguyên Minh Thư<span class="reply-ago-time">một ngày trước</span></p>
                                        <p class="content">có ai đang tôi ở trong thành phố ko</p>
                                        <div class="func-comment">
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>0</a>
                                            <a class="z-like-rate "><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>0</a>
                                            <a class="reply-func">Trả lời</a>
                                        </div>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="more-cmt">
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                    <div id="cmt_fb" class="tab-pane fade">

                    </div>
                  </div>
                </div>
                <div class="home-list-item">
					<div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
                    </div>
                    <ul>
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="https://avatar-nct.nixcdn.com/song/2019/06/22/b/1/2/7/1561218264960.jpg" alt="nguoi toi yeu da trot thuong ai roi - v.a" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Cao Ốc 20</a></h3>
                                <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4>
                            </div>
                        </li>
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="https://avatar-nct.nixcdn.com/song/2019/06/22/b/1/2/7/1561218264960.jpg" alt="nguoi toi yeu da trot thuong ai roi - v.a" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Cao Ốc 20</a></h3>
                                <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4>
                            </div>
                        </li>
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="https://avatar-nct.nixcdn.com/song/2019/07/27/3/f/9/d/1564193325359.jpg" alt="nguoi toi yeu da trot thuong ai roi - v.a" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Ơ Sao Bé Không Lắc</a></h3>
                                <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">BigDaddy</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Emily</a></h4>
                            </div>
                        </li>
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="https://avatar-nct.nixcdn.com/song/2019/06/22/b/1/2/7/1561218264960.jpg" alt="nguoi toi yeu da trot thuong ai roi - v.a" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Cao Ốc 20</a></h3>
                                <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4>
                            </div>
                        </li>
                        <li>
                            <div class="box-left-album">
                                <a href="#" class="box_absolute" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335" wgct="1">4.167</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img src="https://avatar-nct.nixcdn.com/song/2019/07/27/3/f/9/d/1564193325359.jpg" alt="nguoi toi yeu da trot thuong ai roi - v.a" title="Người Tôi Yêu Đã Trót Thương Ai Rồi - V.A"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="#" class="name_song" title="Cao Ốc 20 - B Ray, Đạt G, Masew, K-ICM">Ơ Sao Bé Không Lắc</a></h3>
                                <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">BigDaddy</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Emily</a></h4>
                            </div>
                        </li>
                    </ul>
				</div>
				<div class="home-list-item">
						<div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
                        </div>
                        <div class="home-mv">
                        	<div class="row">
                        		<div class="col-md-3 col-xs-6 col-sm-3">
                        			<div class="videosmall">
                        				<div class="box_absolute">
                                            <span class="view_mv"><span class="icon_view"></span><span>15.571</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="#" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu" class="img">
                                            	<span class="icon_play"></span>
                                            	<img src="https://avatar-nct.nixcdn.com/mv/2019/07/02/9/a/1/7/1562039387631_268.jpg" alt="ba chung ta (three of us)  - han sara, do hieu" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu">
                                            </a>
                                            <span class="icon_time_video">04:19</span>
                                        </div>
                                        <h3><a href="#" title="Tháng 6 Trôi Đi Thật Nhanh (Karaoke) - Reddy (Hữu Duy)" class="name_song_index">Tháng 6 Trôi Đi Thật Nhanh (Karaoke)</a></h3>
                                        <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Reddy (Hữu Duy) trình bày">Reddy (Hữu Duy)</a></h4>
                        			</div>
                        		</div>
                        		<div class="col-md-3 col-xs-6 col-sm-3">
                        			<div class="videosmall">
                        				<div class="box_absolute">
                                            <span class="view_mv"><span class="icon_view"></span><span>15.571</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="#" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu" class="img">
                                            	<span class="icon_play"></span>
                                            	<img src="https://avatar-nct.nixcdn.com/mv/2019/07/25/6/d/3/a/1564019505439_640.jpg" alt="ba chung ta (three of us)  - han sara, do hieu" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu">
                                            </a>
                                            <span class="icon_time_video">04:19</span>
                                        </div>
                                        <h3><a href="#" title="Tháng 6 Trôi Đi Thật Nhanh (Karaoke) - Reddy (Hữu Duy)" class="name_song_index">Tháng 6 Trôi Đi Thật Nhanh (Karaoke)</a></h3>
                                        <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Reddy (Hữu Duy) trình bày">Reddy (Hữu Duy)</a></h4>
                        			</div>
                        		</div>
                        		<div class="col-md-3 col-xs-6 col-sm-3">
                        			<div class="videosmall">
                        				<div class="box_absolute">
                                            <span class="view_mv"><span class="icon_view"></span><span>15.571</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="#" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu" class="img">
                                            	<span class="icon_play"></span>
                                            	<img src="https://avatar-nct.nixcdn.com/mv/2019/07/25/6/d/3/a/1564019505439_640.jpg" alt="ba chung ta (three of us)  - han sara, do hieu" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu">
                                            </a>
                                            <span class="icon_time_video">04:19</span>
                                        </div>
                                        <h3><a href="#" title="Tháng 6 Trôi Đi Thật Nhanh (Karaoke) - Reddy (Hữu Duy)" class="name_song_index">Tháng 6 Trôi Đi Thật Nhanh (Karaoke)</a></h3>
                                        <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Reddy (Hữu Duy) trình bày">Reddy (Hữu Duy)</a></h4>
                        			</div>
                        		</div>
                        		<div class="col-md-3 col-xs-6 col-sm-3">
                        			<div class="videosmall">
                        				<div class="box_absolute">
                                            <span class="view_mv"><span class="icon_view"></span><span>15.571</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="#" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu" class="img">
                                            	<span class="icon_play"></span>
                                            	<img src="https://avatar-nct.nixcdn.com/mv/2019/07/25/6/d/3/a/1564019505439_640.jpg" alt="ba chung ta (three of us)  - han sara, do hieu" title="Ba Chúng Ta (Three Of Us)  - Han Sara, Đỗ Hiếu">
                                            </a>
                                            <span class="icon_time_video">04:19</span>
                                        </div>
                                        <h3><a href="#" title="Tháng 6 Trôi Đi Thật Nhanh (Karaoke) - Reddy (Hữu Duy)" class="name_song_index">Tháng 6 Trôi Đi Thật Nhanh (Karaoke)</a></h3>
                                        <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Reddy (Hữu Duy) trình bày">Reddy (Hữu Duy)</a></h4>
                        			</div>
                        		</div>
                        	</div>
                        </div>
					</div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="sidebar">
					<div class="box_video_recommended">
	                    <div class="tile_box_key" id="autoBox">
	                        <h3><a class="nomore fontsmall">Xem tiếp</a></h3>
	                        <div class="auto-box">
	                            Autoplay
	                            <div>
	                                <input type="checkbox" id="autoplayInput" checked="checked">
	                                <label id="autoplayLabel" for="autoplayInput"></label>
	                            </div>
	                            <span class="icon-info-auto">i</span>
	                            <div class="show-info-auto hideShowCase">Khi bật tính năng Autoplay, video được đề xuất sẽ tự động phát tiếp.</div>
	                        </div>
	                    </div>
	                    <div class="list_item_music">
	                        <ul id="recommendZone" style="overflow: hidden;height: 400px;">
	                            <li id="li_recommend_item">
	                                <div class="box_absolute_video">

	                                    <a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="thum-video">
	                                        <span class="icon_time_video">04:46</span>
	                                        <span class="icon_play"></span>
	                                        <img src="https://avatar-nct.nixcdn.com/mv/2019/07/16/9/3/9/6/1563243956356_268.jpg" alt="chac toi phai quen nguoi thoi - ha nhi" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi">
	                                    </a>
	                                </div>
	                                <div class="info_data">

	                                    <h3><a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="name_song listAutonext">Chắc Tôi Phải Quên Người Thôi</a></h3>
	                                    <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Hà Nhi trình bày" target="_blank">Hà Nhi</a></h4>
	                                    <span class="icon_view" id="NCTCounter_sg_6026072" wgct="1">19.901</span>
	                                </div>
	                            </li>

	                            <li id="li_recommend_item">
	                                <div class="box_absolute_video">

	                                    <a href="#" title="Chờ - YeYe Nhật Hạ" class="thum-video">
	                                        <span class="icon_time_video">05:36</span>
	                                        <span class="icon_play"></span>
	                                        <img src="https://avatar-nct.nixcdn.com/mv/2019/07/11/2/5/7/0/1562829637859_268.jpg" alt="cho - yeye nhat ha" title="Chờ - YeYe Nhật Hạ">
	                                    </a>
	                                </div>
	                                <div class="info_data">

	                                    <h3><a href="#" title="Chờ - YeYe Nhật Hạ" class="name_song listAutonext">Chờ</a></h3>
	                                    <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ YeYe Nhật Hạ trình bày" target="_blank">YeYe Nhật Hạ</a></h4>
	                                    <span class="icon_view" id="NCTCounter_sg_6023720" wgct="1">21.098</span>
	                                </div>
	                            </li>
	                            <li id="li_recommend_item">
	                                <div class="box_absolute_video">

	                                    <a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="thum-video">
	                                        <span class="icon_time_video">04:46</span>
	                                        <span class="icon_play"></span>
	                                        <img src="https://avatar-nct.nixcdn.com/mv/2019/07/16/9/3/9/6/1563243956356_268.jpg" alt="chac toi phai quen nguoi thoi - ha nhi" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi">
	                                    </a>
	                                </div>
	                                <div class="info_data">

	                                    <h3><a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="name_song listAutonext">Chắc Tôi Phải Quên Người Thôi</a></h3>
	                                    <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Hà Nhi trình bày" target="_blank">Hà Nhi</a></h4>
	                                    <span class="icon_view" id="NCTCounter_sg_6026072" wgct="1">19.901</span>
	                                </div>
	                            </li>
	                            <li id="li_recommend_item">
	                                <div class="box_absolute_video">

	                                    <a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="thum-video">
	                                        <span class="icon_time_video">04:46</span>
	                                        <span class="icon_play"></span>
	                                        <img src="https://avatar-nct.nixcdn.com/mv/2019/07/16/9/3/9/6/1563243956356_268.jpg" alt="chac toi phai quen nguoi thoi - ha nhi" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi">
	                                    </a>
	                                </div>
	                                <div class="info_data">

	                                    <h3><a href="#" title="Chắc Tôi Phải Quên Người Thôi - Hà Nhi" class="name_song listAutonext">Chắc Tôi Phải Quên Người Thôi</a></h3>
	                                    <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Hà Nhi trình bày" target="_blank">Hà Nhi</a></h4>
	                                    <span class="icon_view" id="NCTCounter_sg_6026072" wgct="1">19.901</span>
	                                </div>
	                            </li>
	                            <li id="li_recommend_item">
	                                <div class="box_absolute_video">

	                                    <a href="#" title="Chờ - YeYe Nhật Hạ" class="thum-video">
	                                        <span class="icon_time_video">05:36</span>
	                                        <span class="icon_play"></span>
	                                        <img src="https://avatar-nct.nixcdn.com/mv/2019/07/11/2/5/7/0/1562829637859_268.jpg" alt="cho - yeye nhat ha" title="Chờ - YeYe Nhật Hạ">
	                                    </a>
	                                </div>
	                                <div class="info_data">

	                                    <h3><a href="#" title="Chờ - YeYe Nhật Hạ" class="name_song listAutonext">Chờ</a></h3>
	                                    <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap"><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ YeYe Nhật Hạ trình bày" target="_blank">YeYe Nhật Hạ</a></h4>
	                                    <span class="icon_view" id="NCTCounter_sg_6023720" wgct="1">21.098</span>
	                                </div>
	                            </li>
	                        </ul>
	                        <p class="btn_more_item">
	                            <a id="showMoreRecommend" href="javascript:;" class="btn_viewall">xem thêm</a>
	                            <a id="hideMoreRecommend" href="javascript:;" class="btn_viewall hide">rút gọn</a>
	                		</p>
	                    </div>
                    </div>
                    {{ Sidebar::top100New() }}


				</div>
			</div>
		</div>
	</div>
</div>



<?php require_once('footer.php'); ?>
