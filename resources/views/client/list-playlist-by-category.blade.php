@extends('client.layout.master')
@section('page_title')
Playlist
@endsection
@section('content')
<div class="home list-video-hh">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="box_cata_control">
                    <ul class="detail_menu_browsing_dashboard" style="margin-bottom: 30px;">
                        <li class="cate hot havelink">
                            <h3><a href="{{route('indexPlaylist')}}" rel="nofollow" title="MỚI &amp; HOT"
                                    class="{{Request::segment(1) == 'playlist'? 'active' : ''}}" id="video-moi">MỚI
                                    &amp;
                                    HOT</a></h3>
                        </li>
                        @if (!empty($categories))
                        @foreach ($categories as $item)
                        <li class="line"></li>
                        <li class="cate havelink">
                            <h3>
                                <a style="cursor: pointer;"
                                    class="{{Request::segment(1) == 'playlist-'.get_cat_by_id($item['category'])->slug.'.html' || Request::segment(1) == 'playlist-'.get_cat_by_id($item['category'])->slug ? 'active' : ''}}"
                                    href="{{route('playlistByCategory',['slug' => get_cat_by_id($item['category'])->slug])}}"
                                    rel="dofollow" title="{{get_cat_by_id($item['category'])->name}}">
                                    {{get_cat_by_id($item['category'])->name}}
                                </a>
                            </h3>
                        </li>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($item['categories'] as $category)

                        @if ($i <= 7) <li><a pr="list_vn"
                                class="{{Request::segment(1) == 'playlist-'.get_cat_by_id($category)->slug.'.html' || Request::segment(1) == 'playlist-'.get_cat_by_id($category)->slug  ? 'active' : ''}}"
                                href="{{route('playlistByCategory',['slug'=> get_cat_by_id($category)->slug])}}"
                                rel="dofollow"
                                title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a></li>
                            @elseif($i == 8)
                            <li class="view_more_list" style="z-index: 1002;">
                                <div class="dot_more"><span class="dot_cricle"></span><span
                                        class="dot_cricle"></span><span class="dot_cricle"></span> <span
                                        id="listchild_vn">Xem thêm</span></div>
                                <ul>
                                    @elseif($i > 8 && $i <= count($item['categories'])) <li><a pr="listchild_vn"
                                            href="{{route('playlistByCategory',['slug'=> get_cat_by_id($category)->slug])}}"
                                            rel="dofollow"
                                            title="{{get_cat_by_id($category)->name}}">{{get_cat_by_id($category)->name}}</a>
                            </li>
                            @if ($i == count($item['categories']))
                    </ul>
                    </li>
                    @endif

                    @endif

                    <?php $i++ ?>
                    @endforeach
                    @endforeach
                    @endif
                    </ul>
                </div>
                <div class="home-list-item">
                    <div class="tile_box_key">
                        <h2 class="title_of_box_video"><a href="{{route('indexPlaylist')}}">PLAYLIST {{$categoryCurrent->name}}
                                {{Request::segment(1) == 'playlist-'.$categoryCurrent->slug ? Request::segment(1) == 'playlist-'.$categoryCurrent->slug && Request::segment(2) == null ? 'HOT NHẤT' : ' MỚI NHẤT ': 'MỚI NHẤT'}}</a></h2>
                        <div class="btn_view_select">
                            <a href="{{route('playlistByCategory',['slug' => $categoryCurrent->slug])}}"
                                class="{{Request::segment(1) == 'playlist-'.$categoryCurrent->slug.'.html' && Request::segment(2) == null ? 'active' : ''}}"
                                title="Hot nhất">Hot nhất</a>
                            <a href="{{route('playlistByCategoryNew',['slug' => $categoryCurrent->slug])}}"
                                class="{{Request::segment(1) == 'playlist-'.$categoryCurrent->slug && Request::segment(2) == 'moi-nhat.html' ? 'active' : ''}}"
                                title="mới nhất">Mới nhất</a>
                        </div>
                    </div>

                    @if (!empty($playlists) && count($playlists) > 0)
                    <ul>
                        @foreach ($playlists as $playlist)
                        <li>
                            <div class="box-left-album">
                            <a href="{{route('playlist',['name' => name_to_slug($playlist->name),'code' => $playlist->code ])}}" class="box_absolute"
                                    title="{{!empty($playlist->name) ? $playlist->name : '' }}">
                                    <div class="bg_action_info">
                                        <span class="view_listen"><span class="icon_listen"></span><span
                                                id="NCTCounter_pl_63601335"
                                                wgct="1">{{!empty($playlist->listen) ? $playlist->listen : '0'}}</span></span>
                                        <span class="icon_play"></span>

                                    </div>
                                    <span class="avatar"><img
                                            src="{{!empty($playlist->image) ? $playlist->image : asset('images/default.png') }}"
                                            alt="{{!empty($playlist->name) ? $playlist->name : '' }}"
                                            title="{{!empty($playlist->name) ? $playlist->name : '' }}"></span>
                                </a>
                            </div>
                            <div class="info_album">
                                <h3 class="h3seo"><a href="{{route('playlist',['name' => name_to_slug($playlist->name),'code' => $playlist->code ])}}" class="name_song"
                                        title="{{!empty($playlist->name) ? $playlist->name : '' }}">{{!empty($playlist->name) ? $playlist->name : '' }}</a>
                                </h3>
                                {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B
                                                Ray</a>, <a href="#" class="name_singer"
                                                title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#"
                                                class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a
                                                href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a>
                                        </h4> --}}
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="row">
                        {{$playlists->links()}}
                    </div>
                    @else

                    <div class="alert alert-primary" role="alert"
                        style="background: #69bbe6;color: white;font-weight: bold;">
                        Chưa có playlist !!!
                    </div>

                    @endif

                </div>

            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
				<div class="sidebar">
					@include('client.layout.singer-trending')
                    {{-- ****************** --}}
                    @include('client.layout.song_chart')
                    {{-- ****************** --}}
                    @include('client.layout.video_chart')
                    {{-- ****************** --}}
                    @include('client.layout.w_top100')
                    {{-- ****************** --}}
					{{ Sidebar::top100New() }}
				</div>
			</div>

        </div>
    </div>
</div>
@endsection
