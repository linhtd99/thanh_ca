@extends('client.layout.master')
@section('page_title')
Video | {{$video->name}}
@endsection
<style>
.auto-box > div:first-of-type .active:after{
    content: "";
        display: block;
        width: 13px;
        height: 13px;
        border-radius: 8px;
        cursor: pointer;
        position: absolute;
        top: 2px;
        z-index: 1;
        left: 20px;
        background-color: #FFFFFD;
        -webkit-transition: left .2s ease-in-out;
        transition: left .2s ease-in-out;
        }
}

.jw-icon-hd {
        display: none !important;
    }
    div.ytp-pause-overlay.ytp-scroll-min {
         display: none !important;
    }
    a.ytp-watermark.yt-uix-sessionlink {
        display: none !important;
    }
    .ytp-chrome-top-buttons {
        display: none !important;
    }
</style>

@section('content')
{{-- {{dd($video)}} --}}
<div class="video home">

	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="box_playing">
					<div class="box-players" id="box-players">
                        <div id="players" class="players">

                        </div>
                    </div>
	                <div id="infoVideo" class="info_name_songmv">

                        <div class="topbreadCrumb">

                            <span><a href="">Nghe nhạc</a></span>
                            {{-- › <span><a href="#">Video Việt Nam</a>, <a href="#">Video Nhạc Trẻ</a></span> --}}
                            › <span>

                                @if (!empty($video->artists) && count($video->artists) > 0)
                                    @foreach ($video->artists as $key => $item)
                                         <a href="{{route('artistDetail',['slug' => $item->name])}}">{{$item->name}}</a>
                                            @if (($key+1) < count($video->artists))
                                                ,
                                            @elseif(($key+1) == count($video->artists))

                                            @endif

                                    @endforeach
                                @else
                                    <a>Chưa cập nhật</a>
                                @endif
                        </div>

                        <div class="name_title">
                        	<h1>{{$video->name}}</h1>
                        	<span style="font-size: 22px;"> - </span>
                        	<h2 class="name-singer" style="color: #000;"> <?php $i = 0?>
                                @if (!empty($video->artists) && count($video->artists) > 0)
                                    @foreach ($video->artists as $key => $item)
                                    <a href="{{route('artistDetail',['slug' => $item->slug])}}" class="name_singer" title="{{$item->name}}"
                                        target="_blank">{{$item->name}}</a>
                                        @if (($key+1) < count($video->artists))
                                            ,
                                        @elseif(($key+1) == count($video->artists))

                                        @endif
                                    @endforeach
                                @else
                                     <a>Chưa cập nhật</a>
                                @endif
                                </h2>
                        	<div class="box-link-songmv"><a href="#" title="Nghe phiên bản Audio của Video này" class="btn-link-songmv btn-to-music"></a></div></div>
						<div class="show_listen"><span id="NCTCounter_sg_6010701" wgct="1">{{!empty($video->listens->listen) ? $video->listens->listen : '0'}}</span></div>
                    </div>
				</div>
				<div class="box_menu_player new" style="border-top: solid 1px #ececec;">
                    <div class="user_upload">
                        {{-- <span ic="userImageNowplaying"><a href="#" class=""><img src="https://avatar-nct.nixcdn.com/avatar/2015/08/26/6/7/6/7/1440574626064.jpg" title="nct_official" class="userImageNowplaying"></a></span> --}}
                        <i>Upload bởi: </i><br>
                        <div style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;"><span><a  rel="nofollow" target="_top" title="nct_official" class="">{{!empty($video->author_id) ? $video->author_id : 'admin'}}</a></span></div>
                    </div>
                    <ul style="float: right">
                        <li style="display: inline-block;"><a id="btnReportError" title="Báo lỗi" class=""><span
                                    class="icon_report"></span></a></li>
                        <li style="display: inline-block;" class="box-share-like-new">
                            @php
                            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .
                            "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            @endphp
                            <div class="fb-like" data-href="{{ $actual_link }}" data-width="" data-layout="button_count" data-action="like"
                                data-size="small" data-show-faces="true" data-share="true"></div>
                        </li>
                    </ul>
                </div>
                <div class="report_luong" style="display: none">
                    <div class="show_noti"></div>
                    <div style="padding:0 20 20 20">
                        <h3>Bạn gặp vấn đề gì ?</h3>
                        <form method="POST">
                            <p>
                                <input type="radio" id="test1" value="Vi phạm bản quyền" name="radio_group" checked>
                                <label for="test1">Vi phạm bản quyền</label>
                            </p>
                            <p>
                                <input type="radio" id="test2" value="Không play được" name="radio_group">
                                <label for="test2">Không play được</label>
                            </p>
                            <p>
                                <input type="radio" id="test3" value="Không tải được" name="radio_group">
                                <label for="test3">Không tải được</label>
                            </p>
                            <p>
                                <input type="radio" id="test4" value="Chất lượng kém" name="radio_group">
                                <label for="test4">Chất lượng kém</label>
                            </p>
                            <p>
                                <input type="radio" id="test5" value="Thông tin chưa chính xác" name="radio_group">
                                <label for="test5">Thông tin chưa chính xác</label>
                            </p>
                            <p>
                                <input type="radio" id="test6" value="Lời bài hát chưa chính xác" name="radio_group">
                                <label for="test6">Lời bài hát chưa chính xác</label>
                            </p>
                            <p>
                                <input type="radio" id="test7" value="Karaoke chưa chính xác" name="radio_group">
                                <label for="test7">Karaoke chưa chính xác</label>
                            </p>
                            <p>
                                <input type="radio" id="test8" value="0" name="radio_group">
                                <label for="test8">Lỗi khác</label>
                            </p>
                            <div class="text_" style="display:none">
                                <h5>Nội dung</h5>
                                <textarea name="full" cols="30" rows="4" name="content" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-primary send_report" style="margin-top:10px">Gửi báo lỗi</button>
                        </form>
                    </div>
                </div>
                <div class="lyric" id="_divLyricHtml">
                    <div class="pd_name_lyric">
                        <h2 class="name_lyric"><b>Lời bài hát: {{$video->name}}</b></h2>
                        <p class="name_post">Lời đăng bởi: <a>{{!empty($video->author_id) ? $video->author_id : 'admin'}}</a></p>
                    </div>
                    <p id="divLyric" class="pd_lyric trans" style="height:auto;max-height:255px;overflow:hidden;">
                        @php echo !empty($video->description) ? $video->description : 'Chưa có lời bài hát' @endphp


                    </p>

                    <div class="more_add" id="divMoreAddLyric">
                        <a href="#" id="seeMoreLyric" title="Xem toàn bộ" class="btn_view_more">Xem toàn bộ<span class="down"></span></a>
                        <a href="#" id="hideMoreLyric" title="Thu gọn" class="btn_view_hide hide-hh">Thu gọn<span class="up"></span></a>

                    </div>
                </div>
                <div class="comment tab-box-music">
                    <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#cmt_cus">Bình luận theo ThanhCa.tv</a></li>
                    <li><a data-toggle="tab" href="#cmt_fb">Bình luận Facebook</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="cmt_cus" class="tab-pane fade in active">
                        <div class="comment-header">
                            <div class="comment-total">{{!empty($totalComment) ? $totalComment->total_comment : '0' }} Bình luận</div>
                            <div class="sorting-block-wrapper clearfix">
                                <div class="sorting-by-wrapper">
                                    <a>
                                        <div class="active-sorting">Bình luận {{empty($_GET['s']) ? 'mới nhất' : 'nổi bật'}} <i
                                                class="fa fa-angle-down" aria-hidden="true"></i></div>
                                    </a>
                                    <div class="dropdown-menu-list sort-comment z-hide">
                                        <ul>
                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code])}}?s=highlight">Bình
                                                        luận nổi bật</a></div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item"><a
                                                        href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code])}}">Bình
                                                        luận mới nhất</a></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list-wrapper">
                            <ul class="list-comment">
                                @if (count($comments) > 0)
                                    @foreach ($comments as $comment)
                                        <li class="comment-item parent_comment" idCommentCurrent="{{$comment->id}}">
                                            <p class="medium-circle-card comment-avatar"><img
                                                    src="{{!empty($comment->customer->avatar) ? $comment->customer->avatar : asset('client/img/default-avatar.png')}}" alt="Zing MP3"></p>
                                            <div class="post-comment">
                                            <p class="username"><span>{{!empty($comment->customer->name) ? $comment->customer->name : $comment->customer->username }}</span><span class="reply-ago-time">{{ $comment->date }}</span></p>
                                                <p class="content">{{$comment->content}}</p>
                                                <div class="func-comment">
                                                    <a style="cursor:pointer" class="z-like-rate
                                                    <?php
                                                        if (Auth::guard('customers')->check()) {
                                                            if (!empty($comment->arr_customer_id)) {
                                                                $arr_customer_id = explode(',',$comment->arr_customer_id);
                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                   if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                      echo "active";
                                                                   } else {
                                                                      echo "";
                                                                   }
                                                                }
                                                            }
                                                        } else {
                                                            echo "";
                                                        }
                                                    ?>
                                                    " onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$comment->id}})"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span class="total_like">{{!empty($comment->total_like) ? $comment->total_like : '0'}}</span></a>

                                                    <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                    @if (Auth::guard('customers')->check())
                                                        @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                            <a class="delete-comment" style="cursor:pointer;color:red" url="{{url('delete_comment/'.$comment->id)}}">Xóa</a>
                                                        @endif
                                                    @endif
                                                </div>
                                                @php
                                                    $comments_reply = $comment->getChildComment();
                                                @endphp
                                                <div class="comment-reply-list-wrapper">
                                                    <ul class="list-comment list-comment-reply">
                                                        @foreach ($comments_reply as $commentChild)
                                                        <li class="comment-item">
                                                            <p class="medium-circle-card comment-avatar"><img
                                                                    src="{{!empty($commentChild->customer->avatar) ? $commentChild->customer->avatar : asset('client/img/default-avatar.png')}}"
                                                                    alt="Zing MP3"></p>
                                                            <div class="post-comment">
                                                                <p class="username"><span>{{!empty($commentChild->customer->name) ? $commentChild->customer->name : $commentChild->customer->username }}</span><span class="reply-ago-time">{{ $commentChild->date }}</span></p>
                                                                <p class="content">{{$commentChild->content}}</p>
                                                                <div class="func-comment">
                                                                    <a style="cursor:pointer" class="z-like-rate
                                                                    <?php
                                                                        if (Auth::guard('customers')->check()) {
                                                                            if (!empty($commentChild->arr_customer_id)) {
                                                                                $arr_customer_id = explode(',',$commentChild->arr_customer_id);
                                                                                foreach ($arr_customer_id as $key => $customer_id) {
                                                                                if ($customer_id == Auth::guard('customers')->user()->id) {
                                                                                    echo "active";
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                }
                                                                            }

                                                                        } else {
                                                                            echo "";
                                                                        }

                                                                    ?>
                                                                    " onclick="likeComment(this,{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : '0'}},{{$commentChild->id}})"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span class="total_like">{{!empty($commentChild->total_like) ? $commentChild->total_like : '0'}}</span></a>

                                                                    <a class="reply-func" style="cursor:pointer">Trả lời</a>
                                                                    @if (Auth::guard('customers')->check())
                                                                        @if (Auth::guard('customers')->user()->id == $comment->customer_id)
                                                                            <a class="delete-comment" style="cursor:pointer;color:red" url="{{url('delete_comment/'.$commentChild->id)}}">Xóa</a>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif


                                @if (!Auth::guard('customers')->check())
                                    <div class="btn-login" style="text-align: center">
                                        <button style="text-align: center" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal-login">Đăng nhâp để bình luận</button>
                                    </div>
                                @else
                                <li class="comment-item">
                                    <p class="medium-circle-card comment-avatar"><img
                                    src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}" alt="Zing MP3"></p>
                                    <div class="post-comment">
                                            <form method="POST" action="{{route('saveComment')}}">
                                                @csrf
                                                <input type="hidden" name="status" value="-1">
                                                <input type="hidden" name="code" value="{{$video->code}}">
                                                <input type="hidden" name="customer_id" value="{{ Auth::guard('customers')->user()->id }}">
                                                <input type="hidden" name="parent_id" value="0">
                                                <textarea class="form-control" placeholder="" name="content" id="content"></textarea>
                                                <button type="submit" class="btn btn-success green"> Lưu</button>
                                            </form>
                                        <div class="comment-reply-list-wrapper"></div>
                                    </div>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="more-cmt" code="{{$video->code}}" comment-current="10" s="{{!empty($_GET['s']) ? $_GET['s'] : 'normal'}}">
                            <a onclick="more_comment(this)">Xem thêm</a>
                        </div>
                    </div>
                    <div id="cmt_fb" class="tab-pane fade">
                    <div class="fb-comments" data-href="{{route('detailVideo',['name' => name_to_slug($video->name),'code' => $video->code])}}" data-width="100%"
                            data-numposts="5"></div>
                    </div>
                  </div>
                </div>
                @if (!empty($playList4) && count($playList4) > 0)
                <div class="home-list-item">
					<div class="tile_box_key">
                        <h2><a title="Nghe gì hôm nay" href="#">PLAYLIST | ALBUM</a></h2>
                    </div>
                    <ul>
                            @foreach ($playList4 as $item)
                            <li>
                                <div class="box-left-album">
                                    <a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="box_absolute" title="{{$item->name}}">
                                        <div class="bg_action_info">
                                            <span class="view_listen"><span class="icon_listen"></span><span id="NCTCounter_pl_63601335"
                                        wgct="1">{{!empty(countListenHelper($item->code)->listen) ? countListenHelper($item->code)->listen : '0'}}</span></span>
                                            <span class="icon_play"></span>

                                        </div>
                                        <span class="avatar"><img src="{{$item->image}}" alt="{{$item->name}}" title="{{$item->name}}"></span>
                                    </a>
                                </div>
                                <div class="info_album">
                                    <h3 class="h3seo"><a href="{{route('playlist',['name' => name_to_slug($item->name),'code' => $item->code])}}" class="name_song" title="{{$item->name}}">{{$item->name}}</a></h3>
                                    {{-- <h4><a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ B Ray trình bày">B Ray</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Đạt G trình bày">Đạt G</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ Masew trình bày">Masew</a>, <a href="#" class="name_singer" title="Tìm các bài hát, playlist, mv do ca sĩ K-ICM trình bày">K-ICM</a></h4> --}}
                                </div>
                            </li>
                            @endforeach


                        </ul>
                    </div>
                @endif
                @if (count($mv) > 0 && !empty($mv))
                    <div class="home-list-item">
                        <div class="tile_box_key">
                            <h2><a title="Nghe gì hôm nay" href="#">VIDEO | MV</a></h2>
                        </div>
                        <div class="home-mv">
                            <div class="row">
                                <?php $i = 1 ?>
                                @foreach ($mv as $video1)

                                <div class="col-md-3 col-xs-6 col-sm-3">
                                    <div class="videosmall">
                                        <div class="box_absolute">
                                            <span class="view_mv"><span
                                                    class="icon_view"></span><span>{{!empty(countListenHelper($video1['code'])['listen']) ? countListenHelper($video1['code'])['listen'] : '0' }}</span></span>
                                            <span class="tab_lable_"></span>
                                            <a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code']])}}"
                                                title="{{$video1['name']}}" class="img">
                                                <span class="icon_play"></span>
                                                <img src="{{!empty($video1['image']) ? $video1['image'] : getVideoYoutubeApi($video1['link'])['thumbnail']['mqDefault']}}"
                                                    alt="{{$video1['name']}}" title="{{$video1['name']}}">
                                            </a>
                                            <span class="icon_time_video">{{getVideoYoutubeApi($video1['link'])['duration_sec']}}</span>
                                        </div>
                                        <h3><a href="{{route('detailVideo',['name' => name_to_slug($video1['name']),'code' => $video1['code'] ])}}"
                                                title="{{$video1['name']}}" class="name_song_index">{{$video1['name']}}</a></h3>
                                        <h4>
                                            {{-- {{dd($video1->artists)}} --}}
                                            @if (!empty($video1['artists']) && count($video1['artists']) > 0)
                                            @foreach ($video1['artists'] as $key => $item)
                                            <a href="{{route('artistDetail',['slug' => $item['slug']])}}" class="name_singer"
                                                title="{{$item['name']}}" target="_blank">{{$item['name']}}</a>
                                            @if (($key+1) < count($video1['artists'])) , @elseif(($key+1)==count($video1['artists']))@endif
                                                @endforeach @else <a>Chưa cập nhật</a>
                                                @endif
                                        </h4>
                                    </div>
                                </div>

                                <?php
                                        if ($i == 4) {
                                            break;
                                        };
                                        $i++;
                                ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    </div>
                @endif

			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="sidebar">
					<div class="box_video_recommended">
	                    <div class="tile_box_key" id="autoBox">
	                        <h3><a class="nomore fontsmall">Xem tiếp</a></h3>
	                        <div class="auto-box">
	                            Autoplay
	                            <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                                    <label class="onoffswitch-label active" for="myonoffswitch" style="background: rgb(93, 196, 222);"></label>
                                </div>
	                            <span class="icon-info-auto">i</span>
	                            <div class="show-info-auto hideShowCase">Khi bật tính năng Autoplay, video được đề xuất sẽ tự động phát tiếp.</div>
	                        </div>
	                    </div>
	                    <div class="list_item_music">
                            <ul id="recommendZone" style="overflow: hidden;height: 400px;">
                            @foreach ($videoRandom5 as  $item)
                                <li class="li_recommend_item">
                                    <div class="box_absolute_video">

                                        <a href="{{route('detailVideo',['name' => name_to_slug($item->name),'code' => $item->code])}}" title="{{$item->name}}" class="thum-video">
                                        <span class="icon_time_video">{{getVideoYoutubeApi($item->link)['duration_sec']}}</span>
                                            <span class="icon_play"></span>
                                        <img src="{{!empty($item->image) ? $item->image : getVideoYoutubeApi($item->link)['thumbnail']['mqDefault']}}"
                                                alt="{{$item->name}}" title="{{$item->name}}">
                                        </a>
                                    </div>
                                    <div class="info_data">

                                    <h3><a href="{{route('detailVideo',['name' => name_to_slug($item->name),'code' => $item->code])}}" title="{{$item->name}}" class="name_song listAutonext">{{$item->name}}</a></h3>
                                        <h4 style="color: #a2a2a2; text-overflow: ellipsis;white-space: nowrap">
                                             @if (!empty($item->artists) && count($item->artists) > 0)
                                                @foreach ($item->artists as $key => $artist)
                                                    <a href="{{route('artistDetail',['slug' => $artist->slug])}}" class="name_singer" title="{{$artist->name}}" target="_blank">{{$artist->name}}</a>
                                                    @if (($key+1) < count($item->artists))
                                                        ,
                                                    @elseif(($key+1) == count($item->artists))

                                                    @endif
                                                @endforeach
                                            @else
                                                <a class="name_singer">Chưa cập nhật</a>
                                            @endif
                                        </h4>
                                        <span class="icon_view" id="NCTCounter_sg_6026072" wgct="1">{{!empty($item->countListen()['listen']) ? $item->countListen()['listen'] : 0}}</span>
                                    </div>
                                </li>
                            @endforeach
	                        </ul>
	                        {{-- <p class="btn_more_item">
	                            <a id="showMoreRecommend" href="javascript:;" class="btn_viewall">xem thêm</a>
	                            <a id="hideMoreRecommend" href="javascript:;" class="btn_viewall hide">rút gọn</a>
	                		</p>  --}}
	                    </div>
                    </div>
                    {{-- --******************-- --}}
                    @include('client.layout.w_top100')
                    {{-- --******************-- --}}

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('js')
{{-- <script src="http://www.youtube.com/player_api"></script> --}}

<script src="{{ asset('js/jwplayer.js') }}"></script>
<script>jwplayer.key="YgtWotBOi+JsQi+stgRlQ3SK21W2vbKi/K2V86kVbwU=";</script>
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
@if (!empty($video->video) || !empty($video->link))
    <script>
    var $width = $(".box-players").width();
    var $height = $width/1.5;
    var player = jwplayer("players").setup({
        "file": "{{ !empty($video->video) ? $video->video : $video->link }}",
        "width" : $width,
        "height" : $height,
        "autostart": true,
    });
    player.on('complete', function(){
        if($('#myonoffswitch:checked').length == 1) {
            console.log($('#myonoffswitch:checked').length);
            window.location.href = $('.li_recommend_item:eq(0)').find('.thum-video').attr('href');
        }
    })

$(window).resize(function() {
    var $width = $(".box-players").width();
    var $height = $width/1.5;
    jwplayer().resize($width,$height);
});
</script>
@endif


<script>
    $('body').find('.jw-icon-hd').remove();
</script>

<script>
    $('.send_report').on('click', function(e){
        // console.log(this);
         e.preventDefault();
        var Form = $(this).parents('form');
        let dataForm = new FormData(Form[0]);
        dataForm.set('_token', '{{csrf_token()}}');
        dataForm.set('user_id',`{{Auth::guard('customers')->check() ? Auth::guard('customers')->user()->id : 0 }}`);
        dataForm.set('provider','video');
        dataForm.set('code','{{$video->code }}')
        var checkedValue = $('input[name=radio_group]:checked').val();
        if (checkedValue == '0') {
            if ($('textarea[name=full]').val().trim().length == 0) {
                $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                    Nội dung không được bỏ trống !
                </div>`);
            }else{
                $.ajax({
                    url : "{{route('saveReport')}}",
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: dataForm,
                }).done(
                    result => {
                    if (result.status) {
                        $('.show_noti').html(`<div class="alert alert-success" role="alert">
                            Cám ơn ý kiến đóng góp của bạn !
                        </div>`);

                    }else{
                        $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                            Có lỗi xảy ra !
                        </div>`);
                    }
                    setTimeout(() => {
                            $('.show_noti').html('');
                            $('#btnReportError').trigger('click');
                    }, 2500);

                });
            }
        }else{
            $.ajax({
                    url : "{{route('saveReport')}}",
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: dataForm,
                }).done(
                    result => {

                     if (result.status) {
                        $('.show_noti').html(`<div class="alert alert-success" role="alert">
                            Cám ơn ý kiến đóng góp của bạn !
                        </div>`);
                    }else{
                        $('.show_noti').html(`<div class="alert alert-danger" role="alert">
                            Có lỗi xảy ra !
                        </div>`);
                    }
                    setTimeout(() => {
                            $('.show_noti').html('');
                            $('#btnReportError').trigger('click');
                    }, 2500);
            });
        }

	});
    $('#btnReportError').on('click',function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.report_luong').css('display','none');
        }else{
            $(this).addClass('active');
            $('.report_luong').css('display','block');
        }
    });

    $('input[name=radio_group]').on('click',function(){
        if($('#test8').is(':checked')) {
            $('.text_').css('display','block');
        }else{
            $('.text_').css('display','none');
        }
    })

</script>
    <script>
        if($('#myonoffswitch:checked').length == 1){
            localStorage.setItem("nextVideo", true);
        }
       setTimeout(function(){

        $.ajax({

           type:'POST',

           url:'{{route("videoListen")}}',

           data:{
			   code:'{{$video["code"]}}'
			},

           success:function(data){

            console.log('+view')

           }

        });



	}, 150000);

    if (localStorage.getItem("nextVideo") == 'false') {
            $('#myonoffswitch').prop('checked', false);
            $('.onoffswitch').find('label').css('background','#c0c0c0');
            $('.onoffswitch').find('label').removeClass('active');
            // console.log($('#myonoffswitch:checked').length);
    }else{
            $('#myonoffswitch').prop('checked', true);
    }
    $(document).on('click','.delete-comment',function(e){
        e.preventDefault();
        r = confirm("Xóa comment!!!");
        if (r == true) {
            window.location.href = $(this).attr('url');
        } else {
            txt = "You pressed Cancel!";
        }
    })

    $(document).on('click', '.reply-func', function () {
        <?php
            if (Auth::guard('customers')->check()){
        ?>
            if($(this).hasClass('have_write_comment')){
                $(this).removeClass('have_write_comment');
                $(this).parents('.comment-item').find('.comment-remove').remove();
            }else{
                $(this).addClass('have_write_comment');
                if ($(this).parents('.comment-item').has('li.comment-remove')) {
                    $(this).parents('.comment-item').find('.comment-remove').remove();
                    parent_id = $(this).parents('.parent_comment').attr('idcommentcurrent');
                    username = $(this).parent('.func-comment').parent('.post-comment').find('.username > span:eq(0)').text();
                    // console.log(username);
                    $(this).parents('.comment-item').find('.comment-reply-list-wrapper').append(
                    `<li class="comment-item comment-remove">
                        <p class="medium-circle-card comment-avatar"><img
                                src="{{!empty(Auth::guard('customers')->user()->image) ? Auth::guard('customers')->user()->image : asset('client/img/default-avatar.png')}}"
                                alt="Zing MP3"></p>
                        <div class="post-comment">
                            <form method="POST" action="{{route('saveComment')}}">
                                @csrf
                                <input type="hidden" name="status" value="-1">
                                <input type="hidden" name="code" value="{{$video->code}}">
                                <input type="hidden" name="customer_id" value="{{ !empty(Auth::guard('customers')->check()) ? Auth::guard('customers')->user()->id : '' }}">
                                <input type="hidden" name="parent_id" value="${parent_id}" >
                                <textarea class="form-control" placeholder="" name="content" id="content">@${username}:</textarea>
                                <button type="submit" class="btn btn-success green"> Lưu</button>
                            </form>
                            <div class="comment-reply-list-wrapper"></div>
                        </div>
                    </li>`);
                };

            }
        <?php
            }else{
        ?>
            alert('Đăng nhập để sử dụng chức năng này !!!');
        <?php
            }
        ?>

    });
    </script>
    <script>

        var player;
        function onYouTubePlayerAPIReady() {
            player = new YT.Player('playerVideo', {
              width: '100%',
              height: '100%',
              videoId: `{{getIdVideo($video['link'])}}`,
              events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
              }
            });
        }

        // autoplay video
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        // when video ends
        function onPlayerStateChange(event) {
            if(event.data === 0 && $('#myonoffswitch:checked').length == 1) {
                console.log($('#myonoffswitch:checked').length);
                window.location.href = $('.li_recommend_item:eq(0)').find('.thum-video').attr('href');
            }
        }

    $('#myonoffswitch').click(function(){
        console.log($('#myonoffswitch:checked').length);
       if($('#myonoffswitch:checked').length == 1){
            $('.onoffswitch').find('label').css('background','#5DC4DE');
            $('.onoffswitch').find('label').addClass('active');
            localStorage.setItem("nextVideo", true);
       }else{
            $('.onoffswitch').find('label').css('background','#c0c0c0');
            $('.onoffswitch').find('label').removeClass('active');
            localStorage.setItem("nextVideo", false);
       }
    });

	$.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

   <?php
        if(Auth::guard('customers')->check()){
    ?>
        $.ajax({

            type:'POST',

            url:'{{route("addToHistory")}}',

            data:{
                code:'{{$video->code}}',
                customer_id : `{{Auth::guard('customers')->user()->id}}`,
                provider : 'video',
             },

            success:function(data){
                console.log('+ add history');
            }
        });
    <?php
        }
    ?>



	</script>
@endsection
