@extends('client.layout.master')
<style>
@import url('https://fonts.googleapis.com/css?family=Abril+Fatface|Lato');
body {
	 background: #d3deea;
}
 .page404 .top {
	 margin-top: 30px;
}
 .page404 .container {
	 margin: 0 auto;
	 position: relative;
	 width: 250px;
	 height: 250px;
	 margin-top: -40px;
}
 .page404 .ghost {
	 width: 50%;
	 height: 53%;
	 left: 25%;
	 top: 10%;
	 position: absolute;
	 border-radius: 50% 50% 0 0;
	 background: #ededed;
	 border: 1px solid #bfc0c0;
	 border-bottom: none;
	 animation: float 2s ease-out infinite;
}
 .page404 .ghost-copy {
	 width: 50%;
	 height: 53%;
	 left: 25%;
	 top: 10%;
	 position: absolute;
	 border-radius: 50% 50% 0 0;
	 background: #ededed;
	 border: 1px solid #bfc0c0;
	 border-bottom: none;
	 animation: float 2s ease-out infinite;
	 z-index: 0;
}
.page404 .face {
	 position: absolute;
	 width: 100%;
	 height: 60%;
	 top: 20%;
}
.page404 .eye, .eye-right {
	 position: absolute;
	 background: #585959;
	 width: 13px;
	 height: 13px;
	 border-radius: 50%;
	 top: 40%;
}
.page404 .eye {
	 left: 25%;
}
.page404 .eye-right {
	 right: 25%;
}
.page404  .mouth {
	 position: absolute;
	 top: 50%;
	 left: 45%;
	 width: 10px;
	 height: 10px;
	 border: 3px solid;
	 border-radius: 50%;
	 border-color: transparent #585959 #585959 transparent;
	 transform: rotate(45deg);
}
.page404  .one, .two, .three, .four {
	 position: absolute;
	 background: #ededed;
	 top: 85%;
	 width: 25%;
	 height: 23%;
	 border: 1px solid #bfc0c0;
	 z-index: 0;
}
.page404 .one {
	 border-radius: 0 0 100% 30%;
	 left: -1px;
}
.page404 .two {
	 left: 23%;
	 border-radius: 0 0 50% 50%;
}
.page404 .three {
	 left: 50%;
	 border-radius: 0 0 50% 50%;
}
.page404 .four {
	 left: 74.5%;
	 border-radius: 0 0 30% 100%;
}
.page404 .shadow {
	 position: absolute;
	 width: 30%;
	 height: 7%;
	 background: #bfc0c0;
	 left: 35%;
	 top: 80%;
	 border-radius: 50%;
	 animation: scale 2s infinite;
}
 @keyframes scale {
	 0% {
		 transform: scale(1);
	}
	 50% {
		 transform: scale(1.1);
	}
	 100% {
		 transform: scale(1);
	}
}
 @keyframes float {
	 50% {
		 transform: translateY(15px);
	}
}
.page404 .bottom {
	 margin-top: 10px;
}
/*text styling*/
.page404 h1 {
	 font-family: 'Abril Fatface', serif;
	 color: #ededed;
	 text-align: center;
	 font-size: 9em;
	 margin: 0;
	 text-shadow: -1px 0 #bfc0c0, 0 1px #bfc0c0, 1px 0 #bfc0c0, 0 -1px #bfc0c0;
}
.page404 h3 {
	 font-family: 'Lato', sans-serif;
	 font-size: 2em;
	 text-transform: uppercase;
	 text-align: center;
	 color: #bfc0c0;
	 margin-top: -20px;
	 font-weight: 900;
}
 .page404 p {
	 text-align: center;
	 font-family: 'Lato', sans-serif;
	 color: #585959;
	 font-size: 0.6em;
	 margin-top: -20px;
	 text-transform: uppercase;
}
 .page404 .search {
	 text-align: center;
}
 .page404 .buttons {
	 display: flex;
	 align-items: center;
	 justify-content: center;
	 margin-top: 10px;
}
/*search style*/
.page404 .search-bar {
	 border: 1px solid #bfc0c0;
	 padding: 5px;
	 height: 32px;
	 margin-left: -30px;
	 width: 200px;
	 outline: none;
}
 .page404 .search-bar:focus {
	 border: 1px solid #d3deea;
}
 .page404 .search-btn {
	 position: absolute;
	 width: 30px;
	 height: 32px;
	 border: 1px solid #bfc0c0;
	 background: #bfc0c0;
	 text-align: center;
	 color: #ededed;
	 cursor: pointer;
	 font-size: 1em;
	 outline: none;
}
 .page404 .search-btn:hover {
	 background: #ededed;
	 border: 1px solid #ededed;
	 color: #bfc0c0;
	 transition: all 0.2s ease;
}
.page404 .btn {
	 background: #ededed;
	 padding: 15px 20px;
	 margin: 5px;
	 color: #585959;
	 font-family: 'Lato', sans-serif;
	 text-transform: uppercase;
	 font-size: 0.6em;
	 letter-spacing: 1px;
	 border: 0;
}
.page404 .btn:hover {
	 background: #bfc0c0;
	 transition: all 0.4s ease-out;
}



</style>
@section('content')
<div class="page404">
    <div id="background"></div>
    <div class="top">
        <h1>404</h1>
        <h3>page not found</h3>
    </div>
    <div class="container">
        <div class="ghost-copy">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
            <div class="four"></div>
        </div>
        <div class="ghost">
            <div class="face">
                <div class="eye"></div>
                <div class="eye-right"></div>
                <div class="mouth"></div>
            </div>
        </div>
        <div class="shadow"></div>
    </div>
    <div class="bottom">
        {{-- <p>Boo, looks like a ghost stole this page!</p> --}}
        <form class="search" action="{{ route('search') }}">
            <input type="text" class="search-bar" name="q" placeholder="Tìm kiếm từ khóa ... ">
            <button type="submit" class="search-btn">
                <i class="fa fa-search"></i>
            </button>
        </form>
        <div class="buttons">
            {{-- <button class="btn">Back</button> --}}
		<a href="{{ url('/') }}" class="btn">Trang chủ</a>
        </div>
    </div>
</div >

@endsection
